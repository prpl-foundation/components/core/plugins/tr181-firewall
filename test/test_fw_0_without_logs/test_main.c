/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "mock.h"
#include "test_dm_chain_no_logs.h"
#include "test_dm_dmz_no_logs.h"
#include "test_fw_ifacesetting_no_logs.h"
#include "test_dm_level_no_logs.h"
#include "test_dm_pinhole_no_logs.h"
#include "test_dm_service_no_logs.h"
#include "test_dm_portmapping_no_logs.h"
#include "test_dm_porttrigger_no_logs.h"
#include "test_nat_staticnat_no_logs.h"

int ut_count[UT_MAX] = {};
const char* ut_local_objects[] = {
    "Firewall.",
    "NAT.",
    NULL
};

#define RESET_COUNT memset(ut_count, 0, sizeof(ut_count))

static amxc_var_t count_var;

static void save_counts(const char* key) {
    amxc_var_t* var = amxc_var_add_key(amxc_htable_t, &count_var, key, NULL);

    amxc_var_add_key(int32_t, var, "amxb_call", ut_count[UT_AMXB_CALL]);
    amxc_var_add_key(int32_t, var, "amxb_call_local", ut_count[UT_AMXB_CALL_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_call_remote", ut_count[UT_AMXB_CALL_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_async_call", ut_count[UT_AMXB_ASYNC_CALL]);
    amxc_var_add_key(int32_t, var, "amxb_async_call_local", ut_count[UT_AMXB_ASYNC_CALL_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_async_call_remote", ut_count[UT_AMXB_ASYNC_CALL_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_get", ut_count[UT_AMXB_GET]);
    amxc_var_add_key(int32_t, var, "amxb_get_local", ut_count[UT_AMXB_GET_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_get_remote", ut_count[UT_AMXB_GET_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_get_multiple", ut_count[UT_AMXB_GET_MULTIPLE]);

    amxc_var_add_key(int32_t, var, "amxb_set", ut_count[UT_AMXB_SET]);
    amxc_var_add_key(int32_t, var, "amxb_set_local", ut_count[UT_AMXB_SET_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_set_remote", ut_count[UT_AMXB_SET_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_set_multiple", ut_count[UT_AMXB_SET_MULTIPLE]);

    amxc_var_add_key(int32_t, var, "amxb_add", ut_count[UT_AMXB_ADD]);
    amxc_var_add_key(int32_t, var, "amxb_add_local", ut_count[UT_AMXB_ADD_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_add_remote", ut_count[UT_AMXB_ADD_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_del", ut_count[UT_AMXB_DEL]);
    amxc_var_add_key(int32_t, var, "amxb_del_local", ut_count[UT_AMXB_DEL_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_del_remote", ut_count[UT_AMXB_DEL_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_get_supported", ut_count[UT_AMXB_GET_SUPPORTED]);
    amxc_var_add_key(int32_t, var, "amxb_get_supported_local", ut_count[UT_AMXB_GET_SUPPORTED_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_get_supported_remote", ut_count[UT_AMXB_GET_SUPPORTED_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_describe", ut_count[UT_AMXB_DESCRIBE]);
    amxc_var_add_key(int32_t, var, "amxb_describe_local", ut_count[UT_AMXB_DESCRIBE_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_describe_remote", ut_count[UT_AMXB_DESCRIBE_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_list", ut_count[UT_AMXB_LIST]);
    amxc_var_add_key(int32_t, var, "amxb_list_local", ut_count[UT_AMXB_LIST_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_list_remote", ut_count[UT_AMXB_LIST_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_resolve", ut_count[UT_AMXB_RESOLVE]);
    amxc_var_add_key(int32_t, var, "amxb_resolve_local", ut_count[UT_AMXB_RESOLVE_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_resolve_remote", ut_count[UT_AMXB_RESOLVE_REMOTE]);

    amxc_var_add_key(int32_t, var, "amxb_get_instances", ut_count[UT_AMXB_GET_INSTANCES]);
    amxc_var_add_key(int32_t, var, "amxb_get_instances_local", ut_count[UT_AMXB_GET_INSTANCES_LOCAL]);
    amxc_var_add_key(int32_t, var, "amxb_get_instances_remote", ut_count[UT_AMXB_GET_INSTANCES_REMOTE]);

    amxc_var_add_key(int32_t, var, "fwinterface_fw_add_chain", ut_count[UT_FWINTERFACE_FW_ADD_CHAIN]);
    amxc_var_add_key(int32_t, var, "fwinterface_fw_add_rule", ut_count[UT_FWINTERFACE_FW_ADD_RULE]);
    amxc_var_add_key(int32_t, var, "fwinterface_fw_replace_rule", ut_count[UT_FWINTERFACE_FW_REPLACE_RULE]);
    amxc_var_add_key(int32_t, var, "fwinterface_fw_delete_rule", ut_count[UT_FWINTERFACE_FW_DELETE_RULE]);
    amxc_var_add_key(int32_t, var, "fwinterface_fw_apply", ut_count[UT_FWINTERFACE_FW_APPLY]);
}

static void print_counts(int fd) {
    amxc_var_dump(&count_var, fd);
    amxc_var_dump(&count_var, STDOUT_FILENO);
}

static void init_counts(void) {
    amxc_var_init(&count_var);
    amxc_var_set_type(&count_var, AMXC_VAR_ID_HTABLE);
}

static void clean_counts(void) {
    amxc_var_clean(&count_var);
}

// you may comment some TEST_XYZ defines to limit the scope of the tests while developing
#define TEST_CHAIN
#define TEST_DMZ
#define TEST_IFSETTING
#define TEST_LEVEL
#define TEST_PINHOLE
#define TEST_SERVICE
#define TEST_PORTMAP
#define TEST_PORTTRIG
#define TEST_NAT_IFSETTING
#define TEST_STATNAT

int main(void) {
    int ret = 0;
    int fd = open("../../output/result/ut_count/fcall_data.txt", O_CREAT | O_WRONLY, 0644);
    if(fd < 0) {
        printf("error: failed to open fcall_data.txt %d (%s:%d)\n", errno, __FILE__, __LINE__); fflush(stdout);
        ret = 1;
        goto exit;
    }

    init_counts();

#ifdef TEST_CHAIN
    const struct CMUnitTest tests_chain[] = {
        cmocka_unit_test_setup_teardown(test_chain_status, test_chain_setup, NULL),
        cmocka_unit_test_setup_teardown(test_source_mac, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_rule_ip_or_mask, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_chain_add_log_rule, NULL, test_chain_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("fw_chain", tests_chain, NULL, NULL);
    when_failed(ret, exit);
    save_counts("fw_chain");
#endif

#ifdef TEST_DMZ
    const struct CMUnitTest tests_dmz[] = {
        cmocka_unit_test_setup_teardown(test_dmz_initialization, test_dmz_setup, NULL),
        cmocka_unit_test_setup_teardown(test_add_dmz, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_dmz_missing_interface, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_dmz_with_interface, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_disable_dmz, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_dmz, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_dmz_lease_time, NULL, test_dmz_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("fw_dmz", tests_dmz, NULL, NULL);
    when_failed(ret, exit);
    save_counts("fw_dmz");
#endif

#ifdef TEST_IFSETTING
    const struct CMUnitTest tests_interfacesetting[] = {
        cmocka_unit_test_setup_teardown(test_dm_alias, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_dm_interface, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_dm_delete_instance, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_child_services, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_stealth_mode, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_accept_udp_traceroute, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_spoofing_mode, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_icmpv6_passthrough_mode, test_ifsetting_setup, test_ifsetting_teardown),
        cmocka_unit_test_setup_teardown(test_iface_isolation, setup_iface_isolation, teardown_iface_isolation),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("fw_ifacesetting", tests_interfacesetting, NULL, NULL);
    when_failed(ret, exit);
    save_counts("fw_ifacesetting");
#endif

#ifdef TEST_LEVEL
    const struct CMUnitTest tests_level[] = {
        cmocka_unit_test_setup_teardown(test_policy_with_chain_change_default_policy, test_policy_with_chain_setup, NULL),
        cmocka_unit_test_setup_teardown(test_policy_with_chain_change_chain_of_inactive_policy, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_policy_with_chain_change_chain_of_active_policy, NULL, test_policy_with_chain_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("fw_level", tests_level, NULL, NULL);
    when_failed(ret, exit);
    save_counts("fw_level");
#endif

#ifdef TEST_PINHOLE
    const struct CMUnitTest tests_pinhole[] = {
        cmocka_unit_test_setup_teardown(test_pinhole_initialization, test_pinhole_setup, NULL),
        cmocka_unit_test_setup_teardown(test_add_pinhole_success, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_twos_pinhole_failed, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_change_integer_protocol, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_change_wrong_protocol, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_add_1_to_source_prefixes, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_remove_1_from_source_prefixes, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_add_1_to_source_prefixes, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_disable, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_remove_1_from_source_prefixes_while_disabled, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_enable, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_source_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_source_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_destination_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_destination_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_ipversion4_change_dest_ip_to_ipv6, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_ipversion4_change_dest_ip_to_ipv4, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_change_ipversion_to_ipv6, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_ipversion6_change_dest_ip_to_ipv6, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_ipversion6_change_dest_ip_to_ipv4, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_change_ipversion_to_unused, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_ipversionx_change_dest_ip_to_ipv6, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_ipversionx_change_dest_ip_to_ipv4, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_change_ipversion_to_ipv4, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_lease_time, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_dest_range, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_src_range, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_pinhole_range, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_method_setPinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_method_list_one_pinhole, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_method_list_all_pinholes, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_method_deletePinhole, NULL, test_pinhole_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("fw_pinhole", tests_pinhole, NULL, NULL);
    when_failed(ret, exit);
    save_counts("fw_pinhole");
#endif

#ifdef TEST_SERVICE
    const struct CMUnitTest tests_service[] = {
        cmocka_unit_test_setup_teardown(test_add_service_v4_with_double_prefix, test_service_setup, NULL),
        cmocka_unit_test_setup_teardown(test_remove_service_v4_with_double_prefix, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_service_v4, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_service_v4_change_protocol, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_service_v4_change_ipversion, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_service_v4_change_action, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_service_v4, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_service_v6_with_double_prefix, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_service_v6_with_double_dport, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_service_v6_with_double_protocol, NULL, test_service_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("fw_service", tests_service, NULL, NULL);
    when_failed(ret, exit);
    save_counts("fw_service");
#endif

#ifdef TEST_PORTMAP
    const struct CMUnitTest tests_portmap[] = {
        cmocka_unit_test_setup_teardown(test_portmapping_initialization, test_nat_portmappings_setup, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_status, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_create_instances, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_1_transaction, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_with_hairpinnat, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_with_hairpinnat_with_lease, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_with_hairpinnat_with_lease_if_disabled, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_with_hairpinnat_with_range, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_with_hairpinnat_with_hostname, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_portmapping_with_hairpinnat_with_mac_address, NULL, test_nat_portmappings_teardown),
        cmocka_unit_test_setup_teardown(test_portmapping_csv_protocols, test_nat_portmappings_setup, test_nat_portmappings_teardown),
        cmocka_unit_test_setup_teardown(test_portmapping_allowed_origins, test_nat_portmappings_setup, test_nat_portmappings_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("nat_portmap", tests_portmap, NULL, NULL);
    when_failed(ret, exit);
    save_counts("nat_portmap");
#endif

#ifdef TEST_PORTTRIG
    const struct CMUnitTest tests_porttrigger[] = {
        cmocka_unit_test_setup_teardown(test_add_porttrigger, test_porttrigger_setup, NULL),
        cmocka_unit_test_setup_teardown(test_add_porttrigger_rule, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_enable_porttrigger, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_porttrigger_change_destination_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_activate_rule_with_wrong_protocol, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_activate_rule_with_wrong_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_active_rule, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_activate_rule_with_wrong_protocol, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_activate_rule_with_wrong_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_activate_rule_with_wrong_host, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_rule_change_destination_port, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_add_porttrigger_rule_2, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_remove_porttrigger_rule_2, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_wait_until_expired, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_active_rule_with_different_host, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_delete_porttrigger, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_change_MaxPortTriggerNumber, NULL, test_porttrigger_teardown),
        cmocka_unit_test_setup_teardown(test_porttrigger_csv_protocol, test_porttrigger_setup, test_porttrigger_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("nat_porttrigger", tests_porttrigger, NULL, NULL);
    when_failed(ret, exit);
    save_counts("nat_porttrigger");
#endif

#ifdef TEST_STATNAT
    const struct CMUnitTest tests_statnat[] = {
        cmocka_unit_test_setup_teardown(test_statnat_add_host, statnat_setup, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_host_toggle_enable, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_add_portmap, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_change_portmap, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_second_portmap, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_change_host, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_remove_portmap, NULL, NULL),
        cmocka_unit_test_setup_teardown(test_statnat_remove_host, NULL, statnat_teardown),
    };

    RESET_COUNT;
    ret |= cmocka_run_group_tests_name("nat_staticnat", tests_statnat, NULL, NULL);
    when_failed(ret, exit);
    save_counts("nat_staticnat");
#endif

    print_counts(fd);
exit:
    amxo_resolver_import_close_all();
    clean_counts();
    close(fd);
    return ret;
}
