/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "firewall.h"

#include "mock.h"
#include "test_dm_portmapping.h"

#define TEST_IP "192.168.1.100"
#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX_INBOUND "[FW][ACCPT_IN_CONN..."
#define LOG_PREFIX_ALL_ALLOWED "[FW][ACCPT_ALL]:"
#define SRC "source"
#define DEST "destination"

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};

static const char* json_string_portmap_filter(bool reverse, int protocol, bool log, const char* src, const char* dest, int dport) {
    size_t len = 0;

    snprintf(_buf, sizeof(_buf) - 1,
             "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_PortForwarding\","
             "\"enable\":true,"
             "\"protocol\":%d,"
             "\"ipv6\":false,",
             protocol);

    if(src != NULL) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len,
                 "\"%s\":\"%s\","
                 "\"%s_excluded\":false,",
                 !reverse ? SRC : DEST, src,
                 !reverse ? SRC : DEST);
    }

    if(dest != NULL) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len,
                 "\"%s\":\"%s\","
                 "\"%s_excluded\":false,"
                 "\"%s_port\":%d,"
                 "\"%s_port_range_max\":0,",
                 !reverse ? DEST : SRC, dest,
                 !reverse ? DEST : SRC,
                 !reverse ? DEST : SRC, dport,
                 !reverse ? DEST : SRC);
    }

    if(log) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"target\":13,"
                 "\"conntrack_state\":\"NEW\","
                 "\"target_option\":{"
                 "\"level\":%d,"
                 "\"logflags\":0,"
                 "\"prefix\":\"%s\"}",
                 LOG_LEVEL, LOG_PREFIX_ALL_ALLOWED);
    } else {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"target\":6,"
                 "\"target_option\":1");
    }

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len, "}");

    return _buf;
}

static const char* json_string_portmap_forward(int protocol, bool log, const char* src, const char* dest, int dport) {
    return json_string_portmap_filter(false, protocol, log, src, dest, dport);
}

static const char* json_string_portmap_reverse(int protocol, bool log, const char* src, const char* dest, int dport) {
    return json_string_portmap_filter(true, protocol, log, src, dest, dport);
}

static const char* json_string_portmap_nat(int protocol, const char* src, const char* dest, int dport, const char* to_addr, int to_port) {
    size_t len = 0;

    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"nat\","
             "\"chain\":\"PREROUTING_PortForwarding_%d\","
             "\"enable\":true,"
             "\"protocol\":%d,"
             "\"ipv6\":false,",
             src != NULL ? 1 : (dport == 0) ? 4 : 3,
             protocol);

    if(dport != 0) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len,
                 "\"destination_port\":\"%d\","
                 "\"destination_port_range_max\":\"0\",",
                 dport);
    }

    if(src != NULL) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len,
                 "\"source\":\"%s\","
                 "\"source_excluded\":false,",
                 src);
    }

    if(dest != NULL) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len,
                 "\"destination\":\"%s\","
                 "\"destination_excluded\":false,",
                 dest);
    }

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len,
             "\"target\":8,"
             "\"target_option\":{\"ip\":\"%s\",\"max_port\":0,\"min_port\":%d}}",
             to_addr,
             to_port);

    return _buf;
}

static const char* json_string_portmap_hairpinnat(int protocol, const char* src, const char* dest, int dport, const char* to_addr) {
    size_t len = 0;

    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"nat\","
             "\"chain\":\"POSTROUTING_PortForwarding\","
             "\"enable\":true,"
             "\"protocol\":%d,"
             "\"ipv6\":false,"
             "\"destination\":\"%s\","
             "\"destination_port\":\"%d\","
             "\"source\":\"%s\",",
             protocol,
             dest,
             dport,
             src);

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len,
             "\"target\":9,"
             "\"target_option\":{\"ip\":\"%s\",\"max_port\":65535,\"min_port\":1024}}",
             to_addr);

    return _buf;
}

static const char* json_string_ifsetting_masquerade(const char* iface_out) {
    size_t len = 0;

    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"nat\","
             "\"chain\":\"POSTROUTING_DEFAULT\","
             "\"enable\":true,"
             "\"out_interface\":\"%s\","
             "\"ipv6\":false,",
             iface_out);

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len,
             "\"target\":6,"
             "\"target_option\":3}");

    return _buf;
}

static void a_populate_filter_forward_log(fw_rule_t* rule1) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule1, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule1, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_INBOUND), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void a_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void a_populate_filter_reverse_log(fw_rule_t* rule2) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule2, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule2, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_ALL_ALLOWED), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void a_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void a_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_nat_rule_port_75(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 75), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_nat_rule_port_81(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 81), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_nat_rule_with_range(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule3, 100), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.1.100/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void b_populate_filter_forward_log(fw_rule_t* rule1) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule1, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule1, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_INBOUND), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void b_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void b_populate_filter_reverse_log(fw_rule_t* rule2) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule2, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule2, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_ALL_ALLOWED), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void b_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void b_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void b_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.240/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void c_populate_filter_forward_log(fw_rule_t* rule1) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule1, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule1, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_INBOUND), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void c_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void c_populate_filter_reverse_log(fw_rule_t* rule2) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule2, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule2, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_ALL_ALLOWED), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void c_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void c_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void c_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.250/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void d_populate_filter_forward_log(fw_rule_t* rule1) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule1, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule1, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_INBOUND), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void d_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void d_populate_filter_reverse_log(fw_rule_t* rule2) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule2, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule2, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_ALL_ALLOWED), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void d_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void d_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void d_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.241/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void e_populate_filter_forward_log(fw_rule_t* rule1) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule1, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule1, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_INBOUND), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void e_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void e_populate_filter_reverse_log(fw_rule_t* rule2) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule2, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule2, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_ALL_ALLOWED), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void e_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void e_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void e_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.250/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

int test_nat_portmappings_setup(void** state) {
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_all_allowed.odl");
    dm = amxut_bus_dm();
    return 0;
}

int test_nat_portmappings_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_portmapping_initialization(UNUSED void** state) {
    amxd_object_t* pm_template = NULL;
    uint32_t remaining_time = 0;

    pm_template = amxd_dm_findf(dm, "NAT.PortMapping.");
    assert_non_null(pm_template);

    remaining_time = amxd_object_get_uint32_t(pm_template, "RemainingLeaseTime", NULL);
    assert_int_equal(remaining_time, 0);
}

void test_portmapping_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false); // for legacy reasons
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    expect_query_wan_ipv4_addresses();

    print_message("object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled by setting Enable to true\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. set ScheduleRef to an Inactive Schedule -> Status should be Inactive\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Inactive");

    print_message("4. add Active Schedule to ScheduleRef -> Status should be Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1,Schedules.Schedule.2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("5. set ScheduleRef to a Disabled Schedule -> Status should be Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("6. add Active Schedule to ScheduleRef -> Status should be Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1,Schedules.Schedule.3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("7. make Status == Disabled by setting Enable to false, ScheduleRef is not taken into account anymore\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("8. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_create_instances(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_t trans2;

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    amxd_trans_init(&trans2);
    amxd_trans_select_pathf(&trans2, "NAT.");
    amxd_trans_set_value(uint32_t, &trans2, "MaxPortMappingNumberOfEntries", 10);
    assert_int_equal(amxd_trans_apply(&trans2, dm), 0);
    amxd_trans_clean(&trans2);
    handle_events();

    assert_int_equal(amxd_trans_init(&trans), 0);

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);                        // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    print_message("2. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
}

void test_portmapping_1_transaction(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);                        // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("2. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();
    expect_query_wan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("4. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_lease(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();
    expect_query_wan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(a_populate_hairpin_rule, 1);
    handle_events();

    amxut_timer_go_to_future_ms((3600 * 1000) - 1);
    handle_events();

    assert_EndTime_now_or_later("NAT.PortMapping.pm-test");

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);

    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);
    amxut_timer_go_to_future_ms(2); // 3599s + 2ms > LeaseDuration
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_null(object);

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_lease_if_disabled(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    assert_EndTime_now_or_later("NAT.PortMapping.pm-test");

    amxut_timer_go_to_future_ms(3600 * 1000);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_null(object);

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_range(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    amxd_object_t* object2 = NULL;
    amxd_object_t* object3 = NULL;

    expect_query_lan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test-2");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    expect_query_lan_ipv4_addresses();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test-3");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. object 'NAT.PortMapping.pm-test-2' must exist and must be disabled\n");
    object2 = amxd_dm_findf(dm, "NAT.PortMapping.pm-test-2");
    assert_non_null(object2);
    assert_int_equal(amxd_object_get_params(object2, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("3. object 'NAT.PortMapping.pm-test-3' must exist and must be disabled\n");
    object3 = amxd_dm_findf(dm, "NAT.PortMapping.pm-test-3");
    assert_non_null(object3);
    assert_int_equal(amxd_object_get_params(object3, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("4. make Status == Enabled for the first instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPortEndRange", 100);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule_with_range, 1);
    expect_fw_replace_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("5. make Status == Enabled for the second instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object2);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object2);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPortEndRange", 0);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 5);
    expect_fw_replace_rule(a_populate_filter_forward, 6);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 7);
    expect_fw_replace_rule(a_populate_filter_reverse, 8);
    expect_fw_replace_rule(a_populate_nat_rule, 2);
    expect_fw_replace_rule(a_populate_hairpin_rule, 2);

    handle_events();

    assert_int_equal(amxd_object_get_params(object2, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("6. make Status == Enabled for the third instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object3);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object3);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 75);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPortEndRange", 70);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 9);
    expect_fw_replace_rule(a_populate_filter_forward, 10);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 11);
    expect_fw_replace_rule(a_populate_filter_reverse, 12);
    expect_fw_replace_rule(a_populate_nat_rule_port_75, 3);
    expect_fw_replace_rule(a_populate_hairpin_rule, 3);

    handle_events();

    assert_int_equal(amxd_object_get_params(object3, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("7. make Status == Disabled\n");
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule_with_range, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("8. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    print_message("9. remove the second instance\n");
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test-2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object2 = NULL;

    print_message("10. remove the third instance\n");
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule_port_75, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test-3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object3 = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_hostname(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    amxd_object_t* object_gmap = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    object_gmap = amxd_dm_findf(dm, "Devices.Device.4.");
    assert_non_null(object_gmap);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "DESKTOP-FV3HVV1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(b_populate_filter_forward_log, 1);
    expect_fw_replace_rule(b_populate_filter_forward, 2);
    expect_fw_replace_rule(b_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(b_populate_filter_reverse, 4);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make GMAP IPAddress == 192.168.99.250\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.99.250");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(b_populate_filter_forward_log, 1);
    expect_fw_delete_rule(b_populate_filter_forward, 1);
    expect_fw_delete_rule(b_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(b_populate_filter_reverse, 1);
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(b_populate_hairpin_rule, 1);

    expect_fw_replace_rule(c_populate_filter_forward_log, 1);
    expect_fw_replace_rule(c_populate_filter_forward, 2);
    expect_fw_replace_rule(c_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(c_populate_filter_reverse, 4);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_hairpin_rule, 1);

    handle_events();

    print_message("4. make GMAP Name == DESKTOP-FV3HVV2\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "Name", "DESKTOP-FV3HVV2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(c_populate_filter_forward_log, 1);
    expect_fw_delete_rule(c_populate_filter_forward, 1);
    expect_fw_delete_rule(c_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(c_populate_filter_reverse, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_delete_rule(c_populate_hairpin_rule, 1);

    handle_events();

    print_message("5. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("6. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_mac_address(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    amxd_object_t* object_gmap = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    object_gmap = amxd_dm_findf(dm, "Devices.Device.3.");
    assert_non_null(object_gmap);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "6C:02:E0:0A:E7:95");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(d_populate_filter_forward_log, 1);
    expect_fw_replace_rule(d_populate_filter_forward, 2);
    expect_fw_replace_rule(d_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(d_populate_filter_reverse, 4);
    expect_fw_replace_rule(d_populate_nat_rule, 1);
    expect_fw_replace_rule(d_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make GMAP IPAddress == 192.168.99.250\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.99.250");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(d_populate_filter_forward_log, 1);
    expect_fw_delete_rule(d_populate_filter_forward, 1);
    expect_fw_delete_rule(d_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(d_populate_filter_reverse, 1);
    expect_fw_delete_rule(d_populate_nat_rule, 1);
    expect_fw_delete_rule(d_populate_hairpin_rule, 1);

    expect_fw_replace_rule(e_populate_filter_forward_log, 1);
    expect_fw_replace_rule(e_populate_filter_forward, 2);
    expect_fw_replace_rule(e_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(e_populate_filter_reverse, 4);
    expect_fw_replace_rule(e_populate_nat_rule, 1);
    expect_fw_replace_rule(e_populate_hairpin_rule, 1);

    handle_events();

    print_message("4. make GMAP PhysAddress == 6C:02:E0:0A:E7:96\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "PhysAddress", "6C:02:E0:0A:E7:96");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(e_populate_filter_forward_log, 1);
    expect_fw_delete_rule(e_populate_filter_forward, 1);
    expect_fw_delete_rule(e_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(e_populate_filter_reverse, 1);
    expect_fw_delete_rule(e_populate_nat_rule, 1);
    expect_fw_delete_rule(e_populate_hairpin_rule, 1);

    handle_events();

    print_message("5. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("6. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_csv_protocols(UNUSED void** state) {
    amxd_trans_t trans;

    assert_int_equal(amxd_trans_init(&trans), 0);

    expect_query_lan_ipv4_addresses();

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP,AH,TCP"); // include protocol AH because it has no ports
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.100/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(51, true, NULL, "192.168.1.100/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_forward(6, true, NULL, "192.168.1.100/32", 8000), 3);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.100/32", 8000), 4);
    expect_fw_replace_rule_json(json_string_portmap_forward(51, false, NULL, "192.168.1.100/32", 8000), 5);
    expect_fw_replace_rule_json(json_string_portmap_forward(6, false, NULL, "192.168.1.100/32", 8000), 6);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.100/32", 8000), 7);
    expect_fw_replace_rule_json(json_string_portmap_reverse(51, true, NULL, "192.168.1.100/32", 8000), 8);
    expect_fw_replace_rule_json(json_string_portmap_reverse(6, true, NULL, "192.168.1.100/32", 8000), 9);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.100/32", 8000), 10);
    expect_fw_replace_rule_json(json_string_portmap_reverse(51, false, NULL, "192.168.1.100/32", 8000), 11);
    expect_fw_replace_rule_json(json_string_portmap_reverse(6, false, NULL, "192.168.1.100/32", 8000), 12);
    expect_fw_replace_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.100/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_nat(51, NULL, NULL, 80, "192.168.1.100/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_nat(6, NULL, NULL, 80, "192.168.1.100/32", 8000), 3);

    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.PortMapping.pm-test", "Status", "Enabled");
}

void test_portmapping_address_0_0_0_0(UNUSED void** state) {
    amxd_trans_t trans;

    assert_int_equal(amxd_trans_init(&trans), 0);

    expect_query_lan_ipv4_addresses();

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-99");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "192.168.1.3");
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "0.0.0.0/0");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 35000);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 7547);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(json_string_portmap_forward(6, true, "0.0.0.0/0", "192.168.1.3/32", 7547), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(6, false, "0.0.0.0/0", "192.168.1.3/32", 7547), 2);
    expect_fw_replace_rule_json(json_string_portmap_reverse(6, true, "0.0.0.0/0", "192.168.1.3/32", 7547), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(6, false, "0.0.0.0/0", "192.168.1.3/32", 7547), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(6, "0.0.0.0/0", NULL, 35000, "192.168.1.3/32", 7547), 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.PortMapping.pm-99", "Status", "Enabled");
}

void test_portmapping_allowed_origins(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object;


    print_message("1. set NAT.PortMappingAllowedOrigins to accept Internal rule\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "PortMappingAllowedOrigins", "User,System,UPnp,Controller,Static,UPnP_IWF,Internal");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "PortMappingAllowedOrigins"), NULL), "User,System,UPnp,Controller,Static,UPnP_IWF,Internal");

    print_message("2. add portmapping with Internal origin\n");
    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-allowed-origin");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Internal");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-allowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");

    print_message("3. remove Internal from PortMappingAllowedOrigins\n");
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "PortMappingAllowedOrigins", "User,System,UPnp,Controller,Static,UPnP_IWF");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "PortMappingAllowedOrigins"), NULL), "User,System,UPnp,Controller,Static,UPnP_IWF");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-allowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Inactive");

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    print_message("4. add portmapping with Internal origin\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-disallowed-origin");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Internal");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 81);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-disallowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Inactive");

    print_message("5. add Internal to PortMappingAllowedOrigins\n");
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "PortMappingAllowedOrigins", "User,System,UPnp,Controller,Static,UPnP_IWF,Internal");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(a_populate_filter_forward_log, 5);
    expect_fw_replace_rule(a_populate_filter_forward, 6);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 7);
    expect_fw_replace_rule(a_populate_filter_reverse, 8);
    expect_fw_replace_rule(a_populate_nat_rule_port_81, 2);
    handle_events();

    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-allowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");
}

void test_portmapping_update_portmapping(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object;

    amxd_trans_init(&trans);

    print_message("1. add portmapping with 100 LeaseTime\n");
    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-lease-100");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Internal");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 100);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-lease-100.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");

    print_message("2. Go forward 2sec\n");
    amxut_timer_go_to_future_ms(2000);
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-lease-100.");
    assert_non_null(object);
    amxd_status_t status = amxd_status_ok;
    assert_int_equal(98, amxd_object_get_uint32_t(object, "RemainingLeaseTime", &status));

    print_message("3. Refresh Lease Timer\n");
    amxc_var_t args;
    amxc_var_t ret;
    object = amxd_dm_findf(dm, "NAT.");
    assert_non_null(object);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "protocol", "TCP");
    amxc_var_add_key(cstring_t, &args, "origin", "Internal");
    amxc_var_add_key(uint32_t, &args, "externalPort", 80);
    amxc_var_add_key(uint32_t, &args, "leaseDuration", 100);
    assert_int_equal(amxd_object_invoke_function(object, "updatePortMapping", &args, &ret), amxd_status_ok);
    amxut_bus_handle_events();
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-lease-100.");
    assert_non_null(object);
    assert_int_equal(100, amxd_object_get_uint32_t(object, "RemainingLeaseTime", &status));
}

void test_portmapping_create_instance_failed(UNUSED void** state) {
    amxd_trans_t trans;

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);                         // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_commit_failed(3);
    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.PortMapping.pm-test", "Status", "Error");

    print_message("2. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
}

void test_portmapping_allinterfaces_hairpinnat(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t result;

    expect_query_wan_netdevname();
    expect_query_wan_addresses();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, "natiface-98");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    handle_events();

    expect_query_lan_ipv4_addresses();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "portmap-99");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "AllInterfaces", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "");  // must be empty
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP");
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", ""); // must be empty
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "192.168.1.3");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_hairpinnat(17, "192.168.1.1/24", "192.168.1.3/32", 8000, "192.168.99.221/32"), 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.PortMapping.portmap-99", "Status", "Enabled");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.natiface-98");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_hairpinnat(17, "192.168.1.1/24", "192.168.1.3/32", 8000, "192.168.99.221/32"), 1);
    expect_fw_delete_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    // no hairpin nat rule added because NAT.InterfaceSetting is disabled
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.PortMapping.portmap-99", "Status", "Enabled");

    expect_query_wan_netdevname();
    mocknm_expect_openQuery_getAddrs("Device.IP.Interface.2.", "[{\"Address\":\"2001:db8::100\",\"Family\":\"ipv6\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"64\"}]");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.natiface-98");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    // no hairpin nat rule added because NAT.InterfaceSetting has no ipv4 addresses
    expect_fw_replace_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    handle_events();

    amxc_var_init(&result);
    amxc_var_set(jstring_t, &result, "[{\"Address\":\"192.168.99.221\",\"Family\":\"ipv4\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"24\",\"Scope\":\"global\"}"
                 ",{\"Address\":\"192.168.99.222\",\"Family\":\"ipv4\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"24\",\"Scope\":\"global\"}]");
    assert_int_equal(amxc_var_cast(&result, AMXC_VAR_ID_LIST), 0);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    // if the NAT.InterfaceSetting has multiple ipv4 addresses, then multiple rules will be added but this is useless because packets will match only to the first rule
    expect_fw_replace_rule_json(json_string_portmap_hairpinnat(17, "192.168.1.1/24", "192.168.1.3/32", 8000, "192.168.99.221/32"), 1);
    expect_fw_replace_rule_json(json_string_portmap_hairpinnat(17, "192.168.1.1/24", "192.168.1.3/32", 8000, "192.168.99.222/32"), 2);
    mocknm_change_query_result("Device.IP.Interface.2.getAddrs(flag=\"ipv4\", traverse=\"down\")", &result);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_del_inst(&trans, 0, "natiface-98");
    expect_fw_delete_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    expect_fw_delete_rule_json(json_string_portmap_hairpinnat(17, "192.168.1.1/24", "192.168.1.3/32", 8000, "192.168.99.221/32"), 1);
    expect_fw_delete_rule_json(json_string_portmap_hairpinnat(17, "192.168.1.1/24", "192.168.1.3/32", 8000, "192.168.99.222/32"), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, true, NULL, "192.168.1.3/32", 8000), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(17, false, NULL, "192.168.1.3/32", 8000), 2);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, true, NULL, "192.168.1.3/32", 8000), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17, false, NULL, "192.168.1.3/32", 8000), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(17, NULL, NULL, 80, "192.168.1.3/32", 8000), 1);
    // no hairpin nat rule added because there is no NAT.InterfaceSetting instance
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxc_var_clean(&result);
}

void test_portmapping_externalport_wildcard(UNUSED void** state) {
    amxd_trans_t trans;

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();
    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping");
    amxd_trans_add_inst(&trans, 0, "portmap-60");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 0);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 1401);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "192.168.1.100");
    amxd_trans_set_value(bool, &trans, "Log", false);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping");
    amxd_trans_add_inst(&trans, 0, "portmap-61");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 1400);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 1402);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "192.168.1.101");
    amxd_trans_set_value(bool, &trans, "Log", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(json_string_portmap_forward(6, false, NULL, "192.168.1.100/32", 0), 1); // no destination port because ExternalPort is zero so InternalPort should be ignored
    expect_fw_replace_rule_json(json_string_portmap_reverse(6, false, NULL, "192.168.1.100/32", 0), 2);
    expect_fw_replace_rule_json(json_string_portmap_nat(6, NULL, "192.168.99.221/32", 0, "192.168.1.100/32", 0), 1);
    expect_fw_replace_rule_json(json_string_portmap_hairpinnat(6, "192.168.1.1/24", "192.168.1.100/32", 0, "192.168.99.221/32"), 1);

    expect_fw_replace_rule_json(json_string_portmap_forward(6, false, NULL, "192.168.1.101/32", 1402), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(6, false, NULL, "192.168.1.101/32", 1402), 4);
    expect_fw_replace_rule_json(json_string_portmap_nat(6, NULL, "192.168.99.221/32", 1400, "192.168.1.101/32", 1402), 1); // not index 2 because it is in different chain then portmap-60's nat rule
    expect_fw_replace_rule_json(json_string_portmap_hairpinnat(6, "192.168.1.1/24", "192.168.1.101/32", 1402, "192.168.99.221/32"), 2);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_nat_interface_validators(UNUSED void** state) {
    amxd_trans_t trans;

    expect_query_wan_netdevname();
    expect_query_wan_addresses();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, "natiface-test");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    handle_events();

    expect_query_wan_netdevname_no_device();
    expect_query_wan_addresses_no_device();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.natiface-test.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    expect_fw_replace_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    handle_events();

    expect_query_wan_netdevname_logical();
    expect_query_wan_addresses_logical();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.natiface-test.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.1.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    expect_fw_replace_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    handle_events();

    expect_query_wan_netdevname_logical_no_device();
    expect_query_wan_addresses_logical_no_device();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.natiface-test.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Logical.Interface.1.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    expect_fw_replace_rule_json(json_string_ifsetting_masquerade("eth0"), 1);
    handle_events();

    // //Testing wrong cases
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.natiface-test.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Interface.1.");
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_portmapping_expired_endtime(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    amxc_ts_t now;

    assert_int_equal(amxd_trans_init(&trans), 0);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-99");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    assert_int_equal(amxc_ts_now(&now), 0); // if EndTime != 0001-01-01T00:00:00Z at instance-added event and time now is > EndTime, the portmapping is removed because of LeaseDuration
    amxd_trans_set_value(amxc_ts_t, &trans, "EndTime", &now);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-99");
    assert_null(object);

    amxd_trans_clean(&trans);
}