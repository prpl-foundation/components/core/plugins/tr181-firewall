/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include "mock.h"
#include "test_dm_enable.h"

static amxd_dm_t* dm = NULL;

static void a_populate_input_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "INPUT_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_input6_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "INPUT6_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, false), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_forward_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_forward6_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD6_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, false), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_prerouting_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_RAW), 0);
    assert_int_equal(fw_rule_set_chain(rule, "PREROUTING_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_prerouting6_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_RAW), 0);
    assert_int_equal(fw_rule_set_chain(rule, "PREROUTING6_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, false), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_prerouting_rule_nat(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "PREROUTING_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_postrouting_rule_nat(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_Firewall_Enable"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

int test_fw_enable_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int test_fw_enable_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_fw_enable(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    print_message("1. object 'Firewall.' must exist and must be enabled\n");
    object = amxd_dm_findf(dm, "Firewall.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_true(GET_BOOL(&params, "Enable"));

    print_message("2. make Enable == false\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_input_rule, 1);
    expect_fw_replace_rule(a_populate_input6_rule, 1);
    expect_fw_replace_rule(a_populate_forward_rule, 1);
    expect_fw_replace_rule(a_populate_forward6_rule, 1);
    expect_fw_replace_rule(a_populate_prerouting_rule, 1);
    expect_fw_replace_rule(a_populate_prerouting6_rule, 1);
    expect_fw_replace_rule(a_populate_prerouting_rule_nat, 1);
    expect_fw_replace_rule(a_populate_postrouting_rule_nat, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));

    print_message("3. make Enable == true\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_input_rule, 1);
    expect_fw_delete_rule(a_populate_input6_rule, 1);
    expect_fw_delete_rule(a_populate_forward_rule, 1);
    expect_fw_delete_rule(a_populate_forward6_rule, 1);
    expect_fw_delete_rule(a_populate_prerouting_rule, 1);
    expect_fw_delete_rule(a_populate_prerouting6_rule, 1);
    expect_fw_delete_rule(a_populate_prerouting_rule_nat, 1);
    expect_fw_delete_rule(a_populate_postrouting_rule_nat, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_true(GET_BOOL(&params, "Enable"));

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}