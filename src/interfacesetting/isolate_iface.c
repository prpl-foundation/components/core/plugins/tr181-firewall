/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "logs/logs.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <ipat/ipat.h>
#include <string.h>

#define ME "fw-isol"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    ifsetting_t* ifsetting = NULL;
    isolate_t* isolate = NULL;
    bool update = false;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    ifsetting = (ifsetting_t*) object->priv;
    when_null_trace(ifsetting, exit, ERROR, "IterfaceSetting[%s] is NULL", OBJECT_NAMED);

    isolate = ifsetting->isolate;
    when_null(isolate, exit);

    update = log_update(log,
                        ENABLE_LOG,
                        log_get_dm_controller_param(object, NULL, NULL),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        data);
    if(update) {
        isolate_iface_changed(ifsetting);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

// caller responsible for calling fw_commit / fw_apply
static bool activate_isolate_iface(ifsetting_t* ifsetting, amxc_var_t* list_of_subnets, ipversion_t version) {
    fw_rule_t* r = NULL;
    bool ipv4 = version == IPv4;
    isolate_t* isolate = NULL;
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folder = NULL;
    fw_folder_t* folders[] = {NULL, NULL};
    bool do_commit = false;
    const char* chain = ipv4 ? "PREROUTING_Isolation" : "PREROUTING6_Isolation";

    SAH_TRACEZ_INFO(ME, "activate %s %s", ipv4 ? "ipv4" : "ipv6", ifsetting->name);

    when_null_trace(ifsetting, exit, ERROR, "ifsetting is NULL");
    isolate = ifsetting->isolate;
    when_null_trace(isolate, exit, ERROR, "%s is missing isolate struct.", ifsetting->name);
    log_folder = ipv4 ? isolate->folders.log_folders.folder : isolate->folders.log_folders.folder6;
    folder = ipv4 ? isolate->folders.rule_folders.folder : isolate->folders.rule_folders.folder6;
    when_null_trace(folder, exit, ERROR, "%s is missing folder", ifsetting->name);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folders[0] = log_folder;
    folders[1] = folder;
    size_t loop_start = log_get_enable(&isolate->folders.log) ? 0 : 1;
#else //FIREWALL_LOGS
    folders[0] = folder;
    size_t loop_start = 0;
#endif //FIREWALL_LOGS

    when_null_trace(amxc_var_get_first(list_of_subnets), exit, INFO, "%s is waiting for subnets", ifsetting->name);
    when_str_empty_trace(ifsetting->fwiface.default_netdevname, exit, INFO, "%s is waiting for netdevname", ifsetting->name);

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        if(folders[i] == NULL) {
            continue;
        }
        fw_folder_set_feature(folders[i], FW_FEATURE_DESTINATION);

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, exit, ERROR, "Failed to fetch common rule");

        fw_rule_set_table(r, TABLE_RAW);
        fw_rule_set_chain(r, chain);
        fw_rule_set_ipv4(r, ipv4);
        fw_rule_set_in_interface(r, ifsetting->fwiface.default_netdevname);
        if(folders[i] != log_folder) {
            fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP);
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        else {
            set_log_rule_specific_settings(r, &ifsetting->isolate->folders.log);
        }
#endif //FIREWALL_LOGS

        when_failed_trace(fw_folder_push_rule(folders[i], r), exit, ERROR, "Failed to push common rule");

        amxc_var_for_each(var, list_of_subnets) {
            fw_rule_t* s = NULL;

            s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_DESTINATION);
            when_null_trace(s, exit, ERROR, "Failed to fetch feature rule");
            fw_rule_set_destination(s, GET_CHAR(var, NULL));
            when_failed_trace(fw_folder_push_rule(folders[i], s), exit, ERROR, "Failed to push feature rule");
        }

        fw_folder_set_enabled(folders[i], true);
    }

    do_commit = true;

exit:
    return do_commit;
}

static void deactivate_isolate_iface(ifsetting_t* ifsetting, ipversion_t version) {
    when_null(ifsetting, exit);
    when_null(ifsetting->isolate, exit);
    folder_container_delete_rules(&ifsetting->isolate->folders, version, true);
    when_failed_trace(fw_apply(), exit, ERROR, "fw_apply failed");
exit:
    return;
}

void isolate_iface_changed(ifsetting_t* ifsetting) {
    const char* isolate_iface = NULL;
    amxd_object_t* object_to_isolate = NULL;
    ifsetting_t* ifsetting_to_isolate = NULL;
    bool apply = false;

    deactivate_isolate_iface(ifsetting, IPvAll);

    isolate_iface = object_da_string(ifsetting->object, "IsolateInterface");
    when_str_empty_trace(isolate_iface, exit, INFO, "skip %s because IsolateInterface is empty", ifsetting->name);

    object_to_isolate = amxd_dm_findf(fw_get_dm(), "%s", isolate_iface);
    when_null_trace(object_to_isolate, exit, ERROR, "skip %s because no object %s", ifsetting->name, isolate_iface);
    when_true_trace(object_to_isolate == ifsetting->object, exit, ERROR, "skip %s because IsolateInterface is self reference", ifsetting->name);
    when_null_trace(object_to_isolate->priv, exit, INFO, "skip %s because no priv data", ifsetting->name);
    ifsetting_to_isolate = (ifsetting_t*) object_to_isolate->priv;

    // in case the netdevname query is closed, activate_isolate_iface will notice this and that is ok
    apply |= activate_isolate_iface(ifsetting, &ifsetting_to_isolate->isolate->subnets, IPv4);
    apply |= activate_isolate_iface(ifsetting, &ifsetting_to_isolate->isolate->subnets_v6, IPv6);

    if(apply) {
        when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed");
        when_failed_trace(fw_apply(), exit, ERROR, "fw_apply failed");
    }
exit:
    return;
}

static void isolate_iface_notifier_address_changed(ifsetting_t* ifsetting_to_isolate, ipversion_t version) {
    bool apply = false;

    amxd_object_for_each(instance, it, amxd_object_get_parent(ifsetting_to_isolate->object)) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        ifsetting_t* ifsetting = NULL;
        const char* iso_iface = NULL;
        char buf[512] = {0};
        bool match = false;

        iso_iface = object_da_string(instance, "IsolateInterface");
        if((instance == ifsetting_to_isolate->object) || (instance->priv == NULL) || STRING_EMPTY(iso_iface)) {
            continue;
        }
        ifsetting = (ifsetting_t*) instance->priv;

        snprintf(buf, sizeof(buf) - 1, "Firewall.%sInterfaceSetting.%d.", fw_get_vendor_prefix(), amxd_object_get_index(ifsetting_to_isolate->object));
        if(strstr(buf, iso_iface) != NULL) {
            match = true;
        } else {
            snprintf(buf, sizeof(buf) - 1, "Firewall.%sInterfaceSetting.[Alias=='%s'].", fw_get_vendor_prefix(), object_da_string(ifsetting_to_isolate->object, "Alias"));
            match = (strstr(buf, iso_iface) != NULL);
        }

        if(!match) {
            continue;
        }

        deactivate_isolate_iface(ifsetting, version);
        if((version == IPvAll) || (version == IPv4)) {
            apply |= activate_isolate_iface(ifsetting, &ifsetting_to_isolate->isolate->subnets, IPv4);
        }
        if((version == IPvAll) || (version == IPv6)) {
            apply |= activate_isolate_iface(ifsetting, &ifsetting_to_isolate->isolate->subnets_v6, IPv6);
        }
    }

    if(apply) {
        when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed");
        when_failed_trace(fw_apply(), exit, ERROR, "fw_apply failed");
    }
exit:
    return;
}

static amxc_var_t* result_to_list_of_subnets(const amxc_var_t* result) {
    amxc_var_t* list_of_subnets = NULL;

    amxc_var_new(&list_of_subnets);
    amxc_var_set_type(list_of_subnets, AMXC_VAR_ID_LIST);

    amxc_var_for_each(var, result) {
        char cidr[IPAT_STRLEN_SAFE + 1] = {0};
        const char* type_flags = GET_CHAR(var, "TypeFlags");

        if((type_flags != NULL) && (strstr(type_flags, "@lla") != NULL)) {
            snprintf(cidr, sizeof(cidr) - 1, "%s/128", GET_CHAR(var, "Address"));
        } else {
            int prefixlen = GET_INT32(var, "PrefixLen");
            const char* addr = GET_CHAR(var, "Address");
            char* subnet = NULL;

            subnet = ipat_text_mask_apply_direct(addr, prefixlen, ipat_bits_upper, ipat_oper_and, false);
            if(subnet == NULL) {
                SAH_TRACEZ_ERROR(ME, "Failed to mask %s/%d", addr, prefixlen);
                continue;
            }

            snprintf(cidr, sizeof(cidr) - 1, "%s/%d", subnet, GET_INT32(var, "PrefixLen"));
            FREE(subnet);
        }

        if(amxc_var_add(cstring_t, list_of_subnets, cidr) == NULL) {
            SAH_TRACEZ_ERROR(ME, "failed to add %s", cidr);
            continue;
        }
    }

    return list_of_subnets;
}

static void address_changed(const amxc_var_t* result, void* userdata, ipversion_t version) {
    amxd_object_t* object = (amxd_object_t*) userdata;
    ifsetting_t* ifsetting = NULL;
    amxc_var_t* subnet_list_old = NULL;
    amxc_var_t* subnet_list_new = NULL;
    bool ipv4 = version == IPv4;
    int cmp_result = -1;

    when_null(object, exit);
    when_null(object->priv, exit);
    ifsetting = (ifsetting_t*) object->priv;

    if(ipv4) {
        subnet_list_old = &ifsetting->isolate->subnets;
    } else {
        subnet_list_old = &ifsetting->isolate->subnets_v6;
    }

    subnet_list_new = result_to_list_of_subnets(result);
    when_failed_trace(amxc_var_compare(subnet_list_old, subnet_list_new, &cmp_result), exit, ERROR, "can't compare variants");
    when_true_trace(cmp_result == 0, exit, INFO, "%s nothing changed", ifsetting->name);

    SAH_TRACEZ_INFO(ME, "%s subnet changed", ifsetting->name);
    when_failed_trace(amxc_var_move(subnet_list_old, subnet_list_new), exit, ERROR, "variant move failed");

    isolate_iface_notifier_address_changed(ifsetting, version);

exit:
    amxc_var_delete(&subnet_list_new);
    return;
}

static void address_v4_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    address_changed(result, userdata, IPv4);
}

static void address_v6_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    address_changed(result, userdata, IPv6);
}

int isolate_iface_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;
    int rv = 1;
    const char* iface = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    ifsetting = (ifsetting_t*) object->priv;

    when_false(ifsetting->isolate == NULL, exit);
    ifsetting->isolate = (isolate_t*) calloc(1, sizeof(isolate_t));
    when_null(ifsetting->isolate, exit);

    amxc_var_init(&ifsetting->isolate->subnets);
    amxc_var_set_type(&ifsetting->isolate->subnets, AMXC_VAR_ID_LIST);
    amxc_var_init(&ifsetting->isolate->subnets_v6);
    amxc_var_set_type(&ifsetting->isolate->subnets_v6, AMXC_VAR_ID_LIST);

    folder_container_init(&ifsetting->isolate->folders, NULL);

    iface = object_da_string(object, "Interface");
    if(!STRING_EMPTY(iface)) {
        // first (for no particular reason) activate isolation for others
        ifsetting->isolate->query = netmodel_openQuery_getAddrs(iface, FW_QUERY_SUBSCR, "ipv4", netmodel_traverse_down, address_v4_changed, object);
        when_null_trace(ifsetting->isolate->query, exit, ERROR, "%s failed to open query", ifsetting->name);
        ifsetting->isolate->query_v6 = netmodel_openQuery_getAddrs(iface, FW_QUERY_SUBSCR, "ipv6", netmodel_traverse_down, address_v6_changed, object);
        when_null_trace(ifsetting->isolate->query_v6, exit, ERROR, "%s failed to open query", ifsetting->name);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        log_init(object, &ifsetting->isolate->folders.log, &ifsetting->fwiface, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
                 true, log_get_dm_controller_param(object, NULL, NULL), TARGET_DROP, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

        // now activate isolation for ourself
        isolate_iface_changed(ifsetting);
    }

    rv = 0;
exit:
    FW_PROFILING_OUT();
    return rv;
}

int isolate_iface_cleanup(ifsetting_t* ifsetting) {
    int rv = 1;

    when_null(ifsetting, exit);

    _netmodel_closeQuery(&ifsetting->isolate->query);
    _netmodel_closeQuery(&ifsetting->isolate->query_v6);

    amxc_var_clean(&ifsetting->isolate->subnets);
    amxc_var_clean(&ifsetting->isolate->subnets_v6);

    folder_container_clean(&ifsetting->isolate->folders);
    when_failed_trace(fw_apply(), exit, ERROR, "fw_apply failed");

    rv = 0;
exit:
    FREE(ifsetting->isolate);
    return rv;
}

void _isolate_iface_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    ifsetting_t* ifsetting = NULL;
    amxd_object_t* obj = amxd_dm_signal_get_object(fw_get_dm(), event_data);

    when_null(obj, exit);
    when_null(obj->priv, exit);
    ifsetting = (ifsetting_t*) obj->priv;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    if(ifsetting->isolate != NULL) {
        log_update(&ifsetting->isolate->folders.log,
                   ENABLE_LOG,
                   log_get_dm_controller_param(obj, event_data, NULL),
                   !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                   event_data);
    }
#endif //FIREWALL_LOGS
    isolate_iface_changed(ifsetting);

exit:
    FW_PROFILING_OUT();
    return;
}
