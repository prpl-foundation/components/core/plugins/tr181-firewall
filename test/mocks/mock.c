/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <fwrules/fw_rule.h>

#include "firewall.h"
#include "mock.h"

bool rule_is_being_deleted = false;

#define string_equal(a, b, parameter) do { \
        const char* aa = a; \
        const char* bb = b; \
        if(!aa) { aa = "";} \
        if(!bb) { bb = "";} \
        if(strcmp(aa, bb) != 0) { \
            snprintf(error_message, sizeof(error_message) - 1, "ERROR: Parameter '%s' is different, <plugin> %s != <test> %s\n", parameter, a, b); \
            goto exit; \
        } \
} while(0)

#define function_return_string_equal(function, a, b, parameter) do { \
        string_equal(function(a), function(b), parameter); \
} while(0)

#define param_equal(a, b, parameter) do { \
        if(a != b) { \
            snprintf(error_message, sizeof(error_message) - 1, "ERROR: Parameter '%s' is different, <plugin> %d != <test> %d\n", parameter, (int) a, (int) b); \
            goto exit; \
        } \
} while(0)

#define function_return_equal(function, a, b, parameter) do { \
        param_equal(function(a), function(b), parameter); \
} while(0)

int ut_rule_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    const fw_rule_t* a = (const fw_rule_t*) value;
    const fw_rule_t* b = (const fw_rule_t*) check_value_data;
    char error_message[1024] = {0};
    bool ipv4 = true;
    int rv = 0;

    function_return_string_equal(fw_rule_get_table, a, b, "table");
    function_return_string_equal(fw_rule_get_chain, a, b, "chain");
    function_return_string_equal(fw_rule_get_in_interface, a, b, "in_interface");
    function_return_string_equal(fw_rule_get_out_interface, a, b, "out_interface");
    function_return_string_equal(fw_rule_get_source, a, b, "source");
    function_return_string_equal(fw_rule_get_source_mask, a, b, "source_mask");
    function_return_string_equal(fw_rule_get_destination, a, b, "destination");
    function_return_string_equal(fw_rule_get_destination_mask, a, b, "destination_mask");
    function_return_string_equal(fw_rule_get_source_mac_address, a, b, "source_mac_address");
    function_return_string_equal(fw_rule_get_conntrack_state, a, b, "conntrack_state");
    function_return_string_equal(fw_rule_get_source_ipset, a, b, "source_ipset");
    function_return_string_equal(fw_rule_get_destination_ipset, a, b, "destination_ipset");


    if(!rule_is_being_deleted) {
        function_return_equal(fw_rule_is_enabled, a, b, "enabled");
    }

    function_return_equal(fw_rule_get_ipv4, a, b, "ipv4");
    ipv4 = fw_rule_get_ipv4(a);
    function_return_equal(fw_rule_get_protocol, a, b, "protocol");
    function_return_equal(fw_rule_get_target, a, b, "target");
    if(ipv4) {
        function_return_equal(fw_rule_get_icmp_type, a, b, "icmp_type");
    } else {
        function_return_equal(fw_rule_get_icmpv6_type, a, b, "icmp_type");
    }
    function_return_equal(fw_rule_get_source_port, a, b, "source_port");
    function_return_equal(fw_rule_get_source_port_range_max, a, b, "source_port_range_max");
    function_return_equal(fw_rule_get_destination_port, a, b, "destination_port");
    function_return_equal(fw_rule_get_destination_port_range_max, a, b, "destination_port_range_max");
    function_return_equal(fw_rule_get_in_interface_excluded, a, b, "in_interface_excluded");
    function_return_equal(fw_rule_get_out_interface_excluded, a, b, "out_interface_excluded");
    function_return_equal(fw_rule_get_source_excluded, a, b, "source_excluded");
    function_return_equal(fw_rule_get_destination_excluded, a, b, "destination_excluded");
    function_return_equal(fw_rule_get_source_port_excluded, a, b, "source_port_excluded");
    function_return_equal(fw_rule_get_destination_port_excluded, a, b, "destination_port_excluded");
    function_return_equal(fw_rule_get_source_mac_excluded, a, b, "source_mac_excluded");
    function_return_equal(fw_rule_get_dscp, a, b, "dscp");
    function_return_equal(fw_rule_get_dscp_excluded, a, b, "dscp_excluded");
    function_return_equal(fw_rule_get_source_ipset_excluded, a, b, "source_ipset_excluded");
    function_return_equal(fw_rule_get_destination_ipset_excluded, a, b, "destination_ipset_excluded");

    fw_rule_target_t target = fw_rule_get_target(a);
    if(target == FW_RULE_TARGET_POLICY) {
        function_return_equal(fw_rule_get_target_policy_option, a, b, "target_policy_option");
    } else if(target == FW_RULE_TARGET_DNAT) {
        const char* ip_a = NULL;
        const char* ip_b = NULL;
        uint32_t min_port_a = 0;
        uint32_t min_port_b = 0;
        uint32_t max_port_a = 0;
        uint32_t max_port_b = 0;
        fw_rule_get_target_dnat_options(a, &ip_a, &min_port_a, &max_port_a);
        fw_rule_get_target_dnat_options(b, &ip_b, &min_port_b, &max_port_b);
        string_equal(ip_a, ip_b, "dnat_ip");
        param_equal(min_port_a, min_port_b, "dnat_min_port");
        param_equal(max_port_a, max_port_b, "dnat_max_port");
    } else if(target == FW_RULE_TARGET_SNAT) {
        const char* ip_a = NULL;
        const char* ip_b = NULL;
        uint32_t min_port_a = 0;
        uint32_t min_port_b = 0;
        uint32_t max_port_a = 0;
        uint32_t max_port_b = 0;
        fw_rule_get_target_snat_options(a, &ip_a, &min_port_a, &max_port_a);
        fw_rule_get_target_snat_options(b, &ip_b, &min_port_b, &max_port_b);
        string_equal(ip_a, ip_b, "snat_ip");
        param_equal(min_port_a, min_port_b, "snat_min_port");
        param_equal(max_port_a, max_port_b, "snat_max_port");
    } else if(target == FW_RULE_TARGET_CHAIN) {
        function_return_string_equal(fw_rule_get_target_chain_option, a, b, "target_chain_option");
    } else if(target == FW_RULE_TARGET_NFQUEUE) {
        uint32_t a_qnum = 0;
        uint32_t a_qtotal = 0;
        uint32_t a_flags = 0;
        uint32_t b_qnum = 0;
        uint32_t b_qtotal = 0;
        uint32_t b_flags = 0;
        fw_rule_get_target_nfqueue_options(a, &a_qnum, &a_qtotal, &a_flags);
        fw_rule_get_target_nfqueue_options(b, &b_qnum, &b_qtotal, &b_flags);
        param_equal(a_qnum, b_qnum, "nfqueue_qnum");
        param_equal(a_qtotal, b_qtotal, "nfqueue_qtotal");
        param_equal(a_flags, b_flags, "nfqueue_flags");
    } else if(target == FW_RULE_TARGET_LOG) {
        uint8_t a_level = 0;
        uint8_t a_logflags = 0;
        uint8_t b_level = 0;
        uint8_t b_logflags = 0;
        const char* a_prefix = NULL;
        const char* b_prefix = NULL;
        fw_rule_get_target_log_options(a, &a_level, &a_logflags, &a_prefix);
        fw_rule_get_target_log_options(b, &b_level, &b_logflags, &b_prefix);
        string_equal(a_prefix, b_prefix, "target_option.prefix");
        param_equal(a_level, b_level, "target_option.level");
        param_equal(a_logflags, b_logflags, "target_option.flags");
    } else {
        fprintf(stdout, "UNKNOWN TARGET not compared (%s:%d %s)!\n", __func__, __LINE__, __FILE__);
        goto exit;
    }

    rv = 1;

exit:
    if(rv != 1) {
        print_message("<rule from plugin>\n");
        fw_rule_dump(a, STDOUT_FILENO);
        print_message("<rule from unit test>\n");
        fw_rule_dump(b, STDOUT_FILENO);
        print_message("%s\n", error_message);
    }
    fw_rule_delete((fw_rule_t**) &b);
    return rv;
}

int ut_rule_new(fw_rule_t** rule) { // todo add to lib_fwrules
    assert_int_equal(fw_rule_new(rule), 0);

    /**
     * Remove the new rule because otherwise the test influences the behavior
     * of lib_fwrules and the firewall manager. Lib_fwrules adds all new rules
     * to a global list. Function fw_commit also triggers the callback on these
     * "test" rules.
     */
    amxc_llist_it_take(&(*rule)->g_it);
    return 0;
}

void duplicate_fw_rule(fw_rule_t* dest, const fw_rule_t* src) {
    // Create a copy of the original rule and also create a copy of the hash table.
    // Otherwise the hash table is shared between the copy and the original.
    // The variant dest->ht needs to be freed with amxc_var_delete.
    assert_non_null(memcpy(dest, src, sizeof(fw_rule_t)));
    amxc_var_new(&dest->ht);
    assert_int_equal(amxc_var_copy(dest->ht, src->ht), 0);
}

fw_rule_t* json_to_fw_rule(const char* json) {
    fw_rule_t* ut_rule = NULL;
    amxc_var_t* var = NULL;
    const char* convert_uint32[] = {
        "protocol",
        "target",
        "target_option",
        "target_option.min_port",
        "target_option.max_port",
        "target_option.qnum",
        "target_option.qtotal",
        "target_option.flags",
        "destination_port",
        "destination_port_range_max",
        "source_port",
        "source_port_range_max",
        NULL
    };
    const char* convert_uint8[] = {
        "target_option.level",
        "target_option.flags",
        NULL
    };

    assert_int_equal(ut_rule_new(&ut_rule), 0);    /* freed at expect_check */

    amxc_var_set(jstring_t, ut_rule->ht, json);
    assert_int_equal(amxc_var_cast(ut_rule->ht, AMXC_VAR_ID_HTABLE), 0);

    var = amxc_var_take_key(ut_rule->ht, "table"); /* table is not part of htable */
    assert_non_null(var);
    fw_rule_set_table(ut_rule, GET_CHAR(var, NULL));
    amxc_var_delete(&var);

    var = amxc_var_take_key(ut_rule->ht, "chain"); /* chain is not part of htable */
    assert_non_null(var);
    fw_rule_set_chain(ut_rule, GET_CHAR(var, NULL));
    amxc_var_delete(&var);

    var = amxc_var_take_key(ut_rule->ht, "enable"); /* enable is not part of htable */
    assert_non_null(var);
    fw_rule_set_enabled(ut_rule, GET_BOOL(var, NULL));
    amxc_var_delete(&var);

    for(int i = 0; convert_uint32[i] != NULL; i++) {
        var = GETP_ARG(ut_rule->ht, convert_uint32[i]); /* because libamxj sets var type int but libfwrules does constcast to uint32 in getters */
        if((var == NULL) || (amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE)) {
            continue;
        }
        assert_int_equal(amxc_var_cast(var, AMXC_VAR_ID_UINT32), 0);
    }

    for(int i = 0; convert_uint8[i] != NULL; i++) {
        var = GETP_ARG(ut_rule->ht, convert_uint8[i]); /* because libamxj sets var type int but libfwrules does constcast to uint32 in getters */
        if((var == NULL) || (amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE)) {
            continue;
        }
        assert_int_equal(amxc_var_cast(var, AMXC_VAR_ID_UINT8), 0);
    }

    return ut_rule;
}

static void _expect_query_wan_ipv4_addresses(const char* interface) {
    mocknm_expect_openQuery_getAddrs(interface, "[{\"Address\":\"192.168.99.221\",\"Family\":\"ipv4\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"24\"}]");
}

void expect_query_wan_ipv4_addresses(void) {
    _expect_query_wan_ipv4_addresses("Device.IP.Interface.2.");
}

void expect_query_wan_ipv4_addresses_no_device(void) {
    _expect_query_wan_ipv4_addresses("IP.Interface.2.");
}

void expect_query_wan_ipv4_addresses_logical(void) {
    _expect_query_wan_ipv4_addresses("Device.Logical.Interface.1.");
}

void expect_query_wan_ipv4_addresses_logical_no_device(void) {
    _expect_query_wan_ipv4_addresses("Logical.Interface.1.");
}

static void _expect_query_wan_ipv6_addresses(const char* interface) {
    mocknm_expect_openQuery_getAddrs(interface, "[{\"Address\":\"2001:db8::100\",\"Family\":\"ipv6\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"64\"}]");
}

void expect_query_wan_ipv6_addresses(void) {
    _expect_query_wan_ipv6_addresses("Device.IP.Interface.2.");
}

void expect_query_wan_ipv6_addresses_no_device(void) {
    _expect_query_wan_ipv6_addresses("IP.Interface.2.");
}

void expect_query_wan_ipv6_addresses_logical(void) {
    _expect_query_wan_ipv6_addresses("Device.Logical.Interface.1.");
}

void expect_query_wan_ipv6_addresses_logical_no_device(void) {
    _expect_query_wan_ipv6_addresses("Logical.Interface.1.");
}

static void _expect_query_wan_addresses(const char* interface) {
    mocknm_expect_openQuery_getAddrs(interface, "[{\"Address\":\"192.168.99.221\",\"Family\":\"ipv4\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"24\"}"
                                     ",{\"Address\":\"2001:db8::100\",\"Family\":\"ipv6\",\"NetDevName\":\"eth0\",\"PrefixLen\":\"64\"}]");
}

void expect_query_wan_addresses(void) {
    _expect_query_wan_addresses("Device.IP.Interface.2.");
}

void expect_query_wan_addresses_no_device(void) {
    _expect_query_wan_addresses("IP.Interface.2.");
}

void expect_query_wan_addresses_logical(void) {
    _expect_query_wan_addresses("Device.Logical.Interface.1.");
}

void expect_query_wan_addresses_logical_no_device(void) {
    _expect_query_wan_addresses("Logical.Interface.1.");
}

static void _expect_query_wan_netdevname(const char* interface) {
    mocknm_expect_openQuery_getFirstParameter(interface, "eth0");
}

void expect_query_wan_netdevname(void) {
    _expect_query_wan_netdevname("Device.IP.Interface.2.");
}

void expect_query_wan_netdevname_no_device(void) {
    _expect_query_wan_netdevname("IP.Interface.2.");
}

void expect_query_wan_netdevname_logical(void) {
    _expect_query_wan_netdevname("Device.Logical.Interface.1.");
}

void expect_query_wan_netdevname_logical_no_device(void) {
    _expect_query_wan_netdevname("Logical.Interface.1.");
}

static void _expect_query_wan_netdevnames(const char* interface) {
    mocknm_expect_openQuery_getParameters(interface, "{\"ip-wan\":\"eth0\"}");
}

void expect_query_wan_netdevnames(void) {
    _expect_query_wan_netdevnames("Device.IP.Interface.2.");
}

void expect_query_wan_netdevnames_no_device(void) {
    _expect_query_wan_netdevnames("IP.Interface.2.");
}

void expect_query_wan_netdevnames_logical(void) {
    _expect_query_wan_netdevnames("Device.Logical.Interface.1.");
}

void expect_query_wan_netdevnames_logical_no_device(void) {
    _expect_query_wan_netdevnames("Logical.Interface.1.");
}

// lan
void expect_query_lan_ipv4_addresses(void) {
    // Scope global is important for spoofing rules
    mocknm_expect_openQuery_getAddrs("Device.IP.Interface.3.", "[{\"Address\":\"192.168.1.1\",\"Family\":\"ipv4\",\"NetDevName\":\"br-lan\",\"PrefixLen\":\"24\",\"Scope\":\"global\"}"
                                     ",{\"Address\":\"192.168.3.1\",\"Family\":\"ipv4\",\"NetDevName\":\"br-lan\",\"PrefixLen\":\"24\",\"Scope\":\"global\"}]");
}

void expect_query_lan_ipv6_addresses(void) {
    // TypeFlags important for isolation rules
    mocknm_expect_openQuery_getAddrs("Device.IP.Interface.3.", "[{\"Address\":\"fc::1:10:18ff:fe01:2101\",\"Family\":\"ipv6\",\"NetDevName\":\"br-lan\",\"PrefixLen\":\"64\",\"Scope\":\"global\"}"
                                     ",{\"Address\":\"fe80::9483:8ce9:2ae5:ca32\",\"Family\":\"ipv6\",\"NetDevName\":\"br-lan\",\"PrefixLen\":\"64\",\"Scope\":\"link\", \"TypeFlags\":\"@lla\"}]");
}

void expect_query_lan_netdevname(void) {
    mocknm_expect_openQuery_getFirstParameter("Device.IP.Interface.3.", "br-lan");
}

void expect_query_lan_netdevnames(void) {
    mocknm_expect_openQuery_getParameters("Device.IP.Interface.3.", "{\"ip-lan\":\"br-lan\"}");
}

// guest

void expect_query_guest_ipv4_addresses(void) {
    mocknm_expect_openQuery_getAddrs("Device.IP.Interface.4.", "[{\"Address\":\"192.168.2.1\",\"Family\":\"ipv4\",\"NetDevName\":\"br-guest\",\"PrefixLen\":\"24\"}]");
}

void expect_query_guest_ipv6_addresses(void) {
    mocknm_expect_openQuery_getAddrs("Device.IP.Interface.4.", "[]");
}

void expect_query_guest_netdevname(void) {
    mocknm_expect_openQuery_getFirstParameter("Device.IP.Interface.4.", "br-guest");
}

void expect_query_guest_netdevnames(void) {
    mocknm_expect_openQuery_getParameters("Device.IP.Interface.4.", "{\"ip-guest\":\"br-guest\"}");
}