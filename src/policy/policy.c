/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "policy/policy.h"
#include "fwrule_helpers.h" // for set_log_rule_specific_settings
#include "logs/logs.h"
#include "firewall.h"       // for fw_get_chain
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <string.h>

#define ME "policy"

static void netdevname_v4_changed(firewall_interface_t* fwiface);
static void netdevname_v6_changed(firewall_interface_t* fwiface);

static void policy_set_target(fw_rule_t* s, target_t target, const char* chain, bool ipv4, const char* in, const char* out, amxd_object_t* object) {
    when_null(s, exit);
    (void) object; // when tracing is disabled, variable is not used anymore

    if(!STRING_EMPTY(in)) {
        fw_rule_set_in_interface(s, in);
    }
    if(!STRING_EMPTY(out)) {
        fw_rule_set_out_interface(s, out);
    }

    switch(target) {
    case TARGET_CHAIN:
        if(!STRING_EMPTY(chain)) {
            amxc_string_t buf;
            const char* target_chain = NULL;
            amxc_string_init(&buf, 0);
            //Force the chain to be FORWARD + (6) + _ + "Name" for backward compatibility, removing this conversion may break tests and default files used with old tr181-firewall versions
            if(strstr(chain, "FORWARD") != NULL) {                 //i.e.: FORWARD_L_Low = FORWARD_L_Low
                amxc_string_setf(&buf, "%s", chain);
                if((strstr(chain, "FORWARD6") == NULL) && !ipv4) { //i.e.: for IPv6 FORWARD_L_Low = FORWARD6_L_Low
                    amxc_string_replace(&buf, "FORWARD", "FORWARD6", UINT32_MAX);
                }
            } else if(chain[0] == '6') {           //i.e.: 6_L_Low = FORWARD6_L_Low
                amxc_string_setf(&buf, "FORWARD%s", chain);
            } else if(chain[0] == '_') {           //i.e.: _L_Low = FORWARD_L_Low
                amxc_string_setf(&buf, "FORWARD%s%s", ipv4 ? "" : "6", chain);
            } else {                               //i.e.: L_Low = FORWARD_L_Low
                amxc_string_setf(&buf, "FORWARD%s_%s", ipv4 ? "" : "6", chain);
            }
            target_chain = amxc_string_get(&buf, 0);
            SAH_TRACEZ_INFO(ME, "Policy[%s] set in[%s] out[%s] target chain[%s]", OBJECT_NAMED, in, out, target_chain);
            fw_rule_set_target_chain(s, target_chain);
            amxc_string_clean(&buf);
        } else {
            SAH_TRACEZ_WARNING(ME, "Policy[%s] Chain is empty, set in[%s] out[%s] target RETURN", OBJECT_NAMED, in, out);
            fw_rule_set_target_return(s);
        }
        break;
    case TARGET_DROP:
        SAH_TRACEZ_INFO(ME, "Policy[%s] set in[%s] out[%s] target drop", OBJECT_NAMED, in, out);
        fw_rule_set_target_policy(s, FW_RULE_POLICY_DROP);
        break;
    case TARGET_REJECT:
        SAH_TRACEZ_INFO(ME, "Policy[%s] set in[%s] out[%s] target reject", OBJECT_NAMED, in, out);
        fw_rule_set_target_policy(s, FW_RULE_POLICY_REJECT);
        break;
    default:
        SAH_TRACEZ_INFO(ME, "Policy[%s] set in[%s] out[%s] target accept", OBJECT_NAMED, in, out);
        fw_rule_set_target_policy(s, FW_RULE_POLICY_ACCEPT);
    }

exit:
    return;
}

static bool activate_policy_chain(bool ipv4, policy_t* policy, bool* skip_reverse_chain) {
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? policy->common.folders.rule_folders.folder : policy->common.folders.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = (ipv4 ? policy->common.folders.log_folders.folder : policy->common.folders.log_folders.folder6);
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&policy->common.folders.log) ? 0 : 1;
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* src_fwiface = ipv4 ? &policy->common.src_fwiface : &policy->common.src6_fwiface;
    firewall_interface_t* dest_fwiface = ipv4 ? &policy->common.dest_fwiface : &policy->common.dest6_fwiface;
    amxd_object_t* object = policy->common.object;
    const char* name = NULL;
    const char* policy_chain = fw_get_chain(ipv4 ? "policy" : "policy6", ipv4 ? "FORWARD_Firewall" : "FORWARD6_Firewall");
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    *skip_reverse_chain = false;

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if((folders[i] == log_folder) && (policy->target == TARGET_CHAIN)) {
            continue;
        }
#endif //FIREWALL_LOGS

        when_failed_trace(fw_folder_set_feature(folders[i], FW_FEATURE_POLICY), exit, ERROR, "Failed to set FEATURE_POLICY");

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, policy_chain);
        fw_rule_set_ipv4(r, ipv4);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &policy->common.folders.log);
        }
#endif //FIREWALL_LOGS
        fw_folder_push_rule(folders[i], r);

        if(policy->chain != NULL) {
            name = chain_get_name(policy->chain->object);
        }

        if(src_fwiface->netdev_required) {
            amxc_llist_for_each(src_iter, &src_fwiface->linifaces) {
                linux_interface_t* src_liniface = amxc_container_of(src_iter, linux_interface_t, it);
                if(STRING_EMPTY(src_liniface->netdevname)) {
                    continue;
                }

                amxc_llist_for_each(dest_iter, &dest_fwiface->linifaces) {
                    linux_interface_t* dest_liniface = amxc_container_of(dest_iter, linux_interface_t, it);
                    fw_rule_t* s = NULL;

                    if(STRING_EMPTY(dest_liniface->netdevname)) {
                        continue;
                    }

                    if(strncmp(src_liniface->netdevname, dest_liniface->netdevname, INTERFACE_LEN - 1) == 0) {
                        SAH_TRACEZ_INFO(ME, "Policy[%s] discards src[%s] dst[%s]", OBJECT_NAMED, src_liniface->netdevname, dest_liniface->netdevname);
                        continue;
                    }

                    SAH_TRACEZ_INFO(ME, "Policy[%s] uses src[%s] dst[%s]", OBJECT_NAMED, src_liniface->netdevname, dest_liniface->netdevname);

                    if(folders[i] == log_folder) {
                        s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                        when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                        if(!STRING_EMPTY(src_liniface->netdevname)) {
                            fw_rule_set_in_interface(s, src_liniface->netdevname);
                        }
                        if(!STRING_EMPTY(dest_liniface->netdevname)) {
                            fw_rule_set_out_interface(s, dest_liniface->netdevname);
                        }
                        SAH_TRACEZ_INFO(ME, "Policy[%s] LOG was added for policy.", OBJECT_NAMED);
                    } else {
                        s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                        when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                        policy_set_target(s, policy->target, name, ipv4, src_liniface->netdevname, dest_liniface->netdevname, policy->common.object);
                    }

                    fw_folder_push_rule(folders[i], s);
                }
            }
        } else { // in case Firewall.Config!=Policy a policy without interfaces is used to jump to the Firewall.Level.{i}.Chain
            if(folders[i] == folder) {
                fw_rule_t* s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                policy_set_target(s, policy->target, name, ipv4, "", "", policy->common.object);
                fw_folder_push_rule(folders[i], s);
            }
            *skip_reverse_chain = true;
        }
        when_failed_trace(fw_folder_set_enabled(folders[i], true), exit, ERROR, "Failed to enable Policy[%s] %s %schain.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
        SAH_TRACEZ_INFO(ME, "Policy[%s] %s %schain was succesfully enabled.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            log_set_enable(&policy->common.folders.log, true);
        }
#endif //FIREWALL_LOGS
    }
    res = true;
exit:
    return res;
}

static bool activate_default_policy_chain(bool ipv4, policy_t* policy, bool skip_reverse_chain) {
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? policy->default_policy.rule_folders.folder : policy->default_policy.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? policy->default_policy.log_folders.folder : policy->default_policy.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&policy->common.folders.log) ? 0 : 1;
    log_set_enable(&policy->default_policy.log, false);
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* src_fwiface = ipv4 ? &policy->common.src_fwiface : &policy->common.src6_fwiface;
    firewall_interface_t* dest_fwiface = ipv4 ? &policy->common.dest_fwiface : &policy->common.dest6_fwiface;
    amxd_object_t* object = policy->common.object;
    const char* name = NULL;
    const char* policy_chain = fw_get_chain(ipv4 ? "policy" : "policy6", ipv4 ? "FORWARD_Firewall" : "FORWARD6_Firewall");
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_true_status(skip_reverse_chain, exit, res = true);
    when_null_status(policy->chain, exit, res = true);
    when_true_status(policy->chain->target == TARGET_NONE, exit, res = true);

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if((folders[i] == log_folder) && (policy->chain->target == TARGET_CHAIN)) {
            continue;
        }
#endif //FIREWALL_LOGS

        when_failed_trace(fw_folder_set_feature(folders[i], FW_FEATURE_POLICY), exit, ERROR, "Failed to set FEATURE_POLICY");

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, policy_chain);
        fw_rule_set_ipv4(r, ipv4);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &policy->common.folders.log);
        }
#endif //FIREWALL_LOGS
        fw_folder_push_rule(folders[i], r);

        if(policy->chain != NULL) {
            name = chain_get_name(policy->chain->object);
        }

        if(src_fwiface->netdev_required) {
            amxc_llist_for_each(src_iter, &src_fwiface->linifaces) {
                linux_interface_t* src_liniface = amxc_container_of(src_iter, linux_interface_t, it);
                if(STRING_EMPTY(src_liniface->netdevname)) {
                    continue;
                }

                amxc_llist_for_each(dest_iter, &dest_fwiface->linifaces) {
                    linux_interface_t* dest_liniface = amxc_container_of(dest_iter, linux_interface_t, it);
                    fw_rule_t* s = NULL;

                    if(STRING_EMPTY(dest_liniface->netdevname)) {
                        continue;
                    }

                    if(strncmp(src_liniface->netdevname, dest_liniface->netdevname, INTERFACE_LEN - 1) == 0) {
                        SAH_TRACEZ_INFO(ME, "Policy[%s] default policy discards src[%s] dst[%s]", OBJECT_NAMED, src_liniface->netdevname, dest_liniface->netdevname);
                        continue;
                    }

                    SAH_TRACEZ_INFO(ME, "Policy[%s] default policy uses src[%s] dst[%s]", OBJECT_NAMED, src_liniface->netdevname, dest_liniface->netdevname);

                    //Add log
                    if(folders[i] == log_folder) {
                        s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                        when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                        if(!STRING_EMPTY(src_liniface->netdevname)) {
                            fw_rule_set_in_interface(s, src_liniface->netdevname);
                        }
                        if(!STRING_EMPTY(dest_liniface->netdevname)) {
                            fw_rule_set_out_interface(s, dest_liniface->netdevname);
                        }
                        SAH_TRACEZ_INFO(ME, "Policy[%s] LOG was added for default policy.", OBJECT_NAMED);
                    } else {
                        s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                        when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                        SAH_TRACEZ_INFO(ME, "Policy[%s] use default chain[%s]", OBJECT_NAMED, name);
                        policy_set_target(s, policy->chain->target, name, ipv4, src_liniface->netdevname, dest_liniface->netdevname, policy->common.object);
                    }
                    fw_folder_push_rule(folders[i], s);
                }
            }
        }
        when_failed_trace(fw_folder_set_enabled(folders[i], true), exit, ERROR, "Failed to enable Policy[%s] %s %sdefault policy.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            log_set_enable(&policy->default_policy.log, true);
        }
#endif //FIREWALL_LOGS
        SAH_TRACEZ_INFO(ME, "Policy[%s] %s %sdefault policy was succesfully enabled.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
    }
    res = true;
exit:
    return res;
}

static bool activate_reverse_chain(bool ipv4, policy_t* policy, bool skip_reverse_chain) {
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? policy->reverse.rule_folders.folder : policy->reverse.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? policy->reverse.log_folders.folder : policy->reverse.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&policy->reverse.log) ? 0 : 1;
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* src_fwiface = ipv4 ? &policy->common.src_fwiface : &policy->common.src6_fwiface;
    firewall_interface_t* dest_fwiface = ipv4 ? &policy->common.dest_fwiface : &policy->common.dest6_fwiface;
    amxd_object_t* object = policy->common.object;
    const char* rname = NULL;
    const char* policy_chain = fw_get_chain(ipv4 ? "policy" : "policy6", ipv4 ? "FORWARD_Firewall" : "FORWARD6_Firewall");
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_true_status(skip_reverse_chain, exit, res = true);

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if((folders[i] == log_folder) && (policy->target_reverse == TARGET_CHAIN)) {
            continue;
        }
#endif //FIREWALL_LOGS

        when_failed_trace(fw_folder_set_feature(folders[i], FW_FEATURE_POLICY), exit, ERROR, "Failed to set FEATURE_POLICY");

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, policy_chain);
        fw_rule_set_ipv4(r, ipv4);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &policy->reverse.log);
        }
#endif //FIREWALL_LOGS
        fw_folder_push_rule(folders[i], r);

        //for the reverse chain, switch in and out interface.
        if(policy->chain_reverse != NULL) {
            rname = chain_get_name(policy->chain_reverse->object);
        }

        amxc_llist_for_each(src_iter, &src_fwiface->linifaces) {
            linux_interface_t* src_liniface = amxc_container_of(src_iter, linux_interface_t, it);
            if(STRING_EMPTY(src_liniface->netdevname)) {
                continue;
            }

            amxc_llist_for_each(dest_iter, &dest_fwiface->linifaces) {
                linux_interface_t* dest_liniface = amxc_container_of(dest_iter, linux_interface_t, it);
                fw_rule_t* s = NULL;

                if(STRING_EMPTY(dest_liniface->netdevname)) {
                    continue;
                }

                if(strncmp(src_liniface->netdevname, dest_liniface->netdevname, INTERFACE_LEN - 1) == 0) {
                    SAH_TRACEZ_INFO(ME, "Policy[%s] reverse discards src[%s] dst[%s]", OBJECT_NAMED, src_liniface->netdevname, dest_liniface->netdevname);
                    goto exit;
                }

                SAH_TRACEZ_INFO(ME, "Policy[%s] reverse uses src[%s] dst[%s]", OBJECT_NAMED, dest_liniface->netdevname, src_liniface->netdevname);

                if(folders[i] == log_folder) {
                    s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                    when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                    if(!STRING_EMPTY(dest_liniface->netdevname)) {
                        fw_rule_set_in_interface(s, dest_liniface->netdevname);
                    }
                    if(!STRING_EMPTY(src_liniface->netdevname)) {
                        fw_rule_set_out_interface(s, src_liniface->netdevname);
                    }
                    SAH_TRACEZ_INFO(ME, "Policy[%s] LOG was added for reverse chain policy.", OBJECT_NAMED);
                } else {
                    s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                    when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                    policy_set_target(s, policy->target_reverse, rname, ipv4, dest_liniface->netdevname, src_liniface->netdevname, policy->common.object);
                }

                fw_folder_push_rule(folders[i], s);
            }
        }
        when_failed_trace(fw_folder_set_enabled(folders[i], true), exit, ERROR, "Failed to enable Policy[%s] %s %sreverse chain.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
        SAH_TRACEZ_INFO(ME, "Policy[%s] %s %sreverse chain was succesfully enabled.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            log_set_enable(&policy->reverse.log, true);
        }
#endif //FIREWALL_LOGS
    }
    res = true;
exit:
    return res;
}

static bool activate_reverse_default_policy(bool ipv4, policy_t* policy, bool skip_reverse_chain) {
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? policy->reverse_default_policy.rule_folders.folder : policy->reverse_default_policy.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? policy->reverse_default_policy.log_folders.folder : policy->reverse_default_policy.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&policy->reverse.log) ? 0 : 1;
    log_set_enable(&policy->reverse_default_policy.log, false);
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* src_fwiface = ipv4 ? &policy->common.src_fwiface : &policy->common.src6_fwiface;
    firewall_interface_t* dest_fwiface = ipv4 ? &policy->common.dest_fwiface : &policy->common.dest6_fwiface;
    amxd_object_t* object = policy->common.object;
    const char* rname = NULL;
    const char* policy_chain = fw_get_chain(ipv4 ? "policy" : "policy6", ipv4 ? "FORWARD_Firewall" : "FORWARD6_Firewall");
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_true_status(skip_reverse_chain, exit, res = true);
    when_null_status(policy->chain_reverse, exit, res = true);
    when_true_status(policy->chain_reverse->target == TARGET_NONE, exit, res = true);

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if((folders[i] == log_folder) && (policy->chain_reverse->target == TARGET_CHAIN)) {
            continue;
        }
#endif //FIREWALL_LOGS

        when_failed_trace(fw_folder_set_feature(folders[i], FW_FEATURE_POLICY), exit, ERROR, "Failed to set FEATURE_POLICY");

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, policy_chain);
        fw_rule_set_ipv4(r, ipv4);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &policy->reverse.log);
        }
#endif //FIREWALL_LOGS
        fw_folder_push_rule(folders[i], r);

        //for the reverse chain, switch in and out interface.
        if(policy->chain_reverse != NULL) {
            rname = chain_get_name(policy->chain_reverse->object);
        }

        amxc_llist_for_each(src_iter, &src_fwiface->linifaces) {
            linux_interface_t* src_liniface = amxc_container_of(src_iter, linux_interface_t, it);
            if(STRING_EMPTY(src_liniface->netdevname)) {
                continue;
            }

            amxc_llist_for_each(dest_iter, &dest_fwiface->linifaces) {
                linux_interface_t* dest_liniface = amxc_container_of(dest_iter, linux_interface_t, it);
                fw_rule_t* s = NULL;

                if(STRING_EMPTY(dest_liniface->netdevname)) {
                    continue;
                }

                if(strncmp(src_liniface->netdevname, dest_liniface->netdevname, INTERFACE_LEN - 1) == 0) {
                    SAH_TRACEZ_INFO(ME, "Policy[%s] reverse default policy discards src[%s] dst[%s]", OBJECT_NAMED, src_liniface->netdevname, dest_liniface->netdevname);
                    goto exit;
                }

                SAH_TRACEZ_INFO(ME, "Policy[%s] reverse uses src[%s] dst[%s]", OBJECT_NAMED, dest_liniface->netdevname, src_liniface->netdevname);

                if(folders[i] == log_folder) {
                    s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                    when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                    if(!STRING_EMPTY(dest_liniface->netdevname)) {
                        fw_rule_set_in_interface(s, dest_liniface->netdevname);
                    }
                    if(!STRING_EMPTY(src_liniface->netdevname)) {
                        fw_rule_set_out_interface(s, src_liniface->netdevname);
                    }
                    SAH_TRACEZ_INFO(ME, "Policy[%s] LOG was added for reverse chain's default policy.", OBJECT_NAMED);
                } else {
                    s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_POLICY);
                    when_null_trace(s, exit, ERROR, "Policy[%s] failed to fetch rule", OBJECT_NAMED);

                    SAH_TRACEZ_INFO(ME, "Policy[%s] reverse uses default chain[%s]", OBJECT_NAMED, rname);
                    policy_set_target(s, policy->chain_reverse->target, rname, ipv4, dest_liniface->netdevname, src_liniface->netdevname, policy->common.object);
                }

                fw_folder_push_rule(folders[i], s);
            }
        }
        when_failed_trace(fw_folder_set_enabled(folders[i], true), exit, ERROR, "Failed to enable Policy[%s] %s %sreverse default policy was succesfully enabled.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
        SAH_TRACEZ_INFO(ME, "Policy[%s] %s %sreverse default policy was succesfully enabled.", ipv4 ? "v4" : "v6", OBJECT_NAMED, folders[i] == log_folder ? "LOG " : "");
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            log_set_enable(&policy->reverse_default_policy.log, true);
        }
#endif //FIREWALL_LOGS
    }
    res = true;
exit:
    return res;
}

static bool activate_policy(bool ipv4, policy_t* policy) {
    firewall_interface_t* src_fwiface = ipv4 ? &policy->common.src_fwiface : &policy->common.src6_fwiface;
    firewall_interface_t* dest_fwiface = ipv4 ? &policy->common.dest_fwiface : &policy->common.dest6_fwiface;
    amxd_object_t* object = policy->common.object;
    const char* src_netmodel_interface = amxc_string_get(&src_fwiface->netmodel_interface, 0);
    const char* dest_netmodel_interface = amxc_string_get(&dest_fwiface->netmodel_interface, 0);
    bool res = false;
    bool waiting = false;
    bool skip_reverse_chain = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    waiting = src_fwiface->netdev_required && STRING_EMPTY(src_fwiface->default_netdevname);
    when_true_trace(waiting, exit, NOTICE, "Policy[%s] waiting for interface[%s]", OBJECT_NAMED, src_netmodel_interface);
    waiting = dest_fwiface->netdev_required && STRING_EMPTY(dest_fwiface->default_netdevname);
    when_true_trace(waiting, exit, NOTICE, "Policy[%s] waiting for interface[%s]", OBJECT_NAMED, dest_netmodel_interface);

    when_false_trace(activate_policy_chain(ipv4, policy, &skip_reverse_chain), exit, ERROR, "Failed to activate the chain for Policy[%s]", OBJECT_NAMED);

    when_false_trace(activate_default_policy_chain(ipv4, policy, skip_reverse_chain), exit, ERROR, "Failed to activate the default policy's chain for Policy[%s]", OBJECT_NAMED);

    if(strcmp(src_netmodel_interface, dest_netmodel_interface) == 0) {
        skip_reverse_chain = true;
    }

    when_false_trace(activate_reverse_chain(ipv4, policy, skip_reverse_chain), exit, ERROR, "Failed to activate the reverse chain for Policy[%s]", OBJECT_NAMED);

    when_false_trace(activate_reverse_default_policy(ipv4, policy, skip_reverse_chain), exit, ERROR, "Failed to activate the default policy's reverse chain for Policy[%s]", OBJECT_NAMED);

    res = true;
exit:
    return res;
}

policy_t* create_firewall_policy(amxd_object_t* object) {
    policy_t* policy = NULL;

    SAH_TRACEZ_NOTICE(ME, "Policy[%s] is new", OBJECT_NAMED);

    policy = (policy_t*) calloc(1, sizeof(policy_t));
    when_null(policy, exit);

    common_init(&policy->common, object);
    folder_container_init(&policy->default_policy, &policy->common);
    folder_container_init(&policy->reverse, &policy->common);
    folder_container_init(&policy->reverse_default_policy, &policy->common);
    policy->status_ipv4 = FIREWALL_DISABLED;
    policy->status_ipv6 = FIREWALL_DISABLED;

    policy->common.src_fwiface.query_result_changed = netdevname_v4_changed;
    policy->common.src_fwiface.query_priv = policy;
    policy->common.dest_fwiface.query_result_changed = netdevname_v4_changed;
    policy->common.dest_fwiface.query_priv = policy;
    policy->common.src6_fwiface.query_result_changed = netdevname_v6_changed;
    policy->common.src6_fwiface.query_priv = policy;
    policy->common.dest6_fwiface.query_result_changed = netdevname_v6_changed;
    policy->common.dest6_fwiface.query_priv = policy;

exit:
    return policy;
}

static bool delete_firewall_policy(policy_t** policy) {
    when_null(*policy, exit);
    SAH_TRACEZ_INFO(ME, "Delete Firewall Policy");

    common_clean(&(*policy)->common);
    folder_container_clean(&(*policy)->default_policy);
    folder_container_clean(&(*policy)->reverse);
    folder_container_clean(&(*policy)->reverse_default_policy);

    free(*policy);
    *policy = NULL;
exit:
    return true;
}

typedef enum {
    POL_IPv4,
    POL_IPv6,
    POL_IPvAny,
} policy_metadata_t;

static bool deactivate_firewall_policy(policy_t* policy, policy_metadata_t metadata) {
    amxd_object_t* object = policy->common.object;
    ipversion_t ipversion = IPvAll;
    bool commit = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(metadata == POL_IPv4) {
        ipversion = IPv4;
    } else if(metadata == POL_IPv6) {
        ipversion = IPv6;
    } else {
        ipversion = IPvAll;
    }

    if(metadata != POL_IPv6) {
        bool policy_is_active = (policy->status_ipv4 == FIREWALL_ENABLED);

        if(policy_is_active) {
            SAH_TRACEZ_NOTICE(ME, "Policy[%s] v4 is about to be deactivated", OBJECT_NAMED);
            folder_container_delete_rules(&policy->common.folders, IPv4, false);
            folder_container_delete_rules(&policy->default_policy, IPv4, false);
            folder_container_delete_rules(&policy->reverse, IPv4, false);
            folder_container_delete_rules(&policy->reverse_default_policy, IPv4, false);
            commit = true;
            policy->status_ipv4 = FIREWALL_DISABLED;
        }
    }

    if(metadata != POL_IPv4) {
        bool policy_is_active = (policy->status_ipv6 == FIREWALL_ENABLED);

        if(policy_is_active) {
            SAH_TRACEZ_NOTICE(ME, "Policy[%s] v6 is about to be deactivated", OBJECT_NAMED);
            folder_container_delete_rules(&policy->common.folders, IPv6, false);
            folder_container_delete_rules(&policy->default_policy, IPv6, false);
            folder_container_delete_rules(&policy->reverse, IPv6, false);
            folder_container_delete_rules(&policy->reverse_default_policy, IPv6, false);
            commit = true;
            policy->status_ipv6 = FIREWALL_DISABLED;
        }
    }

    if(commit) {
        if(fw_commit(fw_rule_callback) != 0) {
            SAH_TRACEZ_ERROR(ME, "Policy[%s] fw_commit failed", OBJECT_NAMED);
        }

        folder_container_disable_folders(&policy->common.folders, ipversion);
        folder_container_disable_folders(&policy->default_policy, ipversion);
        folder_container_disable_folders(&policy->reverse, ipversion);
        folder_container_disable_folders(&policy->reverse_default_policy, ipversion);
    }

    if((((policy->common.ipversion != IPv4) && (policy->common.ipversion != IPv6)) && (policy->status_ipv4 == FIREWALL_DISABLED) && (policy->status_ipv6 == FIREWALL_DISABLED)) ||
       ((policy->common.ipversion == IPv4) && (policy->status_ipv4 == FIREWALL_DISABLED)) ||
       ((policy->common.ipversion == IPv6) && (policy->status_ipv6 == FIREWALL_DISABLED))) {
        common_set_status(&policy->common, FIREWALL_DISABLED);
    }

    return true;
}

/*
 * Commit policy firewall rules
 * return true if Success, false when iptables rules Failed
 */
static bool commit_policy_rules(policy_t* policy, ipversion_t ipversion) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = false;
    status_t status = FIREWALL_ERROR;
    folder_container_t* folders_array[] = {NULL, NULL, NULL, NULL};

    when_null(policy, out);
    folders_array[0] = &policy->common.folders;
    folders_array[1] = &policy->default_policy;
    folders_array[2] = &policy->reverse;
    folders_array[3] = &policy->reverse_default_policy;

    reorder_array_folder_containers(folders_array, sizeof(folders_array) / sizeof(folders_array[0]));

    if(fw_commit(fw_rule_callback) == 0) {
        SAH_TRACEZ_INFO(ME, "Commit rule: Success");
        status = FIREWALL_ENABLED;
    } else {
        SAH_TRACEZ_ERROR(ME, "Commit rule: Error");
        status = FIREWALL_ERROR;

        folder_container_delete_rules(&policy->common.folders, ipversion, false);
        folder_container_delete_rules(&policy->default_policy, ipversion, false);
        folder_container_delete_rules(&policy->reverse, ipversion, false);
        folder_container_delete_rules(&policy->reverse_default_policy, ipversion, false);

        fw_commit(fw_rule_callback);

        folder_container_disable_folders(&policy->common.folders, ipversion);
        folder_container_disable_folders(&policy->default_policy, ipversion);
        folder_container_disable_folders(&policy->reverse, ipversion);
        folder_container_disable_folders(&policy->reverse_default_policy, ipversion);
        goto out;
    }

    res = true;

out:
    if(ipversion != IPv6) {
        policy->status_ipv4 = status;
    }
    if(ipversion != IPv4) {
        policy->status_ipv6 = status;
    }
    FW_PROFILING_OUT();
    return res;
}

static bool activate_firewall_policy(policy_t* policy, policy_metadata_t metadata) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = true;
    bool commit = false;
    amxd_object_t* object = NULL;
    (void) object; // when tracing is disabled, variable is not used anymore

    object = policy->common.object;

    if(policy->common.enable == false) {
        SAH_TRACEZ_INFO(ME, "Policy[%s] is not enabled", OBJECT_NAMED);
        FW_PROFILING_OUT();
        return true;
    }

    if((metadata != POL_IPv6) && (policy->common.ipversion != IPv6)) {
        if(policy->status_ipv4 != FIREWALL_ENABLED) {
            SAH_TRACEZ_INFO(ME, "Policy[%s] apply IPV4", OBJECT_NAMED);
            res = activate_policy(true, policy);
            if(!res) {
                policy->status_ipv4 = FIREWALL_ERROR;
                folder_container_delete_rules(&policy->common.folders, IPv4, false);
                folder_container_delete_rules(&policy->default_policy, IPv4, false);
                folder_container_delete_rules(&policy->reverse, IPv4, false);
                folder_container_delete_rules(&policy->reverse_default_policy, IPv4, false);
                SAH_TRACEZ_ERROR(ME, "Policy[%s] v4 failed to activate", OBJECT_NAMED);
            } else {
                SAH_TRACEZ_INFO(ME, "Policy[%s] v4 succesfully activated", OBJECT_NAMED);
                policy->status_ipv4 = FIREWALL_ENABLED;
            }
            commit = true;
        } else {
            SAH_TRACEZ_INFO(ME, "Policy[%s] v4 already active", OBJECT_NAMED);
        }
    }

    if((metadata != POL_IPv4) && (policy->common.ipversion != IPv4)) {
        if(policy->status_ipv6 != FIREWALL_ENABLED) {
            SAH_TRACEZ_INFO(ME, "Policy[%s] apply IPV6", OBJECT_NAMED);
            res = activate_policy(false, policy);
            if(!res) {
                policy->status_ipv6 = FIREWALL_ERROR;
                folder_container_delete_rules(&policy->common.folders, IPv6, false);
                folder_container_delete_rules(&policy->default_policy, IPv6, false);
                folder_container_delete_rules(&policy->reverse, IPv6, false);
                folder_container_delete_rules(&policy->reverse_default_policy, IPv6, false);
                SAH_TRACEZ_ERROR(ME, "Policy[%s] v4 failed to activate", OBJECT_NAMED);
            } else {
                SAH_TRACEZ_INFO(ME, "Policy[%s] v6 succesfully activated", OBJECT_NAMED);
                policy->status_ipv6 = FIREWALL_ENABLED;
            }
            commit = true;
        } else {
            SAH_TRACEZ_INFO(ME, "Policy[%s] v6 already active", OBJECT_NAMED);
        }
    }

    if(commit) {
        if(!commit_policy_rules(policy, policy->common.ipversion)) {
            SAH_TRACEZ_ERROR(ME, "Policy[%s] fw_commit failed", OBJECT_NAMED);
        }
    }

    if((policy->status_ipv4 == FIREWALL_ERROR) ||
       (policy->status_ipv6 == FIREWALL_ERROR)) {
        common_set_status(&policy->common, FIREWALL_ERROR);
    } else if((((policy->common.ipversion != IPv4) && (policy->common.ipversion != IPv6)) && (policy->status_ipv4 == FIREWALL_ENABLED) && (policy->status_ipv6 == FIREWALL_ENABLED)) ||
              ((policy->common.ipversion == IPv4) && (policy->status_ipv4 == FIREWALL_ENABLED)) ||
              ((policy->common.ipversion == IPv6) && (policy->status_ipv6 == FIREWALL_ENABLED))) {
        common_set_status(&policy->common, FIREWALL_ENABLED);
    }

    FW_PROFILING_OUT();
    return res;
}

// assumes policy_t* is never NULL
bool policy_activate(policy_t* policy) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_true(policy->common.flags == 0, exit);
    deactivate_firewall_policy(policy, POL_IPvAny);
    activate_firewall_policy(policy, POL_IPvAny);
    fw_apply();
    policy->common.flags = 0;
exit:
    FW_PROFILING_OUT();
    return true;
}

bool policy_activate_object(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    extendedpolicy_t* priv_data = NULL;
    policy_t* policy = NULL;
    priv_data = object->priv;
    when_null(priv_data, exit);
    policy = priv_data->policy;
    if(policy != NULL) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        update_policy_logs(policy, NULL, NULL, NULL);
#endif //FIREWALL_LOGS
        deactivate_firewall_policy(policy, POL_IPvAny);
        activate_firewall_policy(policy, POL_IPvAny);
        fw_apply();
        policy->common.flags = 0;
        return true;
    }

exit:
    FW_PROFILING_OUT();
    return false;
}

// assumes policy_t* is never NULL
bool policy_deactivate(policy_t* policy) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    deactivate_firewall_policy(policy, POL_IPvAny);
    fw_apply();
    policy->common.flags = 0;
    FW_PROFILING_OUT();
    return true;
}

bool policy_deactivate_object(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    extendedpolicy_t* priv_data = NULL;
    policy_t* policy = NULL;
    priv_data = object->priv;
    when_null(priv_data, exit);
    policy = priv_data->policy;
    if(policy != NULL) {
        deactivate_firewall_policy(policy, POL_IPvAny);
        fw_apply();
        policy->common.flags = 0;
        return true;
    }

exit:
    FW_PROFILING_OUT();
    return false;
}

// assumes firewall_interface_t* and query_priv are never NULL
static void netdevname_v4_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    policy_t* policy = (policy_t*) fwiface->query_priv;

    when_true(policy->common.flags != 0, exit); // already being configured
    policy->common.flags = FW_MODIFIED;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_policy_logs(policy, NULL, NULL, NULL);
#endif //FIREWALL_LOGS
    deactivate_firewall_policy(policy, POL_IPv4);
    activate_firewall_policy(policy, POL_IPv4);

    fw_apply();
    policy->common.flags = 0;

exit:
    FW_PROFILING_OUT();
}

// assumes firewall_interface_t* and query_priv are never NULL
static void netdevname_v6_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    policy_t* policy = (policy_t*) fwiface->query_priv;

    when_true(policy->common.flags != 0, exit); // already being configured
    policy->common.flags = FW_MODIFIED;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_policy_logs(policy, NULL, NULL, NULL);
#endif //FIREWALL_LOGS
    deactivate_firewall_policy(policy, POL_IPv6);
    activate_firewall_policy(policy, POL_IPv6);

    fw_apply();
    policy->common.flags = 0;

exit:
    FW_PROFILING_OUT();
}

void policy_free(policy_t* policy) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    deactivate_firewall_policy(policy, POL_IPvAny);
    delete_firewall_policy(&policy);
    fw_apply();
    FW_PROFILING_OUT();
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
target_t get_policy_log_target(policy_t* policy, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    target_t target = TARGET_NONE;
    chain_t* chain = NULL;

    when_null(policy, exit);

    target = reverse ? policy->target_reverse : policy->target;
    when_false(target == TARGET_CHAIN, exit);
    chain = reverse ? policy->chain_reverse : policy->chain;
    when_null(chain, exit);
    target = chain->target;

exit:
    FW_PROFILING_OUT();
    return target;
}

bool update_policy_logs(policy_t* p_policy, amxd_object_t* object, const amxc_var_t* const data_rule, const amxc_var_t* const data_log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool update = false;
    policy_t* policy = NULL;

    if(p_policy == NULL) {
        when_null(object, exit);
        policy = (policy_t*) object->priv;
        when_null(policy, exit);
    } else {
        policy = p_policy;
    }

    when_null(policy, exit);

    log_set_policy(&policy->common.folders.log, get_policy_log_target(policy, false));
    log_set_policy(&policy->reverse.log, get_policy_log_target(policy, true));

    update |= log_update(&policy->common.folders.log,
                         log_get_dm_enable_param(policy->common.object, data_rule, NULL),
                         log_get_dm_controller_param(policy->common.object, data_rule, NULL),
                         !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                         data_log);

    update |= log_update(&policy->reverse.log,
                         log_get_dm_enable_param(policy->common.object, data_rule, NULL),
                         log_get_dm_controller_param(policy->common.object, data_rule, NULL),
                         !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                         data_log);
exit:
    FW_PROFILING_OUT();
    return update;
}
#endif //FIREWALL_LOGS