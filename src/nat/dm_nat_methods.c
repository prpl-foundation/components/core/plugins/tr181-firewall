/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "nat/dm_nat.h"
#include "rules.h"
#include "firewall_utilities.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>

#define ME "portmapping"

static amxd_status_t update_port_mapping(amxd_object_t* portmapping,
                                         amxc_var_t* lease_duration,
                                         amxc_var_t* internal_port,
                                         amxc_var_t* description) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, portmapping);
    if(lease_duration) {
        amxd_trans_set_uint32_t(&trans, "LeaseDuration", GET_UINT32(lease_duration, NULL));
    }
    if(internal_port) {
        amxd_trans_set_uint32_t(&trans, "InternalPort", GET_UINT32(internal_port, NULL));
    }
    if(description) {
        amxd_trans_set_cstring_t(&trans, "Description", GET_CHAR(description, NULL));
    }
    status = amxd_trans_apply(&trans, fw_get_dm());
    amxd_trans_clean(&trans);
    return status;
}

amxd_status_t _updatePortMapping(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* portmapping = NULL;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    when_null_trace(GET_ARG(args, "protocol"), exit, ERROR, "protocol argument is missing");
    when_null_trace(GET_ARG(args, "externalPort"), exit, ERROR, "externalPort argument is missing");
    when_null_trace(GET_ARG(args, "origin"), exit, ERROR, "origin argument is missing");
    portmapping = amxd_dm_findf(fw_get_dm(), "NAT.PortMapping.[Protocol==\"%s\" && ExternalPort==\"%d\" && Origin==\"%s\"].", GET_CHAR(args, "protocol"), GET_UINT32(args, "externalPort"), GET_CHAR(args, "origin"));
    when_null(portmapping, exit);
    if(GET_ARG(args, "leaseDuration") && (GET_UINT32(amxd_object_get_param_value(portmapping, "LeaseDuration"), NULL) == GET_UINT32(args, "leaseDuration"))) {
        rule_t* rule = portmapping->priv;

        status = amxp_timer_start(rule->lease_timer, rule->lease_duration * 1000);
        if(GET_ARG(args, "internalPort") || GET_ARG(args, "description")) {
            status = update_port_mapping(portmapping, GET_ARG(args, "leaseDuration"), GET_ARG(args, "internalPort"), GET_ARG(args, "description"));
        }
    } else {
        status = update_port_mapping(portmapping, GET_ARG(args, "leaseDuration"), GET_ARG(args, "internalPort"), GET_ARG(args, "description"));
    }

exit:
    FW_PROFILING_OUT();
    return status;
}