/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_policy.h"

static amxd_dm_t* dm = NULL;

#define POLICY_NOT_FOUND 2

int test_policy_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int test_policy_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

static void a_populate_forward_rule(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(r, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_forward_rule(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(r, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_chain(r, "FORWARD_Medium"), 0);
}

static void a_populate_reverse_rule(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(r, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(r, "eth0"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_reverse_rule(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(r, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(r, "eth0"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_chain(r, "FORWARD_Medium"), 0);
}

static void a_populate_forward6_rule(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(r, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv6(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void a_populate_reverse6_rule(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(r, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(r, "eth0"), 0);
    assert_int_equal(fw_rule_set_ipv6(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

void test_policy_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* policy = NULL;

    expect_query_lan_netdevnames(); // for ipv4
    expect_query_lan_netdevnames(); // for ipv6
    expect_query_wan_netdevnames(); // for ipv4
    expect_query_wan_netdevnames(); // for ipv6

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "Firewall.Policy.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "TargetChain", "Accept");        // for legacy reasons
    amxd_trans_set_value(cstring_t, &trans, "ReverseTargetChain", "Accept"); // for legacy reasons
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    print_message("precondition: Status should be Disabled\n");
    policy = amxd_dm_findf(dm, "Firewall.Policy.1");
    assert_non_null(policy);
    assert_int_equal(amxd_object_get_params(policy, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_BOOL(&params, "Enable"), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, policy);
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.2.");      // WAN
    amxd_trans_set_value(cstring_t, &trans, "DestinationInterface", "Device.IP.Interface.3."); // LAN
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_forward_rule, 1);
    expect_fw_replace_rule(a_populate_reverse_rule, 2);
    expect_fw_replace_rule(a_populate_forward6_rule, 1);
    expect_fw_replace_rule(a_populate_reverse6_rule, 2);

    handle_events();

    assert_int_equal(amxd_object_get_params(policy, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, policy);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_forward_rule, 1);
    expect_fw_delete_rule(a_populate_reverse_rule, 1);
    expect_fw_delete_rule(a_populate_forward6_rule, 1);
    expect_fw_delete_rule(a_populate_reverse6_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(policy, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_policy_with_target_chain(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* policy = NULL;
    const char* target_chain = "Firewall.Chain.Medium";

    expect_query_lan_netdevnames(); // for ipv4
    expect_query_wan_netdevnames(); // for ipv4

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("precondition #1: the chain must exist\n");
    amxd_trans_select_pathf(&trans, "Firewall.Chain.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Alias", "Medium");
    amxd_trans_set_value(cstring_t, &trans, "Name", "Medium");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    policy = amxd_dm_findf(dm, "Firewall.Policy.1");

    print_message("precondition #2: status == Disabled\n");
    assert_non_null(policy);
    assert_int_equal(amxd_object_get_params(policy, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_BOOL(&params, "Enable"), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("enable policy with target chain\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, policy);
    amxd_trans_set_value(cstring_t, &trans, "TargetChain", "Chain");
    amxd_trans_set_value(cstring_t, &trans, "ReverseTargetChain", "Chain");
    amxd_trans_set_value(cstring_t, &trans, "Chain", target_chain);
    amxd_trans_set_value(cstring_t, &trans, "ReverseChain", target_chain);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(b_populate_forward_rule, 1);
    expect_fw_replace_rule(b_populate_reverse_rule, 2);

    handle_events();

    assert_int_equal(amxd_object_get_params(policy, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("cleanup\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, policy);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(b_populate_forward_rule, 1);
    expect_fw_delete_rule(b_populate_reverse_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(policy, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_method_setPolicy(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    expect_query_lan_netdevnames(); // for ipv4
    expect_query_wan_netdevnames(); // for ipv6

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("if no instance exists with alias, a new instance should be added\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    amxc_var_add_key(cstring_t, &args, "sourceInterface", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, &args, "destinationInterface", "Device.IP.Interface.3.");

    assert_int_equal(amxd_object_invoke_function(template, "setPolicy", &args, &retval), 0);
    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_HTABLE);

    print_message("if an instance exists with alias, the instance should be modified\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    amxc_var_add_key(int32_t, &args, "ipversion", 4);

    assert_int_equal(amxd_object_invoke_function(template, "setPolicy", &args, &retval), 0);
    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_HTABLE);
    assert_int_equal(GET_INT32(&retval, "IPVersion"), 4);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_list_one_policy(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;
    int i = 0;
    struct test_data {
        const char* alias;
        int result;
    } td[] = {
        {"toberemoved", 0},
        {"doesNotExist", POLICY_NOT_FOUND},
        {}
    };

    amxc_var_init(&args);
    amxc_var_init(&retval);

    for(i = 0; td[i].alias; i++) {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "alias", td[i].alias);

        assert_int_equal(amxd_object_invoke_function(template, "getPolicy", &args, &retval), td[i].result);

        if(td[i].result == 0) {
            assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_LIST);
            assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &retval)), 1);
        }
    }

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_list_all_policies(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(template, "getPolicy", &args, &retval), 0);

    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_LIST);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &retval)) > 1), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_deletePolicy(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("delete an existing instance\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    assert_int_equal(amxd_object_invoke_function(template, "deletePolicy", &args, &retval), 0);

    print_message("delete of a non existing instance should be silently ignored\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    assert_int_equal(amxd_object_invoke_function(template, "deletePolicy", &args, &retval), 0);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}