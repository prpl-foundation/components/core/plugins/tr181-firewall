/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ipset/dm_ipset.h"
#include "ipset/ipset.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <ipat/ipat.h>
#include <string.h>

#define ME "ipset"

amxd_status_t _fw_match_type_set(amxd_object_t* object,
                                 UNUSED amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 UNUSED amxc_var_t* const retval,
                                 void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    const char* set_type = NULL;
    const char* rule_type = GET_CHAR((amxc_var_t*) priv, NULL);
    amxd_object_t* parent = NULL;

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_str_empty_status(GET_CHAR(args, NULL), exit, status = amxd_status_ok);

    parent = amxd_object_findf(object, "^.^.");
    set_type = object_da_string(parent, "Type");

    when_false_status(strcmp(set_type, rule_type) == 0, exit, status = amxd_status_invalid_value);

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _fw_match_ip_version(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* parent = NULL;
    int ip_version = 0;
    amxc_var_t csv;

    amxc_var_init(&csv);

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_str_empty_status(GET_CHAR(args, NULL), exit, status = amxd_status_ok);

    parent = amxd_object_findf(object, "^.^.");
    ip_version = object_get_ipversion(parent) == IPv4 ? AF_INET : AF_INET6;

    amxc_var_convert(&csv, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(ip, &csv) {
        when_false_status(ip_version == ipat_text_family(GET_CHAR(ip, NULL)), exit, status = amxd_status_invalid_value);
    }

    status = amxd_status_ok;

exit:
    amxc_var_clean(&csv);
    FW_PROFILING_OUT();
    return status;
}


static bool fw_is_valid_port(amxc_var_t* args) {
    uint32_t port = 0;
    bool rv = false;

    when_null(args, exit);
    port = amxc_var_dyncast(uint32_t, args);

    if((port > 0) && (port < 65536)) {
        rv = true;
    }

exit:
    return rv;
}

amxd_status_t _fw_is_valid_port_range(UNUSED amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t csv;
    amxc_var_t var_list;
    amxc_string_t port_range;
    amxc_var_t* port_min = NULL;
    amxc_var_t* port_max = NULL;
    amxc_var_t* separator = NULL;

    amxc_var_init(&csv);
    amxc_var_init(&var_list);
    amxc_string_init(&port_range, 0);

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_str_empty_status(GET_CHAR(args, NULL), exit, status = amxd_status_ok);

    amxc_var_convert(&csv, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(port_str, &csv) {
        amxc_string_set(&port_range, GET_CHAR(port_str, NULL));
        amxc_string_split(&port_range, &var_list, NULL, NULL);
        port_min = amxc_var_get_index(&var_list, 0, AMXC_VAR_FLAG_DEFAULT);
        separator = amxc_var_get_index(&var_list, 1, AMXC_VAR_FLAG_DEFAULT);
        port_max = amxc_var_get_index(&var_list, 2, AMXC_VAR_FLAG_DEFAULT);

        when_false_status(fw_is_valid_port(port_min), exit, status = amxd_status_invalid_value);

        if(separator != NULL) {
            when_false_status(strcmp("-", GET_CHAR(separator, NULL)) == 0, exit, status = amxd_status_invalid_value);
            when_false_status(fw_is_valid_port(port_max), exit, status = amxd_status_invalid_value);
            when_false_status(GET_UINT32(port_max, NULL) > GET_UINT32(port_min, NULL), exit, status = amxd_status_invalid_value);
        }
    }

    status = amxd_status_ok;

exit:
    amxc_var_clean(&csv);
    amxc_var_clean(&var_list);
    amxc_string_clean(&port_range);
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _fw_is_exclude_supported(amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    const char* set_type = NULL;
    amxd_object_t* parent = NULL;

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_false_status(GET_BOOL(args, NULL), exit, status = amxd_status_ok);

    parent = amxd_object_findf(object, "^.^.");
    set_type = object_da_string(parent, "Type");

    when_false_status(strcmp(set_type, "IPAddresses") == 0, exit, status = amxd_status_invalid_value);

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _fw_has_no_references(amxd_object_t* object,
                                    UNUSED amxd_param_t* param,
                                    amxd_action_t reason,
                                    UNUSED const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    set_t* set = NULL;

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);
    when_null_trace(object, exit, ERROR, "Object is NULL");

    set = (set_t*) object->priv;
    status = amxd_status_invalid_action;
    when_true_trace((set != NULL) && (set->refcount > 0), exit, ERROR, "Set is referenced by %d rules", set->refcount);

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}