/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/spoofing_protection.h"
#include "firewall.h" // for fw_get_chain
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <ipat/ipat.h>
#include <string.h>

#define ME "firewall"

#define SPOOFING_PROT_IPV4_STATUS "SpoofingProtectionIPv4Status"
#define SPOOFING_PROT_IPV6_STATUS "SpoofingProtectionIPv6Status"

/**
   @brief
   Delete/clean the Spoofing internal structure.

   @param[in] spoofing Spoofing internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_spoofing(spoofing_protection_t** spoofing) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*spoofing, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete SpoofingProtection");

    _netmodel_closeQuery(&(*spoofing)->query_addresses_v4);
    _netmodel_closeQuery(&(*spoofing)->query_addresses_v6);

    amxc_llist_it_take(&(*spoofing)->it);

    common_clean(&(*spoofing)->common);

    amxc_var_clean(&(*spoofing)->addresses_v4);
    amxc_var_clean(&(*spoofing)->addresses_v6);

    amxp_timer_delete(&(*spoofing)->timer);

    free(*spoofing);
    *spoofing = NULL;
exit:
    FW_PROFILING_OUT();
    return true;
}

static char* to_network(const char* ipaddress, int prefixlen) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_string_t buf;
    char* network = NULL;
    char* mask = NULL;

    amxc_string_init(&buf, 0);

    mask = ipat_text_mask_apply_direct(ipaddress, prefixlen, ipat_bits_upper, ipat_oper_and, false);
    when_null_trace(mask, exit, ERROR, "Failed to get netmask for [%s/%d]", ipaddress, prefixlen);
    amxc_string_setf(&buf, "%s/%d", mask, prefixlen);
    network = amxc_string_take_buffer(&buf);

exit:
    amxc_string_clean(&buf);
    free(mask);
    FW_PROFILING_OUT();
    return network;
}

/**
   @brief
   Add the rule accordding to the input.

   @param[in] network Network to be added to the rule.
   @param[in] folder Folder api structure.
   @param[in] accept Accept or Drop policy.
   @param[in] exclude_interface Add all the rules with excluded input interface.
   @param[in] exclude_address Add all the rules with excluded source address.
   @param[in] is_log_folder True if folder is related to the log

   @return
   True if success, false if failed.
 */
static bool add_rule_for_spoofing(const char* network, fw_folder_t* folder, fw_rule_policy_t policy,
                                  bool exclude_interface, bool exclude_address, bool is_log_folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* s = NULL;
    bool ret = false;

    fw_folder_set_feature(folder, FW_FEATURE_SPOOFING);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SPOOFING);
    when_null_trace(s, out, ERROR, "Failed to add Spoofing Protection rule");

    fw_rule_set_in_interface_excluded(s, exclude_interface);
    if(!is_log_folder) {
        fw_rule_set_target_policy(s, policy);
    }
    fw_rule_set_source(s, network);
    fw_rule_set_source_excluded(s, exclude_address);
    fw_folder_push_rule(folder, s);

    ret = true;
out:
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Add the rules according to the inputs.

   @param[in] htable_it Address hash table iterator.
   @param[in] ipv4 Whether is IPv4 or not.
   @param[in] folder Folder api structure.
   @param[in] accept Accept or Drop policy.
   @param[in] exclude_interface Add all the rules with excluded input interface.
   @param[in] exclude_address Add all the rules with excluded source address.
   @param[in] is_log_folder True if folder is related to the log

   @return
   True if success, false if failed.
 */
static bool add_rule_from_htable(amxc_htable_it_t* htable_it, bool ipv4, fw_folder_t* folder, fw_rule_policy_t policy,
                                 bool exclude_interface, bool exclude_address, bool is_log_folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const amxc_var_t* address_var = NULL;
    uint32_t prefixlen = 0;
    const char* family = NULL;
    const char* address = NULL;
    char* network = NULL;
    bool ret = false;

    address_var = amxc_htable_it_get_data(htable_it, amxc_var_t, hit);

    if((family = GET_CHAR(address_var, "Family")) == NULL) {
        family = "";
    }
    prefixlen = GET_UINT32(address_var, "PrefixLen");
    address = GET_CHAR(address_var, "Address");

    if(ipv4) {
        if(strcmp(family, "ipv4") != 0) {
            ret = true;
            goto out;
        }
    } else {
        if(strcmp(family, "ipv6") != 0) {
            ret = true;
            goto out;
        }
    }

    network = to_network(address, prefixlen);
    when_null(network, out);

    ret = add_rule_for_spoofing(network, folder, policy, exclude_interface, exclude_address, is_log_folder);
    free(network);
    network = NULL;
    when_false(ret, out);

    ret = true;
out:
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Iterate over the addresses and add the rules according to the inputs.

   @param[in] spoofing Spoofing internal structure.
   @param[in] ipv4 Whether is IPv4 or not.
   @param[in] folder Folder api structure.
   @param[in] is_log_folder True if folder is related to the log

   @return
   True if success, false if failed.
 */
static bool add_address_rules_for_spoofing(spoofing_protection_t* spoofing, bool ipv4, fw_folder_t* folder, bool is_log_folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const amxc_htable_t* ht_addresses = NULL;
    amxc_var_t* addresses = ipv4 ? &(spoofing->addresses_v4) : &(spoofing->addresses_v6);
    amxc_var_t* table = (ipv4) ? GET_ARG(addresses, "ipv4") : GET_ARG(addresses, "ipv6");
    amxc_htable_it_t* hit_first = NULL;
    const amxc_var_t* address_var = NULL;
    const char* scope = NULL;
    bool ret = false;

    when_null_status(table, out, ret = true);

    if(ipv4 && !is_log_folder) {
        ret = add_rule_for_spoofing("0.0.0.0/32", folder, FW_RULE_POLICY_ACCEPT, false, false, is_log_folder);
        when_false(ret, out);
    }

    ht_addresses = amxc_var_constcast(amxc_htable_t, table);
    amxc_htable_for_each(it_address, ht_addresses) {
        address_var = amxc_htable_it_get_data(it_address, amxc_var_t, hit);
        if(address_var == NULL) {
            continue;
        }
        scope = GET_CHAR(address_var, "Scope");
        if(STRING_EMPTY(scope)) {
            continue;
        }
        if(strcmp(scope, "global") != 0) {
            continue;
        }
        ret = add_rule_from_htable(it_address, ipv4, folder, FW_RULE_POLICY_DROP, true, false, is_log_folder);
        when_false(ret, out);
    }

    hit_first = amxc_htable_get_first(ht_addresses);
    for(amxc_htable_it_t* it = amxc_htable_get_last(ht_addresses); it != NULL; it = amxc_htable_it_get_previous(it)) {
        if(ipv4) {
            if(it == hit_first) {
                ret = add_rule_from_htable(it, ipv4, folder, FW_RULE_POLICY_DROP, false, true, is_log_folder);
            } else if(!is_log_folder) {
                ret = add_rule_from_htable(it, ipv4, folder, FW_RULE_POLICY_ACCEPT, false, false, is_log_folder);
            }
        } else if(!is_log_folder) {
            ret = add_rule_from_htable(it, ipv4, folder, FW_RULE_POLICY_ACCEPT, false, false, is_log_folder);
        }
        when_false(ret, out);
    }

    if(!ipv4) {
        ret = add_rule_for_spoofing("::/0", folder, FW_RULE_POLICY_DROP, false, false, is_log_folder);
        when_false(ret, out);
    }

    ret = true;
out:
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Activate the spoofing mode (Accept or Drop)

   @param[in] spoofing Spoofing internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_spoofing(spoofing_protection_t* spoofing, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = spoofing->common.object;
    fw_rule_t* r = NULL;
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folder = NULL;
    fw_folder_t* folders[] = {NULL, NULL};
    target_t target = TARGET_NONE;
    bool res = false;
    bool has_addresses = false;
    const char* chain = ipv4 ? fw_get_chain("spoofing", "PREROUTING_Spoofing") : fw_get_chain("spoofing6", "PREROUTING6_Spoofing");
    amxc_var_t* addresses = ipv4 ? &(spoofing->addresses_v4) : &(spoofing->addresses_v6);
    firewall_interface_t* fwiface = &spoofing->common.dest_fwiface;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "SpoofingProtection[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    has_addresses = (0 < GET_UINT32(GET_ARG(addresses, "NumberOfAddress"), NULL));
    when_false_trace(has_addresses, out, INFO, "SpoofingProtection[%s] failed to query the Addresses.", OBJECT_NAMED);

    log_folder = (ipv4) ? spoofing->common.folders.log_folders.folder : spoofing->common.folders.log_folders.folder6;
    folder = (ipv4) ? spoofing->common.folders.rule_folders.folder : spoofing->common.folders.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folders[0] = log_folder;
    folders[1] = folder;
    size_t loop_start = log_get_enable(&spoofing->common.folders.log) ? 0 : 1;
#else //FIREWALL_LOGS
    folders[0] = folder;
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    target = (ipv4) ? spoofing->target : spoofing->target6;

    when_true_status(target == TARGET_ACCEPT, out, res = true);

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        fw_folder_set_feature(folders[i], FW_FEATURE_SPOOFING);

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "SpoofingProtection[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_chain(r, chain);
        fw_rule_set_ipv4(r, ipv4);
        fw_rule_set_in_interface(r, fwiface->default_netdevname);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &spoofing->common.folders.log);
        }
#endif //FIREWALL_LOGS
        fw_rule_set_table(r, TABLE_RAW);
        fw_folder_push_rule(folders[i], r);

        res = add_address_rules_for_spoofing(spoofing, ipv4, folders[i], folders[i] == log_folder);
        when_false(res, out);

        fw_folder_set_enabled(folders[i], true);
    }
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

static void unfold_fw_setting_spoofing(spoofing_protection_t* spoofing, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(spoofing, out);

    folder_container_delete_rules(&spoofing->common.folders, ipv4 ? IPv4 : IPv6, true);

out:
    FW_PROFILING_OUT();
    return;
}

static bool deactivate_spoofing(spoofing_protection_t* spoofing, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = spoofing->common.object;
    status_t status = ipv4 ? spoofing->status : spoofing->status6;
    const cstring_t spoofing_status_key = ipv4 ? SPOOFING_PROT_IPV4_STATUS : SPOOFING_PROT_IPV6_STATUS;
    bool is_active = (status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "SpoofingProtection[%s] SpoofingProtect is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "SpoofingProtection[%s] deactivate SpoofingProtect", OBJECT_NAMED);
    /* do not commit yet, wait for the activate/delete to commit it. */
    unfold_fw_setting_spoofing(spoofing, ipv4);
    _set_status(spoofing->common.object, spoofing_status_key, FIREWALL_ERROR);
    if(ipv4) {
        spoofing->status = FIREWALL_DISABLED;
    } else {
        spoofing->status6 = FIREWALL_DISABLED;
    }

out:
    FW_PROFILING_OUT();
    return true;
}

static bool activate_spoofing(spoofing_protection_t* spoofing, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = spoofing->common.object;
    status_t status = ipv4 ? spoofing->status : spoofing->status6;
    target_t target = ipv4 ? spoofing->target : spoofing->target6;
    const cstring_t spoofing_status_key = ipv4 ? SPOOFING_PROT_IPV4_STATUS : SPOOFING_PROT_IPV6_STATUS;
    bool is_active = (status == FIREWALL_ENABLED);
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(is_active) {
        res = true;
        SAH_TRACEZ_NOTICE(ME, "SpoofingProtection[%s] SpoofingProtection already active", OBJECT_NAMED);
        goto out;
    }

    res = ifsetting_activate_spoofing(spoofing, ipv4);
    when_false(res, out);

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_ERROR(ME, "SpoofingProtection[%s] fw_commit failed", OBJECT_NAMED);
        deactivate_spoofing(spoofing, ipv4);
        goto out;
    }

    if(ipv4) {
        spoofing->status = FIREWALL_ENABLED;
    } else {
        spoofing->status6 = FIREWALL_ENABLED;
    }

    if(target == TARGET_ACCEPT) {
        _set_status(spoofing->common.object, spoofing_status_key, FIREWALL_DISABLED);
    } else {
        _set_status(spoofing->common.object, spoofing_status_key, FIREWALL_ENABLED);
    }

    res = true;

out:
    FW_PROFILING_OUT();
    return res;
}

bool spoofing_implement_changes(spoofing_protection_t* spoofing, bool ipv4_changed, bool ipv6_changed) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object;     // when tracing is disabled, variable is not used anymore

    when_null_trace(spoofing, exit, ERROR, "SpoofingProtection internal structure is NULL.");
    when_false(ipv4_changed | ipv6_changed, exit);

    object = spoofing->common.object;

    if(spoofing->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "SpoofingProtection[%s] about to be deleted", OBJECT_NAMED);
        if(ipv4_changed) {
            deactivate_spoofing(spoofing, true);
        }
        if(ipv6_changed) {
            deactivate_spoofing(spoofing, false);
        }
        delete_fw_spoofing(&spoofing);
    } else if(spoofing->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "SpoofingProtection[%s] about to be add", OBJECT_NAMED);
        if(ipv4_changed) {
            activate_spoofing(spoofing, true);
        }
        if(ipv6_changed) {
            activate_spoofing(spoofing, false);
        }
        spoofing->common.flags = 0;
        spoofing->delayed_change_ipv4 = false;
        spoofing->delayed_change_ipv6 = false;
        amxp_timer_stop(spoofing->timer);
        spoofing->initialized = true;
    } else if(spoofing->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "SpoofingProtection[%s] deactivate, activate", OBJECT_NAMED);
        if(ipv4_changed) {
            deactivate_spoofing(spoofing, true);
            activate_spoofing(spoofing, true);
        }
        if(ipv6_changed) {
            deactivate_spoofing(spoofing, false);
            activate_spoofing(spoofing, false);
        }
        spoofing->common.flags = 0;
        spoofing->delayed_change_ipv4 = false;
        spoofing->delayed_change_ipv6 = false;
        amxp_timer_stop(spoofing->timer);
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a SpoofingProtection
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the Spoofing internal structure.
 */
spoofing_protection_t* create_fw_spoofing(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    spoofing_protection_t* spoofing = NULL;

    SAH_TRACEZ_NOTICE(ME, "SpoofingProtection[%s] is new", OBJECT_NAMED);

    spoofing = (spoofing_protection_t*) calloc(1, sizeof(spoofing_protection_t));
    when_null(spoofing, exit);

    common_init(&spoofing->common, object);

    spoofing->status = FIREWALL_DISABLED;
    spoofing->status6 = FIREWALL_DISABLED;
    _set_status(spoofing->common.object, SPOOFING_PROT_IPV4_STATUS, FIREWALL_ERROR);
    _set_status(spoofing->common.object, SPOOFING_PROT_IPV6_STATUS, FIREWALL_ERROR);
    spoofing->target = TARGET_ACCEPT;
    spoofing->target6 = TARGET_ACCEPT;
    spoofing->query_addresses_v4 = NULL;
    spoofing->query_addresses_v6 = NULL;
    spoofing->timer = NULL;
    spoofing->delayed_change_ipv4 = false;
    spoofing->delayed_change_ipv6 = false;
    spoofing->initialized = false;

    amxc_var_init(&spoofing->addresses_v4);
    amxc_var_init(&spoofing->addresses_v6);
    amxc_var_set_type(&spoofing->addresses_v4, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&spoofing->addresses_v6, AMXC_VAR_ID_HTABLE);
exit:
    FW_PROFILING_OUT();
    return spoofing;
}