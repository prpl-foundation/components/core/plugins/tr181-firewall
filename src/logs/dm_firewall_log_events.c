/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall/dm_firewall.h"
#include "fwrule_helpers.h"
#include "firewall.h"      // for fw_get_dm
#include "init_cleanup.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>

#define ME "firewall"

static void query_log_interface_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    common_t* common = (common_t*) fwiface->query_priv;

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

    amxp_sigmngr_emit_signal(&fw_get_dm()->sigmngr, "firewall_log_added", NULL);
exit:
    FW_PROFILING_OUT();
}

static void set_interface(common_t* common, const char* src_interface, const char* dest_interface, ipversion_t ipversion) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = common->object;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(dest_interface != NULL) {
        firewall_interface_t* fwiface = &common->dest_fwiface;

        if(!STRING_EMPTY(dest_interface)) {
            SAH_TRACEZ_INFO(ME, "Rule[%s] Configure Destination Interface[%s]", OBJECT_NAMED, dest_interface);
            to_linux_interface(dest_interface, ipversion, fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }

    if(src_interface != NULL) {
        firewall_interface_t* fwiface = &common->src_fwiface;

        if(!STRING_EMPTY(src_interface)) {
            SAH_TRACEZ_INFO(ME, "Rule[%s] Configure Source Interface[%s]", OBJECT_NAMED, src_interface);
            to_linux_interface(src_interface, ipversion, fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }
    FW_PROFILING_OUT();
}

static void fw_log_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    common_t* common = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    common = (common_t*) calloc(1, sizeof(common_t));
    when_null(common, exit);

    common_init(common, object);

    common->src_fwiface.query_priv = common;
    common->dest_fwiface.query_priv = common;
    common->src_fwiface.query_result_changed = query_log_interface_changed;
    common->dest_fwiface.query_result_changed = query_log_interface_changed;

    object->priv = common;

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    set_interface(common, GET_CHAR(&params, "FilterSourceInterface"), GET_CHAR(&params, "FilterDestinationInterface"), IPvAll);

exit:
    amxc_var_clean(&params);
    FW_PROFILING_OUT();
    return;
}

void _firewall_log_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));

    when_null(object, exit);
    fw_log_init(object);
exit:
    FW_PROFILING_OUT();
    return;
}

void _firewall_log_changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    common_t* common = NULL;
    const char* src_interface = NULL;
    const char* dest_interface = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    common = (common_t*) object->priv;

    var = GET_ARG(params, "FilterSourceInterface");
    if(var != NULL) {
        src_interface = GET_CHAR(var, "to");
    }

    var = GET_ARG(params, "FilterDestinationInterface");
    if(var != NULL) {
        dest_interface = GET_CHAR(var, "to");
    }

    if((src_interface != NULL) || (dest_interface != NULL)) {
        if(src_interface == NULL) {
            src_interface = object_da_string(object, "FilterSourceInterface");
        }
        if(dest_interface == NULL) {
            dest_interface = object_da_string(object, "FilterDestinationInterface");
        }
        set_interface(common, src_interface, dest_interface, IPvAll);
    }

exit:
    FW_PROFILING_OUT();
    return;
}

static int dm_fw_log_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    fw_log_init(object);
    return amxd_status_ok;
}

void dm_logs_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_string_t log_path;
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");

    amxc_string_init(&log_path, 0);
    amxc_string_setf(&log_path, ".%sLog.*.", fw_get_vendor_prefix());
    amxd_object_for_all(firewall, amxc_string_get(&log_path, 0), dm_fw_log_init, NULL);
    amxc_string_clean(&log_path);
    FW_PROFILING_OUT();
}