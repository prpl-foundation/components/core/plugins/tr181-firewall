/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ipset/dm_ipset.h"
#include "ipset/ipset.h"
#include "firewall.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include "init_cleanup.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <string.h>

#define ME "ipset"

static set_type_t set_type_convert(const char* type, ipversion_t family) {
    set_type_t rv = SET_TYPE_HASH_UNKNOWN;

    if(strcmp(type, "IPAddresses") == 0) {
        switch(family) {
        case IPv4:
            rv = SET_TYPE_HASH_IPV4;
            break;

        case IPv6:
            rv = SET_TYPE_HASH_IPV6;
            break;

        default:
            SAH_TRACEZ_ERROR(ME, "Invalid set family (%u)", family);
            break;
        }
    } else if(strcmp(type, "Ports") == 0) {
        rv = SET_TYPE_HASH_PORT;

    } else if(strcmp(type, "MACAddresses") == 0) {
        rv = SET_TYPE_HASH_MAC;
    }

    return rv;
}

static const char* get_parent_set_name(amxd_object_t* object) {
    bool enabled = false;
    const char* name = NULL;
    amxd_object_t* parent = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");

    parent = amxd_object_findf(object, "^.^.");
    when_null_trace(parent, exit, ERROR, "Cannot find parent Set instance");

    enabled = object_get_enable(parent);
    when_false_trace(enabled, exit, INFO, "Parent Set is not enabled");

    name = amxd_object_get_name(parent, AMXD_OBJECT_NAMED);
    when_str_empty_trace(name, exit, ERROR, "Alias from Set instance is empty");

exit:
    return name;
}

static const char* get_rule_entries(amxd_object_t* object) {
    const char* type = NULL;
    const char* entry_csv_str = NULL;
    amxd_object_t* parent = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");

    parent = amxd_object_findf(object, "^.^.");
    when_null_trace(parent, exit, ERROR, "Cannot find parent Set instance");

    type = object_da_string(parent, "Type");
    when_null_trace(type, exit, ERROR, "Cannot get Type from Set instance");

    if(strcmp(type, "IPAddresses") == 0) {
        entry_csv_str = object_da_string(object, "IPAddressList");
    } else if(strcmp(type, "Ports") == 0) {
        entry_csv_str = object_da_string(object, "PortList");
    } else if(strcmp(type, "MACAddresses") == 0) {
        entry_csv_str = object_da_string(object, "MACAddressList");
    }

exit:
    return entry_csv_str;
}

void set_init(amxd_object_t* object) {
    set_t* set = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_not_null_trace(object->priv, exit, ERROR, "Set is already initialized");

    SAH_TRACEZ_INFO(ME, "Calloc 'set_t' struct for Set[%s]", OBJECT_NAMED);
    set = calloc(1, sizeof(set_t));

    object->priv = set;

    if(object_get_enable(object)) {
        set_create(object);
    }

exit:
    return;
}

void set_create(amxd_object_t* object) {
    set_type_t type = SET_TYPE_HASH_UNKNOWN;
    const char* alias = NULL;
    const char* type_str = NULL;
    bool enable = false;
    int rv = -1;
    amxc_var_t params;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "Object is NULL");

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    type_str = GET_CHAR(&params, "Type");
    alias = GET_CHAR(&params, "Alias");
    enable = GET_BOOL(&params, "Enable");

    when_false_trace(enable, exit, INFO, "Set not enabled");

    SAH_TRACEZ_INFO(ME, "Creating Set[%s]", alias);

    type = set_type_convert(type_str, int_to_ipversion(GET_INT32(&params, "IPVersion")));
    when_true_trace(type == SET_TYPE_HASH_UNKNOWN, exit, ERROR, "Set type is not valid");

    rv = ipset_create(alias, type);
    when_failed(rv, exit);

    SAH_TRACEZ_INFO(ME, "Set '%s' sucessfully created", alias);
    amxd_object_for_all(object, ".Rule.*.", add_entries, NULL);

exit:
    amxc_var_clean(&params);
    return;
}

void set_delete(amxd_object_t* object) {
    const char* alias = NULL;
    int rv = -1;

    when_null_trace(object, exit, ERROR, "Object is NULL");

    alias = amxd_object_get_name(object, AMXD_OBJECT_NAMED);

    SAH_TRACEZ_INFO(ME, "Deleting Set[%s]", alias);

    rv = ipset_destroy(alias);
    when_true_trace(rv == 0, exit, INFO, "Set[%s] sucessfully deleted", alias);

exit:
    return;
}

int add_entries(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    const char* set_name = NULL;
    const char* entry_csv_str = NULL;
    amxc_var_t entry_csv;
    amxd_status_t status = amxd_status_unknown_error;
    bool exclude = false;
    int rv = -1;

    amxc_var_init(&entry_csv);

    set_name = get_parent_set_name(object);
    when_null(set_name, exit);

    SAH_TRACEZ_INFO(ME, "Adding entries defined in Rule[%s] to Set[%s]", OBJECT_NAMED, set_name);

    status = amxd_status_ok;

    exclude = amxd_object_get_value(bool, object, "Exclude", NULL);
    entry_csv_str = get_rule_entries(object);
    when_str_empty_trace(entry_csv_str, exit, INFO, "Entries list is empty");

    amxc_var_set(csv_string_t, &entry_csv, entry_csv_str);
    amxc_var_cast(&entry_csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(entry, &entry_csv) {
        rv = ipset_add_entry(set_name, GET_CHAR(entry, NULL), exclude);
        if(rv == 0) {
            SAH_TRACEZ_INFO(ME, "Entry '%s' sucessfully added to set '%s'", GET_CHAR(entry, NULL), set_name);
        }
    }

exit:
    amxc_var_clean(&entry_csv);
    return status;
}

void update_entries(amxd_object_t* object, bool ignore_rule) {
    const char* set_name = NULL;
    amxc_string_t search_path;
    int rv = -1;

    amxc_string_init(&search_path, 0);

    set_name = get_parent_set_name(object);
    when_null(set_name, exit);

    SAH_TRACEZ_INFO(ME, "Updating entries defined in Set[%s]", set_name);

    rv = ipset_flush_set(set_name);
    when_true_trace(rv != 0, exit, INFO, "Error while flushing Set[%s]", set_name);

    if(ignore_rule) {
        amxc_string_setf(&search_path, "^.^.Rule.[Alias != '%s'].", OBJECT_NAMED);
    } else {
        amxc_string_setf(&search_path, "^.^.Rule.*.");
    }

    amxd_object_for_all(object, amxc_string_get(&search_path, 0), add_entries, NULL);

exit:
    amxc_string_clean(&search_path);
    return;
}

static int dm_set_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    set_init(object);
    return amxd_status_ok;
}

void dm_ipset_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");
    amxd_object_for_all(firewall, ".Set.*.", dm_set_init, NULL);
    FW_PROFILING_OUT();
}


void increment_set_ref(const char* set_path) {
    amxd_object_t* set_object = NULL;
    set_t* set = NULL;

    when_str_empty(set_path, exit);

    set_object = amxd_object_findf(amxd_dm_get_root(fw_get_dm()), "%s", set_path);
    when_null_trace(set_object, exit, ERROR, "Object not found");
    when_null_trace(set_object->priv, exit, ERROR, "Object private data is NULL");

    set = ((set_t*) set_object->priv);
    set->refcount++;

exit:
    return;
}

void decrement_set_ref(const char* set_path) {
    amxd_object_t* set_object = NULL;
    set_t* set = NULL;

    when_str_empty(set_path, exit);

    set_object = amxd_object_findf(amxd_dm_get_root(fw_get_dm()), "%s", set_path);
    when_null_trace(set_object, exit, ERROR, "Set[%s] not found", set_path);
    when_null_trace(set_object->priv, exit, ERROR, "Set private data is NULL");

    set = ((set_t*) set_object->priv);
    if(set->refcount > 0) {
        set->refcount--;
    }

exit:
    return;
}

void _set_added(UNUSED const char* const sig_name,
                const amxc_var_t* const event_data,
                UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, GET_CHAR(event_data, "name"), GET_UINT32(event_data, "index"));

    set_init(object);

    FW_PROFILING_OUT();
    return;
}

void _set_type_changed(UNUSED const char* const sig_name,
                       const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    bool enabled = object_get_enable(object);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t trans;

    if(enabled) {
        set_delete(object);
    }

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_select_pathf(&trans, ".Rule.");
    amxd_object_for_each(instance, it, amxd_object_get(object, "Rule")) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        amxd_trans_del_inst(&trans, amxd_object_get_index(inst), NULL);
    }
    rv = amxd_trans_apply(&trans, fw_get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Can not remove Rule instance, transaction failed: %d", rv);
    }

    if(enabled) {
        set_create(object);
    }

    amxd_trans_clean(&trans);
    FW_PROFILING_OUT();
    return;
}

void _set_enable_changed(UNUSED const char* const sig_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    bool enabled = object_get_enable(object);

    if(enabled) {
        set_create(object);
    } else {
        set_delete(object);
    }

    FW_PROFILING_OUT();
    return;
}

void _set_rule_added(UNUSED const char* const sig_name,
                     const amxc_var_t* const event_data,
                     UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* instance = amxd_object_get_instance(templ, GET_CHAR(event_data, "name"), GET_UINT32(event_data, "index"));

    add_entries(NULL, instance, NULL);

    FW_PROFILING_OUT();
    return;
}

void _set_rule_changed(UNUSED const char* const sig_name,
                       UNUSED const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);

    update_entries(object, false);

    FW_PROFILING_OUT();
    return;
}