/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef FWRULE_HELPERS_H__
#define FWRULE_HELPERS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <fwrules/fw_rule.h>
#include <fwrules/fw_folder.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <schedules/schedule.h>
#include "netmodel.h"           // for firewall_interface_t
#include "ip_address.h"         // for ipversion_t
#include "firewall_utilities.h" // for status_t

#define TABLE_FILTER "filter"
#define TABLE_NAT "nat"
#define TABLE_RAW "raw"

typedef enum {
    FW_NEW = 1,
    FW_DELETED = 2,
    FW_MODIFIED = 4,
} flags_t;

typedef enum {
    TARGET_NONE,
    TARGET_DROP,
    TARGET_ACCEPT,
    TARGET_CHAIN,
    TARGET_REJECT,
    TARGET_RETURN,
    TARGET_LOG,
} target_t;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
typedef enum {
    LOG_FILTER_ALL,
    LOG_FILTER_ALLOWED,
    LOG_FILTER_DENIED
} log_filter_t;

typedef struct _log_info_ {
    int log_level;
    int flags;
    bool enable;
    amxc_string_t* log_prefix;
    amxc_var_t* log_controllers;
} log_info_t;

typedef struct _log_ {
    log_info_t old_log_info;
    log_info_t log_info;
    log_filter_t rule_policy;
    firewall_interface_t* src_interface;
    bool exclude_src_interface;
    firewall_interface_t* dst_interface;
    bool exclude_dst_interface;
    bool use_log_controller_interface;
    amxc_htable_t* subscriptions;
    amxp_slot_fn_t slot_cb;
    amxd_object_t* object;
} log_t;
#endif //FIREWALL_LOGS

typedef struct _folder_wrapper_ {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_t log;
#endif //FIREWALL_LOGS
    fw_folder_t* folder;
    fw_folder_t* folder6;
} folder_wrapper_t;

typedef struct _folder_container_ {
    folder_wrapper_t log_folders;
    folder_wrapper_t rule_folders;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_t log;
#endif //FIREWALL_LOGS
    void* priv_data;
} folder_container_t;

typedef struct common {
    flags_t flags;
    bool enable;
    bool suspend;
    bool validated;
    status_t status;
    folder_container_t folders;
    amxc_var_t protocols;
    ipversion_t ipversion;
    amxd_object_t* object;
    firewall_interface_t dest_fwiface;
    firewall_interface_t src_fwiface;
    firewall_interface_t dest6_fwiface;
    firewall_interface_t src6_fwiface;
    void* priv_data;
    schedule_t* schedule;
    schedule_status_t schedule_status;
    char* object_path;
} common_t;

typedef struct {
    const char* chain;
    struct {
        address_t* src_address;
        address_t* dest_address;
        amxc_llist_t* src_ports;
        amxc_llist_t* dest_ports;
        amxc_var_t* protocols;
    } match;
    struct {
        address_t* address;
        amxc_llist_t* src_ports;
        amxc_llist_t* dest_ports;
    } target;
} dnat_args_t;

typedef struct {
    ipversion_t ipversion;
    bool reverse;
    const char* chain;
    struct {
        const char* iface;
        amxc_var_t* protocols;
        address_t* src_address;
        address_t* dest_address;
        amxc_llist_t* src_ports;
        amxc_llist_t* dest_ports;
    } match;
    struct {
        target_t verdict;
    } target;
} filter_args_t;

target_t string_to_target(const char* target);

bool fw_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag, const char* chain, const char* table, int index);

bool enable_rule(fw_folder_t* log_folder, fw_folder_t* folder, status_t* status, bool commit);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
bool set_log_rule_specific_settings(fw_rule_t* r, const log_t* const log);
#endif //FIREWALL_LOGS

void common_init(common_t* common, amxd_object_t* object);
void common_clean(common_t* common);
void common_set_status(common_t* common, status_t status);

void folder_wrapper_disable_folders(folder_wrapper_t* folder_wrapper, ipversion_t ipversion);
bool folder_wrapper_delete_rules(folder_wrapper_t* folder_wrapper, ipversion_t ipversion, bool commit);
void folder_wrapper_init(folder_wrapper_t* folder_wrapper);
void folder_wrapper_clean(folder_wrapper_t* folder_wrapper);

bool is_folders_initialized(folder_container_t* folders);
void folder_container_init(folder_container_t* folders, common_t* common);
void folder_container_clean(folder_container_t* folders);
bool is_folders_array_ordered(folder_container_t** folder_container_array, int array_size);
bool reorder_array_folder_containers(folder_container_t** folder_container_array, int array_size);
bool folder_container_reorder(folder_container_t* folders);
bool folder_container_delete_rules(folder_container_t* folders, ipversion_t ipversion, bool commit);
void folder_container_disable_folders(folder_container_t* folders, ipversion_t ipversion);

bool folder_require_source_feature(fw_folder_t* folder, address_t* address);
bool folder_require_dest_feature(fw_folder_t* folder, address_t* address);

bool folder_default_set_source(fw_rule_t* default_rule, const char* ip, const char* netmask, bool excluded);
bool folder_default_set_destination(fw_rule_t* default_rule, const char* ip, const char* netmask, bool excluded);
bool folder_default_set_address(fw_rule_t* default_rule, address_t* address, bool ipv4, bool source, bool exclude);
bool folder_default_set_host_address(fw_rule_t* default_rule, address_t* address, bool ipv4, bool source, bool exclude);
bool folder_default_set_in_and_out_interfaces(fw_rule_t* default_rule, const char* dest, bool dest_excl, const char* src, bool src_excl, bool reverse);

bool folder_feature_set_source_mac(fw_folder_t* folder, const char* mac, bool exclude);
bool folder_feature_set_protocol(fw_folder_t* folder, amxc_var_t* protocols);
bool folder_feature_set_source_port(fw_folder_t* folder, amxc_llist_t* ports, bool exclude);
bool folder_feature_set_destination_port(fw_folder_t* folder, amxc_llist_t* ports, bool exclude);
bool folder_feature_set_target_policy(fw_folder_t* folder, fw_rule_policy_t policy);
bool folder_feature_set_address(fw_folder_t* folder, address_t* address, bool ipv4, bool source, bool exclude);
bool folder_feature_set_target(fw_folder_t* folder, target_t target, const char* chain_name, bool ipv4);
bool folder_feature_set_source_and_destination_port(fw_folder_t* folder, amxc_llist_t* src_ports, bool sport_exclude, amxc_llist_t* dst_ports, bool dport_exclude, bool reverse);
bool folder_feature_set_dscp(fw_folder_t* folder, int32_t dscp, bool exclude);
bool folder_feature_set_source_ipset(fw_folder_t* folder, const char* source_set_path, const char* source_set_exclude_path);
bool folder_feature_set_destination_ipset(fw_folder_t* folder, const char* dest_set_path, const char* dest_set_exclude_path);
bool folder_feature_set_target_dnat(fw_folder_t* folder, const char* destination, int int_port, int int_port_max, int ext_port, int ext_port_max);
bool folder_feature_set_target_snat(fw_folder_t* folder, address_t* address, int minport, int maxport);

bool fill_folder_dnat_rule(fw_folder_t* folder, dnat_args_t* args);
bool fill_folder_container_filter_rule(folder_container_t* folders, filter_args_t* args);

#ifdef __cplusplus
}
#endif

#endif // FWRULE_HELPERS_H__
