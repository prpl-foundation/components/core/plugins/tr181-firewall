/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include "mock.h"
#include "test_dm_logs.h"

static amxd_dm_t* dm = NULL;

#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX "[FW][ICMP]:"

static void populate_input_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "INPUT_First_Log"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 1), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void populate_input6_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "INPUT6_First_Log"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, false), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 58), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void populate_forward_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_First_Log"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 1), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void populate_forward6_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD6_First_Log"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, false), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 58), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void populate_output_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "OUTPUT_Last_Log"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 1), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void populate_output6_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "OUTPUT6_Last_Log"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, false), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 58), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void a_populate_filter_rule_log(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_source(rule, "10.0.0.1/32"), 0);
    assert_int_equal(fw_rule_set_source_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_source_port(rule, 7000), 0);
    assert_int_equal(fw_rule_set_source_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "br-lan"), 0);
    assert_int_equal(fw_rule_set_in_interface_excluded(rule, false), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface_excluded(rule, false), 0);
    assert_int_equal(fw_rule_set_destination(rule, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_destination_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_dscp_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void a_populate_filter_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_source(rule, "10.0.0.1/32"), 0);
    assert_int_equal(fw_rule_set_source_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_source_port(rule, 7000), 0);
    assert_int_equal(fw_rule_set_source_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "br-lan"), 0);
    assert_int_equal(fw_rule_set_in_interface_excluded(rule, false), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface_excluded(rule, false), 0);
    assert_int_equal(fw_rule_set_destination(rule, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_destination_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_dscp_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

int test_fw_logs_setup(void** state) {
    expect_query_wan_netdevname(); // for logs
    expect_query_lan_netdevname(); // for logs
    mock_init(state, "logs_icmp_logs.odl");
    dm = amxut_bus_dm();
    return 0;
}

int test_fw_logs_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_fw_icmp_logs(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* log_object = NULL;

    assert_int_equal(amxd_trans_init(&trans), 0);

    log_object = amxd_dm_findf(dm, "Firewall.Log.ICMP.");
    assert_non_null(log_object);

    print_message("change Log to enable\n");
    amxd_trans_select_object(&trans, log_object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule(populate_input_rule, 1);
    expect_fw_replace_rule(populate_input6_rule, 1);
    expect_fw_replace_rule(populate_forward_rule, 1);
    expect_fw_replace_rule(populate_forward6_rule, 1);
    expect_fw_replace_rule(populate_output_rule, 1);
    expect_fw_replace_rule(populate_output6_rule, 1);
    handle_events();
}

void test_fw_wan_to_lan_rule_logs(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* log_object = NULL;
    amxd_object_t* instance = NULL;
    amxd_object_t* chain = NULL;

    amxc_var_init(&params);

    expect_query_wan_netdevname();
    expect_query_lan_netdevname();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "Firewall.Chain.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "L_Low"); // determines which chain is used for the rules: FORWARD_L_Low
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // 1. enable rule, Status == Disabled because chain is not enabled
    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.1");
    assert_non_null(instance);
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.1.0/24");
    amxd_trans_set_value(bool, &trans, "DestIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(bool, &trans, "DestPortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "10.0.0.1");
    amxd_trans_set_value(bool, &trans, "SourceIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", 7000);
    amxd_trans_set_value(bool, &trans, "SourcePortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "DestInterface", "Device.IP.Interface.2.");   // WAN
    amxd_trans_set_value(bool, &trans, "DestInterfaceExclude", false);
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.3."); // LAN
    amxd_trans_set_value(bool, &trans, "SourceInterfaceExclude", false);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(bool, &trans, "DSCPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    // 2. enable chain, rule Status == Enabled
    chain = amxd_dm_findf(dm, "Firewall.Chain.1.");
    assert_non_null(chain);
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_object(&trans, chain);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule(a_populate_filter_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    log_object = amxd_dm_findf(dm, "Firewall.Log.All.");
    assert_non_null(log_object);

    print_message("change Log to enable\n");
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_object(&trans, log_object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(a_populate_filter_rule, 1);
    expect_fw_replace_rule(a_populate_filter_rule_log, 1);
    expect_fw_replace_rule(a_populate_filter_rule, 2);
    handle_events();

    amxc_var_clean(&params);
}