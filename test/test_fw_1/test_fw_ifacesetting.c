/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "firewall.h"

#include <amxc/amxc.h>
#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_fw_ifacesetting.h"
#include "test_common_interfacesetting.h"

#define MAX_TEST_RULES 100

static void print_status(const char* string) {
    printf("- %s\n", string); fflush(stdout);
}

int test_ifsetting_setup(void** state) {
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_2.odl");
    return 0;
}

int test_ifsetting_setup_spoofing(void** state) {
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_spoofing.odl");
    return 0;
}

int test_ifsetting_setup_dropped_conn(void** state) {
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_dropped_conn.odl");
    return 0;
}

int test_ifsetting_setup_accept_conn(void** state) {
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_accept_conn.odl");
    return 0;
}

int test_ifsetting_setup_icpv6_pass(void** state) {
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_3.odl");
    return 0;
}

int test_ifsetting_teardown(void** state) {
    mock_cleanup(state);
    common_test_teardown();
    return 0;
}

void test_dm_alias(UNUSED void** state) {
    int index;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_lan_netdevname();     // for ping6
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();

    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);

    index = add_ifsetting("test_1", "Device.IP.Interface.3.", ICMP_DISABLE, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(index, 0);
}

void test_dm_interface(UNUSED void** state) {
    int index;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();

    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);

    index = add_ifsetting("test_2", "Device.IP.Interface.2.", ICMP_DISABLE, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(index, 0);
}

void test_dm_delete_instance(UNUSED void** state) {
    int index;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();

    print_status("Create an instance with alias = test_1");
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    assert_non_null(services);
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    print_status("Delete instance with alias = test_1");
    delete_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_ifsetting("test_1");

    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_null(ifsetting_instance);

    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_null(service_instance_ipv6);
}

void test_child_services(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    const amxc_var_t* ifsetting_icmpv4_status;
    const amxc_var_t* ifsetting_icmpv6_status;
    const amxc_var_t* service_icmpv4_action;
    const amxc_var_t* service_icmpv6_action;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=false,PassthroughICMPv6Status=");
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(5, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(6, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_icmpv4_status = amxd_object_get_param_value(ifsetting_instance, "ICMPv4EchoRequestStatus");
    assert_non_null(ifsetting_icmpv4_status);
    ifsetting_icmpv6_status = amxd_object_get_param_value(ifsetting_instance, "ICMPv6EchoRequestStatus");
    assert_non_null(ifsetting_icmpv6_status);
    service_icmpv4_action = amxd_object_get_param_value(service_instance_ipv4, "Action");
    assert_non_null(service_icmpv4_action);
    service_icmpv6_action = amxd_object_get_param_value(service_instance_ipv6, "Action");
    assert_non_null(service_icmpv6_action);
    assert_string_equal(GET_CHAR(ifsetting_icmpv4_status, NULL), "Allow");
    assert_string_equal(GET_CHAR(ifsetting_icmpv6_status, NULL), "Allow");
    assert_string_equal(GET_CHAR(service_icmpv4_action, NULL), "Accept");
    assert_string_equal(GET_CHAR(service_icmpv6_action, NULL), "Accept");

    print_status("Modify AcceptICMPEchoRequest="ICMP_IPV4);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_IPV4, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_icmpv4_status, NULL), "Allow");
    assert_string_equal(GET_CHAR(ifsetting_icmpv6_status, NULL), "Reject");
    assert_string_equal(GET_CHAR(service_icmpv4_action, NULL), "Accept");
    assert_string_equal(GET_CHAR(service_icmpv6_action, NULL), "Drop");

    print_status("Modify AcceptICMPEchoRequest="ICMP_IPV6 ",StealthMode=true");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    //Service ipv6
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    rv = modify_ifsetting(index, ICMP_IPV6, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_icmpv4_status, NULL), "Reject");
    assert_string_equal(GET_CHAR(ifsetting_icmpv6_status, NULL), "Allow");
    assert_string_equal(GET_CHAR(service_icmpv4_action, NULL), "Drop");
    assert_string_equal(GET_CHAR(service_icmpv6_action, NULL), "Accept");

    print_status("Modify AcceptICMPEchoRequest="ICMP_DISABLE ",StealthMode=false");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(3, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(4, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_DISABLE, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_icmpv4_status, NULL), "Reject");
    assert_string_equal(GET_CHAR(ifsetting_icmpv6_status, NULL), "Reject");
    assert_string_equal(GET_CHAR(service_icmpv4_action, NULL), "Drop");
    assert_string_equal(GET_CHAR(service_icmpv6_action, NULL), "Drop");

    print_status("Modify AcceptICMPEchoRequest="ICMP_BOTH ",StealthMode=true");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_icmpv4_status, NULL), "Allow");
    assert_string_equal(GET_CHAR(ifsetting_icmpv6_status, NULL), "Allow");
    assert_string_equal(GET_CHAR(service_icmpv4_action, NULL), "Accept");
    assert_string_equal(GET_CHAR(service_icmpv6_action, NULL), "Accept");
}

void test_stealth_mode(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    const amxc_var_t* ifsetting_stealth_status;

    expect_query_lan_netdevname();
    expect_query_lan_netdevname();     // for spoofing
    expect_query_lan_ipv4_addresses(); // for spoofing
    expect_query_lan_ipv6_addresses(); // for spoofing
    expect_query_lan_ipv4_addresses(); // for isolation
    expect_query_lan_ipv6_addresses(); // for isolation
    expect_query_lan_netdevname();     // for service (ping related I guess)
    expect_query_lan_netdevname();     // for service (ping6 related I guess)
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true,PassthroughICMPv6Status=false");
    create_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.3.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_stealth_status = amxd_object_get_param_value(ifsetting_instance, "StealthModeStatus");
    assert_non_null(ifsetting_stealth_status);
    assert_string_equal(GET_CHAR(ifsetting_stealth_status, NULL), "Enabled");

    print_status("Modify StealthMode=false");
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_stealth_status, NULL), "Disabled");

    print_status("Modify AcceptICMPEchoRequest="ICMP_IPV6 ",StealthMode=true");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_IPV6, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_stealth_status, NULL), "Enabled");
}

void test_accept_udp_traceroute(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    const amxc_var_t* ifsetting_status;

    expect_query_lan_netdevname();
    expect_query_lan_netdevname();     // for spoofing
    expect_query_lan_ipv4_addresses(); // for spoofing
    expect_query_lan_ipv6_addresses(); // for spoofing
    expect_query_lan_ipv4_addresses(); // for isolation
    expect_query_lan_ipv6_addresses(); // for isolation
    expect_query_lan_netdevname();     // for service (ping related I guess)
    expect_query_lan_netdevname();     // for service (ping6 related I guess)
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true, AcceptUDPTraceroute=true,PassthroughICMPv6Status=false");
    create_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.3.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_ENABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_status = amxd_object_get_param_value(ifsetting_instance, "AcceptUDPTracerouteStatus");
    assert_non_null(ifsetting_status);
    assert_string_equal(GET_CHAR(ifsetting_status, NULL), "Enabled");

    print_status("Modify AcceptUDPTraceroute=false");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_status, NULL), "Disabled");

    print_status("Modify AcceptICMPEchoRequest="ICMP_IPV6 ",AcceptUDPTraceroute=true");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_IPV6, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_ENABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_status, NULL), "Enabled");
}

void test_spoofing_mode(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    const amxc_var_t* ifsetting_spoofingv4_status;
    const amxc_var_t* ifsetting_spoofingv6_status;

    expect_query_lan_netdevname();
    expect_query_lan_netdevname();     // for spoofing
    expect_query_lan_ipv4_addresses(); // for spoofing
    expect_query_lan_ipv6_addresses(); // for spoofing
    expect_query_lan_ipv4_addresses(); // for isolation
    expect_query_lan_ipv6_addresses(); // for isolation
    expect_query_lan_netdevname();     // for service (ping related I guess)
    expect_query_lan_netdevname();     // for service (ping6 related I guess)
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_BOTH ",StealthMode=false,PassthroughICMPv6Status=false");
    create_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(2, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(3, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(4, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "0.0.0.0/32", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(5, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(6, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(7, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(8, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(3, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(4, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(5, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fe80::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(6, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(3, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(3, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.3.", ICMP_BOTH, SPOOFING_BOTH, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);

    print_status("Waiting for the delayed implement changes...");
    amxut_timer_go_to_future_ms(5001);
    handle_events();

    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_spoofingv4_status = amxd_object_get_param_value(ifsetting_instance, "SpoofingProtectionIPv4Status");
    assert_non_null(ifsetting_spoofingv4_status);
    ifsetting_spoofingv6_status = amxd_object_get_param_value(ifsetting_instance, "SpoofingProtectionIPv6Status");
    assert_non_null(ifsetting_spoofingv6_status);
    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Enabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Enabled");

    print_status("Modify SpoofingProtection="SPOOFING_IPV4);
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fe80::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_IPV4, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Enabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Disabled");

    print_status("Modify SpoofingProtection="SPOOFING_IPV6 ",StealthMode=true");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "0.0.0.0/32", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(3, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(4, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(5, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fe80::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(6, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_IPV6, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Disabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Enabled");

    print_status("Modify SpoofingProtection="SPOOFING_DISABLE ",StealthMode=false");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fe80::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_DISABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Disabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Disabled");

    print_status("Modify SpoofingProtection="SPOOFING_BOTH ",StealthMode=true");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 17, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, 6, -1, NULL, false, NULL, FW_RULE_POLICY_REJECT, false, NULL);

    create_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(2, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(3, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(4, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "0.0.0.0/32", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(5, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(6, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(7, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(8, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(2, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(3, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(4, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fc:0:0:1::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(5, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "fe80::/64", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(6, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV6, false, -1, -1, "::/0", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_BOTH, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Enabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Enabled");

    print_status("Modify LAN IP=192.168.100.1");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "0.0.0.0/32", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.1.0/24", true, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.100.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(2, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(3, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.100.0/24", true, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_SPOOF]:");
    create_rule(4, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "0.0.0.0/32", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(5, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.100.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(6, "br-lan", NULL, true, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(7, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.3.0/24", false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(8, "br-lan", NULL, false, TEST_TABLE_RAW, IFSETTING_CHAIN_SPOOF_IPV4, true, -1, -1, "192.168.100.0/24", true, NULL, FW_RULE_POLICY_DROP, false, NULL);
    modify_netmodel_query("Device.IP.Interface.3.getAddrs(flag=\"ipv4\", traverse=\"down\")",
                          "192.168.100.1");

    print_status("Waiting for the delayed implement changes after changing the LAN IP");
    amxut_timer_go_to_future_ms(5001);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Enabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Enabled");

    print_status("Don't change IP");
    modify_netmodel_query("Device.IP.Interface.3.getAddrs(flag=\"ipv4\", traverse=\"down\")",
                          "192.168.100.1");

    assert_string_equal(GET_CHAR(ifsetting_spoofingv4_status, NULL), "Enabled");
    assert_string_equal(GET_CHAR(ifsetting_spoofingv6_status, NULL), "Enabled");
}

void test_icmpv6_passthrough_mode(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    const amxc_var_t* ifsetting_passthrough_status;

    expect_query_lan_netdevname();
    expect_query_lan_netdevname();     // for spoofing
    expect_query_lan_ipv4_addresses(); // for spoofing
    expect_query_lan_ipv6_addresses(); // for spoofing
    expect_query_lan_ipv4_addresses(); // for isolation
    expect_query_lan_ipv6_addresses(); // for isolation
    expect_query_lan_netdevname();     // for service (ping related I guess)
    expect_query_lan_netdevname();     // for service (ping6 related I guess)
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true,PassthroughICMPv6Status=true");
    create_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.3.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, true, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_passthrough_status = amxd_object_get_param_value(ifsetting_instance, "PassthroughICMPv6Status");
    assert_non_null(ifsetting_passthrough_status);
    assert_string_equal(GET_CHAR(ifsetting_passthrough_status, NULL), "Enabled");

    print_status("Modify PassthroughICMPv6Status=false");
    delete_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_passthrough_status, NULL), "Disabled");

    print_status("Modify PassthroughICMPv6Status=true");
    delete_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, true, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_passthrough_status, NULL), "Enabled");
}

void test_icmpv6_passthrough_mode_with_logs(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    const amxc_var_t* ifsetting_passthrough_status;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true,PassthroughICMPv6Status=true");
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_IN_CONN]:");
    create_rule(2, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_IN_CONN]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_IN_CONN]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, true, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_passthrough_status = amxd_object_get_param_value(ifsetting_instance, "PassthroughICMPv6Status");
    assert_non_null(ifsetting_passthrough_status);
    assert_string_equal(GET_CHAR(ifsetting_passthrough_status, NULL), "Enabled");

    print_status("Modify PassthroughICMPv6Status=false");
    delete_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_IN_CONN]:");
    delete_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_passthrough_status, NULL), "Disabled");

    print_status("Modify PassthroughICMPv6Status=true");
    delete_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_IN_CONN]:");
    create_rule(2, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, true, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_passthrough_status, NULL), "Enabled");
}

void test_log_in_dropped_connection(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_trans_t trans;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    amxd_object_t* log_object_all = NULL;
    amxd_object_t* log_object_denied = NULL;
    const amxc_var_t* ifsetting_log_drop_in_status;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_netdevname();     // for icmpv6 passthrough
    expect_query_wan_netdevname();     // for stealth mode
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for log
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();

    amxd_trans_init(&trans);

    log_object_all = amxd_dm_findf(fw_get_dm(), "Firewall.Log.All.");
    assert_non_null(log_object_all);
    log_object_denied = amxd_dm_findf(fw_get_dm(), "Firewall.Log.DeniedInbound.");
    assert_non_null(log_object_denied);

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true,PassthroughICMPv6=false,LOGDroppedIncomingConnection=true");
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_ENABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_log_drop_in_status = amxd_object_get_param_value(ifsetting_instance, "LOGDroppedIncomingConnectionStatus");
    assert_non_null(ifsetting_log_drop_in_status);
    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Enabled");

    print_status("Modify LOGDroppedIncomingConnection=false");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Disabled");

    print_status("Modify LOGDroppedIncomingConnection=true");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_ENABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Enable to disable\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Enabled");

    print_message("Change Firewall.Log.DeniedInbound.Enable to disable\n");
    amxd_trans_select_object(&trans, log_object_denied);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Disabled");

    print_message("Change Firewall.Log.All.Enable to enable\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Filter to Allowed\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(cstring_t, &trans, "FilterPolicy", "Allowed");
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Disabled");

    print_message("Change Firewall.Log.All.Filter to All\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(cstring_t, &trans, "FilterPolicy", "All");
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_INPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_INPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_in_status, NULL), "Enabled");
}

void test_log_out_dropped_connection(UNUSED void** state) {
    int index;
    int rv = 0;
    amxd_trans_t trans;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    amxd_object_t* log_object_all = NULL;
    amxd_object_t* log_object_denied = NULL;
    const amxc_var_t* ifsetting_log_drop_out_status;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_netdevname();     // for icmpv6 passthrough
    expect_query_wan_netdevname();     // for stealth mode
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for log
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();

    amxd_trans_init(&trans);

    log_object_all = amxd_dm_findf(fw_get_dm(), "Firewall.Log.All.");
    assert_non_null(log_object_all);
    log_object_denied = amxd_dm_findf(fw_get_dm(), "Firewall.Log.DeniedOutbound.");
    assert_non_null(log_object_denied);

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true,PassthroughICMPv6=false,LOGDroppedOutgoingConnection=true");
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_ENABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_log_drop_out_status = amxd_object_get_param_value(ifsetting_instance, "LOGDroppedOutgoingConnectionStatus");
    assert_non_null(ifsetting_log_drop_out_status);
    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Enabled");

    print_status("Modify LOGDroppedOutgoingConnection=false");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Disabled");

    print_status("Modify LOGDroppedOutgoingConnection=true");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_ENABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Enable to disable\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT,All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.DeniedOutbound.Enable to disable\n");
    amxd_trans_select_object(&trans, log_object_denied);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_OUT_ATMPT]:");
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Disabled");

    print_message("Change Firewall.Log.All.Enable to enable\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Filter to Allowed\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(cstring_t, &trans, "FilterPolicy", "Allowed");
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Disabled");

    print_message("Change Firewall.Log.All.Filter to All\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(cstring_t, &trans, "FilterPolicy", "All");
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][BLK_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV4_FORWARD, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_IN_IPV6_FORWARD, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_drop_out_status, NULL), "Enabled");
}

void test_log_out_accept_connection(UNUSED void** state) {
    int index = 0;
    int rv = 0;
    amxd_trans_t trans;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance_ipv4 = NULL;
    amxd_object_t* service_instance_ipv6 = NULL;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;
    amxd_object_t* log_object_all = NULL;
    amxd_object_t* log_object_allowed = NULL;
    const amxc_var_t* ifsetting_log_accept_out_status = NULL;

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_netdevname();     // for icmpv6 passthrough
    expect_query_wan_netdevname();     // for stealth mode
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for log
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();

    amxd_trans_init(&trans);

    log_object_all = amxd_dm_findf(fw_get_dm(), "Firewall.Log.All.");
    assert_non_null(log_object_all);
    log_object_allowed = amxd_dm_findf(fw_get_dm(), "Firewall.Log.AllowedOutbound.");
    assert_non_null(log_object_allowed);

    print_status("Add AcceptICMPEchoRequest="ICMP_BOTH ",SpoofingProtection="SPOOFING_DISABLE ",StealthMode=true,PassthroughICMPv6=false,LOGAcceptOutgoingConnection=true");
    create_rule(1, "eth0", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    index = add_ifsetting("test_1", "Device.IP.Interface.2.", ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_ENABLE);
    assert_int_not_equal(index, 0);
    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    service_instance_ipv4 = amxd_object_get_instance(services, "ifsetting_icmpv4_test_1", 0);
    assert_non_null(service_instance_ipv4);
    service_instance_ipv6 = amxd_object_get_instance(services, "ifsetting_icmpv6_test_1", 0);
    assert_non_null(service_instance_ipv6);

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);
    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, "test_1", 0);
    assert_non_null(ifsetting_instance);

    ifsetting_log_accept_out_status = amxd_object_get_param_value(ifsetting_instance, "LOGAcceptOutgoingConnectionStatus");
    assert_non_null(ifsetting_log_accept_out_status);
    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Enabled");

    print_status("Modify LOGAcceptOutgoingConnection=false");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_DISABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Disabled");

    print_status("Modify LOGAcceptOutgoingConnection=true");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    rv = modify_ifsetting(index, ICMP_BOTH, SPOOFING_DISABLE, STEALTH_MODE_ENABLE, ACCEPT_TR_DISABLE, false, LOG_IN_DROP_DISABLE, LOG_OUT_DROP_DISABLE, LOG_OUT_ACCEPT_ENABLE);
    assert_int_equal(rv, 0);

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Enable to disable\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN,All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT,All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.AllowedOutbound.Enable to disable\n");
    amxd_trans_select_object(&trans, log_object_allowed);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN]:");
    delete_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_OUT_CONN]:");
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Disabled");

    print_message("Change Firewall.Log.All.Enable to enable\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);

    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV4_OUTPUT, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(1, NULL, "eth0", false, TEST_TABLE_FILTER, CHAIN_LOG_OUT_IPV6_OUTPUT, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV4, true, 1, 8, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][ACCPT_CONN_ATMPT,All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, SERVICES_CHAIN_IPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_ACCEPT, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Filter to Allowed\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(cstring_t, &trans, "FilterPolicy", "Allowed");
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Enabled");

    print_message("Change Firewall.Log.All.Filter to All\n");
    amxd_trans_select_object(&trans, log_object_all);
    amxd_trans_set_value(cstring_t, &trans, "FilterPolicy", "All");
    assert_int_equal(amxd_trans_apply(&trans, fw_get_dm()), 0);
    amxd_trans_clean(&trans);

    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    delete_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(1, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(2, "eth0", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    handle_events();

    assert_string_equal(GET_CHAR(ifsetting_log_accept_out_status, NULL), "Enabled");
}

int setup_iface_isolation(void** state) {
    amxd_trans_t trans;

    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs.odl");

    expect_query_wan_netdevname();
    expect_query_wan_netdevname();     // for spoofing
    expect_query_wan_ipv4_addresses(); // for spoofing
    expect_query_wan_ipv6_addresses(); // for spoofing
    expect_query_wan_ipv4_addresses(); // for isolation
    expect_query_wan_ipv6_addresses(); // for isolation
    expect_query_wan_netdevname();     // for service (ping related I guess)
    expect_query_wan_netdevname();     // for service (ping6 related I guess)
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();
    expect_query_wan_netdevname();

    disable_check = true;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, "wan");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();

    disable_check = false;

    return 0;
}

int teardown_iface_isolation(void** state) {
    mock_cleanup(state);
    return 0;
}

static void lan_isolation_populate_ipv4_isolation(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "192.168.2.0/24"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP), 0);
}

static void lan_isolation_populate_ipv4_isolation_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "192.168.2.0/24"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void lan_isolation_populate_ipv4_AcceptICMPEchoRequest(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 1), 0);
    assert_int_equal(fw_rule_set_icmp_type(r, 8), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void lan_isolation_populate_ipv4_AcceptICMPEchoRequest_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 1), 0);
    assert_int_equal(fw_rule_set_icmp_type(r, 8), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void lan_isolation_populate_ipv6_AcceptICMPEchoRequest(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT6_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 58), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_icmpv6_type(r, 128), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void lan_isolation_populate_ipv6_AcceptICMPEchoRequest_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT6_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 58), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-lan"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_icmpv6_type(r, 128), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void guest_isolation_populate_ipv4_isolation_a(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP), 0);
}

static void guest_isolation_populate_ipv4_isolation_a_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void guest_isolation_populate_ipv4_isolation_b(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "192.168.3.0/24"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP), 0);
}

static void guest_isolation_populate_ipv4_isolation_b_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "192.168.3.0/24"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void guest_isolation_populate_ipv6_isolation_a(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING6_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "fc:0:0:1::/64"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP), 0);
}

static void guest_isolation_populate_ipv6_isolation_a_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING6_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "fc:0:0:1::/64"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void guest_isolation_populate_ipv6_isolation_b(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING6_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "fe80::9483:8ce9:2ae5:ca32/128"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP), 0);
}

static void guest_isolation_populate_ipv6_isolation_b_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "raw"), 0);
    assert_int_equal(fw_rule_set_chain(r, "PREROUTING6_Isolation"), 0);
    assert_int_equal(fw_rule_set_destination(r, "fe80::9483:8ce9:2ae5:ca32/128"), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void guest_isolation_populate_ipv4_AcceptICMPEchoRequest(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 1), 0);
    assert_int_equal(fw_rule_set_icmp_type(r, 8), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void guest_isolation_populate_ipv4_AcceptICMPEchoRequest_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 1), 0);
    assert_int_equal(fw_rule_set_icmp_type(r, 8), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, true), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

static void guest_isolation_populate_ipv6_AcceptICMPEchoRequest(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT6_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 58), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_icmpv6_type(r, 128), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT), 0);
}

static void guest_isolation_populate_ipv6_AcceptICMPEchoRequest_log(fw_rule_t* r) {
    assert_int_equal(fw_rule_set_table(r, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(r, "INPUT6_Services"), 0);
    assert_int_equal(fw_rule_set_protocol(r, 58), 0);
    assert_int_equal(fw_rule_set_in_interface(r, "br-guest"), 0);
    assert_int_equal(fw_rule_set_ipv4(r, false), 0);
    assert_int_equal(fw_rule_set_icmpv6_type(r, 128), 0);
    assert_int_equal(fw_rule_set_enabled(r, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(r, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(r, LOG_LEVEL, LOG_FLAGS, "[FW][All]:"), 0);
}

void test_iface_isolation(UNUSED void** state) {
    amxd_trans_t trans;

    expect_query_lan_netdevname();
    expect_query_lan_netdevname();     // for spoofing
    expect_query_lan_ipv4_addresses(); // for spoofing
    expect_query_lan_ipv6_addresses(); // for spoofing
    expect_query_lan_ipv4_addresses(); // for isolation
    expect_query_lan_ipv6_addresses(); // for isolation
    expect_query_lan_netdevname();     // for service (ping related I guess)
    expect_query_lan_netdevname();     // for service (ping6 related I guess)
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();
    expect_query_lan_netdevname();

    expect_query_guest_netdevname();
    expect_query_guest_netdevname();     // for spoofing
    expect_query_guest_ipv4_addresses(); // for spoofing
    expect_query_guest_ipv6_addresses(); // for spoofing
    expect_query_guest_ipv4_addresses(); // for isolation
    expect_query_guest_ipv6_addresses(); // for isolation
    expect_query_guest_netdevname();     // for service (ping related I guess)
    expect_query_guest_netdevname();     // for service (ping6 related I guess)
    expect_query_guest_netdevname();
    expect_query_guest_netdevname();
    expect_query_guest_netdevname();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, "lan");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3.");
    amxd_trans_set_value(cstring_t, &trans, "IsolateInterface", "Firewall.InterfaceSetting.[Alias=='guest'].");
    amxd_trans_set_value(bool, &trans, "StealthMode", true); // otherwise we have to handle fw_replace_rule like AcceptICMPEchoRequest
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    // no rules added because the IsolateInterface doesn't exist yet
    create_rule(2, "br-lan", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(7, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(8, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(7, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(8, "br-lan", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    expect_fw_replace_rule(lan_isolation_populate_ipv4_AcceptICMPEchoRequest_log, 3);
    expect_fw_replace_rule(lan_isolation_populate_ipv4_AcceptICMPEchoRequest, 4);
    expect_fw_replace_rule(lan_isolation_populate_ipv6_AcceptICMPEchoRequest_log, 3);
    expect_fw_replace_rule(lan_isolation_populate_ipv6_AcceptICMPEchoRequest, 4);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, "guest");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.4.");
    amxd_trans_set_value(cstring_t, &trans, "IsolateInterface", "Firewall.InterfaceSetting.2.");
    amxd_trans_set_value(bool, &trans, "StealthMode", true); // otherwise we have to handle fw_replace_rule like AcceptICMPEchoRequest
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(lan_isolation_populate_ipv4_isolation_log, 1);
    expect_fw_replace_rule(lan_isolation_populate_ipv4_isolation, 2);
    expect_fw_replace_rule(guest_isolation_populate_ipv4_isolation_a_log, 3);
    expect_fw_replace_rule(guest_isolation_populate_ipv4_isolation_b_log, 4);
    expect_fw_replace_rule(guest_isolation_populate_ipv4_isolation_a, 5);
    expect_fw_replace_rule(guest_isolation_populate_ipv4_isolation_b, 6); // another rule because getAddrs returns 2 ipv4 addresses for lan
    expect_fw_replace_rule(guest_isolation_populate_ipv6_isolation_a_log, 1);
    expect_fw_replace_rule(guest_isolation_populate_ipv6_isolation_b_log, 2);
    expect_fw_replace_rule(guest_isolation_populate_ipv6_isolation_a, 3);
    expect_fw_replace_rule(guest_isolation_populate_ipv6_isolation_b, 4);
    create_rule(3, "br-guest", "br-lan", false, TEST_TABLE_FILTER, IFSETTING_CHAIN_PASS_ICMPV6, false, 58, 128, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(9, "br-guest", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(10, "br-guest", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV4, true, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    create_rule(9, "br-guest", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_LAST, true, "[FW][All]:");
    create_rule(10, "br-guest", NULL, false, TEST_TABLE_FILTER, IFSETTING_CHAIN_IPV6, false, -1, -1, NULL, false, NULL, FW_RULE_POLICY_DROP, false, NULL);
    expect_fw_replace_rule(guest_isolation_populate_ipv4_AcceptICMPEchoRequest_log, 5);
    expect_fw_replace_rule(guest_isolation_populate_ipv4_AcceptICMPEchoRequest, 6);
    expect_fw_replace_rule(guest_isolation_populate_ipv6_AcceptICMPEchoRequest_log, 5);
    expect_fw_replace_rule(guest_isolation_populate_ipv6_AcceptICMPEchoRequest, 6);
    handle_events();

    // test what happens if IsolateInterface changes (and use index instead of Alias)
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.lan.");
    amxd_trans_set_value(cstring_t, &trans, "IsolateInterface", "Firewall.InterfaceSetting.3.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(lan_isolation_populate_ipv4_isolation_log, 1);
    expect_fw_delete_rule(lan_isolation_populate_ipv4_isolation, 1);
    expect_fw_replace_rule(lan_isolation_populate_ipv4_isolation_log, 1);
    expect_fw_replace_rule(lan_isolation_populate_ipv4_isolation, 2);
    handle_events();

    // test self reference
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.lan.");
    amxd_trans_set_value(cstring_t, &trans, "IsolateInterface", "Firewall.InterfaceSetting.[Alias=='lan'].");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(lan_isolation_populate_ipv4_isolation_log, 1);
    expect_fw_delete_rule(lan_isolation_populate_ipv4_isolation, 1);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.guest.");
    amxd_trans_set_value(cstring_t, &trans, "IsolateInterface", "");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(guest_isolation_populate_ipv4_isolation_a_log, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv4_isolation_b_log, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv4_isolation_a, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv4_isolation_b, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv6_isolation_a_log, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv6_isolation_b_log, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv6_isolation_a, 1);
    expect_fw_delete_rule(guest_isolation_populate_ipv6_isolation_b, 1);
    handle_events();
}
