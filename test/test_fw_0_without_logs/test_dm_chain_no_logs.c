/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_chain_no_logs.h"

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};

static void a_populate_filter_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_source(rule, "10.0.0.1/32"), 0);
    assert_int_equal(fw_rule_set_source_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_source_port(rule, 7000), 0);
    assert_int_equal(fw_rule_set_source_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_in_interface_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination(rule, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_destination_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_dscp_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void b_populate_filter_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_source_mac_address(rule, "aa:bb:cc:dd:ee:ff"), 0);
    assert_int_equal(fw_rule_set_source_mac_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

int test_chain_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int test_chain_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_chain_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;
    amxd_object_t* chain = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    expect_query_lan_netdevname();
    expect_query_wan_netdevname();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "FORWARD_L_Low"); // determines which chain is used for the rules: FORWARD_L_Low
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // 1. enable rule, Status == Disabled because chain is not enabled
    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.1");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.1.0/24");
    amxd_trans_set_value(bool, &trans, "DestIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(bool, &trans, "DestPortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "10.0.0.1");
    amxd_trans_set_value(bool, &trans, "SourceIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", 7000);
    amxd_trans_set_value(bool, &trans, "SourcePortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "DestInterface", "Device.IP.Interface.3.");   // LAN
    amxd_trans_set_value(bool, &trans, "DestInterfaceExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(bool, &trans, "SourceInterfaceExclude", true);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(bool, &trans, "DSCPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    // 2. enable chain, rule Status == Enabled
    chain = amxd_dm_findf(dm, "Firewall.Chain.1.");
    assert_non_null(chain);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, chain);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    // 3. disable when Status == Enabled
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_source_mac(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("add Firewall.Chain.1.Rule.2 with SourceMAC aa:bb:cc:dd:ee:ff, expect status to be enabled\n");
    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.2");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMAC", "aa:bb:cc:dd:ee:ff");
    amxd_trans_set_value(bool, &trans, "SourceMACExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(b_populate_filter_rule, 1);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("delete the instance, expect the rule to be removed\n");
    expect_fw_delete_rule(b_populate_filter_rule, 1);
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 2, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxc_var_clean(&params);
}

static const char* _jstring_filter_rule(const char* src, const char* dest) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_L_Low\","
             "\"enable\":true,"
             "\"ipv6\":false,"
             "\"destination\":\"%s\","
             "\"destination_exclude\":false,"
             "\"source\":\"%s\","
             "\"source_exclude\":false,"
             "\"target\":6,"
             "\"target_option\":1"
             "}",
             dest,
             src
             );
    return _buf;
}

static const char* jstring_filter_rule(const char* src, const char* dest) {
    return _jstring_filter_rule(src, dest);
}

void test_rule_ip_or_mask(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceIP", "192.168.1.2/32");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "192.168.1.0/24");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.2.1/32");
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.2.0/24");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.0/24"), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.1/32"), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "192.168.1.0/24");
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.2.0/24");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.1/32"), 1);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_chain_add_log_rule(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    expect_query_lan_netdevname();
    expect_query_wan_netdevname();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.rule-99");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.1.0/24");
    amxd_trans_set_value(bool, &trans, "DestIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(bool, &trans, "DestPortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "10.0.0.1");
    amxd_trans_set_value(bool, &trans, "SourceIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", 7000);
    amxd_trans_set_value(bool, &trans, "SourcePortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "DestInterface", "Device.IP.Interface.3.");   // LAN
    amxd_trans_set_value(bool, &trans, "DestInterfaceExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(bool, &trans, "SourceInterfaceExclude", true);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(bool, &trans, "DSCPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(cstring_t, &trans, "Target", "Log");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    // 3. disable when Status == Enabled
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}
