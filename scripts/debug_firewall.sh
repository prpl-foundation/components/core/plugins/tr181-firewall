#!/bin/sh

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG

show_title "show firewall information"
show_cmd $SUDO iptables -L -t filter -nv
show_cmd $SUDO iptables -L -t nat -nv
show_cmd $SUDO iptables -L -t mangle -nv
show_cmd $SUDO iptables -L -t raw -nv

show_cmd $SUDO ebtables -t filter -L
show_cmd $SUDO ebtables -t nat -L
show_cmd $SUDO ebtables -t broute -L

show_cmd $SUDO ip6tables -L -t filter -nv
show_cmd $SUDO ip6tables -L -t mangle -nv
show_cmd $SUDO ip6tables -L -t raw -nv

