include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C odl clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vars.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_vars.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_log.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_log.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_netfilter.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_netfilter.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_validators.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_validators.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)
	$(foreach odl,$(wildcard odl/firewall/feature/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/;)
	$(INSTALL) -D -p -m 0644 odl/firewall/firewall_definition.odl $(DEST)/etc/amx/$(COMPONENT)/firewall_definition.odl
	$(INSTALL) -D -p -m 0644 odl/firewall/firewall_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-firewall_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/firewall/firewall_synced.odl $(DEST)/etc/amx/$(COMPONENT)/extensions/firewall_synced.odl
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/extensions
	$(foreach odl,$(wildcard odl/firewall/feature/log/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/extensions/;)
endif
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_DEBUG_PROFILING),y)
	$(INSTALL) -D -p -m 0644 odl/firewall/profiling_definition.odl $(DEST)/etc/amx/$(COMPONENT)/extensions/profiling_definition.odl
endif
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)
	$(foreach odl,$(wildcard odl/nat/feature/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/;)
	$(INSTALL) -D -p -m 0644 odl/nat/nat_definition.odl $(DEST)/etc/amx/$(COMPONENT)/nat_definition.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 odl/nat/nat_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-nat_mapping.odl
endif
ifeq ($(and $(CONFIG_GATEWAY),$(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS)),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/extensions
	$(foreach odl,$(wildcard odl/nat/feature/log/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/extensions/;)
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.gw.d
	$(foreach odl,$(wildcard odl/defaults.gw.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.gw.d/;)
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.ap.d
	$(foreach odl,$(wildcard odl/defaults.ap.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.ap.d/;)
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/$(COMPONENT)
	ln -sfr $(DEST)/etc/amx/$(COMPONENT)/defaults.gw.d $(DEST)/etc/amx/$(COMPONENT)/defaults.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/$(COMPONENT)
	ln -sfr $(DEST)/etc/amx/$(COMPONENT)/defaults.ap.d $(DEST)/etc/amx/$(COMPONENT)/defaults.d
endif
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS),y)
	$(INSTALL) -D -p -m 0644 odl/defaults-optional/00_log_defaults.odl $(DEST)/etc/amx/$(COMPONENT)/defaults.d/00_log_defaults.odl
endif
ifeq ($(and $(CONFIG_GATEWAY),$(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS)),y)
	$(INSTALL) -D -p -m 0644 odl/defaults-optional/99_interfacesetting_log_defaults.odl $(DEST)/etc/amx/$(COMPONENT)/defaults.d/99_interfacesetting_log_defaults.odl
endif
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/debug_firewall.sh $(DEST)/usr/lib/debuginfo/debug_firewall.sh
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/50-disable-ip-forward.sh $(DEST)/etc/uci-defaults/50-disable-ip-forward.sh
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 config/iptables.gw.d/firewall.defaults $(DEST)/etc/amx/$(COMPONENT)/iptables.gw.d/firewall.defaults
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 config/iptables.gw.d/firewall_ipv6.defaults $(DEST)/etc/amx/$(COMPONENT)/iptables.gw.d/firewall_ipv6.defaults
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -D -p -m 0644 config/iptables.ap.d/firewall.defaults $(DEST)/etc/amx/$(COMPONENT)/iptables.ap.d/firewall.defaults
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -D -p -m 0644 config/iptables.ap.d/firewall_ipv6.defaults $(DEST)/etc/amx/$(COMPONENT)/iptables.ap.d/firewall_ipv6.defaults
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/$(COMPONENT)
	ln -sfr $(DEST)/etc/amx/$(COMPONENT)/iptables.gw.d $(DEST)/etc/amx/$(COMPONENT)/iptables.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/$(COMPONENT)
	ln -sfr $(DEST)/etc/amx/$(COMPONENT)/iptables.ap.d $(DEST)/etc/amx/$(COMPONENT)/iptables.d
endif
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/rules6
	$(INSTALL) -D -p -m 0644 config/ipv6_features/*.firewall $(DEST)/etc/amx/$(COMPONENT)/rules6/

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vars.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_vars.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_log.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_log.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_netfilter.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_netfilter.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_validators.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_validators.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 odl/firewall/feature/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/
	$(INSTALL) -D -p -m 0644 odl/firewall/firewall_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/firewall_definition.odl
	$(INSTALL) -D -p -m 0644 odl/firewall/firewall_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-firewall_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/firewall/firewall_synced.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/firewall_synced.odl
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/extensions
	$(INSTALL) -D -p -m 0644 odl/firewall/feature/log/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/
endif
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_DEBUG_PROFILING),y)
	$(INSTALL) -D -p -m 0644 odl/firewall/profiling_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/profiling_definition.odl
endif
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 odl/nat/feature/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/
	$(INSTALL) -D -p -m 0644 odl/nat/nat_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/nat_definition.odl
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 odl/nat/nat_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-nat_mapping.odl
endif
ifeq ($(and $(CONFIG_GATEWAY),$(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS)),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/extensions
	$(INSTALL) -D -p -m 0644 odl/nat/feature/log/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.gw.d
	$(INSTALL) -D -p -m 0644 odl/defaults.gw.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.gw.d/
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.ap.d
	$(INSTALL) -D -p -m 0644 odl/defaults.ap.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.ap.d/
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/$(COMPONENT)
	rm -f $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
	ln -sfr $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.gw.d $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/$(COMPONENT)
	rm -f $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
	ln -sfr $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.ap.d $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d
endif
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS),y)
	$(INSTALL) -D -p -m 0644 odl/defaults-optional/00_log_defaults.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/00_log_defaults.odl
endif
ifeq ($(and $(CONFIG_GATEWAY),$(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS)),y)
	$(INSTALL) -D -p -m 0644 odl/defaults-optional/99_interfacesetting_log_defaults.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/99_interfacesetting_log_defaults.odl
endif
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/debug_firewall.sh $(PKGDIR)/usr/lib/debuginfo/debug_firewall.sh
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0755 scripts/50-disable-ip-forward.sh $(PKGDIR)/etc/uci-defaults/50-disable-ip-forward.sh
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 config/iptables.gw.d/firewall.defaults $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.gw.d/firewall.defaults
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -D -p -m 0644 config/iptables.gw.d/firewall_ipv6.defaults $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.gw.d/firewall_ipv6.defaults
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -D -p -m 0644 config/iptables.ap.d/firewall.defaults $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.ap.d/firewall.defaults
endif
ifeq ($(CONFIG_ACCESSPOINT),y)
	$(INSTALL) -D -p -m 0644 config/iptables.ap.d/firewall_ipv6.defaults $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.ap.d/firewall_ipv6.defaults
endif
ifeq ($(CONFIG_GATEWAY),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/$(COMPONENT)
	rm -f $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.d
	ln -sfr $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.gw.d $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.d
endif
ifeq ($(and $(CONFIG_ACCESSPOINT),$(if $(CONFIG_GATEWAY),,y)),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/$(COMPONENT)
	rm -f $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.d
	ln -sfr $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.ap.d $(PKGDIR)/etc/amx/$(COMPONENT)/iptables.d
endif
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/rules6
	$(INSTALL) -D -p -m 0644 config/ipv6_features/*.firewall $(PKGDIR)/etc/amx/$(COMPONENT)/rules6/
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/firewall/feature/*.odl))
	$(eval ODLFILES += odl/firewall/firewall_definition.odl)
ifeq ($(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS),y)
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/firewall/feature/log/*.odl))
endif
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/nat/feature/*.odl))
	$(eval ODLFILES += odl/nat/nat_definition.odl)
ifeq ($(and $(CONFIG_GATEWAY),$(CONFIG_SAH_AMX_TR181_FIREWALL_LOGS)),y)
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/nat/feature/log/*.odl))
endif

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test