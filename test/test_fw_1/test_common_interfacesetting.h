/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_COMMON_INTERFACE_SETTING_H__
#define __TEST_COMMON_INTERFACE_SETTING_H__

#define ICMP_DISABLE "disable"
#define ICMP_IPV4    "allow_ipv4"
#define ICMP_IPV6    "allow_ipv6"
#define ICMP_BOTH    "allow_both"

#define SPOOFING_DISABLE "none"
#define SPOOFING_IPV4    "ipv4"
#define SPOOFING_IPV6    "ipv6"
#define SPOOFING_BOTH    "both"

#define FIREWALL_ICMP_ALLOW  "Allow"
#define FIREWALL_ICMP_REJECT "Reject"
#define FIREWALL_ICMP_ERROR  "Error"

#define SERVICES_CHAIN_IPV4  "INPUT_Services"
#define SERVICES_CHAIN_IPV6  "INPUT6_Services"
#define IFSETTING_CHAIN_IPV4 "INPUT_InterfaceSettings"
#define IFSETTING_CHAIN_IPV6 "INPUT6_InterfaceSettings"
#define IFSETTING_CHAIN_SPOOF_IPV4 "PREROUTING_Spoofing"
#define IFSETTING_CHAIN_SPOOF_IPV6 "PREROUTING6_Spoofing"
#define IFSETTING_CHAIN_PASS_ICMPV6 "FORWARD6_InterfaceSettings"
#define CHAIN_LOG_IN_IPV4_INPUT "INPUT_Last_Log"
#define CHAIN_LOG_IN_IPV6_INPUT "INPUT6_Last_Log"
#define CHAIN_LOG_IN_IPV4_FORWARD "FORWARD_Last_Log"
#define CHAIN_LOG_IN_IPV6_FORWARD "FORWARD6_Last_Log"
#define CHAIN_LOG_OUT_IPV4_OUTPUT "OUTPUT_Last_Log"
#define CHAIN_LOG_OUT_IPV6_OUTPUT "OUTPUT6_Last_Log"

#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX "[FW][BLK_CONN_ATMPT]:"

#define STEALTH_MODE_ENABLE true
#define STEALTH_MODE_DISABLE false
#define ACCEPT_TR_ENABLE true
#define ACCEPT_TR_DISABLE false
#define LOG_IN_DROP_ENABLE true
#define LOG_IN_DROP_DISABLE false
#define LOG_OUT_DROP_ENABLE true
#define LOG_OUT_DROP_DISABLE false
#define LOG_OUT_ACCEPT_ENABLE true
#define LOG_OUT_ACCEPT_DISABLE false

void ut_ifsetting_populate_rule(fw_rule_t* rule);
void populate_create_rule(const char* interface, const char* out_interface, bool excluded_interface,
                          const char* table, const char* chain, bool ipv4, int protocol, int icmp_type,
                          const char* address, bool excluded_address, const char* netmask, fw_rule_policy_t policy, bool log,
                          const char* log_prefix);
void populate_delete_rule(const char* interface, const char* out_interface, bool excluded_interface,
                          const char* table, const char* chain, bool ipv4, int protocol, int icmp_type,
                          const char* address, bool excluded_address, const char* netmask, fw_rule_policy_t policy, bool log,
                          const char* log_prefix);

#define create_rule(index, interface, out_interface, excluded_interface, \
                    table, chain, ipv4, protocol, icmp_type, \
                    address, excluded_address, netmask, policy, log, log_prefix) do { \
        populate_create_rule(interface, out_interface, excluded_interface, table, chain, ipv4, protocol, icmp_type, address, excluded_address, netmask, policy, log, log_prefix); \
        expect_fw_replace_rule(ut_ifsetting_populate_rule, index); \
}while(0)

#define delete_rule(index, interface, out_interface, excluded_interface, \
                    table, chain, ipv4, protocol, icmp_type, \
                    address, excluded_address, netmask, policy, log, log_prefix) do { \
        populate_delete_rule(interface, out_interface, excluded_interface, table, chain, ipv4, protocol, icmp_type, address, excluded_address, netmask, policy, log, log_prefix); \
        expect_fw_delete_rule(ut_ifsetting_populate_rule, index); \
} while(0)

amxo_parser_t* get_parser(void);
amxd_dm_t* get_dm(void);
int add_ifsetting(const char* alias, const char* interface, const char* icmp_req, const char* spoofing, bool stealth_mode, bool accept_udp_tr, bool passthrough_icmpv6, bool log_in_drop, bool log_out_drop, bool log_out_accept);
int modify_ifsetting(uint32_t instance_index, const char* icmp_req, const char* spoofing, bool stealth_mode, bool accept_udp_tr, bool passthrough_icmpv6, bool log_in_drop, bool log_out_drop, bool log_out_accept);
void delete_ifsetting(const char* alias);
int modify_netmodel_query(const char* query_path, const char* ip);
void common_test_teardown(void);

#endif // __TEST_COMMON_INTERFACE_SETTING_H__
