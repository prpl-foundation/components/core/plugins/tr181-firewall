# tr181-firewall

[TOC]

## variable naming conventions
### allowed meaningless names
- `object` is used for `amxd_object_t*` inside functions in combination with `OBJECT_NAMED`, it must be the important/subject object of a function
- `templ` is used for `amxd_object_t*` and must be the parent template of `object`
- `var`, `var2` are used for `amxc_var_t*` when:
  - inside `dm:object-changed` handler if it is used to see if a parameter is in the event's data because otherwise the number of local variables can become big and the name adds litle value since `var` is only used for a short lifespan until any of the `GET_*` macros is called.
  - as iterator of `amxc_var_for_each(var, ...)`

### avoid names
Because they can mean many things without having context.
- `interface`
  - TR181 reference path, e.g. "Device.IP.Interface.1."
  - "NetModel.Intf.{i}."
  - Network device, e.g. "eth0"
  - ...
- `rule`
  - Firewall.Chain.{i}.Rule.{i}.
  - NAT.PortTrigger.{i}.Rule.{i}.
  - libfwrule's rule
  - rule in Netfiler (iptables) chain
  - ...

## compile and install
```
make
sudo make install
```

## run test
```
make test
```

The unit tests mock some dependencies like Netmodel. Lib_fwinterface is also mocked
so the tests won't add actual Netfilter rules. Lib_fwrules isn't mocked because it 
is a core part of the behavior. The tests are organized by topic.

could be improved:
- add netmodel query events

### debugging with gdb
some useful gdb commands
```
# dump amxc_var_t*
(gdb) p amxc_var_dump(..., 1)

# dump fw_rule_t*
(gdb) p fw_rule_dump(..., 1)
```