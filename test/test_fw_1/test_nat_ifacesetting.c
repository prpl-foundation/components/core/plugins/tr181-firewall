/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include "mock.h"
#include "mock_netmodel.h"
#include "test_nat_ifacesetting.h"

static amxd_dm_t* dm = NULL;

int test_nat_interface_setting_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int test_nat_interface_setting_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

static void populate_nat_rule_without_source(fw_rule_t* rule1) {
    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "POSTROUTING_DEFAULT"), 0);
    assert_int_equal(fw_rule_set_out_interface(rule1, "eth0"), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_MASQUERADE), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void a_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
}

static void b_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
}

static void c_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
    assert_int_equal(fw_rule_set_source(rule, "192.168.2.1/24"), 0);
}

static void d_populate_nat_rule(fw_rule_t* rule) { // todo same as a?
    populate_nat_rule_without_source(rule);
}

static void e_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
    assert_int_equal(fw_rule_set_source(rule, "192.168.20.1/24"), 0);
}

static void f_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
    assert_int_equal(fw_rule_set_source(rule, "192.168.21.1/24"), 0);
}

static void h_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
    assert_int_equal(fw_rule_set_source(rule, "192.168.21.2/24"), 0);
}

static void g_populate_nat_rule(fw_rule_t* rule) {
    populate_nat_rule_without_source(rule);
    assert_int_equal(fw_rule_set_source(rule, "192.168.3.1/24"), 0);
}

void test_nat_rule_without_source(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* instance = NULL;
    amxc_var_t params;

    expect_query_wan_netdevname();
    expect_query_wan_addresses();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add instance but since Enable is false no NAT rule will be added\n");
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    instance = amxd_dm_findf(dm, "NAT.InterfaceSetting.1.");
    assert_non_null(instance);
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("enable the instance, Interface is not set so Status will be Error_Misconfigured\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error_Misconfigured");

    print_message("set Interface, status should be Enabled and a NAT rule will be added\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("remove the instance, the NAT rule must also be removed\n");
    expect_fw_delete_rule(a_populate_nat_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    // cleanup
    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_nat_rule_with_source(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* instance = NULL;
    amxd_object_t* instance2 = NULL;
    amxd_object_t* ip_instance = NULL;
    amxc_var_t params;
    amxc_set_t* flags = mocknm_get_current_flags();

    assert_int_equal(amxc_set_get_count(flags, NULL), 0);

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    ip_instance = amxd_dm_findf(dm, "IP.Interface.20.IPv4Address.");
    assert_non_null(ip_instance);

    print_message("add instance of 'NAT.InterfaceSetting.', no NAT rule will be added because Enable == false\n");
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, "iface_setting");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    instance = amxd_dm_findf(dm, "NAT.InterfaceSetting.2.");
    assert_non_null(instance);
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    expect_query_lan_ipv4_addresses();

    print_message("add a NATInterface instance (alias=iface-1), no NAT rule will be added because Enable == false\n");
    amxd_trans_clean(&trans);
    instance2 = amxd_dm_findf(dm, "NAT.InterfaceSetting.iface_setting.NATInterface.");
    assert_non_null(instance2);
    amxd_trans_select_object(&trans, instance2);
    amxd_trans_add_inst(&trans, 0, "iface-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3."); // LAN
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    expect_query_wan_netdevname();
    expect_query_wan_addresses();

    print_message("enable InterfaceSetting, 'NATInterface.iface-1.' has 2 ip addresses so 2 NAT rules will be added: [192.168.1.0/24, 192.168.3.0/24]\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_nat_rule, 2);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_true(amxc_set_has_flag(flags, TEST_TABLE_NAT));
    assert_true(amxc_set_has_flag(flags, "nat-up"));

    print_message("disable InterfaceSetting, the 2 NAT rules will be removed\n");
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(g_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);  // disable instead of delete to see, with valgrind, if plugin handles cleanup at stop (memory still reachable)
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");
    assert_int_equal(amxc_set_get_count(flags, NULL), 0);

    expect_query_wan_netdevname();
    expect_query_wan_addresses();

    print_message("enable InterfaceSetting again, the 2 NAT rules will be added again\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_nat_rule, 2);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_true(amxc_set_has_flag(flags, "nat"));
    assert_true(amxc_set_has_flag(flags, "nat-up"));

    expect_query_guest_ipv4_addresses();

    print_message("add a second NATInterface (alias=iface-2) for subnet 192.168.2.0/24, 3 NAT rules will be active, one for each subnet\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance2);
    amxd_trans_add_inst(&trans, 0, "iface-2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.4."); // Guest
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(g_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_nat_rule, 2);
    expect_fw_replace_rule(c_populate_nat_rule, 3);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("delete 'NATInterface.iface-2.', the NAT rule with subnet 192.168.2.0/24 will be removed\n");
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(g_populate_nat_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_nat_rule, 2);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance2);
    amxd_trans_del_inst(&trans, 0, "iface-2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_query_guest_ipv4_addresses();

    print_message("add a second NATInterface (alias=iface-2) for subnet 192.168.2.0/24, now 3 NAT rules will be active, one for each subnet\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance2);
    amxd_trans_add_inst(&trans, 0, "iface-2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.4."); // Guest
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(g_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_nat_rule, 2);
    expect_fw_replace_rule(c_populate_nat_rule, 3);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("delete 'NATInterface.iface-1.', the NAT rules with subnet [192.168.1.0/24, 192.168.3.0/24] will be removed\n");
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(g_populate_nat_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance2);
    amxd_trans_del_inst(&trans, 0, "iface-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_query_lan_ipv4_addresses();

    print_message("change Interface of 'NATInterface.iface-2.' to subnet 192.168.1.0/24, the NAT rule will change subnet\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.iface_setting.NATInterface.iface-2.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3."); // LAN
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_nat_rule, 2);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change Interface of 'NATInterface.iface-2.' to 'Device.IP.Interface.20.IPv4Address.1.', the NAT rule will change subnet to 192.168.20.0/24\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.iface_setting.NATInterface.iface-2.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.20.IPv4Address.1.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(g_populate_nat_rule, 1);
    expect_fw_replace_rule(e_populate_nat_rule, 1);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change 'IP.Interface.20.IPv4Address.1.IPAddress' to 192.168.21.1, the NAT rule will change subnet to 192.168.21.0/24\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.20.IPv4Address.1.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.21.1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(e_populate_nat_rule, 1);
    expect_fw_replace_rule(f_populate_nat_rule, 1);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change 'IP.Interface.20.IPv4Address.1.IPAddress' to 192.168.21.2, the NAT rule will not change because the subnet remains 192.168.21.0/24\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.20.IPv4Address.1.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.21.2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(f_populate_nat_rule, 1);
    expect_fw_replace_rule(h_populate_nat_rule, 1);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("remove instance 'IP.Interface.20.IPv4Address.1.', the NAT rule with subnet 192.168.21.0/24 is removed\n");
    expect_fw_delete_rule(h_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, ip_instance);
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error_Misconfigured");
    assert_true(amxc_set_has_flag(flags, "nat"));
    assert_false(amxc_set_has_flag(flags, "nat-up"));

    print_message("add instance 'IP.Interface.20.IPv4Address.1.', a NAT rule with subnet 192.168.21.0/24 is added\n");
    expect_fw_replace_rule(f_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, ip_instance);
    amxd_trans_add_inst(&trans, 1, NULL); // index should be 1 because that is what NATInterface subscribed for
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.21.1");
    amxd_trans_set_value(uint8_t, &trans, "PrefixLen", 24);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_true(amxc_set_has_flag(flags, "nat"));
    assert_true(amxc_set_has_flag(flags, "nat-up"));

    print_message("add instance 'IP.Interface.20.IPv4Address.2.', should have no effect on NAT rules\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, ip_instance);
    amxd_trans_add_inst(&trans, 0, "extra-address");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.22.1");
    amxd_trans_set_value(uint8_t, &trans, "PrefixLen", 24);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("remove instance 'IP.Interface.20.IPv4Address.2.', should have no effect on NAT rules\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, ip_instance);
    amxd_trans_del_inst(&trans, 0, "extra-address");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change 'IP.Interface.20.IPv4Address.1.Status' to 'Disabled', the NAT rule with subnet 192.168.21.0/24 is removed\n");
    expect_fw_delete_rule(f_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.20.IPv4Address.1.");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error_Misconfigured");

    print_message("change 'IP.Interface.20.IPv4Address.1.Status' to 'Enabled', the NAT rule with subnet 192.168.21.0/24 is added\n");
    expect_fw_replace_rule(f_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.20.IPv4Address.1.");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("delete all instances of 'NATInterface.', a NAT rule without subnet is added\n");
    expect_fw_delete_rule(f_populate_nat_rule, 1);
    expect_fw_replace_rule(d_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance2);
    amxd_trans_del_inst(&trans, 0, "iface-2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("delete the InterfaceSetting instance\n");
    expect_fw_delete_rule(d_populate_nat_rule, 1);
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.InterfaceSetting.");
    amxd_trans_del_inst(&trans, 0, "iface_setting");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxc_set_get_count(flags, NULL), 0);
    instance = NULL;
    instance2 = NULL;
    ip_instance = NULL;

    // cleanup
    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}
