/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "pinhole/dm_pinhole.h"
#include "chains.h"
#include "rules.h"
#ifdef WITH_GMAP
#include "devices_query.h"
#endif
#include "logs/logs.h"
#include "firewall.h" // for fw_get_dm
#include "init_cleanup.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <schedules/schedule.h>
#include <fwinterface/interface.h>
#include <string.h>

#define ME "pinhole"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    rule_t* rule = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    rule = (rule_t*) object->priv;
    when_null_trace(rule, exit, ERROR, "Pinhole[%s] is NULL", OBJECT_NAMED);

    if(update_rule_logs(NULL, object, NULL, data, log, false)) {
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

static void schedule_updated(schedule_status_t new_status, void* userdata) {
    rule_t* rule = NULL;

    when_null(userdata, exit);
    rule = (rule_t*) userdata;
    when_null(rule, exit);

    when_true(rule->common.status == FIREWALL_ENABLED && new_status == SCHEDULE_STATUS_ACTIVE, exit);
    when_true(rule->common.status == FIREWALL_DISABLED && (new_status == SCHEDULE_STATUS_DISABLED || new_status == SCHEDULE_STATUS_STACK_DISABLED), exit);

    if(rule->common.schedule_status != new_status) {
        rule_set_enable(rule, true);              // Enable Rule for now, will be disabled again when needed
        rule->common.schedule_status = new_status;
        when_true(rule->common.flags != 0, exit); // already being configured
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    return;
}

static void pinhole_listen_schedule_updates(rule_t* rule, const char* path) {
    bool enable = false;
    amxd_object_t* object = NULL;

    when_null_trace(rule, exit, ERROR, "rule is NULL");

    object = rule->common.object;
    when_null_trace(object, exit, ERROR, "object is NULL");
    enable = amxd_object_get_value(bool, object, "Enable", NULL);
    when_false(enable, exit);

    schedule_close(&(rule->common.schedule));
    rule->common.schedule = NULL;

    if(enable && !STRING_EMPTY(path)) {
        rule->common.schedule = schedule_open(path, schedule_updated, (void*) rule);
        when_null_trace(rule->common.schedule, exit, ERROR, "Rule[%s] Could not start listening for Schedule updates", OBJECT_NAMED);
    } else if(enable && STRING_EMPTY(path) && (rule->common.schedule_status != SCHEDULE_STATUS_EMPTY)) {
        rule_set_enable(rule, true);
        rule->common.schedule_status = SCHEDULE_STATUS_EMPTY;
        when_true(rule->common.flags != 0, exit); // already being configured
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    return;
}

static rule_t* pinhole_create(amxd_object_t* object) {
    rule_t* rule = NULL;

    rule = rule_create(object, true);
    when_null(rule, exit);

    rule->class_type = FIREWALL_PINHOLE;
    rule->target = TARGET_ACCEPT;

    SAH_TRACEZ_NOTICE(ME, "Pinhole[%s] created", OBJECT_NAMED);

exit:
    return rule;
}

static const char* pinhole_netmodel_flags(ipversion_t ipversion) {
    const char* flags = NULL;

    if(ipversion == IPv4) {
        flags = "ipv4 && netdev-bound";
    } else if(ipversion == IPv6) {
        flags = "ipv6 && netdev-bound";
    } else {
        flags = "(ipv4 || ipv6) && netdev-bound";
    }

    return flags;
}

static rule_t* pinhole_init(amxd_object_t* object) {
    rule_t* rule = NULL;
    firewall_interface_t* fwiface = NULL;
    amxc_var_t params;
    const char* interface = NULL;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    rule = pinhole_create(object);
    when_null(rule, exit);

    fwiface = &rule->common.src_fwiface;

    rule_set_src_port(rule, GET_INT32(&params, "SourcePort"), GET_INT32(&params, "SourcePortRangeMax"));
    rule_set_dst_port(rule, GET_INT32(&params, "DestPort"), GET_INT32(&params, "DestPortRangeMax"));
    rule_set_destination(rule, GET_CHAR(&params, "DestIP"));
#ifdef WITH_GMAP
    rule_query_dest_address_by_mac(rule, GET_CHAR(&params, "DestMACAddress"));
#endif
    rule_set_source(rule, GET_CHAR(&params, "SourcePrefixes"));
    rule_set_protocols(rule, GET_CHAR(&params, "Protocol"));
    rule_set_enable(rule, GET_BOOL(&params, "Enable"));

    interface = GET_CHAR(&params, "Interface");
    if(!STRING_EMPTY(interface)) {
        ipversion_t ipversion = int_to_ipversion(GET_INT32(&params, "IPVersion"));
        open_netdev_name_query(interface, ipversion, pinhole_netmodel_flags(ipversion), fwiface);
    } else {
        init_firewall_interface(fwiface);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    init_rule_logs(object, rule, &rule->common.src_fwiface, NULL, false, update_rule_log_info_cb);
#endif //FIREWALL_LOGS
    pinhole_listen_schedule_updates(rule, GET_CHAR(&params, "ScheduleRef"));

exit:
    amxc_var_clean(&params);
    return rule;
}

void _pinhole_added(UNUSED const char* const sig_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));
    rule_t* rule = NULL;

    when_null(object, exit);
    rule = pinhole_init(object);
    when_null(rule, exit);

    rule_activate(rule);
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return;
}

void _pinhole_changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    rule_t* rule = NULL;
    bool update_schedule = false;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);

    rule = (rule_t*) object->priv;
    rule->common.flags = FW_MODIFIED;
    rule->common.validated = false;

    var = GET_ARG(params, "SourcePort");
    var2 = GET_ARG(params, "SourcePortRangeMax");
    if((var != NULL) || (var2 != NULL)) {
        int start = (var != NULL) ? GET_INT32(var, "to") : amxd_object_get_value(int32_t, object, "SourcePort", NULL);
        int end = (var2 != NULL) ? GET_INT32(var2, "to") : amxd_object_get_value(int32_t, object, "SourcePortRangeMax", NULL);
        rule_set_src_port(rule, start, end);
    }

    var = GET_ARG(params, "DestPort");
    var2 = GET_ARG(params, "DestPortRangeMax");
    if((var != NULL) || (var2 != NULL)) {
        int start = (var != NULL) ? GET_INT32(var, "to") : amxd_object_get_value(int32_t, object, "DestPort", NULL);
        int end = (var2 != NULL) ? GET_INT32(var2, "to") : amxd_object_get_value(int32_t, object, "DestPortRangeMax", NULL);
        rule_set_dst_port(rule, start, end);
    }

    var = GET_ARG(params, "DestIP");
    var2 = GET_ARG(params, "DestMACAddress");
    if((var != NULL) || (var2 != NULL)) {
        rule_set_destination(rule, GET_CHAR(var, "to"));
#ifdef WITH_GMAP
        rule_query_dest_address_by_mac(rule, GET_CHAR(var2, "to"));
#endif
    }

    var = GET_ARG(params, "SourcePrefixes");
    if(var != NULL) {
        rule_set_source(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Protocol");
    if(var != NULL) {
        rule_set_protocols(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        update_schedule = GET_BOOL(var, "to");
        rule_set_enable(rule, GET_BOOL(var, "to"));
    }

    var = GET_ARG(params, "Interface");
    var2 = GET_ARG(params, "IPVersion");
    if((var != NULL) || (var2 != NULL)) {
        const char* interface = (var != NULL) ? GET_CHAR(var, "to") : object_da_string(object, "Interface");
        ipversion_t ipversion = (var2 != NULL) ? int_to_ipversion(GET_INT32(var2, "to")) : object_get_ipversion(object);
        firewall_interface_t* fwiface = &rule->common.src_fwiface;
        if(!STRING_EMPTY(interface)) {
            open_netdev_name_query(interface, ipversion, pinhole_netmodel_flags(ipversion), fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }

    if(update_schedule) {
        char* schedule_ref = amxd_object_get_value(cstring_t, object, "ScheduleRef", NULL);
        pinhole_listen_schedule_updates(rule, schedule_ref);
        free(schedule_ref);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_rule_logs(rule, NULL, event_data, NULL, NULL, false);
#endif //FIREWALL_LOGS
    rule_update(rule);

exit:
    FW_PROFILING_OUT();
    return;
}

void _pinhole_schedule_changed(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), data);
    amxc_var_t* params = NULL;
    amxc_var_t* var = NULL;
    rule_t* rule = NULL;
    const char* new_schedule_ref = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);
    when_false(amxd_object_get_value(bool, object, "Enable", NULL), exit); // Not applicable if Pinhole is disabled

    params = GET_ARG(data, "parameters");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    rule = (rule_t*) object->priv;
    // don't set rule->common.flags so pinhole_listen_schedule_updates can decide to (de)activate the rule

    var = GET_ARG(params, "ScheduleRef");
    new_schedule_ref = GET_CHAR(var, "to");

    SAH_TRACEZ_INFO(ME, "Changing Pinhole ScheduleRef from '%s' to '%s'", GET_CHAR(var, "from"), new_schedule_ref);

    pinhole_listen_schedule_updates(rule, new_schedule_ref);

exit:
    FW_PROFILING_OUT();
    return;
}

static int dm_pinhole_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    rule_t* rule = NULL;

    rule = pinhole_init(object);
    when_null(rule, exit);

    rule_activate(rule);
exit:
    return amxd_status_ok;
}

void dm_pinholes_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");
    amxd_object_t* pinhole = amxd_object_findf(firewall, ".Pinhole.");
    uint32_t max_number = 0;
    amxc_string_t buf;

    amxc_string_init(&buf, 0);
    amxc_string_setf(&buf, "%sMaxPinholeNumberOfEntries", fw_get_vendor_prefix());
    max_number = amxc_var_constcast(uint32_t, amxd_object_get_param_value(firewall, amxc_string_get(&buf, 0)));
    amxc_string_clean(&buf);
    amxd_object_set_max_instances(pinhole, max_number);
    amxd_object_for_all(firewall, ".Pinhole.*.", dm_pinhole_init, NULL);

    FW_PROFILING_OUT();
}
