/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "logs/logs.h"
#include "firewall.h"           // for fw_get_dm
#include "firewall_utilities.h" // for STRING_EMPTY
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxo/amxo.h>
#include <string.h>

#define ME "firewall"

#define MAXIMUM_LENGTH_PREFIX 29

#define DEFAULT_LOG_LEVEL 7
#define DEFAULT_LOG_ENABLE_PARAMETER     "Log"
#define DEFAULT_LOG_CONTROLLER_PARAMETER "LogController"
#define DEFAULT_SRC_EXCLUDED_PARAMETER "SourceInterfaceExclude"
#define DEFAULT_DST_EXCLUDED_PARAMETER "DestInterfaceExclude"

typedef struct _log_subscriptions_ {
    amxb_subscription_t* object_changed;
    amxb_subscription_t* object_deleted;
    amxc_htable_it_t it;
} log_subscriptions_t;

bool log_get_enable(const log_t* const log) {
    if(log == NULL) {
        return false;
    }
    return log->log_info.enable;
}

void log_set_enable(log_t* log, bool enable) {
    if(log != NULL) {
        log->log_info.enable = enable;
    }
}

int log_get_level(const log_t* const log) {
    if(log == NULL) {
        return DEFAULT_LOG_LEVEL;
    }
    return log->log_info.log_level;
}

int log_get_flags(const log_t* const log) {
    if(log == NULL) {
        return 0;
    }
    return log->log_info.flags;
}

const csv_string_t log_get_prefix(const log_t* const log) {
    if(log == NULL) {
        return "";
    }
    return amxc_string_get(log->log_info.log_prefix, 0);
}

const csv_string_t log_get_src_intf(const log_t* const log) {
    if(log == NULL) {
        return NULL;
    }
    if(log->src_interface == NULL) {
        return NULL;
    }
    return log->src_interface->default_netdevname;
}

const csv_string_t log_get_dst_intf(const log_t* const log) {
    if(log == NULL) {
        return NULL;
    }
    if(log->dst_interface == NULL) {
        return NULL;
    }
    return log->dst_interface->default_netdevname;
}

amxd_object_t* log_get_object(const log_t* const log) {
    if(log == NULL) {
        return NULL;
    }
    return log->object;
}

static const char* fw_get_log_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(fw_get_parser(), "prefix_fw_log");
    const char* prefix = amxc_var_constcast(cstring_t, setting);
    return (prefix != NULL) ? prefix : "";
}

static void add_last_dot_to_path(amxc_string_t* path_string) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    int string_size = 0;

    when_null(path_string, out);
    string_size = amxc_string_text_length(path_string);
    when_true(string_size == 0, out);

    if(strcmp(amxc_string_get(path_string, string_size - 1), ".") != 0) {
        amxc_string_appendf(path_string, ".");
    }
out:
    FW_PROFILING_OUT();
    return;
}

static const amxc_var_t* get_parameter(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter, const cstring_t default_parameter) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_string_t param_string;
    amxc_string_t param_string_vendor;
    amxc_var_t* params = NULL;
    const amxc_var_t* var = NULL;

    amxc_string_init(&param_string, 0);
    amxc_string_init(&param_string_vendor, 0);

    if(STRING_EMPTY(parameter)) {
        when_str_empty_trace(default_parameter, out, ERROR, "The default parameter is empty.");
        amxc_string_setf(&param_string_vendor, "%s%s", fw_get_vendor_prefix(), default_parameter);
        amxc_string_setf(&param_string, "%s", default_parameter);
    } else {
        amxc_string_setf(&param_string_vendor, "%s%s", fw_get_vendor_prefix(), parameter);
        amxc_string_setf(&param_string, "%s", parameter);
    }

    if(changed_params != NULL) {
        params = GET_ARG(changed_params, "parameters");
    }

    if(params != NULL) {
        var = GET_ARG(changed_params, amxc_string_get(&param_string_vendor, 0));
    }

    when_not_null_status(var, out, var = GET_ARG(var, "to"));
    var = GET_ARG(changed_params, amxc_string_get(&param_string, 0));
    when_not_null_status(var, out, var = GET_ARG(var, "to"));

    when_null_trace(object, out, ERROR, "Object is NULL");
    var = amxd_object_get_param_value(object, amxc_string_get(&param_string_vendor, 0));
    when_not_null(var, out);
    var = amxd_object_get_param_value(object, amxc_string_get(&param_string, 0));

out:
    amxc_string_clean(&param_string);
    amxc_string_clean(&param_string_vendor);
    FW_PROFILING_OUT();
    return var;
}

bool log_get_dm_enable_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool entry_enabled = false;
    const amxc_var_t* var = NULL;

    var = get_parameter(object, changed_params, parameter, DEFAULT_LOG_ENABLE_PARAMETER);
    when_null(var, out);

    entry_enabled = GET_BOOL(var, NULL);

out:
    FW_PROFILING_OUT();
    return entry_enabled;
}

const csv_string_t log_get_dm_controller_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const csv_string_t log_controllers = NULL;
    const amxc_var_t* var = NULL;

    var = get_parameter(object, changed_params, parameter, DEFAULT_LOG_CONTROLLER_PARAMETER);
    when_null(var, out);

    log_controllers = GET_CHAR(var, NULL);

out:
    FW_PROFILING_OUT();
    return log_controllers;
}

bool log_get_dm_src_excl_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool excluded = false;
    const amxc_var_t* var = NULL;

    var = get_parameter(object, changed_params, parameter, DEFAULT_SRC_EXCLUDED_PARAMETER);
    when_null(var, out);

    excluded = GET_BOOL(var, NULL);

out:
    FW_PROFILING_OUT();
    return excluded;
}

bool log_get_dm_dst_excl_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool excluded = false;
    const amxc_var_t* var = NULL;

    var = get_parameter(object, changed_params, parameter, DEFAULT_DST_EXCLUDED_PARAMETER);
    when_null(var, out);

    excluded = GET_BOOL(var, NULL);

out:
    FW_PROFILING_OUT();
    return excluded;
}

void log_set_policy(log_t* log, target_t target) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null_trace(log, out, ERROR, "Log struct is NULL");

    switch(target) {
    case TARGET_ACCEPT:
        log->rule_policy = LOG_FILTER_ALLOWED;
        break;
    case TARGET_DROP:
    case TARGET_REJECT:
        log->rule_policy = LOG_FILTER_DENIED;
        break;
    default:
        log->rule_policy = LOG_FILTER_ALL;
        break;
    }

out:
    FW_PROFILING_OUT();
    return;
}

static bool update_old_log_info(log_t* log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    log_info_t* curr_log = NULL;
    log_info_t* old_log = NULL;
    bool changed = false;

    when_null(log, out);

    curr_log = &(log->log_info);
    old_log = &(log->old_log_info);

    when_null(curr_log, out);
    when_null(old_log, out);

    if(old_log->enable != curr_log->enable) {
        old_log->enable = curr_log->enable;
        changed = true;
    }

    if(old_log->flags != curr_log->flags) {
        old_log->flags = curr_log->flags;
        changed = true;
    }

    if(old_log->log_level != curr_log->log_level) {
        old_log->log_level = curr_log->log_level;
        changed = true;
    }

    if(strcmp(amxc_string_get(old_log->log_prefix, 0), amxc_string_get(curr_log->log_prefix, 0)) != 0) {
        amxc_string_copy(old_log->log_prefix, curr_log->log_prefix);
        changed = true;
    }

    if(!is_var_equal(old_log->log_controllers, curr_log->log_controllers)) {
        amxc_var_copy(old_log->log_controllers, curr_log->log_controllers);
    }

out:
    FW_PROFILING_OUT();
    return changed;
}

static int subscribe_log(log_t* log, const cstring_t path) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    amxc_string_t expression_delete;
    amxc_string_t expression_changed;
    amxc_string_t log_path;
    amxc_string_t path_string;
    amxc_htable_it_t* it_subscriptions = NULL;
    log_subscriptions_t* subscriptions = NULL;
    cstring_t object_path = NULL;
    int rv = -1;

    amxc_string_init(&expression_delete, 0);
    amxc_string_init(&expression_changed, 0);
    amxc_string_init(&log_path, 0);
    amxc_string_init(&path_string, 0);

    when_null_trace(log, out, ERROR, "Log struct is NULL");
    when_str_empty_trace(path, out, ERROR, "Log instance's path is an empty string");
    when_null_trace(log->slot_cb, out, ERROR, "The callback function for the logs is NULL.");
    when_null_trace(log->subscriptions, out, ERROR, "Hash table with subscriptions is NULL");

    object = log->object;
    object_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    when_null_trace(object, out, ERROR, "%s - object is NULL", object_path);

    amxc_string_setf(&log_path, "Firewall.%sLog.", fw_get_vendor_prefix());
    amxc_string_setf(&path_string, "%s", path);
    add_last_dot_to_path(&path_string);

    it_subscriptions = amxc_htable_get(log->subscriptions, amxc_string_get(&path_string, 0));
    when_not_null_status(it_subscriptions, out, rv = 0);
    subscriptions = calloc(1, sizeof(*subscriptions));

    amxc_string_setf(&expression_delete, "notification in ['dm:instance-removed']");
    rv = amxb_subscription_new(&subscriptions->object_deleted,
                               amxb_be_who_has("Firewall."),
                               amxc_string_get(&log_path, 0),
                               amxc_string_get(&expression_delete, 0),
                               log->slot_cb,
                               log);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe ['dm:instance-removed'] for %s.", amxc_string_get(&path_string, 0));
        free(subscriptions);
        goto out;
    }

    amxc_string_setf(&expression_changed, "notification in ['dm:object-changed'] && path matches '%s[0-9]+.$'", amxc_string_get(&log_path, 0));
    rv = amxb_subscription_new(&subscriptions->object_changed,
                               amxb_be_who_has("Firewall."),
                               amxc_string_get(&path_string, 0),
                               amxc_string_get(&expression_changed, 0),
                               log->slot_cb,
                               log);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe ['dm:object-changed'] for %s.", amxc_string_get(&path_string, 0));
        amxb_subscription_delete(&subscriptions->object_deleted);
        free(subscriptions);
        goto out;
    }

    amxc_htable_insert(log->subscriptions, amxc_string_get(&path_string, 0), &subscriptions->it);
    SAH_TRACEZ_INFO(ME, "Succesfully subscribed to [%s] from %s", amxc_string_get(&log_path, 0), object_path);
out:
    free(object_path);
    amxc_string_clean(&expression_delete);
    amxc_string_clean(&expression_changed);
    amxc_string_clean(&log_path);
    amxc_string_clean(&path_string);
    FW_PROFILING_OUT();
    return rv;
}

static void subscribe_all_logs(log_t* log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    log_info_t* curr_log = NULL;

    when_null_trace(log, out, ERROR, "Log struct is NULL");
    curr_log = &(log->log_info);

    amxc_var_for_each(controller, curr_log->log_controllers) {
        const cstring_t path = GET_CHAR(controller, NULL);
        cstring_t object_named_path = NULL;
        amxd_object_t* fw_log_object = NULL;

        if(STRING_EMPTY(path)) {
            SAH_TRACEZ_ERROR(ME, "The Firewall.Log. path is an empty string.");
            continue;
        }
        fw_log_object = amxd_dm_findf(fw_get_dm(), "%s", path);
        if(fw_log_object == NULL) {
            SAH_TRACEZ_INFO(ME, "The object[%s] doesn't exit.", path);
            continue;
        }
        if(amxd_object_instance != amxd_object_get_type(fw_log_object)) {
            SAH_TRACEZ_ERROR(ME, "The object[%s] is not an instance.", path);
            continue;
        }
        object_named_path = amxd_object_get_path(fw_log_object, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
        subscribe_log(log, object_named_path);
        free(object_named_path);
    }

out:
    FW_PROFILING_OUT();
    return;
}

static void unsubscribe_log(log_subscriptions_t** subscriptions) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null_trace((*subscriptions), out, ERROR, "Subscriptions struct is NULL");

    amxb_subscription_delete(&(*subscriptions)->object_deleted);
    amxb_subscription_delete(&(*subscriptions)->object_changed);
    amxc_htable_it_clean(&(*subscriptions)->it, NULL);
    free(*subscriptions);

out:
    FW_PROFILING_OUT();
    return;
}

static void unsubscribe_non_used_logs(log_t* log, const amxc_var_t* const event_data) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_string_t path_string;
    log_info_t* curr_log = NULL;
    amxc_htable_it_t* it_rm_subscription = NULL;
    log_subscriptions_t* subscriptions = NULL;
    const cstring_t notification = NULL;
    const cstring_t name = NULL;
    const cstring_t object_path = NULL;

    amxc_string_init(&path_string, 0);

    when_null_trace(log, out, ERROR, "Log struct is NULL");
    curr_log = &(log->log_info);
    when_null_trace(log->subscriptions, out, ERROR, "Hash table with subscriptions is NULL");

    if(event_data != NULL) {
        notification = GET_CHAR(event_data, "notification");
        if((!STRING_EMPTY(notification)) && (strcmp(notification, "dm:instance-removed") == 0)) {
            name = GET_CHAR(event_data, "name");
            object_path = GET_CHAR(event_data, "object");

            amxc_string_setf(&path_string, "%s%s", object_path, name);
            add_last_dot_to_path(&path_string);
            it_rm_subscription = amxc_htable_get(log->subscriptions, amxc_string_get(&path_string, 0));
            subscriptions = amxc_htable_it_get_data(it_rm_subscription, log_subscriptions_t, it);
            unsubscribe_log(&subscriptions);
        }
    }

    amxc_htable_for_each(it, log->subscriptions) {
        log_subscriptions_t* sub_to_delete = amxc_htable_it_get_data(it, log_subscriptions_t, it);
        const cstring_t path_subscription = amxc_htable_it_get_key(it);
        bool matched = false;

        if(path_subscription == NULL) {
            continue;
        }

        amxc_var_for_each(controller, curr_log->log_controllers) {
            const cstring_t path = NULL;
            if((path = GET_CHAR(controller, NULL)) == NULL) {
                continue;
            }
            if(strcmp(path, path_subscription) == 0) {
                matched = true;
                break;
            }
        }
        if(!matched) {
            unsubscribe_log(&sub_to_delete);
        }
    }

out:
    amxc_string_clean(&path_string);
    FW_PROFILING_OUT();
    return;
}

static void unsubscribe_all_logs(log_t* log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_string_t log_path;

    amxc_string_init(&log_path, 0);

    when_null_trace(log, out, ERROR, "Log struct is NULL");
    when_null(log->subscriptions, out);

    amxc_htable_for_each(it, log->subscriptions) {
        log_subscriptions_t* subscriptions = amxc_htable_it_get_data(it, log_subscriptions_t, it);
        unsubscribe_log(&subscriptions);
    }

out:
    amxc_string_clean(&log_path);
    FW_PROFILING_OUT();
    return;
}

static void log_info_clean(log_info_t* log_info) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(log_info, out);

    amxc_var_delete(&log_info->log_controllers);
    amxc_string_delete(&log_info->log_prefix);
    memset(log_info, 0, sizeof(*log_info));

out:
    FW_PROFILING_OUT();
    return;
}

void log_clean(log_t* log) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null_trace(log, out, ERROR, "Log struct is NULL");
    unsubscribe_all_logs(log);
    amxc_htable_delete(&log->subscriptions, NULL);

    log_info_clean(&(log->log_info));
    log_info_clean(&(log->old_log_info));

    log->rule_policy = 0;
    log->slot_cb = NULL;
    log->subscriptions = NULL;
    log->src_interface = NULL;
    log->dst_interface = NULL;
    log->object = NULL;

out:
    FW_PROFILING_OUT();
    return;
}

static bool filter_interface(firewall_interface_t* entry_intf, firewall_interface_t* intf, bool exclude_interface) {
    bool allow_log = false;
    amxc_string_t tmp_entry_intf;
    amxc_string_t tmp_intf;

    amxc_string_init(&tmp_intf, 0);
    amxc_string_init(&tmp_entry_intf, 0);

    when_null_status(entry_intf, out, allow_log = true);
    when_true_status(amxc_string_is_empty(&entry_intf->netmodel_interface), out, allow_log = true);
    when_null_status(intf, out, allow_log = false);
    when_true_status(amxc_string_is_empty(&intf->netmodel_interface), out, allow_log = false);

    amxc_string_setf(&tmp_entry_intf, "%s", entry_intf->default_netdevname);
    amxc_string_replace(&tmp_entry_intf, " ", "", UINT32_MAX);
    when_true_status(amxc_string_is_empty(&tmp_entry_intf), out, allow_log = false);

    amxc_string_setf(&tmp_intf, "%s", intf->default_netdevname);
    amxc_string_replace(&tmp_intf, " ", "", UINT32_MAX);
    when_true_status(amxc_string_is_empty(&tmp_intf), out, allow_log = false);

    if((strcmp(amxc_string_get(&tmp_entry_intf, 0), amxc_string_get(&tmp_intf, 0)) == 0) && (exclude_interface == false)) {
        allow_log = true;
    }

out:
    amxc_string_clean(&tmp_intf);
    amxc_string_clean(&tmp_entry_intf);
    return allow_log;
}

static bool filter_policy(const cstring_t entry_filter, log_filter_t log_policy) {
    log_filter_t entry_policy = LOG_FILTER_ALL;
    bool allow_log = false;

    if(STRING_EMPTY(entry_filter)) {
        entry_policy = LOG_FILTER_ALL;
    } else if(strcmp(entry_filter, "Denied") == 0) {
        entry_policy = LOG_FILTER_DENIED;
    } else if(strcmp(entry_filter, "Allowed") == 0) {
        entry_policy = LOG_FILTER_ALLOWED;
    }

    allow_log = (entry_policy == LOG_FILTER_ALL) || \
        ((entry_policy == LOG_FILTER_DENIED) && (log_policy == LOG_FILTER_DENIED)) || \
        ((entry_policy == LOG_FILTER_ALLOWED) && (log_policy == LOG_FILTER_ALLOWED));

    return allow_log;
}

static bool filter_log(log_t* log, amxc_var_t* type_specific_log_parameters, amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const cstring_t entry_filter = NULL;
    common_t* common_log = NULL;
    bool allow_log = false;

    when_null_trace(type_specific_log_parameters, out, ERROR, "Variant is NULL.");
    when_null_trace(object, out, ERROR, "Firewall.Log. object is NULL.")
    common_log = object->priv;
    when_null_trace(common_log, out, ERROR, "Common struct from 'Firewall.Log.%s.' is NULL", OBJECT_NAMED)

    entry_filter = GET_CHAR(type_specific_log_parameters, "FilterPolicy");

    when_false(filter_policy(entry_filter, log->rule_policy), out);

    if((log->src_interface == NULL) && log->use_log_controller_interface) {
        log->src_interface = &common_log->src_fwiface;
        log->exclude_src_interface = false;
    }
    when_false(filter_interface(&common_log->src_fwiface, log->src_interface, log->exclude_src_interface), out);

    if((log->dst_interface == NULL) && log->use_log_controller_interface) {
        log->dst_interface = &common_log->dest_fwiface;
        log->exclude_dst_interface = false;
    }
    when_false(filter_interface(&common_log->dest_fwiface, log->dst_interface, log->exclude_dst_interface), out);

    allow_log = true;
out:
    FW_PROFILING_OUT();
    return allow_log;
}

static bool get_info_from_log_controllers(log_t* log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t type_specific_log_parameters;
    bool global_enable = false;
    int32_t log_level = DEFAULT_LOG_LEVEL;
    amxc_string_t prefix;
    log_info_t* log_info = NULL;
    int length_start = 0;
    bool dont_append = false;

    amxc_string_init(&prefix, 0);

    when_null(log, out);
    log_info = &(log->log_info);
    when_null(log_info, out);

    log_info->log_level = DEFAULT_LOG_LEVEL;
    amxc_string_setf(&prefix, "%s[", fw_get_log_prefix());
    length_start = amxc_string_text_length(&prefix);
    amxc_var_for_each(controller, log_info->log_controllers) {
        const cstring_t path = GET_CHAR(controller, NULL);
        const cstring_t entry_prefix = NULL;
        amxd_object_t* fw_log_object = NULL;
        int32_t log_entry_level = DEFAULT_LOG_LEVEL;

        fw_log_object = amxd_dm_findf(fw_get_dm(), "%s", path);

        if(amxd_object_instance != amxd_object_get_type(fw_log_object)) {
            continue;
        }

        amxc_var_init(&type_specific_log_parameters);
        amxd_object_get_params(fw_log_object, &type_specific_log_parameters, amxd_dm_access_protected);

        if(GET_BOOL(&type_specific_log_parameters, "Enable")) {
            log_entry_level = GET_INT32(&type_specific_log_parameters, "Level");
            entry_prefix = GET_CHAR(&type_specific_log_parameters, "Prefix");

            if(!filter_log(log, &type_specific_log_parameters, fw_log_object)) {
                amxc_var_clean(&type_specific_log_parameters);
                continue;
            }

            if(log_entry_level < log_level) {
                log_level = log_entry_level;
            }
            global_enable = true;

            //Concatenate the prefixes
            if(STRING_EMPTY(entry_prefix)) {
                amxc_var_clean(&type_specific_log_parameters);
                continue;
            }

            //Don't add the same prefix multiple times
            if(amxc_string_search(&prefix, entry_prefix, length_start) >= length_start) {
                amxc_var_clean(&type_specific_log_parameters);
                continue;
            }

            if(!dont_append) {
                if(amxc_string_text_length(&prefix) <= (MAXIMUM_LENGTH_PREFIX - strlen(entry_prefix) - 3)) {
                    //Substract 3 from the maximum prefix length because we should count the ',' character and the string "]:" with 2 characters, thus 3 characters (or the '...').
                    if((int) amxc_string_text_length(&prefix) > length_start) {
                        amxc_string_appendf(&prefix, ",");
                    }
                    amxc_string_appendf(&prefix, "%s", entry_prefix);
                } else {
                    dont_append = true;
                    amxc_string_appendf(&prefix, "...");
                }
            }
        }
        amxc_var_clean(&type_specific_log_parameters);
    }

    if(!dont_append) {
        amxc_string_appendf(&prefix, "]:");
    }
    if(amxc_string_text_length(&prefix) > MAXIMUM_LENGTH_PREFIX) { //Should not enter here, but just in case this guarantees extra safety as it can break the system.
        amxc_string_setf(log_info->log_prefix, "[FW][]:");
    } else {
        size_t len = amxc_string_buffer_length(&prefix);
        amxc_string_push_buffer(log_info->log_prefix, amxc_string_take_buffer(&prefix), len);
    }
    log_info->log_level = log_level;

out:
    amxc_string_clean(&prefix);
    FW_PROFILING_OUT();
    return global_enable;
}

static void convert_log_controllers(const csv_string_t log_controllers, amxc_var_t* controllers) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t* tmp_controllers = NULL;
    amxc_string_t log_path;
    amxc_string_t path_list;
    int count = 0;

    amxc_var_new(&tmp_controllers);
    amxc_string_init(&log_path, 0);
    amxc_string_init(&path_list, 0);

    when_null_trace(controllers, out, ERROR, "Controllers variant is NULL");
    when_str_empty_trace(log_controllers, out, ERROR, "Log controllers is an empty string.");

    amxc_var_set(csv_string_t, tmp_controllers, log_controllers);
    amxc_var_cast(tmp_controllers, AMXC_VAR_ID_LIST);
    amxc_string_reset(&path_list);

    amxc_var_for_each(controller, tmp_controllers) {
        const cstring_t path = GET_CHAR(controller, NULL);
        cstring_t object_named_path = NULL;
        amxd_object_t* fw_log_object = NULL;

        if(count > 0) {
            amxc_string_appendf(&path_list, ",");
        }

        amxc_string_setf(&log_path, "%s", path);
        amxc_string_replace(&log_path, " ", "", UINT32_MAX);

        when_true_trace(amxc_string_is_empty(&log_path), out, ERROR, "Empty string is not a valid path.");

        fw_log_object = amxd_dm_findf(fw_get_dm(), "%s", amxc_string_get(&log_path, 0));
        if(fw_log_object == NULL) {
            add_last_dot_to_path(&log_path);
            amxc_string_appendf(&path_list, "%s", amxc_string_get(&log_path, 0));
            count++;
            continue;
        }

        object_named_path = amxd_object_get_path(fw_log_object, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
        amxc_string_appendf(&path_list, "%s", object_named_path);
        free(object_named_path);
        count++;
    }

    amxc_var_set(csv_string_t, controllers, amxc_string_get(&path_list, 0));
    amxc_var_cast(controllers, AMXC_VAR_ID_LIST);

out:
    amxc_var_delete(&tmp_controllers);
    amxc_string_clean(&log_path);
    amxc_string_clean(&path_list);
    FW_PROFILING_OUT();
    return;
}

/**
   @brief
   Update the log data

   @param[in] log Log structure.
   @param[in] log_enable Enable or disable the logs.
   @param[in] log_controllers Log controllers. It's a csv list with Firewall.Log. paths.
   @param[in] exclude_src_interface Exclude source interface.
   @param[in] exclude_dst_interface Exclude destination interface.
   @param[in] event_data Event data when the rule changes or the log controller changes (it can be both).

   @return
   true if the logs have been changed.
 */
bool log_update(log_t* log, bool log_enable,
                const csv_string_t log_controllers,
                bool exclude_src_interface,
                bool exclude_dst_interface,
                const amxc_var_t* const event_data) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t type_specific_log_parameters;
    bool global_enable = false;
    amxc_var_t* controllers = NULL;
    amxc_string_t* string_path = NULL;
    log_info_t* curr_log = NULL;
    log_info_t* old_log = NULL;
    bool old_enable = false;
    bool changed_log_info = false;
    bool update = false;

    amxc_var_init(&type_specific_log_parameters);
    amxc_var_new(&controllers);
    amxc_string_new(&string_path, 0);

    if(log_controllers != NULL) {
        when_str_empty_trace(log_controllers, out, ERROR, "Log controllers is an empty string.");
        convert_log_controllers(log_controllers, controllers);
    }

    when_null_trace(log, out, ERROR, "Log struct is NULL");
    when_null(log->subscriptions, out);
    curr_log = &(log->log_info);
    old_log = &(log->old_log_info);
    log->exclude_src_interface = exclude_src_interface;
    log->exclude_dst_interface = exclude_dst_interface;
    when_null(curr_log->log_prefix, out);
    when_null(old_log->log_prefix, out);
    when_null(curr_log->log_controllers, out);
    when_null(old_log->log_controllers, out);

    if((log_controllers != NULL) && !is_var_equal(controllers, curr_log->log_controllers)) {
        amxc_var_move(curr_log->log_controllers, controllers);
    }

    //If no Callback is sent then we don't subscribe
    if(log->slot_cb != NULL) {
        subscribe_all_logs(log);
        unsubscribe_non_used_logs(log, event_data);
    }

    global_enable = get_info_from_log_controllers(log);
    curr_log->enable = global_enable & log_enable;

    old_enable = old_log->enable;
    changed_log_info = update_old_log_info(log);
    if(curr_log->enable) {
        update = changed_log_info;
    } else {
        update = curr_log->enable != old_enable;
    }

out:
    amxc_var_clean(&type_specific_log_parameters);
    amxc_var_delete(&controllers);
    amxc_string_delete(&string_path);
    FW_PROFILING_OUT();
    return update;
}

/**
   @brief
   Init the log data

   @param[in] object Object.
   @param[in] log Log structure.
   @param[in] src_interface Source interface.
   @param[in] exclude_src_interface Exclude source interface.
   @param[in] dest_interface Destination interface.
   @param[in] exclude_dst_interface Exclude destination interface.
   @param[in] use_log_controller_interface Use the log controller's interface when the the interfaces are NULL.
   @param[in] log_enable Enable or disable the logs.
   @param[in] log_controllers Log controllers. It's a csv list with Firewall.Log. paths.
   @param[in] rule_target Rule's target.
   @param[in] slot_cb Callback function for the subscription on the Firewall.Log objects.

 */
void log_init(amxd_object_t* object,
              log_t* log,
              firewall_interface_t* src_interface,
              bool exclude_src_interface,
              firewall_interface_t* dst_interface,
              bool exclude_dst_interface,
              bool use_log_controller_interface,
              bool log_enable,
              const csv_string_t log_controllers,
              target_t rule_target,
              amxp_slot_fn_t slot_cb) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    uint32_t flags = 0;
    log_info_t* curr_log = NULL;
    log_info_t* old_log = NULL;
    bool global_enable = false;
    amxc_string_t* string_path = NULL;
    cstring_t object_path = NULL;

    amxc_string_new(&string_path, 0);

    when_null_trace(object, out, ERROR, "Object is NULL");
    when_null_trace(log, out, ERROR, "Log struct is NULL");

    object_path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);
    curr_log = &(log->log_info);
    old_log = &(log->old_log_info);

    log->object = object;
    old_log->log_level = DEFAULT_LOG_LEVEL;
    curr_log->log_level = DEFAULT_LOG_LEVEL;
    old_log->enable = false;
    curr_log->enable = false;
    old_log->flags = flags;
    curr_log->flags = flags;
    log->slot_cb = slot_cb;
    log->rule_policy = LOG_FILTER_ALL;
    log->subscriptions = NULL;
    log->use_log_controller_interface = use_log_controller_interface;
    log->src_interface = src_interface;
    log->exclude_src_interface = exclude_src_interface;
    log->dst_interface = dst_interface;
    log->exclude_dst_interface = exclude_dst_interface;

    if(curr_log->log_controllers == NULL) {
        amxc_var_new(&curr_log->log_controllers);
    }
    if(old_log->log_controllers == NULL) {
        amxc_var_new(&old_log->log_controllers);
    }
    if(curr_log->log_prefix == NULL) {
        amxc_string_new(&curr_log->log_prefix, 0);
    }
    if(old_log->log_prefix == NULL) {
        amxc_string_new(&old_log->log_prefix, 0);
    }
    if(log->subscriptions == NULL) {
        amxc_htable_new(&log->subscriptions, 0);
    }

    when_str_empty(log_controllers, out);
    convert_log_controllers(log_controllers, curr_log->log_controllers);
    amxc_var_copy(old_log->log_controllers, curr_log->log_controllers);

    //If no Callback is sent then we don't subscribe
    if(slot_cb != NULL) {
        subscribe_all_logs(log);
    }

    log_set_policy(log, rule_target);
    global_enable = get_info_from_log_controllers(log);
    curr_log->enable = global_enable & log_enable;
    update_old_log_info(log);

out:
    amxc_string_delete(&string_path);
    free(object_path);
    FW_PROFILING_OUT();
    return;
}
