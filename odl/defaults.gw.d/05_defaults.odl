%populate {
    object Firewall {
        parameter Config = "Policy";
        parameter PolicyLevel = "Firewall.Level.Medium.";

        object Level {
            instance add(Alias=Low, Name=Low) {
                parameter Policies = "Firewall.Policy.WAN2LAN_low.,Firewall.Policy.IPV6_WAN2LAN_low.,Firewall.Policy.WAN2GUEST_low.,Firewall.Policy.IPV6_WAN2GUEST_low.";
            }
            instance add(Alias=Medium, Name=Medium) {
                parameter Policies = "Firewall.Policy.WAN2LAN_medium.,Firewall.Policy.IPV6_WAN2LAN_medium.,Firewall.Policy.WAN2GUEST_medium.,Firewall.Policy.IPV6_WAN2GUEST_medium.";
            }
            instance add(Alias=High, Name=High) {
                parameter Policies = "Firewall.Policy.WAN2LAN_high.,Firewall.Policy.IPV6_WAN2LAN_high.,Firewall.Policy.WAN2GUEST_high.,Firewall.Policy.IPV6_WAN2GUEST_high.";
            }
            instance add(Alias=Custom, Name=Custom) {
                parameter Policies = "Firewall.Policy.WAN2LAN_Custom.,Firewall.Policy.IPV6_WAN2LAN_Custom.,Firewall.Policy.WAN2GUEST_Custom.,Firewall.Policy.IPV6_WAN2GUEST_Custom.";
            }
        }

        object Chain {
            instance add(Alias=Low, Name=FORWARD_L_Low) {
                parameter Enable = 1;
                object Rule {
                    instance add() {
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                }
            }
            instance add(Alias=Medium, Name=FORWARD_L_Medium) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=cstate) {
                        parameter Target = "Accept";
                        parameter ConnectionState = "RELATED,ESTABLISHED";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                }
            }
            instance add(Alias=High, Name=FORWARD_L_High) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=cstate) {
                        parameter Target = "Accept";
                        parameter ConnectionState = "RELATED,ESTABLISHED";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                }
            }
            instance add(Alias=IPV6_Low, Name=FORWARD6_L_Low) {
                parameter Enable = 1;
                object Rule {
                    instance add() {
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                }
            }
            instance add(Alias=IPV6_Medium, Name=FORWARD6_L_Medium) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=cstate) {
                        parameter Target = "Accept";
                        parameter ConnectionState = "RELATED,ESTABLISHED";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                }
            }
            instance add(Alias=IPV6_High, Name=FORWARD6_L_High) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=cstate) {
                        parameter Target = "Accept";
                        parameter ConnectionState = "RELATED,ESTABLISHED";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                }
            }
            instance add(Alias=High_Out, Name=FORWARD_L_High_Out) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=ftp-data) {
                        parameter Protocol = 6;
                        parameter DestPort = 20;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=ftp) {
                        parameter Protocol = 6;
                        parameter DestPort = 21;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=ssh) {
                        parameter Protocol = 6;
                        parameter DestPort = 22;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=smtp) {
                        parameter Protocol = 6;
                        parameter DestPort = 25;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=dns-udp) {
                        parameter Protocol = 17;
                        parameter DestPort = 53;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=dns-tcp) {
                        parameter Protocol = 6;
                        parameter DestPort = 53;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=http) {
                        parameter Protocol = 6;
                        parameter DestPort = 80;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=pop3) {
                        parameter Protocol = 6;
                        parameter DestPort = 110;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=ntp) {
                        parameter Protocol = 17;
                        parameter DestPort = 123;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }

                    instance add(Alias=imap) {
                        parameter Protocol = 6;
                        parameter DestPort = 143;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=https) {
                        parameter Protocol = 6;
                        parameter DestPort = 443;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=ip_stb) {
                        parameter '${prefix_}VendorClassID' = "IP-STB";
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=last-tcp-rule) {
                        parameter Protocol = 6;
                        parameter Target = "Reject";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=last-rule) {
                        parameter Target = "Reject";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                }
            }
            instance add(Alias=IPV6_High_Out, Name=FORWARD6_L_High_Out) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=ftp-data) {
                        parameter Protocol = 6;
                        parameter DestPort = 20;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=ftp) {
                        parameter Protocol = 6;
                        parameter DestPort = 21;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=ssh) {
                        parameter Protocol = 6;
                        parameter DestPort = 22;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=smtp) {
                        parameter Protocol = 6;
                        parameter DestPort = 25;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=dns-udp) {
                        parameter Protocol = 17;
                        parameter DestPort = 53;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=dns-tcp) {
                        parameter Protocol = 6;
                        parameter DestPort = 53;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=http) {
                        parameter Protocol = 6;
                        parameter DestPort = 80;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=pop3) {
                        parameter Protocol = 6;
                        parameter DestPort = 110;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=ntp) {
                        parameter Protocol = 17;
                        parameter DestPort = 123;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=imap) {
                        parameter Protocol = 6;
                        parameter DestPort = 143;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=https) {
                        parameter Protocol = 6;
                        parameter DestPort = 443;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=ip_stb) {
                        parameter '${prefix_}VendorClassID' = "IP-STB";
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=last-tcp-rule) {
                        parameter Protocol = 6;
                        parameter Target = "Reject";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                    instance add(Alias=last-rule) {
                        parameter Target = "Reject";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 6;
                    }
                }
            }
            instance add(Alias=Custom_In, Name=FORWARD_L_Custom_In) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=cstate) {
                        parameter Target = "Accept";
                        parameter ConnectionState = "RELATED,ESTABLISHED";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                }
            }
            instance add(Alias=Custom_Out, Name=FORWARD_L_Custom_Out) {
                parameter Enable = 1;
                object Rule {
                    instance add(Alias=ftp-data) {
                        parameter Protocol = 6;
                        parameter DestPort = 20;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=ftp) {
                        parameter Protocol = 6;
                        parameter DestPort = 21;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=ssh) {
                        parameter Protocol = 6;
                        parameter DestPort = 22;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=smtp) {
                        parameter Protocol = 6;
                        parameter DestPort = 25;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=dns-udp) {
                        parameter Protocol = 17;
                        parameter DestPort = 53;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=dns-tcp) {
                        parameter Protocol = 6;
                        parameter DestPort = 53;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=http) {
                        parameter Protocol = 6;
                        parameter DestPort = 80;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=pop3) {
                        parameter Protocol = 6;
                        parameter DestPort = 110;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=ntp) {
                        parameter Protocol = 17;
                        parameter DestPort = 123;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                        parameter IPVersion = 4;
                    }
                    instance add(Alias=imap) {
                        parameter Protocol = 6;
                        parameter DestPort = 143;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=https) {
                        parameter Protocol = 6;
                        parameter DestPort = 443;
                        parameter Target = "Accept";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=last-tcp-rule) {
                        parameter Protocol = 6;
                        parameter Target = "Reject";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                    instance add(Alias=last-rule) {
                        parameter Target = "Reject";
                        parameter SourceAllInterfaces = 1;
                        parameter DestAllInterfaces = 1;
                        parameter Enable = 1;
                    }
                }
            }
        }
    }
}
