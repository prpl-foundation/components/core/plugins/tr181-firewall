/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include "mock.h"
#include "test_nat_staticnat_no_logs.h"

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};
static const char* SRC = "source";
static const char* DEST = "destination";


static const char* jstring_statnat_nat(int protocol, const char* address, int start, int end, const char* to_address, bool dnat) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"nat\","
             "\"chain\":\"%sROUTING_StaticNAT\","
             "\"%s\":\"%s\","
             "\"ipv6\":false,"
             "\"enable\":true,"
             "\"%s_exclude\":false,"
             "\"%s_port\":\"%d\","
             "\"%s_port_range_max\":\"%d\","
             "\"protocol\":\"%d\","
             "\"target\":%d,"
             "\"target_option\":{\"ip\":\"%s\", \"max_port\":0, \"min_port\":0}}",
             dnat ? "PRE" : "POST",
             dnat ? DEST : SRC, address,
             dnat ? DEST : SRC,
             dnat ? DEST : SRC, start,
             dnat ? DEST : SRC, end,
             protocol,
             dnat ? 8 : 9,
             to_address);

    return _buf;
}

static const char* jstring_statnat_dnat(int protocol, const char* dest, int start, int end, const char* to_dest) {
    return jstring_statnat_nat(protocol, dest, start, end, to_dest, true);
}

static const char* jstring_statnat_snat(int protocol, const char* src, int start, int end, const char* to_src) {
    return jstring_statnat_nat(protocol, src, start, end, to_src, false);
}

static const char* jstring_statnat_filter(int protocol, const char* dest, int start, int end, bool reverse) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_StaticNAT\","
             "\"%s\":\"%s\","
             "\"ipv6\":false,"
             "\"enable\":true,"
             "\"%s_exclude\":false,"
             "\"%s_port\":\"%d\","
             "\"%s_port_range_max\":\"%d\","
             "\"protocol\":\"%d\","
             "\"target\":6,"
             "\"target_option\":1}",
             !reverse ? DEST : SRC, dest,
             !reverse ? DEST : SRC,
             !reverse ? DEST : SRC, start,
             !reverse ? DEST : SRC, end,
             protocol);

    return _buf;
}

static const char* jstring_statnat_forward(int protocol, const char* dest, int start, int end) {
    return jstring_statnat_filter(protocol, dest, start, end, false);
}

static const char* jstring_statnat_reverse(int protocol, const char* dest, int start, int end) {
    return jstring_statnat_filter(protocol, dest, start, end, true);
}

int statnat_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int statnat_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_statnat_add_host(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host");
    amxd_trans_add_inst(&trans, 0, "host-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "PublicIPAddress", "172.16.120.111");
    amxd_trans_set_value(cstring_t, &trans, "LANDevice", "192.168.1.2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(0, "172.16.120.111/32", 0, 0, "192.168.1.2/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_snat(0, "192.168.1.2/32", 0, 0, "172.16.120.111/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_forward(0, "192.168.1.2/32", 0, 0), 1);
    expect_fw_replace_rule_json(jstring_statnat_reverse(0, "192.168.1.2/32", 0, 0), 2);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Enabled");
}

void test_statnat_host_toggle_enable(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_statnat_dnat(0, "172.16.120.111/32", 0, 0, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(0, "192.168.1.2/32", 0, 0, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(0, "192.168.1.2/32", 0, 0), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(0, "192.168.1.2/32", 0, 0), 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Disabled");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(0, "172.16.120.111/32", 0, 0, "192.168.1.2/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_snat(0, "192.168.1.2/32", 0, 0, "172.16.120.111/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_forward(0, "192.168.1.2/32", 0, 0), 1);
    expect_fw_replace_rule_json(jstring_statnat_reverse(0, "192.168.1.2/32", 0, 0), 2);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Enabled");
}

void test_statnat_add_portmap(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1.PortMapping");
    amxd_trans_add_inst(&trans, 0, "portmap-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 6000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 6010);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP,UDP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 6000, 6010, "192.168.1.2/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_dnat(17, "172.16.120.111/32", 6000, 6010, "192.168.1.2/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 6000, 6010, "172.16.120.111/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_snat(17, "192.168.1.2/32", 6000, 6010, "172.16.120.111/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 6000, 6010), 3);
    expect_fw_replace_rule_json(jstring_statnat_forward(17, "192.168.1.2/32", 6000, 6010), 4);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 6000, 6010), 5);
    expect_fw_replace_rule_json(jstring_statnat_reverse(17, "192.168.1.2/32", 6000, 6010), 6);
    expect_fw_delete_rule_json(jstring_statnat_dnat(0, "172.16.120.111/32", 0, 0, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(0, "192.168.1.2/32", 0, 0, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(0, "192.168.1.2/32", 0, 0), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(0, "192.168.1.2/32", 0, 0), 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
}

void test_statnat_change_portmap(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1");
    amxd_trans_set_value(int32_t, &trans, "DestPort", 7000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 7010);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 6000, 6010, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_dnat(17, "172.16.120.111/32", 6000, 6010, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 6000, 6010, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(17, "192.168.1.2/32", 6000, 6010, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 6000, 6010), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(17, "192.168.1.2/32", 6000, 6010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 6000, 6010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(17, "192.168.1.2/32", 6000, 6010), 1);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 7000, 7010, "192.168.1.2/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_dnat(17, "172.16.120.111/32", 7000, 7010, "192.168.1.2/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 7000, 7010, "172.16.120.111/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_snat(17, "192.168.1.2/32", 7000, 7010, "172.16.120.111/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 7000, 7010), 1);
    expect_fw_replace_rule_json(jstring_statnat_forward(17, "192.168.1.2/32", 7000, 7010), 2);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 7000, 7010), 3);
    expect_fw_replace_rule_json(jstring_statnat_reverse(17, "192.168.1.2/32", 7000, 7010), 4);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
}

void test_statnat_second_portmap(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1.PortMapping");
    amxd_trans_add_inst(&trans, 0, "portmap-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 0);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 8000, 0, "192.168.1.2/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 8000, 0, "172.16.120.111/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 8000, 0), 5);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 8000, 0), 6);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 8000, 0, "192.168.1.2/32"), 3);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 8000, 0, "172.16.120.111/32"), 3);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 8000, 0), 5);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 8000, 0), 5);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Disabled");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 8000, 0, "192.168.1.2/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 8000, 0, "172.16.120.111/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 8000, 0), 5);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 8000, 0), 6);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Enabled");
}

void test_statnat_change_host(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(cstring_t, &trans, "PublicIPAddress", "172.16.120.112");
    amxd_trans_set_value(cstring_t, &trans, "LANDevice", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 7000, 7010, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_dnat(17, "172.16.120.111/32", 7000, 7010, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 7000, 7010, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(17, "192.168.1.2/32", 7000, 7010, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(17, "192.168.1.2/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(17, "192.168.1.2/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.111/32", 8000, 0, "192.168.1.2/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.2/32", 8000, 0, "172.16.120.111/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.2/32", 8000, 0), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.2/32", 8000, 0), 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Error");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Error");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Error");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(cstring_t, &trans, "PublicIPAddress", "");
    amxd_trans_set_value(cstring_t, &trans, "LANDevice", "192.168.1.3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Error");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Error");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Error");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(cstring_t, &trans, "PublicIPAddress", "172.16.120.112");
    amxd_trans_set_value(cstring_t, &trans, "LANDevice", "192.168.1.3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_dnat(17, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_snat(17, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_replace_rule_json(jstring_statnat_forward(17, "192.168.1.3/32", 7000, 7010), 2);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 7000, 7010), 3);
    expect_fw_replace_rule_json(jstring_statnat_reverse(17, "192.168.1.3/32", 7000, 7010), 4);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 8000, 0, "192.168.1.3/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 8000, 0, "172.16.120.112/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 8000, 0), 5);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 8000, 0), 6);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_dnat(17, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(17, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(17, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(17, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 8000, 0, "192.168.1.3/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 8000, 0, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 8000, 0), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 8000, 0), 1);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Disabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Error");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Error");

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_dnat(17, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_snat(17, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 2);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_replace_rule_json(jstring_statnat_forward(17, "192.168.1.3/32", 7000, 7010), 2);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 7000, 7010), 3);
    expect_fw_replace_rule_json(jstring_statnat_reverse(17, "192.168.1.3/32", 7000, 7010), 4);
    expect_fw_replace_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 8000, 0, "192.168.1.3/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 8000, 0, "172.16.120.112/32"), 3);
    expect_fw_replace_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 8000, 0), 5);
    expect_fw_replace_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 8000, 0), 6);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Enabled");
}

void test_statnat_remove_portmap(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host.host-1.PortMapping");
    amxd_trans_del_inst(&trans, 0, "portmap-1");
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_dnat(17, "172.16.120.112/32", 7000, 7010, "192.168.1.3/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(17, "192.168.1.3/32", 7000, 7010, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(17, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 7000, 7010), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(17, "192.168.1.3/32", 7000, 7010), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1", "Status", "Enabled");
    amxut_dm_param_equals(cstring_t, "NAT.StaticNAT.Host.host-1.PortMapping.portmap-2", "Status", "Enabled");
}

void test_statnat_remove_host(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "NAT.StaticNAT.Host");
    amxd_trans_del_inst(&trans, 0, "host-1");
    expect_fw_delete_rule_json(jstring_statnat_dnat(6, "172.16.120.112/32", 8000, 0, "192.168.1.3/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_snat(6, "192.168.1.3/32", 8000, 0, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(6, "192.168.1.3/32", 8000, 0), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(6, "192.168.1.3/32", 8000, 0), 1);
    expect_fw_replace_rule_json(jstring_statnat_dnat(0, "172.16.120.112/32", 0, 0, "192.168.1.3/32"), 1); // first portmap of host is enabled because last portmap is being removed
    expect_fw_replace_rule_json(jstring_statnat_snat(0, "192.168.1.3/32", 0, 0, "172.16.120.112/32"), 1);
    expect_fw_replace_rule_json(jstring_statnat_forward(0, "192.168.1.3/32", 0, 0), 1);
    expect_fw_replace_rule_json(jstring_statnat_reverse(0, "192.168.1.3/32", 0, 0), 2);
    expect_fw_delete_rule_json(jstring_statnat_dnat(0, "172.16.120.112/32", 0, 0, "192.168.1.3/32"), 1); // then remove portmap of host because host is being removed
    expect_fw_delete_rule_json(jstring_statnat_snat(0, "192.168.1.3/32", 0, 0, "172.16.120.112/32"), 1);
    expect_fw_delete_rule_json(jstring_statnat_forward(0, "192.168.1.3/32", 0, 0), 1);
    expect_fw_delete_rule_json(jstring_statnat_reverse(0, "192.168.1.3/32", 0, 0), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}