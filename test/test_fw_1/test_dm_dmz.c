/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_dmz.h"

#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX_FORWARD "[FW][ACCPT_IN_CONN..."
#define LOG_PREFIX_REVERSE "[FW][ACCPT_ALL]:"

static amxd_dm_t* dm = NULL;

static void a_populate_filter_forward_log(fw_rule_t* filter) {
    assert_int_equal(fw_rule_set_table(filter, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_in_interface(filter, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_destination(filter, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(filter, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(filter, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_FORWARD), 0);
}

static void a_populate_filter_forward(fw_rule_t* filter) {
    assert_int_equal(fw_rule_set_table(filter, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_in_interface(filter, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_destination(filter, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter, true), 0);
    assert_int_equal(fw_rule_set_target_policy(filter, FW_RULE_POLICY_ACCEPT), 0);
}

static void a_populate_filter_reverse_log(fw_rule_t* filter_reverse) {
    assert_int_equal(fw_rule_set_table(filter_reverse, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_reverse, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_out_interface(filter_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter_reverse, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_destination(filter_reverse, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter_reverse, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter_reverse, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(filter_reverse, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(filter_reverse, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_REVERSE), 0);
}

static void a_populate_filter_reverse(fw_rule_t* filter_reverse) {
    assert_int_equal(fw_rule_set_table(filter_reverse, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_reverse, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_out_interface(filter_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter_reverse, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_destination(filter_reverse, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter_reverse, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter_reverse, true), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_reverse, FW_RULE_POLICY_ACCEPT), 0);
}

static void a_populate_dnat_rule(fw_rule_t* dnat) {
    assert_int_equal(fw_rule_set_table(dnat, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(dnat, "PREROUTING_DMZ"), 0);
    assert_int_equal(fw_rule_set_source(dnat, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_destination(dnat, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_ipv6(dnat, false), 0);
    assert_int_equal(fw_rule_set_enabled(dnat, true), 0);
    assert_int_equal(fw_rule_set_target_dnat(dnat, "192.168.1.186", 0, 0), 0);
}

static void a_populate_dnat_lan(fw_rule_t* dnat_lan) {
    assert_int_equal(fw_rule_set_table(dnat_lan, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(dnat_lan, "PREROUTING_LAN_DMZ"), 0);
    assert_int_equal(fw_rule_set_destination(dnat_lan, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_ipv6(dnat_lan, false), 0);
    assert_int_equal(fw_rule_set_enabled(dnat_lan, true), 0);
    assert_int_equal(fw_rule_set_target_chain(dnat_lan, "PREROUTING_DMZ"), 0);
}

static void a_populate_snat_rule(fw_rule_t* snat) {
    assert_int_equal(fw_rule_set_table(snat, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(snat, "POSTROUTING_DMZ"), 0);
    assert_int_equal(fw_rule_set_source(snat, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_destination(snat, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_ipv6(snat, false), 0);
    assert_int_equal(fw_rule_set_enabled(snat, true), 0);
    assert_int_equal(fw_rule_set_target_snat(snat, "192.168.99.221/32", 0, 0), 0);
}

static void b_populate_filter_forward_log(fw_rule_t* filter) {
    assert_int_equal(fw_rule_set_table(filter, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(filter, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_in_interface(filter, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_destination(filter, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(filter, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(filter, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_FORWARD), 0);
}

static void b_populate_filter_forward(fw_rule_t* filter) {
    assert_int_equal(fw_rule_set_table(filter, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(filter, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_in_interface(filter, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_destination(filter, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter, true), 0);
    assert_int_equal(fw_rule_set_target_policy(filter, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_filter_reverse_log(fw_rule_t* filter_reverse) {
    assert_int_equal(fw_rule_set_table(filter_reverse, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(filter_reverse, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_out_interface(filter_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter_reverse, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_destination(filter_reverse, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter_reverse, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter_reverse, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(filter_reverse, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(filter_reverse, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX_REVERSE), 0);
}

static void b_populate_filter_reverse(fw_rule_t* filter_reverse) {
    assert_int_equal(fw_rule_set_table(filter_reverse, "filter"), 0);
    assert_int_equal(fw_rule_set_chain(filter_reverse, "FORWARD_DMZ"), 0);
    assert_int_equal(fw_rule_set_out_interface(filter_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter_reverse, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_destination(filter_reverse, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_ipv6(filter_reverse, false), 0);
    assert_int_equal(fw_rule_set_enabled(filter_reverse, true), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_reverse, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_dnat_rule(fw_rule_t* dnat) {
    assert_int_equal(fw_rule_set_table(dnat, "nat"), 0);
    assert_int_equal(fw_rule_set_chain(dnat, "PREROUTING_DMZ"), 0);
    assert_int_equal(fw_rule_set_source(dnat, "192.168.18.249"), 0);
    assert_int_equal(fw_rule_set_destination(dnat, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_ipv6(dnat, false), 0);
    assert_int_equal(fw_rule_set_enabled(dnat, true), 0);
    assert_int_equal(fw_rule_set_target_dnat(dnat, "192.168.1.186", 0, 0), 0);
}

static void b_populate_dnat_lan(fw_rule_t* dnat_lan) {
    assert_int_equal(fw_rule_set_table(dnat_lan, "nat"), 0);
    assert_int_equal(fw_rule_set_chain(dnat_lan, "PREROUTING_LAN_DMZ"), 0);
    assert_int_equal(fw_rule_set_destination(dnat_lan, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_ipv6(dnat_lan, false), 0);
    assert_int_equal(fw_rule_set_enabled(dnat_lan, true), 0);
    assert_int_equal(fw_rule_set_target_chain(dnat_lan, "PREROUTING_DMZ"), 0);
}

static void b_populate_snat_rule(fw_rule_t* snat) {
    assert_int_equal(fw_rule_set_table(snat, "nat"), 0);
    assert_int_equal(fw_rule_set_chain(snat, "POSTROUTING_DMZ"), 0);
    assert_int_equal(fw_rule_set_source(snat, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_destination(snat, "192.168.1.186"), 0);
    assert_int_equal(fw_rule_set_ipv6(snat, false), 0);
    assert_int_equal(fw_rule_set_enabled(snat, true), 0);
    assert_int_equal(fw_rule_set_target_snat(snat, "192.168.99.221/32", 0, 0), 0);
}

int test_dmz_setup(void** state) {
    expect_query_wan_netdevname();
    expect_query_wan_addresses();  // for NAT.InterfaceSetting.wan
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "dmz.odl");
    dm = amxut_bus_dm();

    return 0;
}

int test_dmz_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_dmz_initialization(UNUSED void** state) {
    amxd_object_t* dmz_template = NULL;
    uint32_t remaining_time = 0;

    dmz_template = amxd_dm_findf(dm, "Firewall.DMZ.");
    assert_non_null(dmz_template);

    remaining_time = amxd_object_get_uint32_t(dmz_template, "RemainingLeaseTime", NULL);
    assert_int_equal(remaining_time, 0);
}

void test_add_dmz(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "Firewall.DMZ.");
    amxd_trans_add_inst(&trans, 0, "dmz-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    assert_int_equal(amxd_object_get_params(dmz, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_dmz_missing_interface(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    print_message("enable the DMZ, status should be Error because of missing Interface\n");
    amxd_trans_select_object(&trans, dmz);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefix", "192.168.18.249");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.186");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(dmz, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_dmz_with_interface(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    expect_query_wan_ipv4_addresses();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    print_message("set Interface, status should be Enabled\n");
    amxd_trans_select_object(&trans, dmz);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward_log, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 2);
    expect_fw_replace_rule(a_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_dnat_rule, 1);
    expect_fw_replace_rule(a_populate_dnat_lan, 1);
    expect_fw_replace_rule(a_populate_snat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(dmz, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_disable_dmz(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    print_message("disable the DMZ, status should be Disabled\n");
    amxd_trans_select_object(&trans, dmz);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward_log, 1);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_dnat_rule, 1);
    expect_fw_delete_rule(a_populate_dnat_lan, 1);
    expect_fw_delete_rule(a_populate_snat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(dmz, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_remove_dmz(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    print_message("remove DMZ\n");
    amxd_trans_select_pathf(&trans, "Firewall.DMZ.");
    amxd_trans_del_inst(&trans, 0, "dmz-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    amxd_trans_clean(&trans);
}

void test_dmz_lease_time(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);

    expect_query_wan_ipv4_addresses();

    print_message("add instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "Firewall.DMZ.");
    amxd_trans_add_inst(&trans, 0, "dmz-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    amxut_dm_param_equals(cstring_t, "Firewall.DMZ.dmz-1.", "Status", "Disabled");

    print_message("enable the DMZ with a LeaseTime\n");
    amxd_trans_select_object(&trans, dmz);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefix", "192.168.18.249");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.186");
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(b_populate_filter_forward_log, 1);
    expect_fw_replace_rule(b_populate_filter_forward, 2);
    expect_fw_replace_rule(b_populate_filter_reverse_log, 3);
    expect_fw_replace_rule(b_populate_filter_reverse, 4);
    expect_fw_replace_rule(b_populate_dnat_rule, 1);
    expect_fw_replace_rule(b_populate_dnat_lan, 1);
    expect_fw_replace_rule(b_populate_snat_rule, 1);

    handle_events();

    amxut_dm_param_equals(cstring_t, "Firewall.DMZ.dmz-1.", "Status", "Enabled");
    assert_EndTime_now_or_later("Firewall.DMZ.dmz-1.");

    amxut_timer_go_to_future_ms((3600 * 1000) - 1);
    handle_events();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    expect_fw_delete_rule(b_populate_filter_forward_log, 1);
    expect_fw_delete_rule(b_populate_filter_forward, 1);
    expect_fw_delete_rule(b_populate_filter_reverse_log, 1);
    expect_fw_delete_rule(b_populate_filter_reverse, 1);
    expect_fw_delete_rule(b_populate_dnat_rule, 1);
    expect_fw_delete_rule(b_populate_dnat_lan, 1);
    expect_fw_delete_rule(b_populate_snat_rule, 1);
    amxut_timer_go_to_future_ms(2); // 3599s + 2ms > LeaseDuration
    handle_events();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_null(dmz);

    amxd_trans_clean(&trans);
}

void test_dmz_lease_time_if_disabled(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* dmz = NULL;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.DMZ.");
    amxd_trans_add_inst(&trans, 0, "dmz-1");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_non_null(dmz);

    amxut_dm_param_equals(cstring_t, "Firewall.DMZ.dmz-1.", "Status", "Disabled");
    assert_EndTime_now_or_later("Firewall.DMZ.dmz-1.");

    amxut_timer_go_to_future_ms(3600 * 1000);
    handle_events();

    dmz = amxd_dm_findf(dm, "Firewall.DMZ.dmz-1.");
    assert_null(dmz);

    amxd_trans_clean(&trans);
}

void test_dmz_expired_endtime(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    amxc_ts_t now;

    assert_int_equal(amxd_trans_init(&trans), 0);

    amxd_trans_select_pathf(&trans, "Firewall.DMZ.");
    amxd_trans_add_inst(&trans, 0, "dmz-99");
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    assert_int_equal(amxc_ts_now(&now), 0); // if EndTime != 0001-01-01T00:00:00Z at instance-added event and time now is > EndTime, the dmz is removed because of LeaseDuration
    amxd_trans_set_value(amxc_ts_t, &trans, "EndTime", &now);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    object = amxd_dm_findf(dm, "Firewall.DMZ.dmz-99");
    assert_null(object);

    amxd_trans_clean(&trans);
}