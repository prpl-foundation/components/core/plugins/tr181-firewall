/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_level.h"

#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX "[FW][All]:"

static amxd_dm_t* dm = NULL;

static void a_populate_policy_forward(fw_rule_t* policy_forward) {
    assert_int_equal(fw_rule_set_table(policy_forward, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_forward, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_forward, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_forward, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_forward, "br-lan"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_forward, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy_forward, FW_RULE_POLICY_ACCEPT), 0);
}

static void a_populate_policy_reverse_log(fw_rule_t* policy_reverse) {
    assert_int_equal(fw_rule_set_table(policy_reverse, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_reverse, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_reverse, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_reverse, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_reverse, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(policy_reverse, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(policy_reverse, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
}

static void a_populate_policy_reverse(fw_rule_t* policy_reverse) {
    assert_int_equal(fw_rule_set_table(policy_reverse, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_reverse, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_reverse, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_reverse, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_reverse, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy_reverse, FW_RULE_POLICY_ACCEPT), 0);
}

static void a_populate_policy6_forward(fw_rule_t* policy6_forward) {
    assert_int_equal(fw_rule_set_table(policy6_forward, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_forward, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_forward, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_forward, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_forward, "br-lan"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_forward, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy6_forward, FW_RULE_POLICY_ACCEPT), 0);
}

static void a_populate_policy6_reverse_log(fw_rule_t* policy6_reverse) {
    assert_int_equal(fw_rule_set_table(policy6_reverse, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_reverse, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_reverse, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_reverse, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_reverse, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(policy6_reverse, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(policy6_reverse, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
}

static void a_populate_policy6_reverse(fw_rule_t* policy6_reverse) {
    assert_int_equal(fw_rule_set_table(policy6_reverse, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_reverse, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_reverse, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_reverse, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_reverse, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_reverse, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy6_reverse, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_policy_forward(fw_rule_t* policy_forward_b) {
    assert_int_equal(fw_rule_set_table(policy_forward_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_forward_b, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_forward_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_forward_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_forward_b, "br-guest"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_forward_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy_forward_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_policy_reverse(fw_rule_t* policy_reverse_b) {
    assert_int_equal(fw_rule_set_table(policy_reverse_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_reverse_b, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_reverse_b, "br-guest"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_reverse_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy_reverse_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_policy6_forward(fw_rule_t* policy6_forward_b) {
    assert_int_equal(fw_rule_set_table(policy6_forward_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_forward_b, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_forward_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_forward_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_forward_b, "br-guest"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_forward_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy6_forward_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void b_populate_policy6_reverse(fw_rule_t* policy6_reverse_b) {
    assert_int_equal(fw_rule_set_table(policy6_reverse_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_reverse_b, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_reverse_b, "br-guest"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_reverse_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy6_reverse_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void c_populate_policy_forward(fw_rule_t* policy_forward_b) {
    assert_int_equal(fw_rule_set_table(policy_forward_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_forward_b, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_forward_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_forward_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_forward_b, "br-lan"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_forward_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy_forward_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void c_populate_policy_reverse(fw_rule_t* policy_reverse_b) {
    assert_int_equal(fw_rule_set_table(policy_reverse_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_reverse_b, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy_reverse_b, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy_reverse_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy_reverse_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void c_populate_policy6_forward(fw_rule_t* policy6_forward_b) {
    assert_int_equal(fw_rule_set_table(policy6_forward_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_forward_b, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_forward_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_forward_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_forward_b, "br-lan"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_forward_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy6_forward_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void c_populate_policy6_reverse(fw_rule_t* policy6_reverse_b) {
    assert_int_equal(fw_rule_set_table(policy6_reverse_b, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_reverse_b, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_in_interface(policy6_reverse_b, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface(policy6_reverse_b, "eth0"), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_reverse_b, true), 0);
    assert_int_equal(fw_rule_set_target_policy(policy6_reverse_b, FW_RULE_POLICY_ACCEPT), 0);
}

static void d_populate_policy_forward(fw_rule_t* policy_forward) {
    assert_int_equal(fw_rule_set_table(policy_forward, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy_forward, "FORWARD_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv4(policy_forward, true), 0);
    assert_int_equal(fw_rule_set_enabled(policy_forward, true), 0);
    assert_int_equal(fw_rule_set_target_chain(policy_forward, "FORWARD_L_Low"), 0);
}

static void d_populate_policy6_forward(fw_rule_t* policy6_forward) {
    assert_int_equal(fw_rule_set_table(policy6_forward, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(policy6_forward, "FORWARD6_Firewall"), 0);
    assert_int_equal(fw_rule_set_ipv6(policy6_forward, true), 0);
    assert_int_equal(fw_rule_set_enabled(policy6_forward, true), 0);
    assert_int_equal(fw_rule_set_target_chain(policy6_forward, "FORWARD6_L_Low"), 0);
}

int test_level_policy_setup(void** state) {
    expect_query_lan_netdevnames(); // for WAN2LAN_low ipv4
    expect_query_lan_netdevnames(); // for WAN2LAN_low ipv6
    expect_query_wan_netdevnames(); // for WAN2LAN_low ipv4
    expect_query_wan_netdevnames(); // for WAN2LAN_low ipv6

    expect_query_lan_netdevnames(); // for WAN2LAN_medium ipv4
    expect_query_lan_netdevnames(); // for WAN2LAN_medium ipv6
    expect_query_wan_netdevnames(); // for WAN2LAN_medium ipv4
    expect_query_wan_netdevnames(); // for WAN2LAN_medium ipv6

    expect_query_wan_netdevname();  // for logs
    expect_query_lan_netdevname();  // for logs

    expect_query_wan_netdevname();  // for logs

    mock_init(state, "config_policy.odl");
    dm = amxut_bus_dm();
    return 0;
}

int test_level_policy_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_policy_add_policy(UNUSED void** state) {
    amxd_trans_t trans;

    expect_query_guest_netdevnames(); // for ipv4
    expect_query_guest_netdevnames(); // for ipv6
    expect_query_wan_netdevnames();   // for ipv4
    expect_query_wan_netdevnames();   // for ipv6

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Policy.");
    amxd_trans_add_inst(&trans, 0, "WAN2GUEST_low");
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DestinationInterface", "Device.IP.Interface.4.");
    amxd_trans_set_value(cstring_t, &trans, "TargetChain", "Accept");
    amxd_trans_set_value(cstring_t, &trans, "ReverseTargetChain", "Accept");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_policy_change_level_policies_by_adding_1_policy(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* log_object = NULL;

    amxd_trans_init(&trans);

    print_message("change Firewall.Level.Medium.Policies\n");
    amxd_trans_select_pathf(&trans, "Firewall.Level.Medium.");
    amxd_trans_set_value(cstring_t, &trans, "Policies", "Firewall.Policy.WAN2LAN_low.,Firewall.Policy.WAN2GUEST_low.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(a_populate_policy_forward, 1); // deleted, see _level_changed implementation
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse, 2);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse, 2);
    expect_fw_replace_rule(b_populate_policy_forward, 3);
    expect_fw_replace_rule(b_populate_policy_reverse, 4);
    expect_fw_replace_rule(b_populate_policy6_forward, 3);
    expect_fw_replace_rule(b_populate_policy6_reverse, 4);
    handle_events();

    log_object = amxd_dm_findf(dm, "Firewall.Log.All.");
    assert_non_null(log_object);

    print_message("change Log.All to enable\n");
    amxd_trans_select_object(&trans, log_object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(a_populate_policy_forward, 1);
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse_log, 2);
    expect_fw_replace_rule(a_populate_policy_reverse, 3);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse_log, 2);
    expect_fw_replace_rule(a_populate_policy6_reverse, 3);
    handle_events();

    print_message("change Log.All to disable\n");
    amxd_trans_select_object(&trans, log_object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(a_populate_policy_forward, 1);
    expect_fw_delete_rule(a_populate_policy_reverse_log, 1);
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse_log, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse, 2);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse, 2);
    handle_events();
}

void test_policy_change_level_policies_by_removing_1_policy(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    print_message("change Firewall.Level.Medium.Policies\n");
    amxd_trans_select_pathf(&trans, "Firewall.Level.Medium.");
    amxd_trans_set_value(cstring_t, &trans, "Policies", "Firewall.Policy.WAN2LAN_low.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(a_populate_policy_forward, 1); // deleted, see _level_changed implementation
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_delete_rule(b_populate_policy_forward, 1);
    expect_fw_delete_rule(b_populate_policy_reverse, 1);
    expect_fw_delete_rule(b_populate_policy6_forward, 1);
    expect_fw_delete_rule(b_populate_policy6_reverse, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse, 2);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse, 2);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_policy_del_policy(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Policy.");
    amxd_trans_del_inst(&trans, 0, "WAN2GUEST_low");
    expect_fw_delete_rule(b_populate_policy_forward, 3);
    expect_fw_delete_rule(b_populate_policy_reverse, 3);
    expect_fw_delete_rule(b_populate_policy6_forward, 3);
    expect_fw_delete_rule(b_populate_policy6_reverse, 3);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

// WARNING test_policy_change_level_policies_by_removing_1_policy_after_policy_is_removed depends on test_policy_add_policy
// ADD TESTS BETWEEN THEM WITH CAUSION

void test_policy_change_level_policies_by_removing_1_policy_after_policy_is_removed(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    print_message("change Firewall.Level.Medium.Policies\n");
    amxd_trans_select_pathf(&trans, "Firewall.Level.Medium.");
    amxd_trans_set_value(cstring_t, &trans, "Policies", "Firewall.Policy.WAN2LAN_low.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(a_populate_policy_forward, 1); // deleted, see _level_changed implementation
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse, 2);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse, 2);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_policy_change_level_policies_of_non_active_level(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    print_message("change Firewall.Level.Low.Policies\n");
    amxd_trans_select_pathf(&trans, "Firewall.Level.Low.");
    amxd_trans_set_value(cstring_t, &trans, "Policies", "Firewall.Policy.WAN2LAN_low.,Firewall.Policy.WAN2LAN_medium.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change Firewall.Level.Low.Policies\n");
    amxd_trans_select_pathf(&trans, "Firewall.Level.Low.");
    amxd_trans_set_value(cstring_t, &trans, "Policies", "Firewall.Policy.WAN2LAN_low."); // restore the original value
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_policy_change_policy_level(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    print_message("change Firewall.PolicyLevel to Firewall.Level.Low.\n");
    amxd_trans_select_pathf(&trans, "Firewall.");
    amxd_trans_set_value(cstring_t, &trans, "PolicyLevel", "Firewall.Level.Low.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(a_populate_policy_forward, 1);
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_replace_rule(c_populate_policy_forward, 1);
    expect_fw_replace_rule(c_populate_policy_reverse, 2);
    expect_fw_replace_rule(c_populate_policy6_forward, 1);
    expect_fw_replace_rule(c_populate_policy6_reverse, 2);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change Firewall.PolicyLevel to Firewall.Level.Medium.\n");
    amxd_trans_select_pathf(&trans, "Firewall.");
    amxd_trans_set_value(cstring_t, &trans, "PolicyLevel", "Firewall.Level.Medium."); // restore the original value
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(c_populate_policy_forward, 1);
    expect_fw_delete_rule(c_populate_policy_reverse, 1);
    expect_fw_delete_rule(c_populate_policy6_forward, 1);
    expect_fw_delete_rule(c_populate_policy6_reverse, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse, 2);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse, 2);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_policy_change_config_to_low(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Level.Low.");
    amxd_trans_set_value(cstring_t, &trans, "Chain", "Firewall.Chain.Low.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change Firewall.Config to Low\n");
    amxd_trans_select_pathf(&trans, "Firewall.");
    amxd_trans_set_value(cstring_t, &trans, "Config", "Low");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(a_populate_policy_forward, 1);
    expect_fw_delete_rule(a_populate_policy_reverse, 1);
    expect_fw_delete_rule(a_populate_policy6_forward, 1);
    expect_fw_delete_rule(a_populate_policy6_reverse, 1);
    expect_fw_replace_rule(d_populate_policy_forward, 3);  // index != 1 because when Firewall.Config changes FROM Policy the policy instances are disabled with a transaction and so in a different event later then the _firewall_changed
    expect_fw_replace_rule(d_populate_policy6_forward, 3); // same
    handle_events();
    amxd_trans_clean(&trans);
}

void test_policy_change_policy_level_when_not_config_policy(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    print_message("change Firewall.PolicyLevel to Firewall.Level.Low.\n");
    amxd_trans_select_pathf(&trans, "Firewall.");
    amxd_trans_set_value(cstring_t, &trans, "PolicyLevel", "Firewall.Level.Low.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change Firewall.PolicyLevel to Firewall.Level.Medium.\n");
    amxd_trans_select_pathf(&trans, "Firewall.");
    amxd_trans_set_value(cstring_t, &trans, "PolicyLevel", "Firewall.Level.Medium."); // restore the original value
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

// WARNING test_policy_change_config_to_policy depends on test_policy_change_config_to_low
// ADD TESTS BETWEEN THEM WITH CAUSION

void test_policy_change_config_to_policy(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    print_message("change Firewall.Config to Policy\n");
    amxd_trans_select_pathf(&trans, "Firewall.");
    amxd_trans_set_value(cstring_t, &trans, "Config", "Policy");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule(d_populate_policy_forward, 1);
    expect_fw_delete_rule(d_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy_forward, 1);
    expect_fw_replace_rule(a_populate_policy_reverse, 2);
    expect_fw_replace_rule(a_populate_policy6_forward, 1);
    expect_fw_replace_rule(a_populate_policy6_reverse, 2);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Level.Low.");
    amxd_trans_set_value(cstring_t, &trans, "Chain", ""); // restore the original value
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}