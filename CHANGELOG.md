# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v8.8.14 - 2025-01-07(10:19:34 +0000)

### Other

- - TR-181 Firewall: IPSet extensions
- - TR181 Firewall: IPSet Datamodel implementation

## Release v8.8.13 - 2024-12-22(09:14:39 +0000)

### Other

- [SSH] Make getDebug working with non-root ssh user.

## Release v8.8.12 - 2024-12-13(10:52:47 +0000)

### Other

- [TR181-Firewall] Blocking internet access to a device does not block ping in ipv6

## Release v8.8.11 - 2024-11-22(13:38:30 +0000)

### Other

- Security level must apply to both lan and guest subnets

## Release v8.8.10 - 2024-11-07(14:19:19 +0000)

### Other

- - TR-181 Firewall: IPSet extensions
- - TR181 Firewall: IPSet Datamodel implementation

## Release v8.8.9 - 2024-10-11(09:18:35 +0000)

### Other

- - [tr181-firewall] All Logs does not work if both Source and Destination interface is filled in

## Release v8.8.8 - 2024-10-10(09:28:25 +0000)

### Other

- - [tr181-firewall] Set default firewall policy for guest networks to "medium"

## Release v8.8.8 - 2024-10-10(09:16:09 +0000)

### Other

- - [tr181-firewall] Set default firewall policy for guest networks to "medium"

## Release v8.8.7 - 2024-10-01(15:40:15 +0000)

### Other

- - [tr181-firewall] Pinhole doesn't work when no ip on the interface

## Release v8.8.6 - 2024-09-26(09:18:43 +0000)

### Other

- tr181 v2.17 compliance Firewall / NAT non-breaking changes

## Release v8.8.5 - 2024-09-19(12:50:07 +0000)

### Other

- Port Mapping Entry Table persistency

## Release v8.8.4 - 2024-09-18(09:45:09 +0000)

### Other

- Port Mapping Entry Table persistency

## Release v8.8.3 - 2024-09-17(07:49:50 +0000)

### Other

- - "Device.Firewall.X_PRPL-COM_InterfaceSetting must be restored by PCM (Second part)"

## Release v8.8.2 - 2024-09-13(13:29:04 +0000)

### Other

- disable config option by default

## Release v8.8.1 - 2024-09-11(16:04:09 +0000)

### Other

- The packets from LAN to WAN can get routed outside before the NAT is up

## Release v8.8.0 - 2024-09-09(10:15:23 +0000)

### New

- - [tr181-upnp] the port opened by UPnP is not closed after 20 minutes of inactivity

## Release v8.7.30 - 2024-09-09(07:19:26 +0000)

### Other

- Static NAT rule are not created without portfowarding rules

## Release v8.7.29 - 2024-09-05(14:26:58 +0000)

### Other

- - "Device.Firewall.X_PRPL-COM_InterfaceSetting must be restored by PCM"

## Release v8.7.28 - 2024-09-03(07:53:52 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v8.7.27 - 2024-08-30(11:29:52 +0000)

### Fixes

- Cannot disable passhtrough ICMPv6

## Release v8.7.26 - 2024-08-27(14:26:03 +0000)

### Other

- regression leaves firewall open after reboot

## Release v8.7.25 - 2024-08-07(16:53:38 +0000)

### Other

- Port Mapping Rule Status is Error for the manageable device MoCA Adapter

## Release v8.7.24 - 2024-08-06(08:19:56 +0000)

### Other

- add support for multiple runlevels

## Release v8.7.23 - 2024-07-26(12:43:19 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v8.7.22 - 2024-07-16(12:45:51 +0000)

### Other

- - [UPNP-IGD][Miniupnpd] Update port mapping lease isn't working

## Release v8.7.21 - 2024-07-16(08:26:55 +0000)

### Other

- - [tr181-firewall] new NAT.PortMapping/InterfaceSetting validator may cause firewall to not start

## Release v8.7.20 - 2024-07-11(13:46:19 +0000)

### Other

- Test 5.4.6 about Multiple portmap entries and precedence rules is failing.

## Release v8.7.19 - 2024-07-11(10:52:30 +0000)

### Other

- - PortMapping are not deleted in 5 minutes after setting LeaseTime = 300

## Release v8.7.18 - 2024-07-09(09:12:20 +0000)

### Other

- implement portmapping's hairpin nat with AllInterfaces

## Release v8.7.17 - 2024-07-08(07:09:46 +0000)

## Release v8.7.16 - 2024-06-25(13:19:17 +0000)

### Other

- Make DestMACAddress functional for pinhole

## Release v8.7.15 - 2024-06-21(08:40:41 +0000)

### Other

- Creating a port forwarding entry through the WebUI works,

## Release v8.7.14 - 2024-06-20(13:53:23 +0000)

## Release v8.7.13 - 2024-06-20(08:36:21 +0000)

### Fixes

- amx plugin should not run as root user

## Release v8.7.12 - 2024-06-17(15:29:40 +0000)

### Other

- - [PRPL][4.0.12.12][SIP ALG]SIP ALG is on even when switch shows disabled

## Release v8.7.11 - 2024-06-17(11:19:52 +0000)

### Other

- PortMapping are not deleted in 5 minutes after setting LeaseTime = 300

## Release v8.7.10 - 2024-06-15(07:02:24 +0000)

### Other

- FIREWALL: Add rule to block new input TCP connection without SYN flag TICKET

## Release v8.7.9 - 2024-06-13(09:09:43 +0000)

### Other

- icmpv6 cdrouter test failing

## Release v8.7.8 - 2024-06-12(06:24:20 +0000)

### Fixes

- Default settings for GW ICMP filtering

## Release v8.7.7 - 2024-06-11(12:55:15 +0000)

### Other

- - [Firewall][LOG] It must be possible to log Spoofed packets on the hgw
- - [tr181-firewall] portmapping hairpin NAT rule can be missconfigured

## Release v8.7.6 - 2024-06-04(08:04:37 +0000)

### Other

- RFC 4890 - Recommendations for Filtering ICMPv6 Messages

## Release v8.7.5 - 2024-05-31(10:24:05 +0000)

### Other

- - [Firewall] PortMapping returns status Enabled after failing

## Release v8.7.4 - 2024-05-31(09:28:17 +0000)

### Other

- - [UPnP][Firewall] Enable/disable ports forwarding functionality

## Release v8.7.3 - 2024-05-30(13:14:50 +0000)

### Other

- - Duplicate DROP Rule in Firewall FORWARD6_L_High_Out

## Release v8.7.2 - 2024-05-28(13:25:37 +0000)

### Other

- - validator is_valid_prefix not resolved

## Release v8.7.1 - 2024-05-28(12:40:56 +0000)

### Other

- Support more protocols than TCP and UDP

## Release v8.7.0 - 2024-05-22(08:00:51 +0000)

### New

- It must be possible to configure fw rules based on VendorClassID

## Release v8.6.0 - 2024-05-21(07:54:16 +0000)

### New

- CLONE - Data model parameters are unavailable for System Settings Page[AcceptUDPTraceroute]

## Release v8.5.5 - 2024-05-17(12:21:44 +0000)

### Fixes

- [libfwrules] breaking change in libfwrules causes regression in firewall icmpv6

## Release v8.5.4 - 2024-05-16(12:27:56 +0000)

### Other

- Enable amx-doc-check and amx-odl-validate

## Release v8.5.3 - 2024-05-08(17:23:09 +0000)

### Other

- [tr181-firewall] Logs all ICMP messages showing the icmp-type.

## Release v8.5.2 - 2024-05-07(12:39:19 +0000)

### Fixes

-  Error in setting ScheduleRef DM for port forwarding, pinhole and port trigger DM

## Release v8.5.1 - 2024-05-04(07:01:43 +0000)

### Fixes

- crash because of strcmp NULL

## Release v8.5.0 - 2024-05-02(07:00:09 +0000)

### New

- add Firewall.Chain.i.Rule.i.X_PRPL-COM_VendorClassID (no implementation)

## Release v8.4.2 - 2024-04-29(13:38:29 +0000)

### Fixes

- support 0.0.0.0[/0] as wildcard

## Release v8.4.1 - 2024-04-25(14:01:09 +0000)

### Other

- [tr181-firewall] Logs for all permitted outbound access requests.

## Release v8.4.0 - 2024-04-25(11:53:54 +0000)

### New

-  [tr181-firewall] Add ScheduleRef parameter (Implementation)

## Release v8.3.3 - 2024-04-23(16:12:08 +0000)

### Fixes

- Add missing APIs for port triggers

## Release v8.3.2 - 2024-04-23(13:45:29 +0000)

### Fixes

- Device.Firewall data model issues part1

## Release v8.3.1 - 2024-04-22(10:15:53 +0000)

### Other

- [tr181-firewall] Logs for all denied outbound access requests.

## Release v8.3.0 - 2024-04-18(12:40:01 +0000)

### New

- Add Support for Static NAT Configuration

## Release v8.2.2 - 2024-04-16(08:46:35 +0000)

### Fixes

- add extra SourcePortRangeMax parameter

## Release v8.2.1 - 2024-04-15(09:31:36 +0000)

### Other

- [tr181-firewall] Logs for all permitted inbound access requests.

## Release v8.2.0 - 2024-04-12(17:20:31 +0000)

### New

- [tr181-firewall] Add ScheduleRef parameter (DM change only)

## Release v8.1.1 - 2024-04-10(07:08:23 +0000)

### Changes

- Make amxb timeouts configurable

## Release v8.1.0 - 2024-04-09(14:05:04 +0000)

### New

- Add StaticNAT datamodel (no implementation)

## Release v8.0.2 - 2024-04-09(08:46:35 +0000)

### Other

- - [CDROUTER][UPNP][IPv6] The box is accepting LeaseTime Update via UpdatePinhole Action [add]

## Release v8.0.1 - 2024-04-08(11:58:12 +0000)

### Fixes

- [Firewall][LOG] The order of the LOG statements in Iptables is wrong

## Release v8.0.0 - 2024-04-05(13:53:21 +0000)

### Breaking

- add extra SourcePortRangeMax parameter

## Release v7.4.0 - 2024-04-04(15:04:42 +0000)

### New

- Add missing APIs for port mappings

## Release v7.3.2 - 2024-04-04(14:44:03 +0000)

### Other

- - [TR181-Firewall] Attacks detection

## Release v7.3.1 - 2024-03-27(16:39:06 +0000)

### Fixes

- optimize the queries the firewall will open

## Release v7.3.0 - 2024-03-25(08:38:40 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v7.2.4 - 2024-03-21(17:30:13 +0000)

### Other

- Firewall Logging: The Home CPE SHALL have the ability to log all denied inbound access requests - Part 2

## Release v7.2.3 - 2024-03-21(09:05:32 +0000)

### Fixes

- After reboot all hosts are disconnected (AKA amb timeouts)

## Release v7.2.2 - 2024-03-18(10:37:16 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v7.2.1 - 2024-03-11(13:21:09 +0000)

### Fixes

- [iptables LOG][tr181-firewall] When firewall logs are enabled, they should not be printed on the console.

## Release v7.2.0 - 2024-03-07(19:18:50 +0000)

### New

- Firewall Logging: The Home CPE SHALL have the ability to log all denied inbound access requests - Part 1

## Release v7.1.0 - 2024-03-05(09:13:32 +0000)

### New

- Web GUI: Require data model parameter to know the used vs max supported NAT

## Release v7.0.0 - 2024-02-28(13:46:37 +0000)

### Breaking

- Guest Wifi must not be able to access private LAN

## Release v6.21.1 - 2024-02-27(15:13:29 +0000)

### Fixes

- [tr181-firewall] PRPLOS must support : RFC 5095, "Deprecation of Type 0 Routing Headers in IPv6", 2007

## Release v6.21.0 - 2024-02-23(11:02:08 +0000)

### New

- [tr181-firewall] PRPLOS must support : RFC 5095, "Deprecation of Type 0 Routing Headers in IPv6", 2007

## Release v6.20.2 - 2024-02-22(12:48:38 +0000)

### Fixes

- [tr181-firewall] Enabling service Log on a service with multiple protocols breaks the iptables rules

## Release v6.20.1 - 2024-02-20(12:55:28 +0000)

### Other

- [Firewall] Custom Chain needs the IPV6 chain on iptables

## Release v6.20.0 - 2024-02-19(11:56:07 +0000)

### New

- [TR181-Firewall] Add a custom Firewall level in the predefined prpl settings

## Release v6.19.0 - 2024-02-15(15:49:53 +0000)

### New

- [TR181-firewall][Pinhole] Need ExternalPortEndRange as part of Device.​Firewall.​Pinhole.​{i}.

## Release v6.18.0 - 2024-02-09(14:23:55 +0000)

### New

-  [TR181-Firewall] map Device.Firewall.X_PRPL-COM_Enable on Device.Firewall.Enable

## Release v6.17.1 - 2024-02-08(14:11:25 +0000)

### Fixes

- Limit amount of pinholes and portmappings

## Release v6.17.0 - 2024-02-05(16:21:06 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v6.16.0 - 2024-01-30(07:45:52 +0000)

### Fixes

- [Firewall] Re-work of InterfaceSettings implementation.

## Release v6.15.4 - 2024-01-23(20:46:12 +0000)

### Fixes

- [Firewall] Device.Firewall.Policy.{i} is not persistent after upgrade

## Release v6.15.3 - 2024-01-23(16:59:05 +0000)

### Fixes

- [SAFRAN PRPL ]NAT/PAT rules disappeared after upgrade

## Release v6.15.2 - 2024-01-22(07:17:48 +0000)

### Other

- [Firewall] Device.Firewall.Policy.{i} is not persistent after upgrade

## Release v6.15.1 - 2024-01-17(09:44:18 +0000)

### Changes

- count the number of low layer calls for observability

## Release v6.15.0 - 2024-01-16(19:49:52 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v6.14.7 - 2024-01-16(17:55:33 +0000)

### Changes

- let expect_check free the test's fw_rule_t

## Release v6.14.6 - 2024-01-16(11:29:42 +0000)

### Changes

- use libamxut

## Release v6.14.5 - 2024-01-08(12:58:24 +0000)

### Fixes

- [Firewall] Profiling of the firewall plugin

## Release v6.14.4 - 2024-01-04(13:41:44 +0000)

### Fixes

- [OJO][ACL] Admin cannot access to the modify the Device.Firewall.PolicyLevel

## Release v6.14.3 - 2023-12-19(11:23:43 +0000)

### Fixes

- [ProcessMonitor] cleanup plugin

## Release v6.14.2 - 2023-12-08(12:57:22 +0000)

### Fixes

- [tr181-firewall] Compilation broken when gmap is not enabled

## Release v6.14.1 - 2023-12-07(10:45:00 +0000)

### Other

- Add firewall rules to accept UDP packets with source ports 5353

## Release v6.14.0 - 2023-12-05(15:49:22 +0000)

### New

- [Data model][Firewall Enable] parameters needed for Broadband Connection Web GUI page

## Release v6.13.5 - 2023-12-01(12:52:14 +0000)

### Other

- Correct CI

## Release v6.13.4 - 2023-12-01(11:37:42 +0000)

### Fixes

- [tr181-firewall] Crash during board startup

## Release v6.13.3 - 2023-11-30(11:01:52 +0000)

### Other

- [tr181-firewall][NAT portmapping]It must be possible to define an InternalClient as Hostname

## Release v6.13.2 - 2023-11-24(11:00:15 +0000)

### Fixes

- [Firewall] Device.Firewall.Policy.{i}.Status is not persistent after upgrade

## Release v6.13.1 - 2023-11-23(16:26:51 +0000)

### Other

- No need for NAT.Interfacesetting.Wan for ap config

## Release v6.13.0 - 2023-11-23(12:42:02 +0000)

### New

-  [FIrewall][SIP ALG][DM] It must be possible to have a enable/disable SIP ALG using the HL API

## Release v6.12.0 - 2023-11-22(10:26:49 +0000)

### New

-  [TR-181-Firewall] Add the possibility to LOG functionality in the firewall

## Release v6.11.3 - 2023-11-16(08:35:30 +0000)

### Fixes

- [tr181-firewall] Crash during boot

## Release v6.11.2 - 2023-11-15(16:33:13 +0000)

### Fixes

- [NAT] LAN Users cannot take advantage from NAT when using the Box Wan IP to reach services

## Release v6.11.1 - 2023-11-14(14:52:14 +0000)

### Fixes

- [IPv6][WAN 2LAN ICMPv6] FORWARD6_InterfaceSettings  chain should be before the PinHole chain

## Release v6.11.0 - 2023-11-14(08:22:04 +0000)

### New

- [LeaseDuration][Firewall]It must be possible to define a 'LeaseDuration" for Pinholes and DMZ Entries.

## Release v6.10.0 - 2023-11-09(15:39:04 +0000)

### New

- [NAT][LeaseDuration][Firewall]It must be possible to define a 'LeaseDuration" for NAT entries

## Release v6.9.0 - 2023-11-02(09:01:46 +0000)

### New

- [IPv6][WAN 2LAN ICMPv6] Allow ipv6 ping to LAN Devices (using GUA address)

## Release v6.8.4 - 2023-10-28(07:38:55 +0000)

### Changes

- Include extensions directory in ODL

## Release v6.8.3 - 2023-10-26(08:55:33 +0000)

## Release v6.8.2 - 2023-10-18(07:05:29 +0000)

### Other

- Allow all type of outgoing traffic from LCM container toward WAN/LAN

## Release v6.8.1 - 2023-10-13(14:14:45 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v6.8.0 - 2023-10-12(11:27:37 +0000)

### New

- [DSLITE][MSS] Adapt MSS for the dslite traffic depending on the MTU of the underlying interface.

## Release v6.7.3 - 2023-10-04(07:31:11 +0000)

### Other

- add missing iptables chain for multicast

## Release v6.7.2 - 2023-09-29(07:34:57 +0000)

### Other

- Removal of unnecessary parts of the datamodel of the firewall for the AP config

## Release v6.7.1 - 2023-09-28(09:36:16 +0000)

### Other

- - [ACL][webui] Fix missing ACL for webui test

## Release v6.7.0 - 2023-09-25(14:07:38 +0000)

### New

- [amxrt][no-root-user][capability drop] tr181-firewall plugin must be adapted to run as non-root and lmited capabilities

## Release v6.6.5 - 2023-09-25(12:40:04 +0000)

### Other

- CLONE - [CDROUTER][IPv6] Verify basic MSS Clamping for TCP...

## Release v6.6.4 - 2023-09-20(17:21:36 +0000)

### Other

- [AP Config]No response for Ping requests from devices LAN to the router

## Release v6.6.3 - 2023-09-07(15:02:10 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v6.6.2 - 2023-09-05(14:52:39 +0000)

## Release v6.6.1 - 2023-08-24(08:31:14 +0000)

### Other

- [NAT][tr181-firewall] Add Origin field to NAT.PortMapping

## Release v6.6.0 - 2023-08-17(14:29:16 +0000)

### New

- [TR181-Firewall]  add protected parameter( Enable && IPVersion) to Level object

## Release v6.5.0 - 2023-08-02(07:43:04 +0000)

### New

- [tr181-NAT] Implement a (vendor) parameter NAT.X_PRPL-COM_MaxPortMappingNumberOfEntries.

## Release v6.4.0 - 2023-07-24(14:05:35 +0000)

### New

- [TR181-Firewall] It must be possible to choose IPv4 and ipv6 firewall...

## Release v6.3.4 - 2023-07-03(15:35:44 +0000)

### Fixes

- Init script has no shutdown function

## Release v6.3.3 - 2023-06-29(15:55:20 +0000)

### Fixes

- WebUI cannot access to Device.Firewall.DMZ.[Alias=='wanDMZ'].

## Release v6.3.2 - 2023-06-23(09:12:44 +0000)

### Fixes

- change validator so that Protocol must be a Comma-separated list of integers range -1 to 255

## Release v6.3.1 - 2023-06-20(09:30:46 +0000)

### Other

- - [HTTPManager][WebUI] Create plugin's ACLs permissions

## Release v6.3.0 - 2023-06-16(09:45:39 +0000)

### New

- [tr181-pcm] Set the usersetting parameters for each plugin

## Release v6.2.2 - 2023-06-15(10:44:52 +0000)

### Fixes

- refactor prefix_create:ip_address.c to use libipat to convert string IP addresses

## Release v6.2.1 - 2023-06-15(08:40:08 +0000)

### Other

- include mod_fw_host optionally

## Release v6.2.0 - 2023-06-01(10:26:11 +0000)

### New

- add ACLs permissions for cwmp user

### Fixes

- Connected Guest client could Not ping DUT --> ODL syntax error

## Release v6.1.17 - 2023-05-23(15:10:14 +0000)

### Fixes

- [Firewall]High level chain should contain a reverse chain that block traffic on both ways.

## Release v6.1.16 - 2023-05-18(05:41:02 +0000)

### Fixes

- High level is blocking authorized connection (Port 25, 20, 21, 80, 443)

## Release v6.1.15 - 2023-05-16(10:22:41 +0000)

### Fixes

- [tr181-firewall] the firewall plugin is crashing just after disabling the rule for HTTP access from LAN

## Release v6.1.14 - 2023-05-12(11:37:17 +0000)

### Fixes

- [TR181-Firewall] When updating a LAN IPv4 address, the PREROUTING_Spoofing chain is not updated

## Release v6.1.13 - 2023-05-11(12:44:28 +0000)

### Fixes

- Switch custom vendor firewall-pinhole parameters to the new standard parameters

## Release v6.1.12 - 2023-05-09(13:35:52 +0000)

### Fixes

- Issue HOP-3381: [NAT]Status of created PortMapping rule is Disabled after reboot

## Release v6.1.11 - 2023-05-09(13:08:51 +0000)

### Fixes

- traffic not forwarded to the third party ipv4 address

## Release v6.1.10 - 2023-05-09(12:00:05 +0000)

### Changes

- Create Default DMZ instance for WAN DMZ for UI

## Release v6.1.9 - 2023-05-05(07:38:43 +0000)

### Fixes

- [CDROUTER] LAN Users cannot take advantage from NAT when using the Box Wan IP to reach service when DMZ rule exists

## Release v6.1.8 - 2023-05-04(13:21:31 +0000)

### Fixes

- DMZ can't be disabled when status is Error

## Release v6.1.7 - 2023-04-28(11:30:16 +0000)

### Fixes

- implement porttrigger's PortEndRange

## Release v6.1.6 - 2023-04-21(10:16:42 +0000)

### Other

- [Firewall][DMZ] DUT forward requests from the LAN to the DMZ host.

## Release v6.1.5 - 2023-04-20(15:08:57 +0000)

### Fixes

- implement service Action "Reject"

## Release v6.1.4 - 2023-04-20(13:26:58 +0000)

### Fixes

- service protocol should be integers

## Release v6.1.3 - 2023-04-20(12:54:39 +0000)

### Fixes

- services and pinholes should support multiple source prefixes

## Release v6.1.2 - 2023-04-20(06:50:29 +0000)

### Fixes

- implement PolicyLevel

## Release v6.1.1 - 2023-04-17(10:26:51 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v6.1.0 - 2023-04-06(12:19:37 +0000)

### New

- LAN Users cannot take advantage from NAT when using the Box Wan IP to reach service

## Release v6.0.2 - 2023-04-05(13:56:20 +0000)

### Fixes

- refactor code as preparation for HairpinNAT

## Release v6.0.1 - 2023-03-21(14:05:15 +0000)

### Fixes

- Crash after reboot

## Release v6.0.0 - 2023-03-20(20:44:43 +0000)

### Breaking

- Adapt and review the current Firewall Data model to the new standards.

## Release v5.0.5 - 2023-03-17(18:46:06 +0000)

### Other

- [baf] Correct typo in config option

## Release v5.0.4 - 2023-03-16(14:36:33 +0000)

### Other

- Add AP config files

## Release v5.0.3 - 2023-03-15(10:55:41 +0000)

### Fixes

- in wanmode dslite the interfaces for ipv4 and ipv6 are different

## Release v5.0.2 - 2023-03-10(13:35:17 +0000)

### Fixes

- refactor code to be maintainable

## Release v5.0.1 - 2023-03-09(09:17:00 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v5.0.0 - 2023-03-02(07:21:09 +0000)

### Breaking

- Client cannot ping the hgw after connecting to WiFi guest

## Release v4.10.4 - 2023-02-24(11:34:34 +0000)

### Other

- Allow loopback

## Release v4.10.3 - 2023-02-24(08:53:14 +0000)

### Other

- Add missing odl target for documentation generation

## Release v4.10.2 - 2023-02-23(09:37:28 +0000)

### Fixes

- remove http service instance from ODL defaults

## Release v4.10.1 - 2023-02-10(08:57:06 +0000)

### Fixes

- [prpl][amx] [Firewall] Implementation of WANAccess blocking

## Release v4.10.0 - 2023-02-09(10:20:52 +0000)

### New

- [prpl][amx] [Firewall] Implementation of WANAccess blocking

## Release v4.9.0 - 2023-01-27(09:47:49 +0000)

### New

- [Firewall] [NAT] Indicate which interfaces are NAT capable in NetModel

## Release v4.8.3 - 2023-01-26(12:12:21 +0000)

### Fixes

- [ipv6][cdrouter] Some tests are failing

## Release v4.8.2 - 2022-12-19(16:00:33 +0000)

### Other

- - [UPNP] UPNP plugin must open the relevant ports for UPNP discovery for both ipv4 and IPv6

## Release v4.8.1 - 2022-12-15(13:54:57 +0000)

### Other

- Add different firewall policies for IPv4 and IPv6

## Release v4.8.0 - 2022-12-15(12:11:27 +0000)

### New

- Add ipv4/6 netmodel flag to netdevname query

## Release v4.7.4 - 2022-12-09(17:35:28 +0000)

### Changes

- upnp service is not available over IPv6

## Release v4.7.3 - 2022-12-09(09:29:17 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v4.7.2 - 2022-12-01(11:21:48 +0000)

### Changes

- [NAT][DSLite] In case dslite is enabled for ipv4, NAT should be disabled

## Release v4.7.1 - 2022-11-15(18:13:50 +0000)

### Changes

- [Hosts] AccessControl block hosts directly in ip6tables

## Release v4.7.0 - 2022-11-15(12:44:00 +0000)

### New

- add firewall rules to forward traffic for PCP mapped packets

## Release v4.6.10 - 2022-11-10(12:01:08 +0000)

### Changes

- [Hosts] AccessControl block hosts directly in iptables

## Release v4.6.9 - 2022-11-08(18:29:38 +0000)

### Fixes

- [amx][firewall] Fix ipv6 issues caused by spoofing protection.

## Release v4.6.8 - 2022-11-08(08:55:42 +0000)

### Fixes

- Switch the wan interface of the tr181-firewall to the Logical.Interface.1.

## Release v4.6.7 - 2022-10-20(07:37:09 +0000)

### Other

- [IPv6][Firewall] IPv6 connectivity is broken by spoofing protection feature

## Release v4.6.6 - 2022-10-13(15:02:40 +0000)

### Fixes

- [NeighbourDiscovery][Firewall]Too many ports are opened by the neighbourdiscovery plugin

## Release v4.6.5 - 2022-09-29(10:22:35 +0000)

### Fixes

- [Firewall][StealthMode] stealth rules open the firewall by default.

## Release v4.6.4 - 2022-09-27(11:32:06 +0000)

### Other

- [tr181-firewall] ip6tables-restore is broken

## Release v4.6.3 - 2022-09-21(07:19:35 +0000)

### Other

- [amx][firewall] Implement Spoofing protection on specific InterfaceSettings.

## Release v4.6.2 - 2022-09-12(06:12:36 +0000)

### Changes

- enable doc-check

## Release v4.6.1 - 2022-09-09(14:30:48 +0000)

### Fixes

- Add missing default ipv6 rules

## Release v4.6.0 - 2022-09-05(11:44:03 +0000)

### New

- [amx][firewall]It must be possible to configure firewall specific InterfaceSettings.

## Release v4.5.3 - 2022-08-30(10:12:33 +0000)

### Fixes

- Cannot configure a Port Mapping via TR069, LeaseTime is defined as RO while should be W

## Release v4.5.2 - 2022-08-29(12:35:18 +0000)

### Fixes

- Add MSS rules for ppp interface traffic

## Release v4.5.1 - 2022-08-23(08:40:44 +0000)

### Changes

- Do not open port 22 on all interfaces by default

## Release v4.5.0 - 2022-08-17(09:19:04 +0000)

### New

- It must be possible to load additional iptables fragments files

## Release v4.4.4 - 2022-08-01(14:12:26 +0000)

### Other

- [Default][IPv6] Add icmpv6 echo accept as default firewall rule

## Release v4.4.3 - 2022-06-30(13:56:14 +0000)

### Fixes

- when bridge br-lan is down eth3 is used

## Release v4.4.2 - 2022-06-30(09:44:29 +0000)

### Fixes

- after reboot default services are not loaded

## Release v4.4.1 - 2022-06-30(08:12:00 +0000)

### Fixes

- Index of insertion too big (ssh plugin)

## Release v4.4.0 - 2022-05-23(19:53:28 +0000)

### New

- UPnP - LAN Communication

## Release v4.3.0 - 2022-05-23(12:01:18 +0000)

### New

- Configure LAN network ranges for NAT.

## Release v4.2.3 - 2022-05-18(10:38:41 +0000)

### Other

- Correct dependencies

## Release v4.2.2 - 2022-05-09(15:35:05 +0000)

### Fixes

- Cannot have a DMZ Host entry with Status Enabled.

## Release v4.2.1 - 2022-04-29(06:40:45 +0000)

### Fixes

- lan devices don't get DHCP offer when fw3 is disabled

## Release v4.2.0 - 2022-03-28(13:49:55 +0000)

### New

- add lcm interface to config

## Release v4.1.2 - 2022-03-24(09:54:59 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v4.1.1 - 2022-03-17(15:46:33 +0000)

### Changes

- use defaults.d directory for default ODL files

## Release v4.1.0 - 2022-03-17(08:43:05 +0000)

### New

- [amx][firewall] Add a debug script for the firewall

## Release v4.0.4 - 2022-03-03(08:35:41 +0000)

### Fixes

- Remove IP flags from netmodel queries to get netdevname

## Release v4.0.3 - 2022-02-25(11:49:09 +0000)

### Other

- Enable core dumps by default

## Release v4.0.2 - 2022-02-24(15:31:34 +0000)

### Fixes

- crash when portmapping is deleted

## Release v4.0.1 - 2022-02-18(15:57:15 +0000)

### Changes

- add a default rule to allow ssh

## Release v4.0.0 - 2022-02-18(13:59:49 +0000)

### Breaking

- bad prefix "X_Prpl_" for vendor specific objects

## Release v3.1.0 - 2022-02-14(08:30:01 +0000)

### New

- Definition of Upgrade Persistent NAT(PortMapping) Configuration
- Definition of Upgrade Persistent Firewall Configuration

## Release v3.0.1 - 2022-02-09(13:52:40 +0000)

### Fixes

- cdrouter-172 test reports firewall on WAN interface as misconfigured

## Release v3.0.0 - 2021-12-21(14:47:29 +0000)

### Breaking

- rename DMZ parameter SourceInterface to Interface

## Release v2.4.1 - 2021-12-14(15:40:44 +0000)

### Other

- [CI] Add libnetmodel as build dependency

## Release v2.4.0 - 2021-12-14(15:07:28 +0000)

### New

- use libnetmodel to translate the TR181 interface path to the interface name

## Release v2.3.5 - 2021-11-23(07:40:20 +0000)

### Other

- [ACL] The TR181 Firewall manager must have a default ACL configuration
- [ACL] The TR181 Firewall manager must have a default ACL configuration

## Release v2.3.4 - 2021-11-15(15:33:28 +0000)

### Fixes

- ipv6, attempt to add chain to (not existing) table nat

## Release v2.3.3 - 2021-11-15(10:16:46 +0000)

### Fixes

- The Firewall level must be configurable in Medium Mode

## Release v2.3.2 - 2021-11-08(13:59:40 +0000)

### Other

- [CI] Disable g++ compilation

## Release v2.3.1 - 2021-11-08(13:04:43 +0000)

### Fixes

- [tr181-firewall] add guest config

## Release v2.3.0 - 2021-11-05(14:55:34 +0000)

### New

- [tr181-firewall] add guest config

### Fixes

- Add missing dependencies

## Release v2.2.1 - 2021-10-28(11:16:45 +0000)

### Changes

- implement Firewall.X_Prpl_PortTrigger

## Release v2.2.0 - 2021-10-28(07:51:36 +0000)

### New

- implement Firewall.X_Prpl_PortTrigger

## Release v2.1.0 - 2021-10-28(07:13:54 +0000)

### New

- allow rules to match source MAC address

## Release v2.0.1 - 2021-10-27(06:52:56 +0000)

### Fixes

- coverity reports DEADCODE

## Release v2.0.0 - 2021-10-12(11:49:22 +0000)

### Breaking

- dhcp(v6) default configuration can be removed

### Fixes

- NAT.InterfaceSetting status is error

## Release v1.6.1 - 2021-10-01(13:04:41 +0000)

### Fixes

- tr181-firewall v1.3.0 won't start due to ODL error

## Release v1.6.0 - 2021-09-29(12:41:27 +0000)

### New

- firewall level should be dynamically configurable

### Other

- reduce code complexity

## Release v1.5.0 - 2021-09-24(09:02:45 +0000)

### New

- implement Firewall.X_Prpl_Pinhole

## Release v1.4.3 - 2021-09-17(11:03:27 +0000)

### Fixes

- setService's argument "interface" should support 'Device.IP.Interface.{i}.' paths

## Release v1.4.2 - 2021-09-16(06:59:08 +0000)

### Fixes

- LAN device cannot fetch webpage

## Release v1.4.1 - 2021-09-16(06:47:07 +0000)

### Fixes

- chain 'INPUT_Services' has no references in table 'nat'

## Release v1.4.0 - 2021-09-15(07:05:25 +0000)

### New

- implement Firewall.X_Prpl_DMZ

## Release v1.3.0 - 2021-09-07(12:35:28 +0000)

### New

- implement NAT.InterfaceSetting to support NAT on the WAN interface
- default policy of built in chains INPUT and FORWARD must be DROP

## Release v1.2.6 - 2021-08-24(16:37:45 +0000)

### Other

- missing default policies

## Release v1.2.5 - 2021-08-20(09:16:38 +0000)

### Fixes

- policy does not always result in ip(6)tables rules

## Release v1.2.4 - 2021-08-20(07:31:53 +0000)

### Fixes

- for_each inception is not allowed because of local variable shadowing

## Release v1.2.3 - 2021-08-19(12:02:28 +0000)

### Fixes

- Index of deletion too big

## Release v1.2.2 - 2021-08-16(07:40:35 +0000)

### Other

- Firewall configuration example

## Release v1.2.1 - 2021-08-12(11:59:10 +0000)

### Fixes

- iptables rule matches icmptype 1 by default
- service parameter "Interface" is not used to match packets

### Other

- Use standard c11
- Use standard c11

## Release v1.2.0 - 2021-08-11(14:46:19 +0000)

### New

- [TR181 Firewall] TR181 chains and rules

### Fixes

- update copyright headers to be conform with the SPDX specification

## Release v1.1.3 - 2021-08-09(10:03:10 +0000)

### Other

- [unit testing] better regression testing
- [unit testing] better regression testing

## Release v1.1.2 - 2021-08-06(12:47:55 +0000)

### Fixes

- Service status is enabled yet no rule found with iptables -L

## Release v1.1.1 - 2021-08-04(10:12:59 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipelines don't stop when memory leaks are detected

## Release v1.1.0 - 2021-08-03(16:18:02 +0000)

### New

- [TR181 Firewall][Portmapping] port forwarding

### Fixes

- [tr181-firewall] warn_unused_result during compilation

### Other

- add firewall level 'Medium'
- add firewall level 'Medium'

## Release v1.0.2 - 2021-07-27(13:31:07 +0000)

- Issue: soft.at.home/plugins/tr181-firewall#2 baf: update versions for libfwrules and libfwinterface

## Release v1.0.1 - 2021-07-16(08:25:52 +0000)

- Issue: soft.at.home/plugins/tr181-firewall#1 baf: missing prorietary parameter

## Release v1.0.0 - 2021-07-13(10:45:56 +0000)

### New

- [TR181 Firewall][Service] firewall open port for local service

