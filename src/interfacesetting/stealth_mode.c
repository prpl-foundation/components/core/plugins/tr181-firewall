/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/stealth_mode.h"
#include "firewall.h" // for fw_get_chain
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "firewall"

#define STEALTH_MODE_STATUS       "StealthModeStatus"

/**
   @brief
   Delete/clean the StealthMode internal structure.

   @param[in] stealth_mode StealthMode internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_stealth_mode(stealth_mode_t** stealth_mode) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*stealth_mode, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete StealthMode");

    amxc_llist_it_take(&(*stealth_mode)->it);

    common_clean(&(*stealth_mode)->common);

    amxc_var_clean(&(*stealth_mode)->protocols);

    free(*stealth_mode);
    *stealth_mode = NULL;
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Activate the stealth mode (Accept or Drop)

   @param[in] stealth_mode StealthMode internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_stealth(stealth_mode_t* stealth_mode, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = stealth_mode->common.object;
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? stealth_mode->common.folders.rule_folders.folder : stealth_mode->common.folders.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? stealth_mode->common.folders.log_folders.folder : stealth_mode->common.folders.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&stealth_mode->common.folders.log) ? 0 : 1;
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* fwiface = &stealth_mode->common.dest_fwiface;
    bool res = false;
    const char* chain = ipv4 ? fw_get_chain("interfacesetting", "INPUT_InterfaceSettings") : fw_get_chain("interfacesetting6", "INPUT6_InterfaceSettings");
    (void) object; // when tracing is disabled, variable is not used anymore

    when_null(amxc_var_get_first(&stealth_mode->protocols), out);
    when_true_status(stealth_mode->target == TARGET_DROP, out, res = true);

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "StealthMode[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        fw_folder_set_feature(folders[i], FW_FEATURE_PROTOCOL);

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "StealthMode[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_chain(r, chain);
        fw_rule_set_ipv4(r, ipv4);
        fw_rule_set_in_interface(r, fwiface->default_netdevname);
        if(folders[i] != log_folder) {
            fw_rule_set_target_policy(r, FW_RULE_POLICY_REJECT);
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        else {
            set_log_rule_specific_settings(r, &stealth_mode->common.folders.log);
        }
#endif //FIREWALL_LOGS

        fw_rule_set_table(r, TABLE_FILTER);
        fw_folder_push_rule(folders[i], r);

        amxc_var_for_each(var, &stealth_mode->protocols) {
            int protocol = GET_INT32(var, NULL);
            fw_rule_t* s = NULL;

            if(protocol <= 1) {
                SAH_TRACEZ_WARNING(ME, "StealthMode[%s] skipped strange protocol %d", OBJECT_NAMED, protocol);
                continue;
            }

            s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_PROTOCOL);
            when_null_trace(s, out, ERROR, "StealthMode[%s] failed to fetch rule", OBJECT_NAMED);

            fw_rule_set_protocol(s, protocol);
            fw_folder_push_rule(folders[i], s);
            SAH_TRACEZ_INFO(ME, "StealthMode[%s] added protocol %d", OBJECT_NAMED, protocol);
        }

        fw_folder_set_enabled(folders[i], true);
    }
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

static void unfold_fw_setting_stealth(stealth_mode_t* stealth_mode) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(stealth_mode, out);

    folder_container_delete_rules(&stealth_mode->common.folders, IPvAll, true);

out:
    FW_PROFILING_OUT();
    return;
}

static bool deactivate_stealth(stealth_mode_t* stealth_mode) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = stealth_mode->common.object;
    bool is_active = (stealth_mode->status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "StealthMode[%s] StealthMode is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "StealthMode[%s] deactivate StealthMode", OBJECT_NAMED);
    /* do not commit yet, wait for the activate/delete to commit it. */
    unfold_fw_setting_stealth(stealth_mode);
    _set_status(stealth_mode->common.object, STEALTH_MODE_STATUS, FIREWALL_ERROR);
    stealth_mode->status = FIREWALL_DISABLED;
out:
    FW_PROFILING_OUT();
    return true;
}

static bool activate_stealth_mode(stealth_mode_t* stealth_mode) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = stealth_mode->common.object;
    bool is_active = (stealth_mode->status == FIREWALL_ENABLED);
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(is_active) {
        res = true;
        SAH_TRACEZ_NOTICE(ME, "StealthMode[%s] StealthMode already active", OBJECT_NAMED);
        goto out;
    }

    res = ifsetting_activate_stealth(stealth_mode, true);
    when_false(res, out);

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_ERROR(ME, "StealthMode[%s] fw_commit failed", OBJECT_NAMED);
        deactivate_stealth(stealth_mode);
        goto out;
    }

    res = ifsetting_activate_stealth(stealth_mode, false);
    when_false(res, out);

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_ERROR(ME, "StealthMode[%s] fw_commit failed", OBJECT_NAMED);
        deactivate_stealth(stealth_mode);
        goto out;
    }

    stealth_mode->status = FIREWALL_ENABLED;
    if(stealth_mode->target == TARGET_REJECT) {
        _set_status(stealth_mode->common.object, STEALTH_MODE_STATUS, FIREWALL_DISABLED);
    } else {
        _set_status(stealth_mode->common.object, STEALTH_MODE_STATUS, FIREWALL_ENABLED);
    }

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}


bool stealth_mode_implement_changes(stealth_mode_t* stealth_mode) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object;     // when tracing is disabled, variable is not used anymore

    when_null_trace(stealth_mode, exit, ERROR, "StealthMode internal structure is NULL.");

    object = stealth_mode->common.object;

    if(stealth_mode->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "StealthMode[%s] about to be deleted", OBJECT_NAMED);
        deactivate_stealth(stealth_mode);
        delete_fw_stealth_mode(&stealth_mode);
    } else if(stealth_mode->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "StealthMode[%s] about to be add", OBJECT_NAMED);
        activate_stealth_mode(stealth_mode);
        stealth_mode->common.flags = 0;
    } else if(stealth_mode->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "StealthMode[%s] deactivate, activate", OBJECT_NAMED);
        deactivate_stealth(stealth_mode);
        activate_stealth_mode(stealth_mode);
        stealth_mode->common.flags = 0;
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a StealthMode
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the StealthMode internal structure.
 */
stealth_mode_t* create_fw_stealth_mode(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    stealth_mode_t* stealth_mode = NULL;

    SAH_TRACEZ_NOTICE(ME, "StealthMode[%s] is new", OBJECT_NAMED);

    stealth_mode = (stealth_mode_t*) calloc(1, sizeof(stealth_mode_t));
    when_null(stealth_mode, exit);

    common_init(&stealth_mode->common, object);

    stealth_mode->status = FIREWALL_DISABLED;
    _set_status(stealth_mode->common.object, STEALTH_MODE_STATUS, FIREWALL_ERROR);
    stealth_mode->target = TARGET_REJECT;
    amxc_var_init(&stealth_mode->protocols);
    amxc_var_set_type(&stealth_mode->protocols, AMXC_VAR_ID_LIST);
exit:
    FW_PROFILING_OUT();
    return stealth_mode;
}