/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "nat/portmapping.h"
#include "ports.h"
#include "protocols.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
#include <amx_conntrack/query.h>
#include <amx_conntrack/conntrack.h>
#endif
#include <unistd.h>
#include <stdlib.h>
#include <ipat/ipat.h>
#include <string.h>
#include <fwinterface/interface.h> // for fw_apply

#define ME "nat"

#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
static void portmapping_ct_event_cb(UNUSED const ct_query_t* query, const char* type, UNUSED const amxc_var_t* result, void* priv) {
    rule_t* rule = (rule_t*) priv;

    if(strcmp(type, "new") == 0) {
        if(rule->ct_count == 0) {
            amxp_timer_stop(rule->inactivity_timer);
        }
        ++rule->ct_count;
    } else if(strcmp(type, "destroy") == 0) {
        if(rule->ct_count != 0) {
            --rule->ct_count;
            if(rule->ct_count == 0) {
                amxp_timer_start(rule->inactivity_timer, rule->inactivity_timeout * 1000);
            }
        }
    }
}

static void portmapping_fill_ct_query_expression(amxd_object_t* object, amxc_string_t* expression) {
    amxc_var_t* addrs = NULL;
    const char* proto_name = NULL;
    uint32_t i = 0;

    addrs = netmodel_getAddrs(GET_CHAR(amxd_object_get_param_value(object, "Interface"), NULL), "", "down");
    amxc_string_appendf(expression, "(("); // all expression and protocols conditions
    proto_name = GET_CHAR(amxd_object_get_param_value(object, "Protocol"), NULL);
    if(strstr("TCP", proto_name) != NULL) {
        amxc_string_appendf(expression, "(protocol == 6)");
        ++i;
    }
    if(strstr("UDP", proto_name) != NULL) {
        if(i > 0) {
            amxc_string_appendf(expression, " || ");
        }
        amxc_string_appendf(expression, "(protocol == 17)");
    }
    amxc_string_appendf(expression, ") && "); // end of protocols
    i = 0;
    amxc_string_appendf(expression, "(");     // start of IPs conditions
    amxc_var_for_each(addr, addrs) {
        if(strcmp(GET_CHAR(addr, "Family"), "ipv4") == 0) {
            if(i == 0) {
                amxc_string_appendf(expression, "((ipversion == ipv4) && ("); // start of ipv4 IP's condition
            } else if(i > 0) {
                amxc_string_appendf(expression, " || ");
            }
            amxc_string_appendf(expression, "(origdstip == \"%s\")", GET_CHAR(addr, "Address"));
            ++i;
        }
    }
    if(i != 0) {
        amxc_string_appendf(expression, "))"); // end of ipv4 IPs condition
    }
    i = 0;
    amxc_var_for_each(addr, addrs) {
        if(strcmp(GET_CHAR(addr, "Family"), "ipv6") == 0) {
            if(i == 0) {
                if(strstr(amxc_string_get(expression, 0), "ipv4") != NULL) {
                    amxc_string_appendf(expression, " || ");                  // ipv4 or ipv6
                }
                amxc_string_appendf(expression, "((ipversion == ipv6) && ("); // start of ipv6 IP's conditons
            } else if(i > 0) {
                amxc_string_appendf(expression, " || ");
            }
            amxc_string_appendf(expression, "(origdstip == \"%s\")", GET_CHAR(addr, "Address"));
            ++i;
        }
    }
    if(i != 0) {
        amxc_string_appendf(expression, "))");                                                                         // end of ipv6 IPs condition
    }
    amxc_string_appendf(expression, ")");                                                                              // end of IP conditions
    amxc_string_appendf(expression, " && origdstport == %d)", amxd_object_get_uint32_t(object, "ExternalPort", NULL)); // end of expression
    amxc_var_delete(&addrs);
}

#endif

rule_t* portmapping_create(amxd_object_t* object) {
    rule_t* portmapping = NULL;
#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
    amxc_string_t expression;
#endif

    portmapping = rule_create(object, true);
    when_null(portmapping, out);

    portmapping->class_type = FIREWALL_PORTFW;
    portmapping->target = TARGET_ACCEPT;
#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
    if(portmapping->inactivity_timeout > 0) {
        ct_init(fw_get_parser());
        portmapping->ct_count = 0;
        amxc_string_init(&expression, 0);
        portmapping_fill_ct_query_expression(object, &expression);
        ct_query_new(&portmapping->ct_query, "tr181-firewall", amxc_string_get(&expression, 0), "new,destroy", portmapping_ct_event_cb, portmapping);
        amxc_string_clean(&expression);
    }
#endif
    SAH_TRACEZ_NOTICE(ME, "Portmapping[%s] created", OBJECT_NAMED);

out:
    return portmapping;
}

static bool portmapping_foreach_match_do_something(const char* interface, const char* ports, const char* csv_protocols, void (* do_something)(rule_t* rule)) {
    bool result = false;
    amxc_var_t protocols;
    amxc_llist_t port_range;
    char* src_interface = NULL;

    amxc_llist_init(&port_range);
    amxc_var_init(&protocols);
    amxc_var_set_type(&protocols, AMXC_VAR_ID_LIST);

    when_null_trace(interface, exit, ERROR, "Missing interface");

    src_interface = netmodel_luckyIntf(interface, "ipv4", netmodel_traverse_down);
    port_range_from_string(&port_range, ports);
    protocol_from_string(&protocols, csv_protocols);

    amxd_object_for_each(instance, it, amxd_dm_findf(fw_get_dm(), "%s", "NAT.PortMapping")) {
        amxd_object_t* rule_obj = amxc_container_of(it, amxd_object_t, it);
        rule_t* rule = NULL;

        if(rule_obj->priv == NULL) {
            continue;
        }
        rule = (rule_t*) rule_obj->priv;

        if(interface != NULL) {
            const char* netmodel_interface = amxc_string_get(&rule->common.src_fwiface.netmodel_interface, 0);
            if((netmodel_interface != NULL) && (strcmp(netmodel_interface, interface) == 0)) {
                SAH_TRACEZ_INFO(ME, "Interfaces[%s - %s] does match", interface, netmodel_interface);
            } else if((src_interface != NULL) && strcmp(rule->common.src_fwiface.default_netdevname, src_interface)) {
                SAH_TRACEZ_INFO(ME, "Interfaces[%s - %s] does not match", rule->common.src_fwiface.default_netdevname, src_interface);
                continue;
            }
        }

        if(!protocol_compare(&protocols, &rule->common.protocols)) {
            continue;
        }

        if(port_range_conflict(&rule->src_ports, &port_range)) {
            SAH_TRACEZ_NOTICE(ME, "Overlapping port range found");
            result = true;
            do_something(rule);
        }
    }
exit:
    port_range_clean(&port_range);
    amxc_var_clean(&protocols);
    free(src_interface);
    return result;
}

static void _suspend(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    rule_suspend(rule, true);
    FW_PROFILING_OUT();
}

static void _resume(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    rule_suspend(rule, false);
    FW_PROFILING_OUT();
}

void portmapping_suspend(const char* interface, const char* ports, const char* protocols) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    portmapping_foreach_match_do_something(interface, ports, protocols, _suspend);
    FW_PROFILING_OUT();
}

void portmapping_resume(const char* interface, const char* ports, const char* protocols) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    portmapping_foreach_match_do_something(interface, ports, protocols, _resume);
    FW_PROFILING_OUT();
}

static int update_hairpinnat_rules(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    rule_t* rule = NULL;

    when_null(object->priv, exit);
    rule = (rule_t*) object->priv;

    rule->common.flags = FW_MODIFIED;
    rule_update(rule);

exit:
    return 0;
}

void portmap_hairpin_address_changed(UNUSED firewall_interface_t* fwiface) {
    amxd_object_for_all(amxd_dm_findf(fw_get_dm(), "NAT"), ".PortMapping.[AllInterfaces==true && Enable == true].", update_hairpinnat_rules, NULL);
    fw_apply();
}
