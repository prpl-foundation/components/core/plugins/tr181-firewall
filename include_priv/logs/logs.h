/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _LOGS_H_
#define _LOGS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "fwrule_helpers.h" // for common_t and log_t

#define EXCLUDE_SRC_INTERFACE true
#define EXCLUDE_DST_INTERFACE true
#define USE_EXCLUDE_PARAM true
#define USE_LOG_CONTROLLER_INTERFACE true
#define ENABLE_LOG true

#ifdef FIREWALL_LOGS
bool log_get_enable(const log_t* const log);
void log_set_enable(log_t* log, bool enable);
int log_get_level(const log_t* const log);
int log_get_flags(const log_t* const log);
const csv_string_t log_get_prefix(const log_t* const log);
const csv_string_t log_get_src_intf(const log_t* const log);
const csv_string_t log_get_dst_intf(const log_t* const log);
amxd_object_t* log_get_object(const log_t* const log);

bool log_get_dm_enable_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter);
const csv_string_t log_get_dm_controller_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter);
bool log_get_dm_src_excl_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter);
bool log_get_dm_dst_excl_param(amxd_object_t* object, const amxc_var_t* changed_params, const cstring_t parameter);

void log_set_policy(log_t* log, target_t target);
bool log_update(log_t* log, bool log_enable,
                const csv_string_t log_controllers,
                bool exclude_src_interface,
                bool exclude_dst_interface,
                const amxc_var_t* const event_data);
void log_init(amxd_object_t* object,
              log_t* log,
              firewall_interface_t* src_interface,
              bool exclude_src_interface,
              firewall_interface_t* dst_interface,
              bool exclude_dst_interface,
              bool use_log_controller_interface,
              bool log_enable,
              const csv_string_t log_controllers,
              target_t rule_target,
              amxp_slot_fn_t slot_cb);
void log_clean(log_t* log);
#endif

#ifdef __cplusplus
}
#endif

#endif // _LOGS_H_
