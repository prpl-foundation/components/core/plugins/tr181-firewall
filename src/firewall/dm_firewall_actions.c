/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall/dm_firewall.h"
#include "firewall/level.h"
#include "firewall/icmp_logs.h"
#include "firewall.h"
#include "ipset/dm_ipset.h"
#include "connectiontracking/connectiontracking.h"
#include "policy/policy.h"
#include "rules.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define ME "firewall"

amxd_status_t _firewall_cleanup(amxd_object_t* object,
                                UNUSED amxd_param_t* param,
                                amxd_action_t reason,
                                UNUSED const amxc_var_t* const args,
                                UNUSED amxc_var_t* const retval,
                                UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    level_t* level = NULL;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(object->priv, exit, status = amxd_status_ok);

    level = (level_t*) object->priv;
    free(level);
    object->priv = NULL;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    clean_icmp_logs();
#endif //FIREWALL_LOGS

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _chain_instance_cleanup(amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    chain_t* chain = NULL;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(object->priv, exit, status = amxd_status_ok);

    chain = (chain_t*) object->priv;

    chain_delete(chain);
    object->priv = NULL;

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _rule_instance_cleanup(amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args,
                                     UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    rule_t* rule = NULL;

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(object->priv, exit, status = amxd_status_ok);
    rule = (rule_t*) object->priv;

    decrement_set_ref(object_da_string(rule->common.object, "SourceMatchSet"));
    decrement_set_ref(object_da_string(rule->common.object, "SourceMatchSetExclude"));
    decrement_set_ref(object_da_string(rule->common.object, "DestMatchSet"));
    decrement_set_ref(object_da_string(rule->common.object, "DestMatchSetExclude"));

    rule_delete(&rule);

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _log_instance_cleanup(amxd_object_t* object,
                                    UNUSED amxd_param_t* param,
                                    amxd_action_t reason,
                                    UNUSED const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    common_t* common = NULL;

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    common = (common_t*) object->priv;
    if(common != NULL) {
        SAH_TRACEZ_INFO(ME, "Destroy Log");
        common_clean(common);
        free(common);
    }

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _level_instance_cleanup(amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    extendedpolicy_t* priv_data = NULL;
    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_true_status(amxd_object_get_type(object) == amxd_object_template,
                     exit,
                     status = amxd_status_ok);
    priv_data = object->priv;
    when_null_status(priv_data, exit, status = amxd_status_ok);
    if(priv_data->policy != NULL) {
        level_desactivate_not_policy(object);
        policy_free(priv_data->policy);
        priv_data->policy = NULL;
    } else {
        level_desactivate_policy(object);
    }
    free(priv_data->policies);
    free(priv_data);
    object->priv = NULL;
    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _sip_cleanup(amxd_object_t* object,
                           UNUSED amxd_param_t* param,
                           amxd_action_t reason,
                           UNUSED const amxc_var_t* const args,
                           UNUSED amxc_var_t* const retval,
                           UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    connectiontracking_t* c = NULL;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(object->priv, exit, status = amxd_status_ok);

    c = (connectiontracking_t*) object->priv;
    delete_firewall_connectiontracking(&c);

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}