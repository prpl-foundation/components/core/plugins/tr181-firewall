/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "firewall.h"

#include "mock.h"
#include "test_dm_portmapping_no_logs.h"

#define TEST_IP "192.168.1.100"

static amxd_dm_t* dm = NULL;

static void a_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void a_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void a_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_nat_rule_port_75(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 75), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_nat_rule_port_81(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 81), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_nat_rule_with_range(fw_rule_t* rule3) {
    const char* dst = "192.168.1.100/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule3, 100), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void a_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.1.100/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void b_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void b_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void b_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.240/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void b_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.240/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void c_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void c_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void c_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void c_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.250/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void d_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void d_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void d_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.241/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void d_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.241/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void e_populate_filter_forward(fw_rule_t* rule1) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule1, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule1, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule1, true), 0);
    assert_int_equal(fw_rule_set_destination(rule1, dst), 0);
    assert_int_equal(fw_rule_set_destination_port(rule1, port), 0);
    assert_int_equal(fw_rule_set_source(rule1, src), 0);
    assert_int_equal(fw_rule_set_target_policy(rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule1, true), 0);
}

static void e_populate_filter_reverse(fw_rule_t* rule2) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule2, "FORWARD_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule2, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule2, true), 0);
    assert_int_equal(fw_rule_set_destination(rule2, src), 0);
    assert_int_equal(fw_rule_set_source(rule2, dst), 0);
    assert_int_equal(fw_rule_set_source_port(rule2, port), 0);
    assert_int_equal(fw_rule_set_target_policy(rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule2, true), 0);
}

static void e_populate_nat_rule(fw_rule_t* rule3) {
    const char* dst = "192.168.99.250/32";
    const char* src = "10.0.0.1/32";
    int port = 8000;

    assert_int_equal(fw_rule_set_table(rule3, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule3, "PREROUTING_PortForwarding_1"), 0);
    assert_int_equal(fw_rule_set_protocol(rule3, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule3, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule3, 80), 0);
    assert_int_equal(fw_rule_set_destination(rule3, "192.168.99.221/32"), 0);
    assert_int_equal(fw_rule_set_source(rule3, src), 0);
    assert_int_equal(fw_rule_set_target_dnat(rule3, dst, port, 0), 0);
    assert_int_equal(fw_rule_set_enabled(rule3, true), 0);
}

static void e_populate_hairpin_rule(fw_rule_t* rule) {
    const char* ip = "192.168.99.250/32";

    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(rule, "POSTROUTING_PortForwarding"), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination(rule, ip), 0);
    assert_int_equal(fw_rule_set_source(rule, "192.168.1.1/24"), 0);
    assert_int_equal(fw_rule_set_target_snat(rule, "192.168.99.221/32", 1024, 65535), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

int test_nat_portmappings_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int test_nat_portmappings_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_portmapping_initialization(UNUSED void** state) {
    amxd_object_t* pm_template = NULL;
    uint32_t remaining_time = 0;

    pm_template = amxd_dm_findf(dm, "NAT.PortMapping.");
    assert_non_null(pm_template);

    remaining_time = amxd_object_get_uint32_t(pm_template, "RemainingLeaseTime", NULL);
    assert_int_equal(remaining_time, 0);
}

void test_portmapping_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false); // for legacy reasons
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    expect_query_wan_ipv4_addresses();

    print_message("object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("4. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_create_instances(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_t trans2;

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    amxd_trans_init(&trans2);
    amxd_trans_select_pathf(&trans2, "NAT.");
    amxd_trans_set_value(uint32_t, &trans2, "MaxPortMappingNumberOfEntries", 10);
    assert_int_equal(amxd_trans_apply(&trans2, dm), 0);
    amxd_trans_clean(&trans2);
    handle_events();

    assert_int_equal(amxd_trans_init(&trans), 0);

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);                        // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    print_message("2. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
}

void test_portmapping_1_transaction(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);                        // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);

    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("2. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();
    expect_query_wan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("4. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_lease(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();
    expect_query_wan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    assert_EndTime_equals("NAT.PortMapping.pm-test.", "0001-01-01T00:00:00Z");

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_EndTime_now_or_later("NAT.PortMapping.pm-test.");

    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);
    amxut_timer_go_to_future_ms(3600 * 1000);

    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_null(object);

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_lease_if_disabled(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_EndTime_now_or_later("NAT.PortMapping.pm-test.");

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxut_timer_go_to_future_ms(3600 * 1000);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_null(object);

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_range(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    amxd_object_t* object2 = NULL;
    amxd_object_t* object3 = NULL;

    expect_query_lan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test-2");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    expect_query_lan_ipv4_addresses();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test-3");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. object 'NAT.PortMapping.pm-test-2' must exist and must be disabled\n");
    object2 = amxd_dm_findf(dm, "NAT.PortMapping.pm-test-2");
    assert_non_null(object2);
    assert_int_equal(amxd_object_get_params(object2, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("3. object 'NAT.PortMapping.pm-test-3' must exist and must be disabled\n");
    object3 = amxd_dm_findf(dm, "NAT.PortMapping.pm-test-3");
    assert_non_null(object3);
    assert_int_equal(amxd_object_get_params(object3, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("4. make Status == Enabled for the first instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPortEndRange", 100);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule_with_range, 1);
    expect_fw_replace_rule(a_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("5. make Status == Enabled for the second instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object2);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object2);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPortEndRange", 0);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule, 2);
    expect_fw_replace_rule(a_populate_hairpin_rule, 2);

    handle_events();

    assert_int_equal(amxd_object_get_params(object2, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("6. make Status == Enabled for the third instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object3);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object3);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 75);
    amxd_trans_set_value(uint32_t, &trans, "ExternalPortEndRange", 70);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_forward, 5);
    expect_fw_replace_rule(a_populate_filter_reverse, 6);
    expect_fw_replace_rule(a_populate_nat_rule_port_75, 3);
    expect_fw_replace_rule(a_populate_hairpin_rule, 3);

    handle_events();

    assert_int_equal(amxd_object_get_params(object3, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("7. make Status == Disabled\n");
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule_with_range, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("8. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    print_message("9. remove the second instance\n");
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test-2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object2 = NULL;

    print_message("10. remove the third instance\n");
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule_port_75, 1);
    expect_fw_delete_rule(a_populate_hairpin_rule, 1);

    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test-3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object3 = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_hostname(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    amxd_object_t* object_gmap = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    object_gmap = amxd_dm_findf(dm, "Devices.Device.4.");
    assert_non_null(object_gmap);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "DESKTOP-FV3HVV1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(b_populate_filter_forward, 1);
    expect_fw_replace_rule(b_populate_filter_reverse, 2);
    expect_fw_replace_rule(b_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make GMAP IPAddress == 192.168.99.250\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.99.250");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(b_populate_filter_forward, 1);
    expect_fw_delete_rule(b_populate_filter_reverse, 1);
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    expect_fw_delete_rule(b_populate_hairpin_rule, 1);

    expect_fw_replace_rule(c_populate_filter_forward, 1);
    expect_fw_replace_rule(c_populate_filter_reverse, 2);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_hairpin_rule, 1);

    handle_events();

    print_message("4. make GMAP Name == DESKTOP-FV3HVV2\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "Name", "DESKTOP-FV3HVV2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(c_populate_filter_forward, 1);
    expect_fw_delete_rule(c_populate_filter_reverse, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_delete_rule(c_populate_hairpin_rule, 1);

    handle_events();

    print_message("5. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("6. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_with_hairpinnat_with_mac_address(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    amxd_object_t* object_gmap = NULL;

    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", true);
    amxd_trans_select_pathf(&trans, ".^");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("1. object 'NAT.PortMapping.pm-test' must exist and must be disabled\n");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    object_gmap = amxd_dm_findf(dm, "Devices.Device.3.");
    assert_non_null(object_gmap);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_false(GET_BOOL(&params, "Enable"));
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("2. make Status == Enabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", "6C:02:E0:0A:E7:95");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    expect_query_wan_ipv4_addresses();

    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(d_populate_filter_forward, 1);
    expect_fw_replace_rule(d_populate_filter_reverse, 2);
    expect_fw_replace_rule(d_populate_nat_rule, 1);
    expect_fw_replace_rule(d_populate_hairpin_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("3. make GMAP IPAddress == 192.168.99.250\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.99.250");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(d_populate_filter_forward, 1);
    expect_fw_delete_rule(d_populate_filter_reverse, 1);
    expect_fw_delete_rule(d_populate_nat_rule, 1);
    expect_fw_delete_rule(d_populate_hairpin_rule, 1);

    expect_fw_replace_rule(e_populate_filter_forward, 1);
    expect_fw_replace_rule(e_populate_filter_reverse, 2);
    expect_fw_replace_rule(e_populate_nat_rule, 1);
    expect_fw_replace_rule(e_populate_hairpin_rule, 1);

    handle_events();

    print_message("4. make GMAP PhysAddress == 6C:02:E0:0A:E7:96\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object_gmap);
    amxd_trans_set_value(cstring_t, &trans, "PhysAddress", "6C:02:E0:0A:E7:96");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(e_populate_filter_forward, 1);
    expect_fw_delete_rule(e_populate_filter_reverse, 1);
    expect_fw_delete_rule(e_populate_nat_rule, 1);
    expect_fw_delete_rule(e_populate_hairpin_rule, 1);

    handle_events();

    print_message("5. make Status == Disabled\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    print_message("6. remove the instance\n");
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_del_inst(&trans, 0, "pm-test");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = NULL;

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

static const char* json_string_portmap_forward(int protocol) {
    static char buf[1024] = {0};
    size_t len = 0;

    snprintf(buf, sizeof(buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_PortForwarding\","
             "\"enable\":true,"
             "\"protocol\":%d,"
             "\"ipv6\":false,"
             "\"destination\":\"192.168.1.100/32\","
             "\"destination_excluded\":false,"
             "\"destination_port\":8000,"
             "\"destination_port_range_max\":0,", protocol);

    len = strlen(buf);
    snprintf(&buf[len], sizeof(buf) - 1 - len, "\"target\":6,"
             "\"target_option\":1");

    len = strlen(buf);
    snprintf(&buf[len], sizeof(buf) - 1 - len, "}");

    return buf;
}

static const char* json_string_portmap_reverse(int protocol) {
    static char buf[1024] = {0};
    size_t len = 0;

    snprintf(buf, sizeof(buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_PortForwarding\","
             "\"enable\":true,"
             "\"protocol\":%d,"
             "\"ipv6\":false,"
             "\"source\":\"192.168.1.100/32\","
             "\"source_port\":8000,"
             "\"source_port_range_max\":0,", protocol);

    len = strlen(buf);
    snprintf(&buf[len], sizeof(buf) - 1 - len, "\"target\":6,"
             "\"target_option\":1");

    len = strlen(buf);
    snprintf(&buf[len], sizeof(buf) - 1 - len, "}");

    return buf;
}

static const char* json_string_portmap_nat(int protocol) {
    static char buf[1024] = {0};

    snprintf(buf, sizeof(buf) - 1, "{\"table\":\"nat\","
             "\"chain\":\"PREROUTING_PortForwarding_3\","
             "\"enable\":true,"
             "\"protocol\":%d,"
             "\"ipv6\":false,"
             "\"destination_port\":\"80\","
             "\"destination_port_range_max\":\"0\","
             "\"target\":8,"
             "\"target_option\":{\"ip\":\"192.168.1.100/32\",\"max_port\":0,\"min_port\":8000}}", protocol);

    return buf;
}

void test_portmapping_csv_protocols(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxc_var_init(&params);

    expect_query_lan_ipv4_addresses();

    print_message("1. add portmapping instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-test");
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP,AH,TCP"); // include protocol AH because it has no ports
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule_json(json_string_portmap_forward(17), 1);
    expect_fw_replace_rule_json(json_string_portmap_forward(51), 2);
    expect_fw_replace_rule_json(json_string_portmap_forward(6), 3);
    expect_fw_replace_rule_json(json_string_portmap_reverse(17), 4);
    expect_fw_replace_rule_json(json_string_portmap_reverse(51), 5);
    expect_fw_replace_rule_json(json_string_portmap_reverse(6), 6);
    expect_fw_replace_rule_json(json_string_portmap_nat(17), 1);
    expect_fw_replace_rule_json(json_string_portmap_nat(51), 2);
    expect_fw_replace_rule_json(json_string_portmap_nat(6), 3);

    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-test");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_portmapping_allowed_origins(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object;


    print_message("1. set NAT.PortMappingAllowedOrigins to accept Internal rule\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "PortMappingAllowedOrigins", "User,System,UPnp,Controller,Static,UPnP_IWF,Internal");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    object = amxd_dm_findf(dm, "NAT.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "PortMappingAllowedOrigins"), NULL), "User,System,UPnp,Controller,Static,UPnP_IWF,Internal");

    print_message("2. add portmapping with Internal origin\n");
    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-allowed-origin");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Internal");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 80);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-allowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");

    print_message("3. remove Internal from PortMappingAllowedOrigins\n");
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "PortMappingAllowedOrigins", "User,System,UPnp,Controller,Static,UPnP_IWF");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(a_populate_filter_forward, 1);
    expect_fw_delete_rule(a_populate_filter_reverse, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "PortMappingAllowedOrigins"), NULL), "User,System,UPnp,Controller,Static,UPnP_IWF");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-allowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Inactive");

    expect_query_wan_ipv4_addresses();
    expect_query_lan_ipv4_addresses();

    print_message("4. add portmapping with Internal origin\n");
    amxd_trans_select_pathf(&trans, "NAT.PortMapping.");
    amxd_trans_add_inst(&trans, 0, "pm-disallowed-origin");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Internal");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "HairpinNAT", false);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "InternalClient", TEST_IP);
    amxd_trans_set_value(cstring_t, &trans, "RemoteHost", "10.0.0.1");
    amxd_trans_set_value(uint32_t, &trans, "ExternalPort", 81);
    amxd_trans_set_value(uint32_t, &trans, "InternalPort", 8000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-disallowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Inactive");

    print_message("5. add Internal to PortMappingAllowedOrigins\n");
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "PortMappingAllowedOrigins", "User,System,UPnp,Controller,Static,UPnP_IWF,Internal");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(a_populate_filter_forward, 1);
    expect_fw_replace_rule(a_populate_filter_reverse, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(a_populate_filter_forward, 3);
    expect_fw_replace_rule(a_populate_filter_reverse, 4);
    expect_fw_replace_rule(a_populate_nat_rule_port_81, 2);
    handle_events();

    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");
    object = amxd_dm_findf(dm, "NAT.PortMapping.pm-allowed-origin.");
    assert_non_null(object);
    assert_string_equal(GET_CHAR(amxd_object_get_param_value(object, "Status"), NULL), "Enabled");

}
