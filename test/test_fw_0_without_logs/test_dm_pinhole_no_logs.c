/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include "mock.h"
#include "test_dm_pinhole_no_logs.h"

#define PINHOLE_NOT_FOUND 2

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};

static const char* _jstring_pinhole(bool reverse, bool ipv4, int protocol, const char* dest_addr, int dest_port,
                                    int dest_port_max, const char* src_addr, int src_port, int src_port_max) {
    size_t len = 0;
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD%s_Pinhole\","
             "\"%s_interface\":\"eth0\","
             "\"ipv6\":%s,",
             ipv4 ? "" : "6",
             reverse ? "out" : "in",
             ipv4 ? "false" : "true");

    if(!STRING_EMPTY(src_addr)) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"source\":\"%s\",",
                 src_addr);
    }

    if(!STRING_EMPTY(dest_addr)) {
        len = strlen(_buf);
        snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"destination\":\"%s\",",
                 dest_addr);
    }

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"source_port\":%d,"
             "\"source_port_range_max\":%d,"
             "\"destination_port\":%d,"
             "\"destination_port_range_max\":%d,"
             "\"protocol\":%d,"
             "\"enable\":true,",
             src_port, src_port_max,
             dest_port, dest_port_max,
             protocol);

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"target\":6,"
             "\"target_option\":1");

    len = strlen(_buf);
    snprintf(&_buf[len], sizeof(_buf) - 1 - len, "}");
    return _buf;
}

static const char* jstring_pinhole_forward(int protocol, const char* dest_addr, int dest_port, int dest_port_max, const char* src_addr, int src_port, int src_port_max) {
    bool ipv4 = (strstr(dest_addr, ":") == NULL);
    return _jstring_pinhole(false, ipv4, protocol, dest_addr, dest_port, dest_port_max, src_addr, src_port, src_port_max);
}

static const char* jstring_pinhole_reverse(int protocol, const char* dest_addr, int dest_port, int dest_port_max, const char* src_addr, int src_port, int src_port_max) {
    bool ipv4 = (strstr(src_addr, ":") == NULL);
    return _jstring_pinhole(true, ipv4, protocol, dest_addr, dest_port, dest_port_max, src_addr, src_port, src_port_max);
}

int test_pinhole_setup(void** state) {
    mock_init(state, "pinhole.odl");
    dm = amxut_bus_dm();
    return 0;
}

int test_pinhole_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_pinhole_initialization(UNUSED void** state) {
    amxd_object_t* pinhole_template = NULL;
    uint32_t remaining_time = 0;

    pinhole_template = amxd_dm_findf(dm, "Firewall.Pinhole.");
    assert_non_null(pinhole_template);

    remaining_time = amxd_object_get_uint32_t(pinhole_template, "RemainingLeaseTime", NULL);
    assert_int_equal(remaining_time, 0);
}

void test_add_twos_pinhole_failed(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_t trans2;
    amxd_trans_t trans3;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans2);
    amxd_trans_select_pathf(&trans2, "Firewall.");
    amxd_trans_set_value(uint32_t, &trans2, "MaxPinholeNumberOfEntries", 2);
    assert_int_equal(amxd_trans_apply(&trans2, dm), 0);
    amxd_trans_clean(&trans2);
    handle_events();

    amxd_trans_init(&trans);
    print_message("add a new pinhole instance\n");

    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    amxd_trans_init(&trans3);
    print_message("add a new pinhole instance\n");

    amxd_trans_select_pathf(&trans3, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans3, 0, "pinhole-2");
    amxd_trans_set_value(cstring_t, &trans3, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans3, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans3, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans3, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans3, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans3, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans3, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(bool, &trans3, "Enable", true);
    assert_int_not_equal(amxd_trans_apply(&trans3, dm), 0);
    amxd_trans_clean(&trans3);
    handle_events();
}

void test_add_pinhole_success(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_t trans2;

    expect_query_wan_netdevname();

    print_message("Set the X_PRPL-COM_MaxPinholeNumberOfEntries to 10 then create instances should success\n");
    amxd_trans_init(&trans2);
    amxd_trans_select_pathf(&trans2, "Firewall.");
    amxd_trans_set_value(uint32_t, &trans2, "MaxPinholeNumberOfEntries", 10);
    assert_int_equal(amxd_trans_apply(&trans2, dm), 0);
    amxd_trans_clean(&trans2);
    handle_events();

    amxd_trans_init(&trans);
    print_message("add a new pinhole instance\n");

    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();
}

void test_add_pinhole(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add a new pinhole instance, status should be disabled because Enable==false\n");

    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);
    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_change_integer_protocol(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("change protocole\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "17");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(17, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(17, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();
    amxd_trans_clean(&trans);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Protocol"), "17");

    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_pinhole_forward(17, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(17, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();
    amxd_trans_clean(&trans);

    amxc_var_clean(&params);
}

void test_change_wrong_protocol(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    char* tab[5] = {
        "udp",
        "tcp",
        "tcp udp",
        "tcp,7",
        "6 17"
    };
    int i = 0;
    for(i = 0; i < 5; i++) {
        amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
        amxd_trans_set_value(cstring_t, &trans, "Protocol", tab[i]);
        assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
        handle_events();
        amxd_trans_clean(&trans);
    }
}

void test_pinhole_add_1_to_source_prefixes(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add prefix to SourcePrefixes\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8,10.0.0.9");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.9/32", 6000, 0), 2);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 3);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.9/32", 6000, 0, "192.168.1.100/32", 6001, 0), 4);
    handle_events();
    amxd_trans_clean(&trans);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_pinhole_remove_1_from_source_prefixes(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove prefix from SourcePrefixes\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.9/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.9/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();
    amxd_trans_clean(&trans);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_pinhole_disable(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("disable pinhole\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.9/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.9/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    handle_events();
    amxd_trans_clean(&trans);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
}

void test_pinhole_remove_1_from_source_prefixes_while_disabled(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove prefix from SourcePrefixes\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
}

void test_pinhole_enable(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("enable pinhole\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.pinhole-1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();
    amxd_trans_clean(&trans);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_remove_pinhole(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove pinhole\n");
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_del_inst(&trans, 0, "pinhole-1");
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxc_var_clean(&params);
}

void test_remove_source_port(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("remove SourcePort\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", -1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 0, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 0, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_add_source_port(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("add SourcePort\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", 6000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 0, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 0, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_remove_destination_port(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("remove DestPort\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "DestPort", -1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 0, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 0, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_add_destination_port(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("add DestPort\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 6001);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 0, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 0, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_ipversion4_change_dest_ip_to_ipv6(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change DestIP to IPv6\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "2001:db8::100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error");

    amxc_var_clean(&params);
}

void test_ipversion4_change_dest_ip_to_ipv4(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change DestIP to IPv4\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_change_ipversion_to_ipv6(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change IPVersion to 6\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error");

    amxc_var_clean(&params);
}

void test_ipversion6_change_dest_ip_to_ipv6(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change DestIP to IPv6\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "2001:db8::100");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", ""); // for now test without this
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "2001:db8::100/128", 6001, 0, "", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "", 6000, 0, "2001:db8::100/128", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_ipversion6_change_dest_ip_to_ipv4(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change DestIP to IPv4\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "2001:db8::100/128", 6001, 0, "", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "", 6000, 0, "2001:db8::100/128", 6001, 0), 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error");

    amxc_var_clean(&params);
}

void test_change_ipversion_to_unused(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change IPVersion to -1\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", -1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_ipversionx_change_dest_ip_to_ipv6(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change DestIP to IPv6\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "2001:db8::100");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", ""); // for now test without this
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "2001:db8::100/128", 6001, 0, "", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "", 6000, 0, "2001:db8::100/128", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_ipversionx_change_dest_ip_to_ipv4(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change DestIP to IPv4\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "2001:db8::100/128", 6001, 0, "", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "", 6000, 0, "2001:db8::100/128", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_change_ipversion_to_ipv4(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("change IPVersion to 4\n");
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(pinhole, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_method_setPinhole(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("if no instance exists with alias, a new instance should be added\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");

    assert_int_equal(amxd_object_invoke_function(template, "setPinhole", &args, &retval), 0);
    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_HTABLE);

    print_message("if an instance exists with alias, the instance should be modified\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "protocol", "17");
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");

    assert_int_equal(amxd_object_invoke_function(template, "setPinhole", &args, &retval), 0);
    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(&retval, "Protocol"), "17");

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_list_one_pinhole(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;
    int i = 0;
    struct test_data {
        const char* alias;
        int result;
    } td[] = {
        {"toberemoved", 0},
        {"doesNotExist", PINHOLE_NOT_FOUND},
        {}
    };

    amxc_var_init(&args);
    amxc_var_init(&retval);

    for(i = 0; td[i].alias; i++) {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "alias", td[i].alias);

        assert_int_equal(amxd_object_invoke_function(template, "getPinhole", &args, &retval), td[i].result);

        if(td[i].result == 0) {
            assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_LIST);
            assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &retval)), 1);
        }
    }

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_list_all_pinholes(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(template, "getPinhole", &args, &retval), 0);

    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_LIST);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &retval)) > 1), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_deletePinhole(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("delete an existing instance\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    assert_int_equal(amxd_object_invoke_function(template, "deletePinhole", &args, &retval), 0);

    print_message("delete of a non existing instance should be silently ignored\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    assert_int_equal(amxd_object_invoke_function(template, "deletePinhole", &args, &retval), 0);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_pinhole_lease_time(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_t trans2;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    print_message("Set the X_PRPL-COM_MaxPinholeNumberOfEntries to 10 then create insatnces should success\n");
    amxd_trans_init(&trans2);
    amxd_trans_select_pathf(&trans2, "Firewall.");
    amxd_trans_set_value(uint32_t, &trans2, "MaxPinholeNumberOfEntries", 10);
    assert_int_equal(amxd_trans_apply(&trans2, dm), 0);
    amxd_trans_clean(&trans2);
    handle_events();

    amxd_trans_init(&trans);
    print_message("add a new pinhole instance\n");
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "DestPortRangeMax", "6000"); // ignored because < DestPort, should Status be Error_Misconfigured?
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 3600);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    assert_EndTime_now_or_later("Firewall.Pinhole.pinhole-1.");

    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    amxut_timer_go_to_future_ms(3600 * 1000);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_null(pinhole);
}

void test_pinhole_dest_range(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    print_message("add a new pinhole instance\n");
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "DestPortRangeMax", "7000");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 0);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 7000, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 7000), 2);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    print_message("Change the DestPortRangeMax\n");
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestPortRangeMax", "0");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 7000, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 7000), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    print_message("Delete instance\n");
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_del_inst(&trans, 0, "pinhole-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_null(pinhole);
}

void test_pinhole_src_range(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "SourcePortRangeMax", "6200");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 0);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 6200), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 6200, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "SourcePortRangeMax", "0");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 6200), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 6200, "192.168.1.100/32", 6001, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_del_inst(&trans, 0, "pinhole-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_null(pinhole);
}

void test_pinhole_range(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* pinhole = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "SourcePortRangeMax", "6200");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "DestPortRangeMax", "7000");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 0);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 7000, "10.0.0.8/32", 6000, 6200), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 6200, "192.168.1.100/32", 6001, 7000), 2);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_object(&trans, pinhole);
    amxd_trans_set_value(cstring_t, &trans, "DestPortRangeMax", "0");
    amxd_trans_set_value(cstring_t, &trans, "SourcePortRangeMax", "0");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 7000, "10.0.0.8/32", 6000, 6200), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 6200, "192.168.1.100/32", 6001, 7000), 1);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    expect_fw_delete_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_delete_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 1);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_del_inst(&trans, 0, "pinhole-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_null(pinhole);
}

void test_method_updatePinhole(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxd_trans_t trans;
    amxc_var_t retval;
    amxc_var_t args;
    amxd_object_t* pinhole = NULL;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("if no instance exists with alias, should return error\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    amxc_var_add_key(cstring_t, &args, "leaseDuration", "60");
    amxc_var_add_key(cstring_t, &args, "origin", "Controller");

    assert_int_not_equal(amxd_object_invoke_function(template, "updatePinhole", &args, &retval), 0);

    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Pinhole.");
    amxd_trans_add_inst(&trans, 0, "pinhole-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "SourcePort", "6000");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "6001");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.0.8");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.1.100");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "Controller");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4); // for legacy reasons
    amxd_trans_set_value(uint32_t, &trans, "LeaseDuration", 60);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_pinhole_forward(6, "192.168.1.100/32", 6001, 0, "10.0.0.8/32", 6000, 0), 1);
    expect_fw_replace_rule_json(jstring_pinhole_reverse(6, "10.0.0.8/32", 6000, 0, "192.168.1.100/32", 6001, 0), 2);
    handle_events();

    pinhole = amxd_dm_findf(dm, "Firewall.Pinhole.pinhole-1.");
    assert_non_null(pinhole);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "pinhole-1");
    amxc_var_add_key(cstring_t, &args, "leaseDuration", "60");
    amxc_var_add_key(cstring_t, &args, "origin", "Controller");

    assert_int_equal(amxd_object_invoke_function(template, "updatePinhole", &args, &retval), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "pinhole-1");
    amxc_var_add_key(cstring_t, &args, "leaseDuration", "120");
    amxc_var_add_key(cstring_t, &args, "origin", "Controller");

    assert_int_equal(amxd_object_invoke_function(template, "updatePinhole", &args, &retval), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}