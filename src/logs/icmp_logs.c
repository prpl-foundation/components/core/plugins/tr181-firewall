/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "fwrule_helpers.h"
#include "firewall.h" // for fw_get_chain
#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "firewall/icmp_logs.h"
#include "logs/logs.h"

#define ME "firewall"

#define PROTOCOL_ICMP 1
#define PROTOCOL_ICMPv6 58

typedef enum {
    FOLDER_INPUT = 1,
    FOLDER_FORWARD = 2,
    FOLDER_OUTPUT = 3,
} icmp_logs_folder_t;

static log_t log_icmp;
static fw_folder_t* input = NULL;
static fw_folder_t* input6 = NULL;
static fw_folder_t* forward = NULL;
static fw_folder_t* forward6 = NULL;
static fw_folder_t* output = NULL;
static fw_folder_t* output6 = NULL;

static void unfold_fw_enable(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_folder_delete_rules(input);
    fw_folder_delete_rules(input6);
    fw_folder_delete_rules(forward);
    fw_folder_delete_rules(forward6);
    fw_folder_delete_rules(output);
    fw_folder_delete_rules(output6);
    fw_commit(fw_rule_callback);
    fw_apply();
    FW_PROFILING_OUT();
}

static void disable_icmp_logs(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    unfold_fw_enable();
    FW_PROFILING_OUT();
}

static bool fw_icmp_logs_activate(bool ipv4, icmp_logs_folder_t icmp_logs_folder) {
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    fw_folder_t* folder = NULL;
    int protocol = ipv4 ? PROTOCOL_ICMP : PROTOCOL_ICMPv6;
    bool res = false;
    const char* chain = NULL;

    if(icmp_logs_folder == FOLDER_INPUT) {
        folder = (ipv4) ? input : input6;
        chain = ipv4 ? fw_get_chain("log_input_first", "INPUT_First_Log") : fw_get_chain("log_input6_first", "INPUT6_First_Log");
    } else if(icmp_logs_folder == FOLDER_FORWARD) {
        folder = (ipv4) ? forward : forward6;
        chain = ipv4 ? fw_get_chain("log_forward_first", "FORWARD_First_Log") : fw_get_chain("log_forward6_first", "FORWARD6_First_Log");
    } else {
        folder = (ipv4) ? output : output6;
        chain = ipv4 ? fw_get_chain("log_output", "OUTPUT_Last_Log") : fw_get_chain("log_output6", "OUTPUT6_Last_Log");
    }

    fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "Failed to fetch rule for the Firewall.ICMPLogs");

    fw_rule_set_chain(r, chain);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_table(r, TABLE_FILTER);
    set_log_rule_specific_settings(r, &log_icmp);
    fw_folder_push_rule(folder, r);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL);
    when_null_trace(s, out, ERROR, "Failed to fetch rule");

    fw_rule_set_protocol(s, protocol);
    fw_folder_push_rule(folder, s);

    fw_folder_set_enabled(folder, true);
    res = true;
out:
    return res;
}

static bool icmp_log_enable_input_ipv4(void) {
    return fw_icmp_logs_activate(true, FOLDER_INPUT);
}

static bool icmp_log_enable_input_ipv6(void) {
    return fw_icmp_logs_activate(false, FOLDER_INPUT);
}

static bool icmp_log_enable_forward_ipv4(void) {
    return fw_icmp_logs_activate(true, FOLDER_FORWARD);
}

static bool icmp_log_enable_forward_ipv6(void) {
    return fw_icmp_logs_activate(false, FOLDER_FORWARD);
}

static bool icmp_log_enable_output_ipv4(void) {
    return fw_icmp_logs_activate(true, FOLDER_OUTPUT);
}

static bool icmp_log_enable_output_ipv6(void) {
    return fw_icmp_logs_activate(false, FOLDER_OUTPUT);
}

static bool enable_icmp_logs(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool status = false;

    when_null(input, exit);
    when_null(input6, exit);
    when_null(forward, exit);
    when_null(forward6, exit);
    when_null(output, exit);
    when_null(output6, exit);

    when_false(icmp_log_enable_input_ipv4(), exit);
    when_false(icmp_log_enable_input_ipv6(), exit);
    when_false(icmp_log_enable_forward_ipv4(), exit);
    when_false(icmp_log_enable_forward_ipv6(), exit);
    when_false(icmp_log_enable_output_ipv4(), exit);
    when_false(icmp_log_enable_output_ipv6(), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.ICMPLogs");
    when_failed_trace(fw_apply(), exit, ERROR, "fw_apply failed for the Firewall.ICMPLogs");

    status = true;

exit:
    if(!status) {
        disable_icmp_logs();
    }
    FW_PROFILING_OUT();
    return status;
}

static bool change_icmp_logs(bool enable) {
    bool status = false;
    if(enable) {
        status = enable_icmp_logs();
    } else {
        disable_icmp_logs();
        status = true;
    }
    return status;
}

bool update_icmp_logs(const amxc_var_t* const event_data) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    bool update = false;
    bool status = false;

    when_null(input, exit);
    object = log_get_object(&log_icmp);
    when_null_trace(object, exit, ERROR, "Object is NULL");

    update = log_update(&log_icmp, log_get_dm_enable_param(object, event_data, "ICMPLogs"),
                        log_get_dm_controller_param(object, event_data, "ICMPLogController"),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        event_data);
    when_false(update, exit);
    status = change_icmp_logs(log_icmp.log_info.enable);

exit:
    FW_PROFILING_OUT();
    return status;
}

/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    update_icmp_logs(data);
    FW_PROFILING_OUT();
    return;
}

void init_icmp_logs(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    fw_folder_new(&input);
    fw_folder_new(&input6);
    fw_folder_new(&forward);
    fw_folder_new(&forward6);
    fw_folder_new(&output);
    fw_folder_new(&output6);

    log_init(object, &log_icmp, NULL, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             log_get_dm_enable_param(object, NULL, "ICMPLogs"), log_get_dm_controller_param(object, NULL, "ICMPLogController"), TARGET_ACCEPT, update_rule_log_info_cb);
    change_icmp_logs(log_icmp.log_info.enable);

    FW_PROFILING_OUT();
}

void clean_icmp_logs(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    disable_icmp_logs();

    fw_folder_delete(&input);
    fw_folder_delete(&input6);
    fw_folder_delete(&forward);
    fw_folder_delete(&forward6);
    fw_folder_delete(&output);
    fw_folder_delete(&output6);

    log_clean(&log_icmp);

    FW_PROFILING_OUT();
}