/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ports.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "firewall"

#define PORT_END(START, END) ((END > 0) ? END : START)

static void delete_port(amxc_llist_it_t* it) {
    port_range_t* port_range = amxc_container_of(it, port_range_t, it);
    free(port_range);
}

void port_range_clean(amxc_llist_t* ports) {
    when_null(ports, out);

    amxc_llist_clean(ports, delete_port);

out:
    return;
}

static bool port_range_fill(port_range_t* port_range, uint32_t start, uint32_t end) {
    bool rv = false;

    when_null(port_range, exit);

    port_range->start = start > 0 ? start : 0;
    port_range->end = end > 0 ? end : 0;

    rv = true;
exit:
    return rv;
}

static port_range_t* port_range_create(amxc_llist_t* list, uint32_t start, uint32_t end) {
    port_range_t* port_range = NULL;

    port_range = (port_range_t*) calloc(1, sizeof(port_range_t));
    when_null(port_range, out);

    port_range_fill(port_range, start, end);

    amxc_llist_append(list, &port_range->it);

out:
    return port_range;
}

bool port_range_from_string(amxc_llist_t* list, const char* ports) {
    bool retval = false;
    amxc_var_t ports_csv;

    amxc_var_init(&ports_csv);

    when_null_trace(list, out, ERROR, "Missing list");
    when_str_empty_trace(ports, out, ERROR, "Missing ports");

    amxc_var_set(csv_string_t, &ports_csv, ports);
    amxc_var_cast(&ports_csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(port, &ports_csv) {
        int start = amxc_var_dyncast(int32_t, port);
        port_range_t* port_range = NULL;
        if(start <= 0) {
            continue;
        }
        port_range = port_range_create(list, start, 0);
        when_null_trace(port_range, out, ERROR, "Failed to convert port %d", start);
    }

    retval = true;
out:
    amxc_var_clean(&ports_csv);
    return retval;
}

bool port_range_in_ports(const amxc_llist_t* const ports, port_range_t* pr_a) {
    int min_a = pr_a->start;
    int max_a = PORT_END(pr_a->start, pr_a->end);

    amxc_llist_iterate(it, ports) {
        port_range_t* pr_b = amxc_container_of(it, port_range_t, it);
        int min_b = 0;
        int max_b = 0;

        min_b = pr_b->start;
        max_b = PORT_END(pr_b->start, pr_b->end);
        if(((min_a >= min_b) && (min_a <= max_b)) || ((min_b >= min_a) && (max_b <= max_a))) {
            SAH_TRACEZ_INFO(ME, "Found overlap: a[%d-%d], b[%d-%d]", min_a, max_a, min_b, max_b);
            return true;
        }
    }

    return false;
}

bool port_range_conflict(const amxc_llist_t* const a, const amxc_llist_t* const b) {
    when_null(a, out);
    when_null(b, out);

    amxc_llist_iterate(it_a, a) {
        port_range_t* pr_a = amxc_container_of(it_a, port_range_t, it);

        if(port_range_in_ports(b, pr_a)) {
            return true;
        }
    }
out:
    return false;
}

static bool append_port_range(amxc_llist_t* list, int start, int end) {
    bool rv = false;

    if(start > 0) {
        port_range_t* port_range = port_range_create(list, start, (start < end) ? end : 0);
        when_null(port_range, out);
    }

    rv = true;
out:
    return rv;
}

bool port_range_from_int(amxc_llist_t* list, int start, int end) {
    port_range_clean(list);
    return append_port_range(list, start, end);
}