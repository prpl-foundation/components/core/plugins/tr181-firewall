/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "firewall.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"

#define PROFILING_LOG_PATH "/tmp/fw_profile"

typedef struct _profiling_stack_ {
    amxc_llist_it_t it;
    amxc_string_t function;
    amxc_string_t file;
    struct timespec ts;
    int line;
    bool in;
    trace_type_t type;
    struct timespec ts_in;
    int identation;
} profiling_stack_t;

static amxc_llist_t* stack_trace = NULL;
static amxc_llist_t* measurement_stack_trace = NULL;
static int count_ident = 0;
static int measurement_count_ident = 0;

static profiling_stack_t* create_entry(const char* function, const char* file, int line, trace_type_t trace_type) {
    profiling_stack_t* profile_entry = NULL;

    when_str_empty(function, exit);
    when_str_empty(file, exit);

    profile_entry = (profiling_stack_t*) calloc(1, sizeof(profiling_stack_t));
    when_null(profile_entry, exit);

    clock_gettime(CLOCK_MONOTONIC_RAW, &profile_entry->ts);

    amxc_string_init(&profile_entry->function, 0);
    amxc_string_init(&profile_entry->file, 0);

    amxc_string_setf(&profile_entry->function, "%s", function);
    amxc_string_setf(&profile_entry->file, "%s", file);
    profile_entry->line = line;
    profile_entry->type = trace_type;

exit:
    return profile_entry;
}

static void delete_entry(profiling_stack_t* entry) {
    when_null(entry, exit);
    amxc_string_clean(&entry->function);
    amxc_string_clean(&entry->file);
    free(entry);
exit:
    return;
}

static void entry_llist_delete(amxc_llist_it_t* it) {
    profiling_stack_t* entry = amxc_container_of(it, profiling_stack_t, it);
    delete_entry(entry);
}

static void add_entry_to_stack(const char* function, const char* file, int line, bool in, trace_type_t trace_type, struct timespec* ts, bool unit_measurement) {
    profiling_stack_t* profile_entry = create_entry(function, file, line, trace_type);
    when_null(profile_entry, exit);
    profile_entry->in = in;
    if(!in) {
        profile_entry->ts_in.tv_nsec = ts->tv_nsec;
        profile_entry->ts_in.tv_sec = ts->tv_sec;
        if(!unit_measurement) {
            count_ident--;
            if(count_ident < 0) {
                count_ident = 0;
            }
            profile_entry->identation = count_ident;
        } else {
            measurement_count_ident--;
            if(measurement_count_ident < 0) {
                measurement_count_ident = 0;
            }
            profile_entry->identation = measurement_count_ident;
        }
    } else {
        ts->tv_nsec = profile_entry->ts.tv_nsec;
        ts->tv_sec = profile_entry->ts.tv_sec;
        if(!unit_measurement) {
            profile_entry->identation = count_ident;
            count_ident++;
        } else {
            profile_entry->identation = measurement_count_ident;
            measurement_count_ident++;
        }
    }

    if(!unit_measurement) {
        if(stack_trace == NULL) {
            amxc_llist_new(&stack_trace);
        }

        when_failed(amxc_llist_append(stack_trace, &profile_entry->it), exit);
    } else {
        if(measurement_stack_trace == NULL) {
            amxc_llist_new(&measurement_stack_trace);
        }

        when_failed(amxc_llist_append(measurement_stack_trace, &profile_entry->it), exit);
    }
exit:
    return;
}

void profilingTraceIn(const char* function, const char* file, int line, trace_type_t trace_type, struct timespec* ts) {
    add_entry_to_stack(function, file, line, true, trace_type, ts, false);
}

void profilingTraceOut(const char* function, const char* file, int line, struct timespec* ts) {
    add_entry_to_stack(function, file, line, false, TRACE_INTERNAL, ts, false);
}

static long int get_elapsed_time_ns(profiling_stack_t* entry) {
    long int ns_in = 0;
    long int ns_out = 0;
    if(!entry->in) {
        ns_in = ((long int) entry->ts_in.tv_sec * 1000000000) + (long int) entry->ts_in.tv_nsec;
        ns_out = ((long int) entry->ts.tv_sec * 1000000000) + (long int) entry->ts.tv_nsec;
        return ns_out - ns_in;
    } else {
        return 0;
    }
}

static long int get_log_elapsed_time_ns(void) {
    const int number_of_probes = 1000;
    long int total_elapsed_time = 0;

    for(int i = 0; i < 1000; i++) {
        profiling_stack_t* measurement_out = NULL;
        struct timespec ts;
        ts.tv_nsec = 0;
        ts.tv_sec = 0;
        long int log_elapsed_time = 0;
        amxc_llist_delete(&measurement_stack_trace, entry_llist_delete);
        add_entry_to_stack("MEASURE_PROFILE", "MEASURE_PROFILE", 0, true, TRACE_INTERNAL, &ts, true);
        add_entry_to_stack("MEASURE_PROFILE", "MEASURE_PROFILE", 0, false, TRACE_INTERNAL, &ts, true);
        measurement_out = amxc_container_of(amxc_llist_take_last(measurement_stack_trace), profiling_stack_t, it);
        log_elapsed_time = get_elapsed_time_ns(measurement_out);
        total_elapsed_time += log_elapsed_time;
    }
    amxc_llist_delete(&measurement_stack_trace, entry_llist_delete);

    return total_elapsed_time / number_of_probes;
}

static void read_previous_log(amxc_string_t* string) {
    FILE* p_file = NULL;
    cstring_t buffer = NULL;
    long length = 0;
    size_t bytes_read = 0;
    const char* fname = PROFILING_LOG_PATH;

    when_str_empty(fname, exit);

    p_file = fopen(fname, "rb");
    when_null(p_file, exit);

    fseek(p_file, 0, SEEK_END);
    length = ftell(p_file);
    fseek(p_file, 0, SEEK_SET);
    buffer = calloc(1, length + 1);
    when_null(buffer, exit);
    bytes_read = fread(buffer, 1, length, p_file);

    if(bytes_read <= 0) {
        free(buffer);
        goto exit;
    }
    amxc_string_push_buffer(string, buffer, length + 1);

exit:
    if(p_file != NULL) {
        fclose(p_file);
    }
}

static void write_log(amxc_string_t* string) {
    FILE* p_file = NULL;
    const char* fname = PROFILING_LOG_PATH;

    when_str_empty(fname, exit);
    when_str_empty(amxc_string_get(string, 0), exit);

    p_file = fopen(fname, "w");
    when_null(p_file, exit);
    fputs(amxc_string_get(string, 0), p_file);
    fclose(p_file);

exit:
    return;
}

amxd_status_t _dumpProfiling(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxc_string_t* log_file = NULL;
    long int log_elapsed_time = get_log_elapsed_time_ns();
    long int total_log_elapsed_time = (log_elapsed_time * amxc_llist_size(stack_trace));

    amxc_string_new(&log_file, 0);
    read_previous_log(log_file);

    amxc_string_appendf(log_file, "\nDUMPING TRACES TO FILE - dumpProfiling()\n");
    amxc_string_appendf(log_file, "Each profiling line adds an overhead of = %ld s, %ld ms, %ld us, %ld ns\n",
                        (long int) (log_elapsed_time / 1000000000),
                        (long int) (log_elapsed_time / 1000000),
                        (long int) (log_elapsed_time / 1000),
                        log_elapsed_time
                        );
    amxc_string_appendf(log_file, "PS.: Elapsed times given are the compensated values considering the overhead created by the profiling\n");
    amxc_string_appendf(log_file, "Total overhead caused by profiling traces = %ld s, %ld ms, %ld us, %ld ns\n\n",
                        (long int) (total_log_elapsed_time / 1000000000),
                        (long int) (total_log_elapsed_time / 1000000),
                        (long int) (total_log_elapsed_time / 1000),
                        total_log_elapsed_time
                        );
    amxc_llist_for_each(it, stack_trace) {
        profiling_stack_t* entry = amxc_container_of(it, profiling_stack_t, it);
        if(entry == NULL) {
            continue;
        }

        struct tm* ptm = localtime(&entry->ts.tv_sec);
        if(ptm == NULL) {
            continue;
        }

        long int elapsed_time = get_elapsed_time_ns(entry) - log_elapsed_time;
        if(elapsed_time < 500) {
            elapsed_time = 500;
        }
        amxc_string_appendf(log_file, "|");
        for(int i = 0; i < entry->identation; i++) {
            amxc_string_appendf(log_file, "    |");
        }
        amxc_string_appendf(log_file, "%d:%02d:%02d.%09ld ",
                            ptm->tm_hour * ptm->tm_yday,
                            ptm->tm_min,
                            ptm->tm_sec,
                            entry->ts.tv_nsec);
        if(entry->type == TRACE_DM_EVENT) {
            amxc_string_appendf(log_file, "EVENT ");
        } else if(entry->type == TRACE_DM_ACTION) {
            amxc_string_appendf(log_file, "ACTION ");
        } else if(entry->type == TRACE_DM_RPC) {
            amxc_string_appendf(log_file, "RPC ");
        } else if(entry->type == TRACE_CALLBACK) {
            amxc_string_appendf(log_file, "CALLBACK ");
        }
        if(entry->in) {
            amxc_string_appendf(log_file, "---------> ");
        } else {
            amxc_string_appendf(log_file, "<<<<<<<<<< ");
        }
        amxc_string_appendf(log_file, "%s@%s:%d ",
                            amxc_string_get(&entry->function, 0),
                            amxc_string_get(&entry->file, 0),
                            entry->line
                            );
        if(!entry->in) {
            amxc_string_appendf(log_file, "ELAPSED TIME = %ld s, %ld ms, %ld us, %ld ns",
                                (long int) (elapsed_time / 1000000000),
                                (long int) (elapsed_time / 1000000),
                                (long int) (elapsed_time / 1000),
                                elapsed_time
                                );
        }
        amxc_string_appendf(log_file, "\n");
    }

    write_log(log_file);
    amxc_llist_delete(&stack_trace, entry_llist_delete);
    amxc_llist_delete(&measurement_stack_trace, entry_llist_delete);
    amxc_string_delete(&log_file);
    count_ident = 0;
    measurement_count_ident = 0;
    return amxd_status_ok;
}