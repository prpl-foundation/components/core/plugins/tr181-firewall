/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _IP_ADDRESS_H_
#define _IP_ADDRESS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

typedef enum {
    IPvAll = 0,
    IPv4 = 4,
    IPv6 = 6,
    IPvUnknown = -1
} ipversion_t;

ipversion_t int_to_ipversion(int int_version);

typedef enum {
    ADDRESS_IPv4,
    ADDRESS_IPv6,
#ifdef WITH_GMAP
    ADDRESS_DEVICE_IPv4,
    ADDRESS_DEVICE_IPv6,
#endif
    ADDRESS_NUM_OF_LISTS // last item in enum
} address_list_index_t;

typedef enum {
    REASON_ADDRESS_STRING = (1 << 1),
    REASON_ADDRESS_DEVICE = (1 << 2),
} required_reason_t;

typedef struct {
    int required; // type integer and not boolean because some datamodel objects might get addresses from parameter AND device query
    amxc_var_t* list[ADDRESS_NUM_OF_LISTS];
    size_t ipv4_size;
    size_t ipv6_size;
} address_t;

void address_init(address_t* address);
void address_clean(address_t* address);
const char* address_first_ipv4_char(address_t* address);
const char* address_first_ipv4_host_address(address_t* address);
bool address_from_string(const char* addresses, address_t* address);
bool address_has_ipversion(address_t* address, ipversion_t ipversion);
size_t address_count(address_t* address, ipversion_t ipversion);
bool address_is_required(address_t* address);
bool require_address(bool require, address_t* address, required_reason_t reason);
const char* network_address(amxc_var_t* prefix);
const char* host_address(amxc_var_t* prefix);

#ifdef WITH_GMAP
bool address_add_device(address_t* address, const char* alias, amxc_var_t* device_params);
bool address_delete_device(address_t* address, const char* alias);
void address_clean_devices(address_t* address);
#endif

#ifdef __cplusplus
}
#endif

#endif // _IP_ADDRESS_H_
