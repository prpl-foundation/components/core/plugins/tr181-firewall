/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall.h"
#include "init_cleanup.h"
#include "firewall_utilities.h" // for STRING_EMPTY
#ifdef WITH_GMAP
#include "devices_query.h"
#endif
#include "profiling/profiling.h"
#include <netmodel/client.h>    // for netmodel_initialize
#include <schedules/schedule.h> // for schedule_initialize
#include <ipset/ipset.h>        // for ipset_initialize
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
#include <amx_conntrack/conntrack.h>
#endif
#include <fwinterface/interface.h> // for fw_apply

#define ME "firewall"

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} fw_app_t;

static fw_app_t app;

static void fw_init(amxd_dm_t* dm,
                    amxo_parser_t* parser) {
    app.dm = dm;
    app.parser = parser;
    netmodel_initialize();
    schedule_initialize();
    porttrigger_nfqueue_start();
    ipset_initialize();
}

static void fw_exit(void) {
#ifdef WITH_GMAP
    fwgmap_cleanup();
#endif
    porttrigger_nfqueue_stop();
    schedule_cleanup();
    netmodel_cleanup();
    ifsettings_cleanup();
    ipset_cleanup();
#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
    ct_cleanup();
#endif

    app.dm = NULL;
    app.parser = NULL;
}

amxd_dm_t* PRIVATE fw_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE fw_get_parser(void) {
    return app.parser;
}

const char* fw_get_vendor_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(fw_get_parser(), "prefix_");
    const char* prefix = amxc_var_constcast(cstring_t, setting);
    return (prefix != NULL) ? prefix : "";
}

const char* get_vendor_prefixed(const char* config_opt, const char* default_value) {
    amxc_var_t* var = NULL;
    const char* prefixed = NULL;
    var = amxo_parser_get_config(fw_get_parser(), config_opt);
    prefixed = GET_CHAR(var, NULL);
    return prefixed != NULL ? prefixed : default_value;
}

const char* fw_get_chain(const char* search_path, const char* default_chain) {
    amxc_var_t* iptables = amxo_parser_get_config(fw_get_parser(), "iptables_chains");
    const char* chain = GETP_CHAR(iptables, search_path);
    if(STRING_EMPTY(chain)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get chain, using default %s", default_chain);
        chain = default_chain;
    }
    return chain;
}

uint32_t get_nfqueue_num(uint32_t default_qnum) {
    amxc_var_t* iptables = amxo_parser_get_config(fw_get_parser(), "netfilter_queue");
    uint32_t qnum = GET_UINT32(iptables, "porttrigger_queue_number");
    if(qnum == 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to get queue number, using default %u", default_qnum);
        qnum = default_qnum;
    }
    return qnum;
}

int _fw_main(int reason,
             amxd_dm_t* dm,
             amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);
    switch(reason) {
    case 0:     // START
        fw_init(dm, parser);
        break;
    case 1:     // STOP
        fw_exit();
        break;
    default:
        break;
    }

    return 0;
}

// assuming between entry point and event app:start the default ODL files are loaded but those don't generate events so initialize them here
void _app_start(UNUSED const char* const sig_name,
                UNUSED const amxc_var_t* const data,
                UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);

#ifdef WITH_GMAP
    if(!fwgmap_init()) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize GMAP");
    }
#endif
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    dm_logs_init();
#endif //FIREWALL_LOGS
    dm_ipset_init();
    dm_firewall_init();
    dm_policies_init();
    dm_ifsetting_init();
    dm_services_init();
    dm_pinholes_init();
    dm_dmz_init();
    dm_nat_init();
    init_static_nat();
    dm_porttriggers_init();
    dm_connectiontracking_init();

    dm_firewall_start();

    fw_apply();

    FW_PROFILING_OUT();
}
