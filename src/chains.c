/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "chains.h"
#include "firewall_utilities.h"    // for OBJECT_NAMED
#include "firewall.h"              // for fw_get_dm
#include "rules.h"                 // for rule_t
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <fwinterface/interface.h> // for fw_apply

#define ME "firewall"

chain_t* chain_create(amxd_object_t* object) {
    chain_t* chain = NULL;

    SAH_TRACEZ_INFO(ME, "Chain[%s] is new", object != NULL ? OBJECT_NAMED : "");

    chain = (chain_t*) calloc(1, sizeof(chain_t));
    when_null(chain, out);

    chain->object = object;
    if(object != NULL) {
        amxd_object_t* level = amxd_dm_findf(fw_get_dm(), "Firewall.Level.%s.", OBJECT_NAMED);
        if(level != NULL) {
            chain->target = string_to_target(object_da_string(level, "DefaultPolicy"));
        } else {
            SAH_TRACEZ_INFO(ME, "Chain[%s] requires level for default policy", OBJECT_NAMED);
        }
        object->priv = chain;
    }

out:
    return chain;
}

bool chain_activate(chain_t* chain) {
    amxd_object_t* object = chain->object;

    when_false_trace(chain_is_enabled(object), exit, INFO, "Chain[%s] is disabled", OBJECT_NAMED);

    SAH_TRACEZ_INFO(ME, "Chain[%s] activate rules", OBJECT_NAMED);

    amxd_object_for_each(instance, it, amxd_object_findf(object, "%s", ".Rule")) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        rule_t* rule = NULL;

        if(obj->priv == NULL) {
            continue;
        }
        rule = (rule_t*) obj->priv;

        if(rule->common.flags != 0) { // already being configured
            continue;
        }
        rule->common.flags = FW_MODIFIED;
        rule_activate(rule);
    }

exit:
    return true;
}

bool chain_deactivate(chain_t* chain) {
    amxd_object_t* object = chain->object;
    SAH_TRACEZ_INFO(ME, "Chain[%s] deactivate rules", OBJECT_NAMED);

    amxd_object_for_each(instance, it, amxd_object_findf(object, "%s", ".Rule")) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        rule_t* rule = NULL;

        if(obj->priv == NULL) {
            continue;
        }
        rule = (rule_t*) obj->priv;

        rule_deactivate(rule);
    }

    return true;
}

bool chain_is_enabled(amxd_object_t* obj) { // todo move to rule.c?
    bool enable = false;

    when_null_status(obj, exit, enable = true); // for all non Firewall.Chain.i.Rule.i users of rule_t (portmap, pinhole, ...)
    when_null(obj->priv, exit);                 // chain not initialized

    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
exit:
    return enable;
}

bool chain_delete(chain_t* chain) {
    SAH_TRACEZ_INFO(ME, "Delete Firewall Chain");

    // Firewall.Chain.i.Rule should have already been destroyed by amx

    if(chain->object != NULL) {
        chain->object->priv = NULL;
    }

    free(chain);
    return true;
}

const char* chain_get_name(amxd_object_t* obj) {
    const char* name = NULL;

    when_null(obj, exit);

    name = object_da_string(obj, "Name");

exit:
    return name;
}
