/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "policy/dm_policy.h"
#include "policy/policy.h"
#include "logs/logs.h"
#include "firewall.h" // for fw_get_dm
#include "init_cleanup.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "policy"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_target_log_cb(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    policy_t* policy = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    policy = (policy_t*) object->priv;
    when_null_trace(policy, exit, ERROR, "Policy[%s] is NULL", OBJECT_NAMED);

    if(update_policy_logs(NULL, object, NULL, data)) {
        policy->common.flags = FW_MODIFIED;
        policy_activate(policy);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

static void set_interface(policy_t* policy, const char* src_interface, const char* dest_interface, ipversion_t ipversion) {
    if(dest_interface != NULL) {
        firewall_interface_t* dest_fwiface = &policy->common.dest_fwiface;
        firewall_interface_t* dest6_fwiface = &policy->common.dest6_fwiface;

        if(!STRING_EMPTY(dest_interface)) {
            SAH_TRACEZ_INFO(ME, "Configure Destination Interface[%s]", dest_interface);
            to_linux_interface_dual_query(dest_interface, ipversion, dest_fwiface, dest6_fwiface);
        } else {
            init_firewall_interface(dest_fwiface);
            init_firewall_interface(dest6_fwiface);
        }
    }

    if(src_interface != NULL) {
        firewall_interface_t* src_fwiface = &policy->common.src_fwiface;
        firewall_interface_t* src6_fwiface = &policy->common.src6_fwiface;

        if(!STRING_EMPTY(src_interface)) {
            SAH_TRACEZ_INFO(ME, "Configure Source Interface[%s]", src_interface);
            to_linux_interface_dual_query(src_interface, ipversion, src_fwiface, src6_fwiface);
        } else {
            init_firewall_interface(src_fwiface);
            init_firewall_interface(src6_fwiface);
        }
    }
}

static chain_t* chain_get_user_data(const char* path) {
    amxd_object_t* object = NULL;
    chain_t* chain = NULL;
    amxd_object_t* root = amxd_dm_get_root(fw_get_dm());

    object = amxd_object_findf(root, "%s", path);
    when_null_trace(object, out, WARNING, "Failed to find chain[%s]", path);

    chain = (chain_t*) object->priv;
    when_null_trace(chain, out, WARNING, "Chain[%s] private data is NULL", OBJECT_NAMED);

out:
    return chain;
}

typedef enum {
    REVERSE,
    FORWARD,
} direction_t;

static void policy_set_target(policy_t* policy, direction_t direction) {
    amxd_object_t* object = policy->common.object;
    target_t* target = direction == FORWARD ? &policy->target : &policy->target_reverse;
    chain_t** target_chain = direction == FORWARD ? &policy->chain : &policy->chain_reverse;

    *target = string_to_target(object_da_string(object, direction == FORWARD ? "TargetChain" : "ReverseTargetChain"));

    if(*target != TARGET_CHAIN) {
        *target_chain = NULL;
    } else {
        const char* path = object_da_string(object, direction == FORWARD ? "Chain" : "ReverseChain");
        when_str_empty_trace(path, exit, ERROR, "Policy[%s] without %s", OBJECT_NAMED, direction == FORWARD ? "Chain" : "ReverseChain");
        *target_chain = chain_get_user_data(path);
    }

exit:
    return;
}

static void policy_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t params;
    policy_t* policy = NULL;
    const char* dest_interface = NULL;
    const char* src_interface = NULL;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    policy = create_firewall_policy(object);
    when_null(policy, exit);
    object->priv = policy;

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    policy->common.ipversion = GET_INT32(&params, "IPVersion");
    policy->common.enable = GET_BOOL(&params, "Enable");
    policy_set_target(policy, FORWARD);
    policy_set_target(policy, REVERSE);
    dest_interface = GET_CHAR(&params, "DestinationInterface");
    src_interface = GET_CHAR(&params, "SourceInterface");
    set_interface(policy, src_interface, dest_interface, policy->common.ipversion);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_init(object, &policy->common.folders.log, &policy->common.src_fwiface, !EXCLUDE_SRC_INTERFACE, &policy->common.dest_fwiface, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             log_get_dm_enable_param(object, NULL, NULL), log_get_dm_controller_param(object, NULL, NULL), get_policy_log_target(policy, false), update_target_log_cb);

    //Initialize reverse log without subscribing
    log_init(object, &policy->reverse.log, &policy->common.dest_fwiface, !EXCLUDE_DST_INTERFACE, &policy->common.src_fwiface, !EXCLUDE_SRC_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             log_get_dm_enable_param(object, NULL, NULL), log_get_dm_controller_param(object, NULL, NULL), get_policy_log_target(policy, true), NULL);
#endif //FIREWALL_LOGS

    policy->common.flags = FW_NEW;
    policy_activate(policy);
    SAH_TRACEZ_INFO(ME, "Policy[%s] initialized", OBJECT_NAMED);

exit:
    FW_PROFILING_OUT();
    amxc_var_clean(&params);
}

void _policy_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const event_data,
                   UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));

    when_null(object, exit);
    policy_init(object);
exit:
    FW_PROFILING_OUT();
    return;
}

void _policy_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const event_data,
                     UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    policy_t* policy = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    const char* dest_interface = NULL;
    const char* src_interface = NULL;
    bool ipversion_changed = false;

    when_null(object, exit);
    when_null(object->priv, exit);
    policy = (policy_t*) object->priv;

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        policy->common.enable = GET_BOOL(var, "to");
        policy->common.flags = FW_MODIFIED;
    }

    var = GET_ARG(params, "TargetChain");
    var2 = GET_ARG(params, "Chain");
    if((var != NULL) || (var2 != NULL)) {
        const char* target_chain = (var != NULL) ? GET_CHAR(var, "to") : object_da_string(object, "TargetChain");
        if((var != NULL) ||
           ((var2 != NULL) && (target_chain != NULL) && (strcmp(target_chain, "Chain") == 0))) {
            policy_set_target(policy, FORWARD);
            policy->common.flags = FW_MODIFIED;
        }
        // else don't set policy->common.flags = FW_MODIFIED
    }

    var = GET_ARG(params, "ReverseTargetChain");
    var2 = GET_ARG(params, "ReverseChain");
    if((var != NULL) || (var2 != NULL)) {
        const char* target_chain = (var != NULL) ? GET_CHAR(var, "to") : object_da_string(object, "ReverseTargetChain");
        if((var != NULL) ||
           ((var2 != NULL) && (target_chain != NULL) && (strcmp(target_chain, "Chain") == 0))) {
            policy_set_target(policy, REVERSE);
            policy->common.flags = FW_MODIFIED;
        }
        // else don't set policy->common.flags = FW_MODIFIED
    }

    var = GET_ARG(params, "IPVersion");
    if(var != NULL) {
        policy->common.ipversion = GET_INT32(var, "to");
        ipversion_changed = true;
        policy->common.flags = FW_MODIFIED;
    }

    src_interface = GETP_CHAR(params, "SourceInterface.to");
    dest_interface = GETP_CHAR(params, "DestinationInterface.to");
    if((src_interface != NULL) || (dest_interface != NULL) || ipversion_changed) {
        policy->common.flags = FW_MODIFIED;
        if((src_interface == NULL) && ipversion_changed) {
            src_interface = object_da_string(object, "SourceInterface");
        }
        if((dest_interface == NULL) && ipversion_changed) {
            dest_interface = object_da_string(object, "DestinationInterface");
        }
        set_interface(policy, src_interface, dest_interface, policy->common.ipversion);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    if(update_policy_logs(policy, NULL, event_data, NULL)) {
        policy->common.flags = FW_MODIFIED;
    }
#endif //FIREWALL_LOGS

    policy_activate(policy);

exit:
    FW_PROFILING_OUT();
    return;
}

static int dm_policy_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    policy_init(object);
    return amxd_status_ok;
}

void dm_policies_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");
    amxd_object_for_all(firewall, ".Policy.*.", dm_policy_init, NULL);
    FW_PROFILING_OUT();
}