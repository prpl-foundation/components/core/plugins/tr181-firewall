/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <ipat/ipat.h>

#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#include "firewall.h"
#include "profiling/profiling.h"
#include "firewall_utilities.h"
#include "interfacesetting/interfacesetting.h"
#include "interfacesetting/log_out_accept.h"
#include "protocols.h"
#include "rules.h"
#include "logs/logs.h"

#define ME "firewall"

#define LOG_ACCEPT_CONN_STATUS    "LOGAcceptOutgoingConnectionStatus"

/**
   @brief
   Delete/clean the LOGAcceptOutgoingConnection internal structure.

   @param[in] log_out_accept LOGAcceptOutgoingConnection internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_log_out_accept(log_out_accept_t** log_out_accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*log_out_accept, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete LOGAcceptOutgoingConnection");

    amxc_llist_it_take(&(*log_out_accept)->it);

    common_clean(&(*log_out_accept)->common);

    FREE(*log_out_accept);
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Activate the LOG all denied inbound mode (Reject or Drop) that comes in the FORWARD chain on the filter table

   @param[in] log_out_accept LOGAcceptOutgoingConnection internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_log_out_accept_output(log_out_accept_t* log_out_accept, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_out_accept->common.object;
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? log_out_accept->common.folders.rule_folders.folder : log_out_accept->common.folders.rule_folders.folder6;
    firewall_interface_t* fwiface = &log_out_accept->common.dest_fwiface;
    bool res = false;
    const char* chain = ipv4 ? fw_get_chain("log_output", "OUTPUT_Last_Log") : fw_get_chain("log_output6", "OUTPUT6_Last_Log");
    (void) object;  // when tracing is disabled, variable is not used anymore

    when_false_status(log_get_enable(&log_out_accept->common.folders.log), out, res = true);

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "Outbound interface is an empty string.");

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "LOGAcceptOutgoingConnection[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_chain(r, chain);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_out_interface(r, fwiface->default_netdevname);
    fw_rule_set_table(r, TABLE_FILTER);
    set_log_rule_specific_settings(r, &(log_out_accept->common.folders.log));
    fw_folder_push_rule(folder, r);

    fw_folder_set_enabled(folder, true);
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

static void unfold_fw_setting_log_out_accept(log_out_accept_t* log_out_accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(log_out_accept, out);

    folder_container_delete_rules(&log_out_accept->common.folders, IPvAll, true);

out:
    FW_PROFILING_OUT();
    return;
}

static bool deactivate_log_out_accept(log_out_accept_t* log_out_accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_out_accept->common.object;
    bool is_active = (log_out_accept->status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "LOGAcceptOutgoingConnection[%s] LOGAcceptOutgoingConnection is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "LOGAcceptOutgoingConnection[%s] deactivate LOGAcceptOutgoingConnection", OBJECT_NAMED);
    /* do not commit yet, wait for the activate/delete to commit it. */
    unfold_fw_setting_log_out_accept(log_out_accept);
    log_out_accept->status = FIREWALL_DISABLED;
out:
    FW_PROFILING_OUT();
    return true;
}

static bool activate_log_out_accept(log_out_accept_t* log_out_accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_out_accept->common.object;
    bool is_active = (log_out_accept->status == FIREWALL_ENABLED);
    bool res = false;
    bool do_commit = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(is_active) {
        res = true;
        SAH_TRACEZ_NOTICE(ME, "LOGAcceptOutgoingConnection[%s] LOGAcceptOutgoingConnection already active", OBJECT_NAMED);
        goto out;
    }

    do_commit |= ifsetting_activate_log_out_accept_output(log_out_accept, true);
    do_commit |= ifsetting_activate_log_out_accept_output(log_out_accept, false);

    if(do_commit) {
        if(fw_commit(fw_rule_callback) != 0) {
            deactivate_log_out_accept(log_out_accept);
            goto out;
        } else {
            log_out_accept->status = FIREWALL_ENABLED;
            if(!log_get_enable(&log_out_accept->common.folders.log)) {
                _set_status(log_out_accept->common.object, LOG_ACCEPT_CONN_STATUS, FIREWALL_DISABLED);
            } else {
                _set_status(log_out_accept->common.object, LOG_ACCEPT_CONN_STATUS, FIREWALL_ENABLED);
            }
        }
    }

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}


bool log_out_accept_implement_changes(log_out_accept_t* log_out_accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object;     // when tracing is disabled, variable is not used anymore

    when_null_trace(log_out_accept, exit, ERROR, "LOGAcceptOutgoingConnection internal structure is NULL.");

    object = log_out_accept->common.object;

    if(log_out_accept->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "LOGAcceptOutgoingConnection[%s] about to be deleted", OBJECT_NAMED);
        deactivate_log_out_accept(log_out_accept);
        delete_fw_log_out_accept(&log_out_accept);
    } else if(log_out_accept->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "LOGAcceptOutgoingConnection[%s] about to be add", OBJECT_NAMED);
        if(!activate_log_out_accept(log_out_accept)) {
            _set_status(object, LOG_ACCEPT_CONN_STATUS, FIREWALL_ERROR);
            SAH_TRACEZ_ERROR(ME, "LOGAcceptOutgoingConnection[%s] activation failed", OBJECT_NAMED);
        }
        log_out_accept->common.flags = 0;
    } else if(log_out_accept->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "LOGAcceptOutgoingConnection[%s] deactivate, activate", OBJECT_NAMED);
        deactivate_log_out_accept(log_out_accept);
        if(!activate_log_out_accept(log_out_accept)) {
            _set_status(object, LOG_ACCEPT_CONN_STATUS, FIREWALL_ERROR);
            SAH_TRACEZ_ERROR(ME, "LOGAcceptOutgoingConnection[%s] activation failed", OBJECT_NAMED);
        }
        log_out_accept->common.flags = 0;
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a LOGAcceptOutgoingConnection
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the LOGAcceptOutgoingConnection internal structure.
 */
log_out_accept_t* create_log_out_accept(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    log_out_accept_t* log_out_accept = NULL;

    SAH_TRACEZ_NOTICE(ME, "LOGAcceptOutgoingConnection[%s] is new", OBJECT_NAMED);

    log_out_accept = (log_out_accept_t*) calloc(1, sizeof(log_out_accept_t));
    when_null(log_out_accept, exit);

    common_init(&log_out_accept->common, object);

    log_out_accept->status = FIREWALL_DISABLED;
    _set_status(log_out_accept->common.object, LOG_ACCEPT_CONN_STATUS, FIREWALL_ERROR);
exit:
    FW_PROFILING_OUT();
    return log_out_accept;
}