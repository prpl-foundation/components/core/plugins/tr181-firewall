/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "rules.h"
#include "firewall.h"           // for fw_get_dm
#include "nat/masquerade.h"
#include "firewall_utilities.h" // for status_t
#ifdef WITH_GMAP
#include "devices_query.h"
#endif
#include "logs/logs.h"
#include "ports.h"
#include "protocols.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <fwrules/fw.h>            // for fw_commit
#include <fwinterface/interface.h> // for fw_apply
#include <string.h>

#define ME "firewall"
static struct {
    class_t type;
    const char* path;
} rule_class_allowed_origins_paths[] = {
    {FIREWALL_FORWARD, NULL},
    {FIREWALL_PORTFW, "NAT.PortMappingAllowedOrigins"},
    {FIREWALL_PORTTRIGGER, NULL},
    {FIREWALL_PINHOLE, NULL}
};

#define RULE_CLASS_ALLOWED_ORIGINS_PATH_SIZE (ARRAY_SIZE(rule_class_allowed_origins_paths))

static const amxc_var_t* rule_get_allowed_origins(const rule_t* rule) {
    const amxc_var_t* allowed_origins = NULL;

    for(uint32_t i = 0; i < RULE_CLASS_ALLOWED_ORIGINS_PATH_SIZE; ++i) {
        if(rule->class_type == rule_class_allowed_origins_paths[i].type) {
            amxd_path_t path;
            amxd_object_t* object;

            amxd_path_init(&path, rule_class_allowed_origins_paths[i].path);
            object = amxd_dm_findf(fw_get_dm(), "%s", amxd_path_get(&path, AMXD_OBJECT_TERMINATE));
            when_null(object, exit_loop);
            when_null(amxd_object_get_param_def(object, amxd_path_get_param(&path)), exit_loop);
            allowed_origins = amxd_object_get_param_value(object, amxd_path_get_param(&path));
exit_loop:
            amxd_path_clean(&path);
            break;
        }
    }
    return allowed_origins;
}

static bool rule_check_allowedorigins(const rule_t* rule) {
    const char* origin = NULL;
    const amxc_var_t* allowed_origins = NULL;
    amxd_object_t* object = rule->common.object;
    amxd_param_t* origin_par = NULL;
    bool allowed = true;

    when_null(object, exit);
    origin_par = amxd_object_get_param_def(object, "Origin");
    when_null(origin_par, exit);
    origin = GET_CHAR(&origin_par->value, NULL);

    allowed_origins = rule_get_allowed_origins(rule);
    when_null(allowed_origins, exit);

    allowed = is_string_in_amxc_var(origin, allowed_origins);

exit:
    return allowed;
}

void rules_allowed_origins_changed(const amxc_var_t* from, const amxc_var_t* to, amxd_object_t* template) {
    amxc_var_t modified_origins;
    amxc_var_t tmp_list;

    amxc_var_init(&modified_origins);
    amxc_var_set_type(&modified_origins, AMXC_VAR_ID_LIST);
    amxc_var_init(&tmp_list);

    when_null(from, exit);
    when_null(to, exit);
    when_null(template, exit);

    when_failed(amxc_var_convert(&tmp_list, from, AMXC_VAR_ID_LIST), exit);

    amxc_var_for_each(origin, &tmp_list) {
        if(!is_string_in_amxc_var(GET_CHAR(origin, NULL), to)) {
            amxc_var_add(cstring_t, &modified_origins, GET_CHAR(origin, NULL));
        }
    }
    when_failed(amxc_var_convert(&tmp_list, to, AMXC_VAR_ID_LIST), exit);

    amxc_var_for_each(origin, &tmp_list) {
        if(!is_string_in_amxc_var(GET_CHAR(origin, NULL), from)) {
            amxc_var_add(cstring_t, &modified_origins, GET_CHAR(origin, NULL));
        }
    }

    amxd_object_for_each(instance, it, template) {
        amxd_object_t* rule_obj = amxc_container_of(it, amxd_object_t, it);
        rule_t* rule = NULL;

        if(rule_obj->priv == NULL) {
            continue;
        }
        rule = (rule_t*) rule_obj->priv;

        if(is_string_in_amxc_var(GET_CHAR(amxd_object_get_param_value(rule_obj, "Origin"), NULL), &modified_origins)) {
            if(rule->common.flags != 0) { // already being configured
                continue;
            }
            rule->common.flags = FW_MODIFIED;
            rule_deactivate(rule);
            rule_activate(rule);
        }
    }

    fw_apply();

exit:
    amxc_var_clean(&modified_origins);
    amxc_var_clean(&tmp_list);
    return;
}

static void rule_set_status(rule_t* rule, status_t status) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(rule, out);
    when_true(rule->common.flags & FW_DELETED, out);

    if(rule->common.status != status) {
        common_set_status(&rule->common, status);
    }

out:
    FW_PROFILING_OUT();
    return;
}

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    rule_t* rule = (rule_t*) fwiface->query_priv;

    when_null_trace(rule, exit, ERROR, "Missing private data");
    when_true(rule->common.flags != 0, exit); // already being configured
    rule->common.flags = FW_MODIFIED;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_rule_logs(NULL, rule->common.object, NULL, NULL, NULL, true);
#endif //FIREWALL_LOGS
    rule_update(rule);

exit:
    FW_PROFILING_OUT();
}

static void fw_callback_inactivity_timeout(UNUSED amxp_timer_t* timer, void* data) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    amxd_object_t* object = (amxd_object_t*) data;
    rule_t* rule = NULL;
    amxd_trans_t trans;
    char rule_name[65] = {0};
    int rv;

    when_failed(amxd_trans_init(&trans), exit);
    when_null(object, exit);
    when_null(object->priv, exit);
    rule = object->priv;
    strncpy(rule_name, amxd_object_get_name(object, AMXD_OBJECT_NAMED), 64);
    when_false(amxd_object_get_type(object) == amxd_object_instance, exit);
    when_failed(amxd_trans_select_object(&trans, amxd_object_get_parent(object)), exit);
    when_failed(amxd_trans_del_inst(&trans, amxd_object_get_index(object), NULL), exit);
    rv = amxd_trans_apply(&trans, fw_get_dm());
    when_failed_trace(rv, exit, ERROR, "Failed to delete rule instance[%s] (%d)", rule_name, rv);
    SAH_TRACEZ_NOTICE(ME, "Rule[%s] has been inactive for %ds and is now deleted", rule_name, rule->inactivity_timeout);
exit:
    amxd_trans_clean(&trans);
    FW_PROFILING_OUT();
}

void _rule_lease_time_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    rule_t* rule = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);

    rule = (rule_t*) object->priv;

    rule_set_lease_duration(&rule, true);
    if(rule == NULL) {
        object = NULL; // handle dangling pointer
        goto exit;
    }

exit:
    FW_PROFILING_OUT();
    return;
}

rule_t* rule_create(amxd_object_t* object, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    rule_t* rule = NULL;

    when_null_trace(object, out, ERROR, "object is NULL");

    SAH_TRACEZ_INFO(ME, "Rule[%s] is new", OBJECT_NAMED);

    rule = (rule_t*) calloc(1, sizeof(rule_t));
    when_null(rule, out);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_init(&rule->log_features.log_in_denied);
    folder_wrapper_init(&rule->log_features.log_in_allowed);
    folder_wrapper_init(&rule->log_features.log_out_denied);
    folder_wrapper_init(&rule->log_features.log_out_allowed);
#endif //FIREWALL_LOGS
    common_init(&rule->common, object);

    if(reverse) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        folder_wrapper_init(&rule->log_features_reverse.log_in_denied);
        folder_wrapper_init(&rule->log_features_reverse.log_in_allowed);
        folder_wrapper_init(&rule->log_features_reverse.log_out_denied);
        folder_wrapper_init(&rule->log_features_reverse.log_out_allowed);
#endif //FIREWALL_LOGS
        folder_container_init(&rule->reverse_folders, &rule->common);
        rule->reverse = true;
    }
    fw_folder_new(&rule->dnat_folder);
    fw_folder_new(&rule->snat_folder);

    if((!is_folders_initialized(&rule->common.folders)) ||
       ((reverse) && !is_folders_initialized(&rule->reverse_folders)) ||
       (rule->dnat_folder == NULL) ||
       (rule->snat_folder == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Rule[%s] failed to create folders", OBJECT_NAMED);
        rule_delete(&rule);
        goto out;
    }

    address_init(&rule->source);
    address_init(&rule->destination);

    rule->common.src_fwiface.query_priv = rule;
    rule->common.dest_fwiface.query_priv = rule;
    rule->common.src_fwiface.query_result_changed = query_result_changed;
    rule->common.dest_fwiface.query_result_changed = query_result_changed;

    rule_set_status(rule, FIREWALL_DISABLED);
    rule->class_type = FIREWALL_FORWARD;
    rule->target = TARGET_DROP;
    rule->inactivity_timeout = amxd_object_get_uint32_t(object, "InactivityTimeout", NULL);
    amxp_timer_new(&(rule->inactivity_timer), fw_callback_inactivity_timeout, object);

    if(rule->inactivity_timeout > 0) {
        amxp_timer_start(rule->inactivity_timer, rule->inactivity_timeout * 1000);
    }

    object->priv = rule;

    amxp_timer_new(&(rule->lease_timer), fw_callback_lease_timer, rule->common.object);
    rule_set_lease_duration(&rule, false);
    if(rule == NULL) {
        object = NULL; // handle dangling pointer
        goto out;
    }

out:
    FW_PROFILING_OUT();
    return rule;
}

static void rule_reset_folders(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(rule, out);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_delete_rules(&rule->log_features.log_in_denied, IPvAll, false);
    folder_wrapper_delete_rules(&rule->log_features.log_in_allowed, IPvAll, false);
    folder_wrapper_delete_rules(&rule->log_features.log_out_denied, IPvAll, false);
    folder_wrapper_delete_rules(&rule->log_features.log_out_allowed, IPvAll, false);
#endif //FIREWALL_LOGS
    folder_container_delete_rules(&rule->common.folders, IPvAll, false);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_delete_rules(&rule->log_features_reverse.log_in_denied, IPvAll, false);
    folder_wrapper_delete_rules(&rule->log_features_reverse.log_in_allowed, IPvAll, false);
    folder_wrapper_delete_rules(&rule->log_features_reverse.log_out_denied, IPvAll, false);
    folder_wrapper_delete_rules(&rule->log_features_reverse.log_out_allowed, IPvAll, false);
#endif //FIREWALL_LOGS
    folder_container_delete_rules(&rule->reverse_folders, IPvAll, false);
    if(rule->dnat_folder != NULL) {
        fw_folder_delete_rules(rule->dnat_folder);
    }
    if(rule->snat_folder != NULL) {
        fw_folder_delete_rules(rule->snat_folder);
    }
    fw_commit(fw_rule_callback);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_disable_folders(&rule->log_features.log_in_denied, IPvAll);
    folder_wrapper_disable_folders(&rule->log_features.log_in_allowed, IPvAll);
    folder_wrapper_disable_folders(&rule->log_features.log_out_denied, IPvAll);
    folder_wrapper_disable_folders(&rule->log_features.log_out_allowed, IPvAll);
#endif //FIREWALL_LOGS
    folder_container_disable_folders(&rule->common.folders, IPvAll);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_disable_folders(&rule->log_features_reverse.log_in_denied, IPvAll);
    folder_wrapper_disable_folders(&rule->log_features_reverse.log_in_allowed, IPvAll);
    folder_wrapper_disable_folders(&rule->log_features_reverse.log_out_denied, IPvAll);
    folder_wrapper_disable_folders(&rule->log_features_reverse.log_out_allowed, IPvAll);
#endif //FIREWALL_LOGS
    folder_container_disable_folders(&rule->reverse_folders, IPvAll);

out:
    FW_PROFILING_OUT();
    return;
}

bool rule_deactivate(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(rule, out);

    if(rule->common.status == FIREWALL_ERROR) {
        rule->common.flags = 0;
        rule_set_status(rule, FIREWALL_DISABLED);
        return true;
    }

    if((rule->common.status == FIREWALL_ENABLED) || (rule->common.status == FIREWALL_PENDING)) {
        amxd_object_t* object = rule->common.object;
        (void) object; // when tracing is disabled, variable is not used anymore
        SAH_TRACEZ_NOTICE(ME, "Rule[%s] Active, deactivate it", OBJECT_NAMED);

        rule_reset_folders(rule);

        if(rule->common.status != FIREWALL_PENDING) {
            rule_set_status(rule, FIREWALL_DISABLED);
        }
    }
out:
    rule->common.flags = 0;
    FW_PROFILING_OUT();
    return true;
}

void rule_delete(rule_t** rulep) {
    rule_t* rule = NULL;
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(rulep, exit);
    when_null(*rulep, exit);
    rule = *rulep;

    rule->common.flags = FW_DELETED;
    rule_deactivate(rule);

#ifdef WITH_GMAP
    stop_gmap_queries(rule);
#endif

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_clean(&rule->log_features.log_in_allowed);
    folder_wrapper_clean(&rule->log_features.log_in_denied);
    folder_wrapper_clean(&rule->log_features.log_out_denied);
    folder_wrapper_clean(&rule->log_features.log_out_allowed);
#endif //FIREWALL_LOGS
    common_clean(&rule->common);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_clean(&rule->log_features_reverse.log_in_allowed);
    folder_wrapper_clean(&rule->log_features_reverse.log_in_denied);
    folder_wrapper_clean(&rule->log_features_reverse.log_out_denied);
    folder_wrapper_clean(&rule->log_features_reverse.log_out_allowed);
#endif //FIREWALL_LOGS
    folder_container_clean(&rule->reverse_folders);

    fw_folder_delete(&rule->dnat_folder);
    fw_folder_delete(&rule->snat_folder);
    fw_commit(fw_rule_callback);

    port_range_clean(&rule->src_ports);
    port_range_clean(&rule->dst_ports);

    address_clean(&rule->source);
    address_clean(&rule->destination);

    amxp_timer_delete(&(rule->lease_timer));
    rule->lease_timer = NULL;

    amxp_timer_delete(&rule->inactivity_timer);

#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
    if(rule->ct_query != NULL) {
        ct_query_delete(&rule->ct_query);
    }
#endif

    if(rule->common.object != NULL) {
        rule->common.object->priv = NULL;
    }
    FREE(rule);
    *rulep = NULL;

    fw_apply();
exit:
    FW_PROFILING_OUT();
    return;
}

void rule_update(rule_t* rule) {
    when_false(rule->common.flags != 0, exit);
    rule_deactivate(rule);
    rule_activate(rule);
    rule->common.flags = 0;
    fw_apply();
exit:
    return;
}

static bool _enable_filter_rule_portfw(rule_t* rule, folder_container_t* folder_container, ipversion_t ipversion, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = true;
    status_t status = FIREWALL_ERROR;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = log_get_enable(&folder_container->log) ? folder_container->log_folders.folder : NULL;
#else
    fw_folder_t* log_folder = NULL;
#endif
    fw_folder_t* folder = folder_container->rule_folders.folder;
    filter_args_t args = {0};

    when_false_trace(ipversion == IPv4, out, ERROR, "PortMapping only supports IPv4");

    args.chain = fw_get_chain("portmapping", "FORWARD_PortForwarding");
    args.ipversion = IPv4;
    args.reverse = reverse;
    args.match.dest_address = &rule->destination;
    args.match.src_address = &rule->source;
    args.match.protocols = &(rule->common.protocols);
    if(!amxc_llist_is_empty(&rule->src_ports)) { // according to tr181 if ExternalPort is zero then InternalPort is ignored
        args.match.dest_ports = &rule->dst_ports;
    }
    args.target.verdict = TARGET_ACCEPT;
    when_false(fill_folder_container_filter_rule(folder_container, &args), error);

    res = enable_rule(log_folder, folder, &status, false);
error:
    rule_set_status(rule, status);
out:
    FW_PROFILING_OUT();
    return res;
}

static bool _enable_filter_rule_porttrigger(rule_t* rule, folder_container_t* folder_container, ipversion_t ipversion, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = true;
    status_t status = FIREWALL_ERROR;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = folder_container->log_folders.folder;
#endif //FIREWALL_LOGS
    fw_folder_t* folder = folder_container->rule_folders.folder;
    filter_args_t args = {0};

    when_false_trace(ipversion == IPv4, out, ERROR, "PortTrigger only supports IPv4");

    args.chain = fw_get_chain("porttrigger", "FORWARD_PortTrigger");
    args.ipversion = IPv4;
    args.reverse = reverse;
    args.match.dest_address = &rule->destination;
    args.match.protocols = &(rule->common.protocols);
    args.match.dest_ports = &rule->dst_ports;
    args.target.verdict = TARGET_ACCEPT;
    when_false(fill_folder_container_filter_rule(folder_container, &args), error);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    res = enable_rule(log_get_enable(&folder_container->log) ? log_folder : NULL, folder, &status, false);
#else //FIREWALL_LOGS
    res = enable_rule(NULL, folder, &status, false);
#endif //FIREWALL_LOGS

error:
    rule_set_status(rule, status);
out:
    FW_PROFILING_OUT();
    return res;
}

static bool _enable_filter_rule_pinhole(rule_t* rule, folder_container_t* folder_container, bool ipv4, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = true;
    status_t status = FIREWALL_ERROR;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? folder_container->log_folders.folder : folder_container->log_folders.folder6;
#endif //FIREWALL_LOGS
    fw_folder_t* folder = ipv4 ? folder_container->rule_folders.folder : folder_container->rule_folders.folder6;
    bool waiting = false;
    firewall_interface_t* fwiface = &rule->common.src_fwiface;
    amxd_object_t* object = rule->common.object;
    filter_args_t args = {0};

    waiting = fwiface->netdev_required && STRING_EMPTY(fwiface->default_netdevname);
    when_true_trace(waiting, out, INFO, "Pinhole[%s] waiting on interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    if(!address_has_ipversion(&rule->destination, ipv4 ? IPv4 : IPv6)) {
        ipversion_t ipversion = object_get_ipversion(object);
        if(ipversion > IPvAll) {
            SAH_TRACEZ_ERROR(ME, "Pinhole[%s] is missing IPv%d address", OBJECT_NAMED, ipversion);
            res = false;
            goto error;
        } else if(!address_has_ipversion(&rule->destination, ipv4 ? IPv6 : IPv4)) {
            SAH_TRACEZ_ERROR(ME, "Pinhole[%s] is missing IPv%d address", OBJECT_NAMED, ipv4 ? 6 : 4);
            res = false;
            goto error;
        } else {
            SAH_TRACEZ_INFO(ME, "Pinhole[%s] skips IPv%d", OBJECT_NAMED, ipversion);
            res = true;
            goto out;
        }
    }

    args.chain = fw_get_chain(ipv4 ? "pinhole" : "pinhole6", ipv4 ? "FORWARD_Pinhole" : "FORWARD6_Pinhole");
    args.ipversion = ipv4 ? IPv4 : IPv6;
    args.reverse = reverse;
    args.match.iface = fwiface->default_netdevname;
    args.match.dest_address = &rule->destination;
    args.match.src_address = &rule->source;
    args.match.protocols = &(rule->common.protocols);
    args.match.src_ports = &rule->src_ports;
    args.match.dest_ports = &rule->dst_ports;
    args.target.verdict = TARGET_ACCEPT;

    when_false(fill_folder_container_filter_rule(folder_container, &args), error);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    res = enable_rule(log_get_enable(&folder_container->log) ? log_folder : NULL, folder, &status, false);
#else //FIREWALL_LOGS
    res = enable_rule(NULL, folder, &status, false);
#endif //FIREWALL_LOGS

error:
    rule_set_status(rule, status);
out:
    FW_PROFILING_OUT();
    return res;
}

static const char* rule_get_target_chain(rule_t* rule) {
    amxd_object_t* object = NULL;
    const char* chain_name = NULL;

    when_false(rule->target == TARGET_CHAIN, exit);

    object = amxd_dm_findf(fw_get_dm(), "%s", object_da_string(rule->common.object, "TargetChain"));
    when_null_trace(object, exit, ERROR, "Rule[%s] has no target chain", rule->common.object_path);
    chain_name = chain_get_name(object);
    when_str_empty_trace(chain_name, exit, ERROR, "Rule[%s] has empty TargetChain", rule->common.object_path);

exit:
    return chain_name;
}

static bool _enable_filter_rule(rule_t* rule, folder_container_t* folder_container, ipversion_t ipversion, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    bool res = true;
    amxc_string_t buffer;
    bool ipv4 = ipversion == IPv4;
    fw_folder_t* folder = ipv4 ? folder_container->rule_folders.folder : folder_container->rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_feature_t* log_features = reverse ? &rule->log_features_reverse : &rule->log_features;
    fw_folder_t* folder_log_in_denied = ipv4 ? log_features->log_in_denied.folder : log_features->log_in_denied.folder6;
    fw_folder_t* folder_log_out_denied = ipv4 ? log_features->log_out_denied.folder : log_features->log_out_denied.folder6;
    fw_folder_t* folder_log_in_allow = ipv4 ? log_features->log_in_allowed.folder : log_features->log_in_allowed.folder6;
    fw_folder_t* folder_log_out_allow = ipv4 ? log_features->log_out_allowed.folder : log_features->log_out_allowed.folder6;
    fw_folder_t* log_folder = ipv4 ? folder_container->log_folders.folder : folder_container->log_folders.folder6;
    fw_folder_t* folders[] = {folder_log_in_denied, folder_log_out_denied, folder_log_in_allow, folder_log_out_allow, log_folder, folder};
#else
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
#endif
    status_t status = FIREWALL_ERROR;
    const char* ctstate = NULL;
    const char* mac = NULL;
    const char* chain_name = NULL;
    amxd_object_t* object = rule->common.object;
    bool waiting = false;
    firewall_interface_t* src_fwiface = &rule->common.src_fwiface;
    firewall_interface_t* dest_fwiface = &rule->common.dest_fwiface;
    (void) object; // when tracing is disabled, variable is not used anymore

    amxc_string_init(&buffer, 0);

    when_null_trace(rule->chain, out, ERROR, "Rule[%s] is missing chain", OBJECT_NAMED);

    waiting = src_fwiface->netdev_required && STRING_EMPTY(src_fwiface->default_netdevname);
    if(waiting) {
        SAH_TRACEZ_INFO(ME, "Rule[%s] waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&src_fwiface->netmodel_interface, 0));
        res = true;
        goto out;
    }

    waiting = dest_fwiface->netdev_required && STRING_EMPTY(dest_fwiface->default_netdevname);
    if(waiting) {
        SAH_TRACEZ_INFO(ME, "Rule[%s] waiting for dest interface[%s]", OBJECT_NAMED, amxc_string_get(&dest_fwiface->netmodel_interface, 0));
        res = true;
        goto out;
    }

    waiting = address_is_required(&rule->source) &&
        !address_has_ipversion(&rule->source, ipversion);
    if(waiting) {
        SAH_TRACEZ_INFO(ME, "Rule[%s] is waiting for IPv%d source addresses", OBJECT_NAMED, ipversion);
        res = true;
        goto out;
    }

    waiting = address_is_required(&rule->destination) &&
        !address_has_ipversion(&rule->destination, ipversion);
    if(waiting) {
        SAH_TRACEZ_INFO(ME, "Rule[%s] is waiting for IPv%d dest addresses", OBJECT_NAMED, ipversion);
        res = true;
        goto out;
    }

    chain_name = chain_get_name(rule->chain);
    when_str_empty_trace(chain_name, out, ERROR, "Chain Name is empty.");
    //Force the chain to be FORWARD + (6) + _ + "Name" for backward compatibility, removing this conversion may break tests and default files used with old tr181-firewall versions
    if(strstr(chain_name, "FORWARD") != NULL) {                 //i.e.: FORWARD_L_Low = FORWARD_L_Low
        amxc_string_setf(&buffer, "%s", chain_name);
        if((strstr(chain_name, "FORWARD6") == NULL) && !ipv4) { //i.e.: for IPv6 FORWARD_L_Low = FORWARD6_L_Low
            amxc_string_replace(&buffer, "FORWARD", "FORWARD6", UINT32_MAX);
        }
    } else if(chain_name[0] == '6') {           //i.e.: 6_L_Low = FORWARD6_L_Low
        amxc_string_setf(&buffer, "FORWARD%s", chain_name);
    } else if(chain_name[0] == '_') {           //i.e.: _L_Low = FORWARD_L_Low
        amxc_string_setf(&buffer, "FORWARD%s%s", ipv4 ? "" : "6", chain_name);
    } else {                                    //i.e.: L_Low = FORWARD_L_Low
        amxc_string_setf(&buffer, "FORWARD%s_%s", ipv4 ? "" : "6", chain_name);
    }

    for(size_t i = 0; i < sizeof(folders) / sizeof(folders[0]); i++) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        log_t* log = &folder_container->log;
        if((folders[i] == folder) && (rule->target == TARGET_LOG)) {
            continue;
        } else if(folders[i] == folder_log_in_allow) {
            log = &log_features->log_in_allowed.log;
        } else if(folders[i] == folder_log_out_denied) {
            log = &log_features->log_out_denied.log;
        } else if(folders[i] == folder_log_in_denied) {
            log = &log_features->log_in_denied.log;
        } else if(folders[i] == folder_log_out_allow) {
            log = &log_features->log_out_allowed.log;
        }

        if((folders[i] != folder) && !log_get_enable(log)) {
            continue;
        }
#endif //FIREWALL_LOGS

        if(folders[i] == folder) {
            fw_folder_set_feature(folders[i], FW_FEATURE_TARGET);
        }
        if(amxc_var_get_first(&rule->common.protocols) != NULL) {
            fw_folder_set_feature(folders[i], FW_FEATURE_PROTOCOL);
        }

        mac = object_da_string(rule->common.object, "SourceMAC");
        if(!STRING_EMPTY(mac)) {
            fw_folder_set_feature(folders[i], FW_FEATURE_SOURCE_MAC);
        } else if(!reverse) {
            bool feature_res = false;
            feature_res = folder_require_source_feature(folders[i], &rule->source);
            when_false_trace(feature_res, out, ERROR, "Rule[%s] failed to set source feature", OBJECT_NAMED);
            feature_res = folder_require_dest_feature(folders[i], &rule->destination);
            when_false_trace(feature_res, out, ERROR, "Rule[%s] failed to set dest feature", OBJECT_NAMED);
        } else {
            bool feature_res = false;
            feature_res = folder_require_source_feature(folders[i], &rule->destination);
            when_false_trace(feature_res, out, ERROR, "Rule[%s] failed to set source feature", OBJECT_NAMED);
            feature_res = folder_require_dest_feature(folders[i], &rule->source);
            when_false_trace(feature_res, out, ERROR, "Rule[%s] failed to set dest feature", OBJECT_NAMED);
        }

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, error, ERROR, "Rule[%s] Failed to fetch default %s rule", OBJECT_NAMED, folders[i] == log_folder ? "log" : "");

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, amxc_string_get(&buffer, 0));
        fw_rule_set_ipv4(r, ipv4);

        if((folders[i] == folder) || (folders[i] == log_folder)) {
            when_false(folder_default_set_in_and_out_interfaces(r,
                                                                amxd_object_get_value(bool, object, "DestAllInterfaces", NULL) ? NULL : rule->common.dest_fwiface.default_netdevname,
                                                                amxd_object_get_value(bool, object, "DestInterfaceExclude", NULL),
                                                                amxd_object_get_value(bool, object, "SourceAllInterfaces", NULL) ? NULL : rule->common.src_fwiface.default_netdevname,
                                                                amxd_object_get_value(bool, object, "SourceInterfaceExclude", NULL),
                                                                reverse), error);
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        else {
            if(!STRING_EMPTY(log_get_src_intf(log))) {
                fw_rule_set_in_interface(r, log_get_src_intf(log));
            }
            if(!STRING_EMPTY(log_get_dst_intf(log))) {
                fw_rule_set_out_interface(r, log_get_dst_intf(log));
            }
        }
#endif //FIREWALL_LOGS

        ctstate = object_da_string(rule->common.object, "ConnectionState");
        if(!STRING_EMPTY(ctstate)) {
            fw_rule_set_conntrack_state(r, ctstate);
        }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] != folder) {
            set_log_rule_specific_settings(r, log);
        }
#endif //FIREWALL_LOGS

        fw_folder_push_rule(folders[i], r);

        when_false(folder_feature_set_address(folders[i],
                                              &rule->source,
                                              ipv4,
                                              true,
                                              amxd_object_get_value(bool, object, "SourceIPExclude", NULL)), error);

        when_false(folder_feature_set_address(folders[i],
                                              &rule->destination,
                                              ipv4,
                                              false,
                                              amxd_object_get_value(bool, object, "DestIPExclude", NULL)), error);

        when_false(folder_feature_set_protocol(folders[i], &(rule->common.protocols)), error);
        if(folders[i] == folder) {
            when_false(folder_feature_set_target(folders[i], rule->target, rule_get_target_chain(rule), ipv4), error);
        }
        when_false(folder_feature_set_source_and_destination_port(folders[i],
                                                                  &rule->src_ports,
                                                                  amxd_object_get_value(bool, rule->common.object, "SourcePortExclude", NULL),
                                                                  &rule->dst_ports,
                                                                  amxd_object_get_value(bool, rule->common.object, "DestPortExclude", NULL), reverse), error);

        when_false(folder_feature_set_dscp(folders[i],
                                           amxd_object_get_value(int32_t, rule->common.object, "DSCP", NULL),
                                           amxd_object_get_value(bool, rule->common.object, "DSCPExclude", NULL)), error);

        when_false(folder_feature_set_source_ipset(folders[i],
                                                   object_da_string(object, "SourceMatchSet"),
                                                   object_da_string(object, "SourceMatchSetExclude")), error);

        when_false(folder_feature_set_destination_ipset(folders[i],
                                                        object_da_string(object, "DestMatchSet"),
                                                        object_da_string(object, "DestMatchSetExclude")), error);

        if(!STRING_EMPTY(mac)) {
            when_false(folder_feature_set_source_mac(folders[i], mac, amxd_object_get_value(bool, rule->common.object, "SourceMACExclude", NULL)), error);
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            log_set_enable(log, true);
        }
#endif //FIREWALL_LOGS
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_set_enabled(folder_log_in_denied, log_get_enable(&log_features->log_in_denied.log));
    fw_folder_set_enabled(folder_log_in_allow, log_get_enable(&log_features->log_in_allowed.log));
    fw_folder_set_enabled(folder_log_out_denied, log_get_enable(&log_features->log_out_denied.log));
    fw_folder_set_enabled(folder_log_out_allow, log_get_enable(&log_features->log_out_allowed.log));
    res = enable_rule(log_get_enable(&folder_container->log) ? log_folder : NULL, folder, &status, false);
#else //FIREWALL_LOGS
    res = enable_rule(NULL, folder, &status, false);
#endif //FIREWALL_LOGS
error:
    rule_set_status(rule, status);
out:
    amxc_string_clean(&buffer);
    FW_PROFILING_OUT();
    return res;
}

/*
 * Commit filter firewall rules
 * return true if Success, false when iptables rules Failed
 */
bool commit_filter_rules(common_t* common, folder_container_t* reverse_folders, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = false;
    status_t status = FIREWALL_ERROR;
    folder_container_t* folders_array[] = {NULL, NULL};

    when_null(common, out);

    folders_array[0] = &common->folders;
    folders_array[1] = reverse_folders;

    if(reverse_folders != NULL) {
        reorder_array_folder_containers(folders_array, sizeof(folders_array) / sizeof(folders_array[0]));
    } else {
        folder_container_reorder(&common->folders);
    }

    if(fw_commit(fw_rule_callback) == 0) {
        SAH_TRACEZ_INFO(ME, "Commit rule: Success");
        status = FIREWALL_ENABLED;
    } else {
        SAH_TRACEZ_ERROR(ME, "Commit rule: Error");
        status = FIREWALL_ERROR;
        folder_container_delete_rules(&common->folders, ipv4 ? IPv4 : IPv6, false);
        folder_container_delete_rules(reverse_folders, ipv4 ? IPv4 : IPv6, false);

        fw_commit(fw_rule_callback);

        folder_container_disable_folders(&common->folders, ipv4 ? IPv4 : IPv6);
        folder_container_disable_folders(reverse_folders, ipv4 ? IPv4 : IPv6);
        goto out;
    }

    res = true;

out:
    if(common != NULL) {
        common_set_status(common, status);
    }
    FW_PROFILING_OUT();
    return res;
}

/*
 * Enable filter firewall rule
 * return true if Success, false when iptables rules Failed
 */
static bool enable_filter_rule(rule_t* rule, ipversion_t ipversion, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = false;
    folder_container_t* folders = reverse ? &rule->reverse_folders : &rule->common.folders;

    if(rule->class_type == FIREWALL_PORTFW) {
        res = _enable_filter_rule_portfw(rule, folders, ipversion, reverse);
    } else if(rule->class_type == FIREWALL_PINHOLE) {
        res = _enable_filter_rule_pinhole(rule, folders, ipversion == IPv4, reverse);
    } else if(rule->class_type == FIREWALL_PORTTRIGGER) {
        res = _enable_filter_rule_porttrigger(rule, folders, ipversion, reverse);
    } else {
        res = _enable_filter_rule(rule, folders, ipversion, reverse);
    }

    FW_PROFILING_OUT();
    return res;
}

static const char* portfw_get_chain(rule_t* rule, const char* chain) {
    int chain_number = 0;
    int features = 0;
    enum {
        REMOTE_HOST = 1 << 0,
        EXTERNAL_PORT = 1 << 1,
    };
    static char buffer[250];

    if(address_has_ipversion(&rule->source, IPv4)) {
        features |= REMOTE_HOST;
    }

    if(!amxc_llist_is_empty(&rule->src_ports)) {
        features |= EXTERNAL_PORT;
    }

    switch(features) {
    case REMOTE_HOST | EXTERNAL_PORT:
        chain_number = 1;
        break;
    case REMOTE_HOST:
        chain_number = 2;
        break;
    case EXTERNAL_PORT:
        chain_number = 3;
        break;
    default:
        chain_number = 4;
    }

    snprintf(buffer, sizeof(buffer) - 3, "%s_%d", chain, chain_number); // 3 chars = '_[1-4]\0'

    return buffer;
}

static bool _enable_nat_rule_portfw(rule_t* rule, fw_folder_t* folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = false;
    status_t status = FIREWALL_ERROR;
    bool waiting = false;
    firewall_interface_t* fwiface = &rule->common.src_fwiface;
    amxd_object_t* object = rule->common.object;
    (void) object; // when tracing is disabled, variable is not used anymore
    dnat_args_t args = {0};

    waiting = (fwiface->addr_required && !address_has_ipversion(&fwiface->address, IPv4));
    when_true_trace(waiting, out, INFO, "PortMap[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    waiting = address_is_required(&rule->source) &&
        !address_has_ipversion(&rule->source, IPv4);
    if(waiting) {
        SAH_TRACEZ_INFO(ME, "PortMap[%s] is waiting for IPv4 source addresses", OBJECT_NAMED);
        res = true;
        goto out;
    }

    if(fwiface->addr_required) {
        args.match.dest_address = &fwiface->address;
    }

    args.chain = portfw_get_chain(rule, fw_get_chain("portmapping_dnat", "PREROUTING_PortForwarding"));
    args.match.protocols = &(rule->common.protocols);
    args.match.src_address = &rule->source;
    args.target.address = &rule->destination;
    args.target.src_ports = &rule->src_ports;
    if(!amxc_llist_is_empty(&rule->src_ports)) { // according to tr181 if ExternalPort is zero then InternalPort is ignored
        args.target.dest_ports = &rule->dst_ports;
    }

    when_false(fill_folder_dnat_rule(folder, &args), error);

    res = enable_rule(NULL, folder, &status, true);
error:
    rule_set_status(rule, status);
out:
    FW_PROFILING_OUT();
    return res;
}

static bool _enable_nat_rule_porttrigger(rule_t* rule, fw_folder_t* folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    const char* destination = NULL;
    bool res = false;
    status_t status = FIREWALL_ERROR;
    const char* chain_name = fw_get_chain("porttrigger_dnat", "PREROUTING_PortTrigger");
    amxd_object_t* object = rule->common.object;
    (void) object; // when tracing is disabled, variable is not used anymore

    /* Only one destination IP address is supported for NAT */
    destination = address_first_ipv4_char(&rule->destination);
    when_str_empty_trace(destination, error, ERROR, "Missing dest ipaddress");

    SAH_TRACEZ_INFO(ME, "Rule[%s] enable nat rule(s)", OBJECT_NAMED);
    fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);
    fw_folder_set_feature(folder, FW_FEATURE_DNAT);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, error, ERROR, "Rule[%s] Failed to fetch default rule", OBJECT_NAMED);

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, chain_name);
    fw_rule_set_ipv4(r, true);     // always IPv4 for port forwarding
    fw_folder_push_rule(folder, r);

    when_false(folder_feature_set_protocol(folder, &(rule->common.protocols)), error);
    when_false(folder_feature_set_target_dnat(folder, destination, 0, 0, 0, 0), error);

    when_false(folder_feature_set_destination_port(folder, &rule->dst_ports, false), error);
    res = enable_rule(NULL, folder, &status, true);
error:
    rule_set_status(rule, status);
    FW_PROFILING_OUT();
    return res;
}

static bool enable_nat_rule(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_folder_t* folder = rule->dnat_folder;

    if(rule->class_type == FIREWALL_PORTFW) {
        retval = _enable_nat_rule_portfw(rule, folder);
    } else if(rule->class_type == FIREWALL_PORTTRIGGER) {
        retval = _enable_nat_rule_porttrigger(rule, folder);
    } else {
        retval = true;
    }

    FW_PROFILING_OUT();
    return retval;
}

/*
 * Enable snat firewall rule for portforwarding (hairpin portforwarding)
 * return true if Success, false when iptables rules Failed
 */
static bool enable_hairpin_source_nat_rule(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = rule->common.object;
    fw_folder_t* folder = rule->snat_folder;
    firewall_interface_t* src_fwiface = &rule->common.src_fwiface;
    firewall_interface_t* dest_fwiface = &rule->common.dest_fwiface;
    fw_rule_t* r = NULL;
    const char* chain_name = fw_get_chain("portmapping_snat", "POSTROUTING_PortForwarding");
    bool waiting = false;
    bool res = false;
    status_t status = FIREWALL_ERROR;
    const char* destination = NULL;
    bool all_ifaces = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    all_ifaces = amxd_object_get_value(bool, object, "AllInterfaces", NULL);

    if(!all_ifaces) {
        waiting = !address_has_ipversion(&src_fwiface->address, IPv4);
        when_true_trace(waiting, out, INFO, "PortMap[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&src_fwiface->netmodel_interface, 0));
    }

    waiting = (dest_fwiface->addr_required && !address_has_ipversion(&dest_fwiface->address, IPv4));
    when_true_trace(waiting, out, INFO, "PortMap[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&dest_fwiface->netmodel_interface, 0));

    // Only one destination IP address is supported for NAT
    destination = address_first_ipv4_char(&rule->destination);
    when_str_empty_trace(destination, out, INFO, "PortMap[%s] is waiting for dest address", OBJECT_NAMED);

    SAH_TRACEZ_INFO(ME, "PortMap[%s] enable source nat rule(s)", OBJECT_NAMED);
    fw_folder_set_feature(folder, FW_FEATURE_SNAT);
    fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "PortMap[%s] Failed to fetch default rule", OBJECT_NAMED);

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, chain_name);

    folder_default_set_source(r, address_first_ipv4_char(&dest_fwiface->address), NULL, false);

    fw_rule_set_ipv4(r, true);                                   // always IPv4 for port forwarding

    folder_default_set_destination(r, destination, NULL, false);

    fw_folder_push_rule(folder, r);

    when_false(folder_feature_set_protocol(folder, &(rule->common.protocols)), out);

    if(!amxc_llist_is_empty(&rule->src_ports)) { // according to tr181 if ExternalPort is zero then InternalPort is ignored
        when_false(folder_feature_set_destination_port(folder, &rule->dst_ports, false), out);
    }

    if(!all_ifaces) {
        when_false(folder_feature_set_target_snat(folder, &src_fwiface->address, 1024, 65535), out);
    } else {
        uint32_t count = 0;
        amxd_object_for_each(instance, it, amxd_dm_findf(fw_get_dm(), "NAT.InterfaceSetting.")) {
            amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
            firewall_interface_t* fwiface = NULL;

            if(!amxd_object_get_value(bool, instance, "Enable", NULL)) {
                continue;
            }

            fwiface = waniface_get_addresses(instance);

            if((fwiface == NULL) || !address_has_ipversion(&fwiface->address, IPv4)) {
                continue;
            }

            when_false(folder_feature_set_target_snat(folder, &fwiface->address, 1024, 65535), out);
            count++;
        }
        if(count == 0) { // I don't think portmap should be in status Error if hairpin nat has no interfaces to enable for
            res = true;
            status = FIREWALL_ENABLED;
            fw_folder_delete_rules(folder);
            fw_folder_set_enabled(folder, false);
            goto out;
        }
    }

    res = enable_rule(NULL, folder, &status, true);
out:
    rule_set_status(rule, status);
    FW_PROFILING_OUT();
    return res;
}

static ipversion_t rule_get_ipversion(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ipversion_t ipversion = IPvUnknown;

    if((rule->class_type == FIREWALL_PORTFW) || (rule->class_type == FIREWALL_PORTTRIGGER)) {
        ipversion = IPv4;
    } else {
        ipversion = object_get_ipversion(rule->common.object);
    }

    FW_PROFILING_OUT();
    return ipversion;
}

static bool rule_activate_ipv4(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    amxd_object_t* object = rule->common.object;

    if(enable_filter_rule(rule, IPv4, false) == false) {
        SAH_TRACEZ_WARNING(ME, "Rule[%s] failed to enable filter rule", OBJECT_NAMED);
        goto error;
    }
    if(rule->reverse) { // todo maybe the forward and reverse can be activated at the same time to avoid recalculating some stuff (e.g. llist size for addresses)
        if(enable_filter_rule(rule, IPv4, true) == false) {
            SAH_TRACEZ_WARNING(ME, "Rule[%s] failed to enable reverse filter rule", OBJECT_NAMED);
            folder_container_disable_folders(&rule->common.folders, IPv4);
            goto error;
        }
    }
    when_false_trace(commit_filter_rules(&rule->common, &rule->reverse_folders, true), error, ERROR, "Rule[%s] failed to to commit filter rules", OBJECT_NAMED);

    if(enable_nat_rule(rule) == false) {
        SAH_TRACEZ_WARNING(ME, "Rule[%s] failed to enable NAT rule", OBJECT_NAMED);
        goto error;
    }
    if(amxd_object_get_value(bool, object, "HairpinNAT", NULL)) {
        if(!enable_hairpin_source_nat_rule(rule)) {
            SAH_TRACEZ_WARNING(ME, "Rule[%s] failed to enable IPv4 hairpin NAT Rule", OBJECT_NAMED);
            goto error;
        }
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

static bool rule_activate_ipv6(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    amxd_object_t* object = rule->common.object;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(enable_filter_rule(rule, IPv6, false) == false) {
        SAH_TRACEZ_WARNING(ME, "Rule[%s] failed to enable filter rule", OBJECT_NAMED);
        goto error;
    }
    if(rule->reverse) {
        if(enable_filter_rule(rule, IPv6, true) == false) {
            SAH_TRACEZ_WARNING(ME, "Rule[%s] failed to enable reverse filter rule", OBJECT_NAMED);
            folder_container_disable_folders(&rule->common.folders, IPv6);
            goto error;
        }
    }
    when_false_trace(commit_filter_rules(&rule->common, &rule->reverse_folders, false), error, ERROR, "Rule[%s] failed to to commit filter rules", OBJECT_NAMED);

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

static bool rule_check_datamodel_set_path(rule_t* rule, const char* set_path) {
    amxd_object_t* object = rule->common.object;
    amxd_object_t* set_object = NULL;
    ipversion_t rule_ipversion = IPvUnknown;
    ipversion_t set_ipversion = IPvUnknown;
    bool valid = false;

    when_str_empty_status(set_path, exit, valid = true);

    set_object = amxd_object_findf(amxd_dm_get_root(fw_get_dm()), "%s", set_path);

    when_null_trace(set_object, exit, ERROR, "SET object doesn't exist");
    when_false_trace(object_get_enable(set_object), exit, ERROR, "SET object is not enabled");

    rule_ipversion = object_get_ipversion(object);
    set_ipversion = object_get_ipversion(set_object);
    when_false_trace(rule_ipversion == IPvAll || rule_ipversion == set_ipversion, exit, ERROR, "Rule IPVersion and SET IPVersion are not compatible");

    valid = true;
exit:
    return valid;

}

static bool rule_check_datamodel(rule_t* rule) {
    amxd_object_t* object = rule->common.object;

    when_true(rule->common.validated, out);

    if(rule->class_type == FIREWALL_PINHOLE) {
        const char* mac = NULL;
        const char* ip = NULL;
        const char* iface = NULL;

        mac = object_da_string(object, "DestMACAddress");
        ip = object_da_string(object, "DestIP");
        when_true_trace(STRING_EMPTY(mac) && STRING_EMPTY(ip),
                        out, ERROR, "Pinhole[%s] misconf because DestMACAddress and DestIP are empty", OBJECT_NAMED);

        iface = object_da_string(object, "Interface");
        when_str_empty_trace(iface, out, ERROR, "Pinhole[%s] misconf because Interface is empty", OBJECT_NAMED);
    } else if(rule->class_type == FIREWALL_FORWARD) {
        const char* src_set = object_da_string(object, "SourceMatchSet");
        const char* src_set_exclude = object_da_string(object, "SourceMatchSetExclude");
        const char* dest_set = object_da_string(object, "DestMatchSet");
        const char* dest_set_exclude = object_da_string(object, "DestMatchSetExclude");

        when_true_trace(!STRING_EMPTY(src_set) && !STRING_EMPTY(src_set_exclude), out, ERROR, "Rule[%s] misconf because SourceMatchSet and SourceMatchSetExclude are both defined", OBJECT_NAMED);
        when_true_trace(!STRING_EMPTY(dest_set) && !STRING_EMPTY(dest_set_exclude), out, ERROR, "Rule[%s] misconf because DestMatchSet and DestMatchSetExclude are both defined", OBJECT_NAMED);
        when_false_trace(rule_check_datamodel_set_path(rule, src_set), out, ERROR, "Rule[%s] misconf because SourceMatchSet is invalid", OBJECT_NAMED);
        when_false_trace(rule_check_datamodel_set_path(rule, src_set_exclude), out, ERROR, "Rule[%s] misconf because SourceMatchSetExclude is invalid", OBJECT_NAMED);
        when_false_trace(rule_check_datamodel_set_path(rule, dest_set), out, ERROR, "Rule[%s] misconf because DestMatchSet is invalid", OBJECT_NAMED);
        when_false_trace(rule_check_datamodel_set_path(rule, dest_set_exclude), out, ERROR, "Rule[%s] misconf because DestMatchSetExclude is invalid", OBJECT_NAMED);
    }

    rule->common.validated = true;
out:
    return rule->common.validated;
}

// assumes rule_t* is never NULL
bool rule_activate(rule_t* rule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    firewall_interface_t* src_fwiface = &rule->common.src_fwiface;
    firewall_interface_t* dest_fwiface = &rule->common.dest_fwiface;
    ipversion_t ipversion = rule_get_ipversion(rule);
    status_t status = FIREWALL_ERROR;
    schedule_status_t schedule_status = rule->common.schedule_status;
    bool allowed = true;
    bool retval = false;
    bool waiting = false;

    when_false_trace(chain_is_enabled(rule->chain), out, INFO, "Rule[%s] can't enable because Chain[%s] is disabled", rule->common.object_path, amxd_object_get_name(rule->chain, AMXD_OBJECT_NAMED));

    if(!rule_check_datamodel(rule)) {
        rule_set_status(rule, FIREWALL_ERROR_MISCONFIG);
        goto out;
    }

    allowed = rule_check_allowedorigins(rule);
    if(!allowed) {
        SAH_TRACEZ_NOTICE(ME, "%s is inactive (AllowedOrigins)", rule->common.object_path);
        rule_set_status(rule, FIREWALL_INACTIVE);
        goto out;
    }

    if((schedule_status != SCHEDULE_STATUS_EMPTY) && (schedule_status != SCHEDULE_STATUS_ACTIVE)) {
        if(schedule_status == SCHEDULE_STATUS_INACTIVE) {
            rule_set_status(rule, FIREWALL_INACTIVE);
        } else if((schedule_status == SCHEDULE_STATUS_DISABLED) || (schedule_status == SCHEDULE_STATUS_STACK_DISABLED)) {
            rule_set_status(rule, FIREWALL_DISABLED);
        } else {
            rule_set_status(rule, FIREWALL_ERROR);
        }
        rule_set_enable(rule, false);
        goto out;
    }

    if(!rule->common.enable) {
        SAH_TRACEZ_NOTICE(ME, "%s is disabled", rule->common.object_path);
        retval = true;
        goto out;
    }

    if(rule->common.suspend) {
        SAH_TRACEZ_NOTICE(ME, "%s is suspended", rule->common.object_path);
        retval = true;
        goto out;
    }

    if(rule->common.status == FIREWALL_ENABLED) {
        SAH_TRACEZ_NOTICE(ME, "%s is already enabled", rule->common.object_path);
        retval = true;
        goto out;
    }

    waiting = (src_fwiface->netdev_required && STRING_EMPTY(src_fwiface->default_netdevname));
    when_true_trace(waiting, out, INFO, "%s is waiting for interface[%s]", rule->common.object_path, amxc_string_get(&src_fwiface->netmodel_interface, 0));

    waiting = (dest_fwiface->netdev_required && STRING_EMPTY(dest_fwiface->default_netdevname));
    when_true_trace(waiting, out, INFO, "%s is waiting for interface[%s]", rule->common.object_path, amxc_string_get(&dest_fwiface->netmodel_interface, 0));

    waiting = (src_fwiface->addr_required && !address_has_ipversion(&src_fwiface->address, ipversion));
    when_true_trace(waiting, out, INFO, "%s is waiting for interface[%s] address", rule->common.object_path, amxc_string_get(&src_fwiface->netmodel_interface, 0));

    waiting = (dest_fwiface->addr_required && !address_has_ipversion(&dest_fwiface->address, ipversion));
    when_true_trace(waiting, out, INFO, "%s is waiting for interface[%s] address", rule->common.object_path, amxc_string_get(&dest_fwiface->netmodel_interface, 0));

    if(ipversion != IPv6) {
        when_false(rule_activate_ipv4(rule), cleanup);
    }
    if(ipversion != IPv4) {
        when_false(rule_activate_ipv6(rule), cleanup);
    }

    status = FIREWALL_ENABLED;
cleanup:
    if(status != FIREWALL_ENABLED) {
        rule_reset_folders(rule);
    }
    retval = (status == FIREWALL_ENABLED);

    rule_set_status(rule, status);
out:
    rule->common.flags = 0;
    FW_PROFILING_OUT();
    return retval;
}

void rule_suspend(rule_t* rule, bool suspend) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    when_null(rule, out);
    (void) object; // when tracing is disabled, variable is not used anymore

    object = rule->common.object;

    if(suspend == rule->common.suspend) {
        SAH_TRACEZ_INFO(ME, "Rule[%s] is already %ssuspended", OBJECT_NAMED, (suspend) ? "" : "un");
        goto out;
    }

    rule->common.suspend = suspend;

    if(suspend) {
        if(rule->common.status == FIREWALL_ENABLED) {
            rule->common.flags = FW_MODIFIED;
            SAH_TRACEZ_WARNING(ME, "Rule[%s] is about to be suspend", OBJECT_NAMED);
            rule_update(rule);
        } else {
            SAH_TRACEZ_NOTICE(ME, "Rule[%s] is not active", OBJECT_NAMED);
        }
    } else {
        if(rule->common.status != FIREWALL_ENABLED) {
            rule->common.flags = FW_MODIFIED;
            SAH_TRACEZ_WARNING(ME, "Rule[%s] is about to be unsuspended", OBJECT_NAMED);
            rule_update(rule);
        }
    }

out:
    FW_PROFILING_OUT();
    return;
}

void rule_set_target(rule_t* rule, const char* target) {
    rule->target = string_to_target(target);
}

void rule_set_log_target(rule_t* rule, const char* target) {
    rule->log_target = string_to_target(target);
}

void rule_set_protocol(rule_t* rule, int protocol) {
    rule_set_protocols(rule, int_to_char(protocol > 0 ? protocol : 0));
}

void rule_set_protocols(rule_t* rule, const char* csv_protocols) {
    protocol_from_string(&rule->common.protocols, csv_protocols);
}

void rule_set_src_port(rule_t* rule, int start, int end) {
    port_range_from_int(&rule->src_ports, start, end);
}

void rule_set_dst_port(rule_t* rule, int start, int end) {
    port_range_from_int(&rule->dst_ports, start, end);
}

void rule_set_source(rule_t* rule, const char* address) {
    address_from_string(address, &rule->source);
}

void rule_set_destination(rule_t* rule, const char* address) {
    address_from_string(address, &rule->destination);
}

void rule_set_enable(rule_t* rule, bool enable) {
    rule->common.enable = enable;
}

void rule_set_lease_duration(rule_t** rule, bool force_update_endtime) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* obj = NULL;

    when_null(*rule, exit);
    obj = (*rule)->common.object;
    set_lease_duration(&obj, (*rule)->lease_timer, &(*rule)->lease_duration, force_update_endtime);
    if(obj == NULL) {
        *rule = NULL; // handle dangling pointers
        goto exit;
    }

exit:
    FW_PROFILING_OUT();
}

amxd_status_t _RemainingLeaseTime_rule_lease_onread(amxd_object_t* const object,
                                                    AMXB_UNUSED amxd_param_t* const param,
                                                    amxd_action_t reason,
                                                    AMXB_UNUSED const amxc_var_t* const args,
                                                    amxc_var_t* const retval,
                                                    AMXB_UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    rule_t* rule = NULL;
    uint32_t remaining_time = 0;

    when_null(object, exit);
    //The rule must be on an instance object
    when_false(amxd_object_instance == amxd_object_get_type(object), exit);
    rule = (rule_t*) object->priv;
    when_null(rule, exit);

    when_null(retval, exit);
    if(reason != action_param_read) {
        SAH_TRACEZ_ERROR(ME, "Wrong reason");
        status = amxd_status_invalid_action;
        goto exit;
    }

    if(rule->lease_duration > 0) {
        amxp_timers_calculate();
        remaining_time = (uint32_t) amxp_timer_remaining_time(rule->lease_timer) / 1000;
    }

    //Set the retval to the value we found
    if(amxc_var_set_uint32_t(retval, remaining_time) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set parameter RemainingLeaseTime");
        goto exit;
    }

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

void fw_callback_lease_timer(amxp_timer_t* timer, void* data) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    amxd_object_t* object = (amxd_object_t*) data;
    amxd_trans_t* trans = NULL;
    int index = -1;
    char* path = NULL;

    when_null(data, exit);
    when_null(object, exit);

    index = amxd_object_get_index(object);

    when_failed(amxd_trans_new(&trans), exit);
    when_failed(amxd_trans_select_object(trans, object), exit);
    if(index <= 0) {
        //If index <= 0 that means something was wrong with the function amxd_object_get_index, so just disable the current Object.
        amxd_trans_set_value(bool, trans, "Enable", false);
    } else {
        //It's an instance object, and by the requirement we should delete the instance when the time expires.
        when_failed(amxd_trans_select_pathf(trans, ".^"), exit);
        when_failed(amxd_trans_del_inst(trans, index, NULL), exit);
    }
    if(amxd_trans_apply(trans, fw_get_dm()) != amxd_status_ok) {
        path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);
        SAH_TRACEZ_ERROR(ME, "Failed to delete/disable the instance: %s after the lease time", path);
        goto exit;
    }

exit:
    amxp_timer_stop(timer);
    amxd_trans_delete(&trans);
    free(path);
    FW_PROFILING_OUT();
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
static void _init_rule_logs(amxd_object_t* object, rule_t* rule, firewall_interface_t* src_interface, firewall_interface_t* dest_interface,
                            bool use_excluded_parameter, amxp_slot_fn_t slot_cb, bool reverse) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    bool log_enable = log_get_dm_enable_param(object, NULL, NULL);
    bool contains_src_interface = (src_interface != NULL ? (src_interface->query != NULL ? true : false) : false);
    bool contains_dest_interface = (dest_interface != NULL ? (dest_interface->query != NULL ? true : false) : false);
    bool src_excluded_parameter = use_excluded_parameter ? log_get_dm_src_excl_param(object, NULL, NULL) : !EXCLUDE_SRC_INTERFACE;
    bool dst_excluded_parameter = use_excluded_parameter ? log_get_dm_dst_excl_param(object, NULL, NULL) : !EXCLUDE_DST_INTERFACE;
    target_t rule_target = rule->target == TARGET_LOG ? rule->log_target : rule->target;

    when_true(reverse && !rule->reverse, exit);

    if(reverse) {
        log_init(object, &rule->reverse_folders.log, dest_interface, dst_excluded_parameter, src_interface, src_excluded_parameter, !USE_LOG_CONTROLLER_INTERFACE,
                 log_enable, log_get_dm_controller_param(object, NULL, NULL), rule_target, NULL);
        log_init(object, &rule->log_features_reverse.log_in_denied.log, NULL, !EXCLUDE_SRC_INTERFACE, src_interface, src_excluded_parameter, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_dest_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerDeniedInbound"), rule_target, slot_cb);
        log_init(object, &rule->log_features_reverse.log_in_allowed.log, NULL, !EXCLUDE_SRC_INTERFACE, src_interface, src_excluded_parameter, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_dest_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerAllowedInbound"), rule_target, slot_cb);
        log_init(object, &rule->log_features_reverse.log_out_denied.log, dest_interface, dst_excluded_parameter, NULL, !EXCLUDE_DST_INTERFACE, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_src_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerDeniedOutbound"), rule_target, slot_cb);
        log_init(object, &rule->log_features_reverse.log_out_allowed.log, dest_interface, dst_excluded_parameter, NULL, !EXCLUDE_DST_INTERFACE, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_src_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerAllowedOutbound"), rule_target, slot_cb);
    } else {
        log_init(object, &rule->common.folders.log, src_interface, src_excluded_parameter, dest_interface, dst_excluded_parameter, !USE_LOG_CONTROLLER_INTERFACE,
                 log_enable, log_get_dm_controller_param(object, NULL, NULL), rule_target, slot_cb);
        log_init(object, &rule->log_features.log_in_denied.log, NULL, !EXCLUDE_SRC_INTERFACE, dest_interface, dst_excluded_parameter, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_src_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerDeniedInbound"), rule_target, slot_cb);
        log_init(object, &rule->log_features.log_in_allowed.log, NULL, !EXCLUDE_SRC_INTERFACE, dest_interface, dst_excluded_parameter, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_src_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerAllowedInbound"), rule_target, slot_cb);
        log_init(object, &rule->log_features.log_out_denied.log, src_interface, src_excluded_parameter, NULL, !EXCLUDE_DST_INTERFACE, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_dest_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerDeniedOutbound"), rule_target, slot_cb);
        log_init(object, &rule->log_features.log_out_allowed.log, src_interface, src_excluded_parameter, NULL, !EXCLUDE_DST_INTERFACE, USE_LOG_CONTROLLER_INTERFACE,
                 log_enable & !contains_dest_interface & (rule->target != TARGET_LOG), log_get_dm_controller_param(object, NULL, "LogControllerAllowedOutbound"), rule_target, slot_cb);
    }

exit:
    FW_PROFILING_OUT();
    return;
}

/**
   @brief
   Init the log data for the rule_t

   @param[in] object Rule object.
   @param[in] p_rule Rule structure.
   @param[in] src_interface Source interface.
   @param[in] dest_interface Destination interface.
   @param[in] use_excluded_parameter If true, then try to fetch the SourceInterfaceExclude and DestInterfaceExclude. If false, then just set these values to false.
   @param[in] slot_cb Callback function for the subscription on the Firewall.Log objects.

 */
void init_rule_logs(amxd_object_t* object, rule_t* rule, firewall_interface_t* src_interface, firewall_interface_t* dest_interface,
                    bool use_excluded_parameter, amxp_slot_fn_t slot_cb) {
    FW_PROFILING_IN(TRACE_CALLBACK);

    when_null(object, exit);
    when_null(rule, exit);

    _init_rule_logs(object, rule, src_interface, dest_interface, use_excluded_parameter, slot_cb, false);
    _init_rule_logs(object, rule, src_interface, dest_interface, use_excluded_parameter, slot_cb, true);

exit:
    FW_PROFILING_OUT();
    return;
}

static bool update_rule_log_features(rule_t* p_rule, const amxc_var_t* const data_rule, const amxc_var_t* const data_log, log_t* log, bool reverse) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    bool log_enable = false;
    bool contains_src_interface = false;
    bool contains_dst_interface = false;
    bool log_contains_src_intf = false;
    bool log_contains_dst_intf = false;
    bool contains_interface = false;
    bool update = false;
    const cstring_t controllers = NULL;
    rule_t* rule = NULL;
    amxd_object_t* object = NULL;
    log_t* log_in_denied = NULL;
    log_t* log_in_allowed = NULL;
    log_t* log_out_denied = NULL;
    log_t* log_out_allowed = NULL;
    log_t* logs[] = {NULL, NULL, NULL, NULL};

    if(p_rule == NULL) {
        when_null(object, exit);
        rule = (rule_t*) object->priv;
        when_null(rule, exit);
    } else {
        rule = p_rule;
    }

    object = rule->common.object;
    when_null(object, exit);

    contains_src_interface = rule->common.src_fwiface.query != NULL ? true : false;
    contains_dst_interface = rule->common.dest_fwiface.query != NULL ? true : false;
    log_contains_src_intf = reverse ? contains_dst_interface : contains_src_interface;
    log_contains_dst_intf = reverse ? contains_src_interface : contains_dst_interface;
    log_enable = log_get_dm_enable_param(object, data_rule, NULL);

    log_in_denied = !reverse ? &rule->log_features.log_in_denied.log : &rule->log_features_reverse.log_in_denied.log;
    log_in_allowed = !reverse ? &rule->log_features.log_in_allowed.log : &rule->log_features_reverse.log_in_allowed.log;
    log_out_denied = !reverse ? &rule->log_features.log_out_denied.log : &rule->log_features_reverse.log_out_denied.log;
    log_out_allowed = !reverse ? &rule->log_features.log_out_allowed.log : &rule->log_features_reverse.log_out_allowed.log;

    if(log != NULL) {
        logs[0] = log;
    } else {
        logs[0] = log_in_denied;
        logs[1] = log_in_allowed;
        logs[2] = log_out_denied;
        logs[3] = log_out_allowed;
    }

    for(size_t i = 0; i < sizeof(logs) / sizeof(logs[0]); i++) {
        if(logs[i] == NULL) {
            continue;
        }

        if(logs[i] == log_in_denied) {
            controllers = log_get_dm_controller_param(object, data_rule, "LogControllerDeniedInbound");
            contains_interface = log_contains_src_intf;
        } else if(logs[i] == log_in_allowed) {
            controllers = log_get_dm_controller_param(object, data_rule, "LogControllerAllowedInbound");
            contains_interface = log_contains_src_intf;
        } else if(logs[i] == log_out_denied) {
            controllers = log_get_dm_controller_param(object, data_rule, "LogControllerDeniedOutbound");
            contains_interface = log_contains_dst_intf;
        } else if(logs[i] == log_out_allowed) {
            controllers = log_get_dm_controller_param(object, data_rule, "LogControllerAllowedOutbound");
            contains_interface = log_contains_dst_intf;
        } else {
            controllers = NULL;
            contains_interface = log_contains_src_intf;
        }

        log_set_policy(logs[i], rule->target == TARGET_LOG ? rule->log_target : rule->target);
        update |= log_update(logs[i],
                             log_enable & !contains_interface & (rule->target != TARGET_LOG),
                             controllers,
                             !EXCLUDE_SRC_INTERFACE,
                             !EXCLUDE_DST_INTERFACE,
                             data_log);
    }

exit:
    FW_PROFILING_OUT();
    return update;
}

/**
   @brief
   Update the log data for the rule_t

   @param[in] p_rule Rule structure. That's optional and can be NULL if the object is specified and the object->priv contains the rule_t.
   @param[in] object Rule object. That's optional and can be NULL if the p_rule is specified.
   @param[in] data_rule Event data when the rule changes. (it can be NULL if called from another place)
   @param[in] data_log Event data when the log changes. (it can be NULL if called from another place)
   @param[in] log Log structure. (If specified all the changes are made using the log structure.)
   @param[in] use_excluded_parameter If true, then try to fetch the SourceInterfaceExclude and DestInterfaceExclude. If false, then just set these values to false.

   @return
   true if the logs have been changed.
 */
bool update_rule_logs(rule_t* p_rule, amxd_object_t* object, const amxc_var_t* const data_rule, const amxc_var_t* const data_log, log_t* log, bool use_excluded_parameter) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool update = false;
    rule_t* rule = NULL;

    if(p_rule == NULL) {
        when_null(object, exit);
        rule = (rule_t*) object->priv;
        when_null(rule, exit);
    } else {
        rule = p_rule;
    }

    if((log == NULL) || (log == &rule->common.folders.log)) {
        log_set_policy(&rule->common.folders.log, rule->target == TARGET_LOG ? rule->log_target : rule->target);
        update = log_update(&rule->common.folders.log,
                            log_get_dm_enable_param(rule->common.object, data_rule, NULL),
                            log_get_dm_controller_param(rule->common.object, data_rule, NULL),
                            use_excluded_parameter ? log_get_dm_src_excl_param(rule->common.object, data_rule, NULL) : !EXCLUDE_SRC_INTERFACE,
                            use_excluded_parameter ? log_get_dm_dst_excl_param(rule->common.object, data_rule, NULL) : !EXCLUDE_DST_INTERFACE,
                            data_log);

        if(rule->reverse) {
            log_set_policy(&rule->reverse_folders.log, rule->target == TARGET_LOG ? rule->log_target : rule->target);
            update |= log_update(&rule->reverse_folders.log,
                                 log_get_dm_enable_param(rule->common.object, data_rule, NULL),
                                 log_get_dm_controller_param(rule->common.object, data_rule, NULL),
                                 use_excluded_parameter ? log_get_dm_dst_excl_param(rule->common.object, data_rule, NULL) : !EXCLUDE_SRC_INTERFACE,
                                 use_excluded_parameter ? log_get_dm_src_excl_param(rule->common.object, data_rule, NULL) : !EXCLUDE_DST_INTERFACE,
                                 data_log);
        }
    }
    if((log == NULL) || (log != &rule->common.folders.log)) {
        update |= update_rule_log_features(rule, data_rule, data_log, log, false);
        if(rule->reverse) {
            update |= update_rule_log_features(rule, data_rule, data_log, log, true);
        }
    }

exit:
    FW_PROFILING_OUT();
    return update;
}
#endif //FIREWALL_LOGS
