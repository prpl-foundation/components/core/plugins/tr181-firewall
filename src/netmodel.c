/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "netmodel.h"
#include "firewall_utilities.h" // for STRING_EMPTY
#include "profiling/profiling.h"
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>

#define ME "firewall"

static linux_interface_t* linux_interface_list_find(amxc_llist_t* linifaces, const char* netmodel_interface) {
    linux_interface_t* liniface = NULL;

    if(STRING_EMPTY(netmodel_interface)) {
        return NULL;
    }

    amxc_llist_for_each(iter, linifaces) {
        const char* netmodel_interface_b = NULL;
        liniface = amxc_container_of(iter, linux_interface_t, it);
        netmodel_interface_b = amxc_string_get(&liniface->netmodel_interface, 0);
        if(netmodel_interface_b == NULL) {
            liniface = NULL;
            continue;
        }
        if(strcmp(netmodel_interface, netmodel_interface_b) == 0) {
            break;
        }
        liniface = NULL;
    }

    return liniface;
}

static void linux_interface_list_mark_all(amxc_llist_t* linifaces) {
    amxc_llist_for_each(iter, linifaces) {
        linux_interface_t* liniface = amxc_container_of(iter, linux_interface_t, it);
        liniface->marked = true;
    }
}

static int linux_interface_list_delete_all_marked(amxc_llist_t* linifaces) {
    int count = 0;

    amxc_llist_for_each(iter, linifaces) {
        linux_interface_t* liniface = amxc_container_of(iter, linux_interface_t, it);

        if(liniface->marked) {
            amxc_llist_it_take(&(liniface->it));
            amxc_string_clean(&liniface->netmodel_interface);
            free(liniface);
            count++;
        }
    }

    return count;
}

static void handle_interface_char(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    firewall_interface_t* fwiface = (firewall_interface_t*) userdata;
    const char* netdevname = NULL;

    when_null_trace(fwiface, exit, ERROR, "userdata is NULL");
    when_null_trace(fwiface->query_priv, exit, ERROR, "priv is NULL");

    netdevname = GET_CHAR(result, NULL);
    if(netdevname == NULL) {
        netdevname = "";
    }

    when_true(strncmp(fwiface->default_netdevname, netdevname, INTERFACE_LEN - 1) == 0, exit);

    if(fwiface->query_result_pre_change != NULL) {
        fwiface->query_result_pre_change(fwiface);
    }

    snprintf(fwiface->default_netdevname, INTERFACE_LEN, "%s", netdevname);

    fwiface->query_result_changed(fwiface);

exit:
    FW_PROFILING_OUT();
    return;
}

// keeping this impl for policy only
static void handle_interface_list(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    firewall_interface_t* fwiface = (firewall_interface_t*) userdata;
    amxc_llist_it_t* it = NULL;
    bool modified = false;

    when_null_trace(fwiface, exit, ERROR, "userdata is NULL");
    when_null_trace(fwiface->query_priv, exit, ERROR, "priv is NULL");

    if(fwiface->query_result_pre_change != NULL) {
        SAH_TRACEZ_ERROR(ME, "pre change hook not implemented");
    }

    linux_interface_list_mark_all(&fwiface->linifaces);

    amxc_var_for_each(var, result) {
        const char* netmodel_interface = amxc_var_key(var);
        const char* netdevname = NULL;
        linux_interface_t* liniface = NULL;

        if((netdevname = GET_CHAR(var, NULL)) == NULL) {
            netdevname = "";
        }

        liniface = linux_interface_list_find(&fwiface->linifaces, netmodel_interface);

        if(liniface == NULL) {
            liniface = calloc(1, sizeof(linux_interface_t));
            amxc_string_set(&liniface->netmodel_interface, netmodel_interface);
            amxc_llist_append(&fwiface->linifaces, &liniface->it);
            modified = true;
        }
        if(strncmp(liniface->netdevname, netdevname, INTERFACE_LEN - 1) != 0) {
            strncpy(liniface->netdevname, netdevname, INTERFACE_LEN - 1);
            modified = true;
        }
        liniface->marked = false;
    }

    if(linux_interface_list_delete_all_marked(&fwiface->linifaces) > 0) {
        modified = true;
    }

    it = amxc_llist_get_first(&fwiface->linifaces);
    if(it != NULL) {
        linux_interface_t* liniface = amxc_container_of(it, linux_interface_t, it);

        if(strncmp(fwiface->default_netdevname, liniface->netdevname, INTERFACE_LEN - 1) != 0) {
            snprintf(fwiface->default_netdevname, INTERFACE_LEN, "%s", liniface->netdevname);
            modified = true;
        }

        SAH_TRACEZ_INFO(ME, "Use interface %s [%s] as default", amxc_string_get(&fwiface->netmodel_interface, 0), fwiface->default_netdevname);
    } else {
        SAH_TRACEZ_INFO(ME, "Clear interface %s", amxc_string_get(&fwiface->netmodel_interface, 0));
        memset(fwiface->default_netdevname, 0, INTERFACE_LEN);
    }

    if(modified) {
        SAH_TRACEZ_INFO(ME, "Interface[%s] is changed, re-apply", fwiface->default_netdevname);
        fwiface->query_result_changed(fwiface);
    }
exit:
    FW_PROFILING_OUT();
    return;
}


static bool _to_linux_interface(const char* netmodel_interface, int ipversion, const char* aflags, firewall_interface_t* fwiface, netmodel_query_t* (*open_query)(const char*, const char*, const char*, const char*, const char*, netmodel_callback_t, void*), netmodel_callback_t handler) {
    const char* current_netmodel_interface = amxc_string_get(&fwiface->netmodel_interface, 0);
    bool skip_update = false;
    bool rv = false;

    when_null(netmodel_interface, exit);
    when_null_trace(fwiface->query_result_changed, exit, ERROR, "missing post change hook");

    skip_update = ((current_netmodel_interface != NULL) &&
                   (strcmp(current_netmodel_interface, netmodel_interface) == 0) &&
                   (fwiface->ipversion == ipversion));
    if(skip_update) {
        SAH_TRACEZ_INFO(ME, "Nothing to do");
        rv = true;
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Interface[%s] or ipversion %d changed (from [%s] %d)", netmodel_interface, ipversion, current_netmodel_interface == NULL ? "" : current_netmodel_interface, fwiface->ipversion);

    init_firewall_interface(fwiface);

    fwiface->netdev_required = !STRING_EMPTY(netmodel_interface);
    if(fwiface->netdev_required) {
        amxc_string_set(&fwiface->netmodel_interface, netmodel_interface);
        fwiface->ipversion = ipversion;

        fwiface->query = open_query(netmodel_interface, FW_QUERY_SUBSCR, "NetDevName", aflags, netmodel_traverse_down, handler, fwiface);
        if(fwiface->query == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to open NetDevName query");
        }
    } else {
        SAH_TRACEZ_NOTICE(ME, "Reset interface parameter");
    }

    rv = true;

exit:
    return rv;
}

// keeping this for legacy reasons, atm only policy is adapted to support dual query
// assumes firewall_interface_t* is never NULL
bool to_linux_interface(const char* netmodel_interface, ipversion_t ipversion, firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const char* flags = NULL;

    if(netmodel_interface == NULL) {
        init_firewall_interface(fwiface);
        goto exit;
    }

    if(fwiface->query_flags != NULL) {
        flags = fwiface->query_flags;
    } else if(ipversion == IPv4) {
        flags = "ipv4";
    } else if(ipversion == IPv6) {
        flags = "ipv6";
    } else {
        flags = "(ipv4 || ipv6)";
    }

    _to_linux_interface(netmodel_interface, ipversion, flags, fwiface, netmodel_openQuery_getFirstParameter, handle_interface_char);

exit:
    FW_PROFILING_OUT();
    return true;
}

// assumes firewall_interface_t* is never NULL
bool to_linux_interface_dual_query(const char* netmodel_interface, int ipversion, firewall_interface_t* fwiface, firewall_interface_t* v6_fwiface) {
    if(netmodel_interface == NULL) {
        init_firewall_interface(fwiface);
        init_firewall_interface(v6_fwiface);
        goto exit;
    }

    if(ipversion != IPv6) {
        _to_linux_interface(netmodel_interface, ipversion, "ipv4", fwiface, netmodel_openQuery_getParameters, handle_interface_list);
    }
    if(ipversion != IPv4) {
        _to_linux_interface(netmodel_interface, ipversion, "ipv6", v6_fwiface, netmodel_openQuery_getParameters, handle_interface_list);
    }

exit:
    return true;
}

static void handle_query_result_getaddrs(const char* sig_name UNUSED, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    firewall_interface_t* fwiface = (firewall_interface_t*) userdata;
    amxc_string_t cidr;
    uint32_t count = 0;

    amxc_string_init(&cidr, 256);

    when_null_trace(fwiface, exit, ERROR, "No firewall interface structure give");

    amxc_var_for_each(var, result) {
        const char* netdevname = NULL;
        const char* ipaddress = NULL;
        uint32_t prefixlen = 0;

        netdevname = GET_CHAR(var, "NetDevName");
        if(netdevname == NULL) {
            netdevname = "";
        }

        ipaddress = GET_CHAR(var, "Address");
        if(ipaddress == NULL) {
            ipaddress = "";
        }

        prefixlen = GET_UINT32(var, "PrefixLen");

        if(count == 0) {
            if(fwiface->query_result_pre_change != NULL) {
                SAH_TRACEZ_ERROR(ME, "pre change hook not implemented");
            }

            strncpy(fwiface->default_netdevname, netdevname, INTERFACE_LEN - 1);
        }

        if(count == 0) {
            amxc_string_setf(&cidr, "%s/%u", ipaddress, prefixlen);
        } else {
            amxc_string_appendf(&cidr, ",%s/%u", ipaddress, prefixlen);
        }

        count++;
    }

    address_from_string(amxc_string_get(&cidr, 0), &fwiface->address);

    fwiface->query_result_changed(fwiface);
exit:
    amxc_string_clean(&cidr);

    FW_PROFILING_OUT();
}

static void handle_query_result_getnetdevname(const char* sig_name UNUSED, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    firewall_interface_t* fwiface = (firewall_interface_t*) userdata;
    const char* netdevname = NULL;

    when_null_trace(fwiface, exit, ERROR, "No firewall interface structure give");

    netdevname = GET_CHAR(result, NULL);
    if(netdevname == NULL) {
        netdevname = "";
    }

    if(fwiface->query_result_pre_change != NULL) {
        SAH_TRACEZ_ERROR(ME, "pre change hook not implemented");
    }

    strncpy(fwiface->default_netdevname, netdevname, INTERFACE_LEN - 1);
    fwiface->query_result_changed(fwiface);
exit:
    FW_PROFILING_OUT();
}

static bool ip_address_query(const char* netmodel_interface, const char* flags, firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;
    when_null_trace(fwiface->query_result_changed, exit, ERROR, "missing post change hook");
    fwiface->query = netmodel_openQuery_getAddrs(netmodel_interface, FW_QUERY_SUBSCR, flags, netmodel_traverse_down, handle_query_result_getaddrs, fwiface);
    when_null_trace(fwiface->query, exit, ERROR, "Failed to open query");
    rv = true;
exit:
    FW_PROFILING_OUT();
    return rv;
}

static bool netdev_name_query(const char* netmodel_interface, const char* flags, firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;
    when_null_trace(fwiface->query_result_changed, exit, ERROR, "missing post change hook");

    fwiface->query = netmodel_openQuery_getFirstParameter(netmodel_interface, FW_QUERY_SUBSCR, "NetDevName", flags, netmodel_traverse_down, handle_query_result_getnetdevname, fwiface);
    when_null_trace(fwiface->query, exit, ERROR, "Failed to open query");
    rv = true;
exit:
    FW_PROFILING_OUT();
    return rv;
}

static bool get_netmodel_information(const char* netmodel_interface, ipversion_t ipversion, const char* aflags, firewall_interface_t* fwiface, bool address_required) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const char* current_netmodel_interface = amxc_string_get(&fwiface->netmodel_interface, 0);
    bool skip_update = false;

    if(netmodel_interface == NULL) {
        init_firewall_interface(fwiface);
        goto exit;
    }

    skip_update = ((current_netmodel_interface != NULL) &&
                   (strcmp(current_netmodel_interface, netmodel_interface) == 0) &&
                   (fwiface->ipversion == ipversion));
    when_true_trace(skip_update, exit, INFO, "Nothing to do");

    SAH_TRACEZ_INFO(ME, "Interface[%s] or ipversion %d changed (from [%s] %d)", netmodel_interface, ipversion, current_netmodel_interface == NULL ? "" : current_netmodel_interface, fwiface->ipversion);

    init_firewall_interface(fwiface);

    when_str_empty(netmodel_interface, exit);

    fwiface->addr_required = address_required;
    amxc_string_set(&fwiface->netmodel_interface, netmodel_interface);
    fwiface->ipversion = ipversion;
    if(fwiface->addr_required) {
        ip_address_query(netmodel_interface, aflags, fwiface);
    } else {
        netdev_name_query(netmodel_interface, aflags, fwiface);
    }

exit:
    FW_PROFILING_OUT();
    return true;
}

bool open_ip_address_query(const char* netmodel_interface, ipversion_t ipversion, const char* aflags, firewall_interface_t* fwiface) {
    return get_netmodel_information(netmodel_interface, ipversion, aflags, fwiface, true);
}

bool open_netdev_name_query(const char* netmodel_interface, ipversion_t ipversion, const char* aflags, firewall_interface_t* fwiface) {
    return get_netmodel_information(netmodel_interface, ipversion, aflags, fwiface, false);
}

void init_firewall_interface(firewall_interface_t* fwiface) {
    memset(fwiface->default_netdevname, 0, INTERFACE_LEN);
    amxc_string_set(&fwiface->netmodel_interface, "");
    address_init(&fwiface->address);
    fwiface->netdev_required = false;
    fwiface->addr_required = false;
    fwiface->ipversion = IPvUnknown;
    _netmodel_closeQuery(&fwiface->query);
}

static void interface_delete(amxc_llist_it_t* it) {
    linux_interface_t* liniface = amxc_container_of(it, linux_interface_t, it);
    amxc_string_clean(&liniface->netmodel_interface);
    free(liniface);
}

void clean_firewall_interface(firewall_interface_t* fwiface) {
    _netmodel_closeQuery(&fwiface->query);
    amxc_llist_clean(&fwiface->linifaces, interface_delete);
    amxc_string_clean(&fwiface->netmodel_interface);
    address_clean(&fwiface->address);
}

void _netmodel_closeQuery(netmodel_query_t** query) {
    when_null(query, exit);
    when_null(*query, exit);
    netmodel_closeQuery(*query);
    *query = NULL;
exit:
    return;
}