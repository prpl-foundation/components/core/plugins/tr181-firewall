/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>


#include "mock_ipset.h"

typedef struct {
    amxc_htable_it_t it;
    amxc_llist_t entries;
} mock_set_t;

static amxc_htable_t* mock_sets = NULL;

void __wrap_ipset_initialize(void) {
    amxc_htable_new(&mock_sets, 5);
}

static void mock_set_it_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    mock_set_t* set = amxc_htable_it_get_data(it, mock_set_t, it);
    amxc_llist_clean(&set->entries, amxc_string_list_it_free);
}

void __wrap_ipset_cleanup(void) {
    amxc_htable_delete(&mock_sets, mock_set_it_delete);
}

int __wrap_ipset_create(const char* set, set_type_t type) {
    check_expected(set);
    check_expected(type);
    return 0;
}

int __wrap_ipset_destroy(const char* set) {
    check_expected(set);
    return 0;
}

int __wrap_ipset_add_entry(const char* set, const char* entry, bool nomatch) {
    check_expected(set);
    check_expected(entry);
    check_expected(nomatch);
    return 0;
}

int __wrap_ipset_flush_set(const char* set) {
    check_expected(set);
    return 0;
}

void expect_ipset_add_entry(const char* set_val, const char* entry_val, bool nomatch_val) {
    expect_string(__wrap_ipset_add_entry, set, set_val);
    expect_string(__wrap_ipset_add_entry, entry, entry_val);
    expect_value(__wrap_ipset_add_entry, nomatch, nomatch_val);
}

void expect_ipset_create(const char* set_val, set_type_t type_val) {
    expect_string(__wrap_ipset_create, set, set_val);
    expect_value(__wrap_ipset_create, type, type_val);
}

void expect_ipset_update(const char* set_val, set_type_t type_val) {
    expect_string(__wrap_ipset_destroy, set, set_val);
    expect_string(__wrap_ipset_create, set, set_val);
    expect_value(__wrap_ipset_create, type, type_val);
}
