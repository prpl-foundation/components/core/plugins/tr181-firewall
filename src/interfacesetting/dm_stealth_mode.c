/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/dm_stealth_mode.h"
#include "interfacesetting/stealth_mode.h"
#include "protocols.h"
#include "logs/logs.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define ME "firewall"

/**
   @brief
   Translate the Netmodel interface and add to the common structure.

   @param[in] stealth_mode StealthMode internal structure.
   @param[in] interface NetModel interface.
 */
static void set_interface(stealth_mode_t* stealth_mode, const char* interface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    firewall_interface_t* fwiface = &stealth_mode->common.dest_fwiface;
    if(!STRING_EMPTY(interface)) {
        to_linux_interface(interface, 0, fwiface);
    } else {
        init_firewall_interface(fwiface);
    }
    FW_PROFILING_OUT();
}

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    common_t* common = (common_t*) fwiface->query_priv;
    when_null_trace(common, exit, ERROR, "Common object is NULL.");
    stealth_mode_t* stealth_mode = (stealth_mode_t*) common->priv_data;
    when_null_trace(stealth_mode, exit, ERROR, "StealthMode object is NULL.");

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_update(&common->folders.log,
               stealth_mode->target == TARGET_REJECT,
               log_get_dm_controller_param(common->object, NULL, NULL),
               !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
               NULL);
#endif //FIREWALL_LOGS
    stealth_mode_implement_changes(stealth_mode);
exit:
    FW_PROFILING_OUT();
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    ifsetting_t* ifsetting = NULL;
    stealth_mode_t* stealth_mode = NULL;
    bool update = false;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    ifsetting = (ifsetting_t*) object->priv;
    when_null_trace(ifsetting, exit, ERROR, "IterfaceSetting[%s] is NULL", OBJECT_NAMED);

    stealth_mode = ifsetting->stealth_mode;
    when_null(stealth_mode, exit);

    update = log_update(log,
                        stealth_mode->target == TARGET_REJECT,
                        log_get_dm_controller_param(object, NULL, NULL),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        data);
    if(update) {
        stealth_mode->common.flags = FW_MODIFIED;
        stealth_mode_implement_changes(stealth_mode);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

/**
   @brief
   Initialise the StealthMode internal structure.

   @param[in] instance Object with the InterfaceSetting's instance.

   @return
   0 if success, 1 if failed.
 */
int fw_stealth_mode_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;
    stealth_mode_t* stealth_mode = NULL;
    int ret_value = 1;
    bool res = false;

    when_null(object, exit);
    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);

    stealth_mode = ifsetting->stealth_mode;
    if(stealth_mode == NULL) {
        stealth_mode = create_fw_stealth_mode(object);
        when_null(stealth_mode, exit);
        ifsetting->stealth_mode = stealth_mode;
    }

    stealth_mode->common.dest_fwiface.query_priv = &stealth_mode->common;
    stealth_mode->common.dest_fwiface.query_result_changed = query_result_changed;
    stealth_mode->common.priv_data = stealth_mode;

    res = protocol_from_string(&stealth_mode->protocols, "17,6");
    if(!res) {
        SAH_TRACEZ_WARNING(ME, "StealthMode[%s] parsing protocols failed", OBJECT_NAMED);
    }

    stealth_mode->target = amxd_object_get_value(bool, object, "StealthMode", NULL) ? TARGET_DROP : TARGET_REJECT;

    set_interface(stealth_mode, object_da_string(object, "Interface"));

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_init(object, &stealth_mode->common.folders.log, &stealth_mode->common.dest_fwiface, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             stealth_mode->target == TARGET_REJECT, log_get_dm_controller_param(object, NULL, NULL), TARGET_REJECT, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

    stealth_mode->common.flags = FW_NEW;

    ret_value = 0;
exit:
    FW_PROFILING_OUT();
    return ret_value;
}

/**
   @brief
   Set the flags to "delete" and update the StealthMode.

   @param[in] ifsetting InterfaceSetting's struct

   @return
   0 if success, 1 if failed.
 */
int fw_stealth_mode_cleanup(ifsetting_t* ifsetting) {
    stealth_mode_t* stealth_mode = NULL;
    bool res;
    int ret_value = 1;

    when_null(ifsetting, exit);

    stealth_mode = ifsetting->stealth_mode;
    if(stealth_mode != NULL) {
        stealth_mode->common.flags = FW_DELETED;
        res = stealth_mode_implement_changes(stealth_mode);
        when_false(res, exit);
    }

    ret_value = 0;
exit:
    return ret_value;
}

void _stealth_mode_changed(const char* const sig_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    ifsetting_t* ifsetting = NULL;
    stealth_mode_t* stealth_mode = NULL;
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* obj_setting = amxd_dm_signal_get_object(dm, event_data);
    amxc_var_t* var = NULL;
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null(dm, exit);
    when_null(obj_setting, exit);
    ifsetting = (ifsetting_t*) obj_setting->priv;
    when_null(ifsetting, exit);
    stealth_mode = ifsetting->stealth_mode;
    when_null(stealth_mode, exit);

    stealth_mode->common.flags = FW_MODIFIED;

    var = GETP_ARG(event_data, "parameters.StealthMode");
    if(var != NULL) {
        stealth_mode->target = GET_BOOL(var, "to") ? TARGET_DROP : TARGET_REJECT;
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_update(&stealth_mode->common.folders.log,
               stealth_mode->target == TARGET_REJECT,
               log_get_dm_controller_param(obj_setting, event_data, NULL),
               !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
               event_data);
#endif //FIREWALL_LOGS
    stealth_mode_implement_changes(stealth_mode);
exit:
    FW_PROFILING_OUT();
    return;
}
