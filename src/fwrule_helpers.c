/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "fwrule_helpers.h"
#include "firewall.h"
#include "ports.h"
#include "logs/logs.h" // for log_clean
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <fwrules/fw_rule.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "firewall"
#define MISSING_DEFAULT_RULE "Missing default rule"
#define FETCH_FAILED "Failed to fetch feature rule"

target_t string_to_target(const char* s) {
    target_t target = TARGET_DROP;

    when_null_trace(s, exit, ERROR, "target is null");

    if(strcmp(s, "Accept") == 0) {
        target = TARGET_ACCEPT;
    } else if((strcmp(s, "TargetChain") == 0) ||
              (strcmp(s, "Chain") == 0)) {
        target = TARGET_CHAIN;
    } else if(strcmp(s, "Reject") == 0) {
        target = TARGET_REJECT;
    } else if(strcmp(s, "Return") == 0) {
        target = TARGET_RETURN;
    } else if(strcmp(s, "Log") == 0) {
        target = TARGET_LOG;
    } else {
        if(strcmp(s, "Drop") != 0) {
            SAH_TRACE_ERROR("Unknown target[%s]", s);
        }
    }

exit:
    return target;
}

bool fw_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag, const char* chain, const char* table, int index) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    int ret = -1;

    if((flag == FW_RULE_FLAG_NEW) || (flag == FW_RULE_FLAG_MODIFIED)) {
        ret = fw_replace_rule(rule, index);
    } else if(flag == FW_RULE_FLAG_DELETED) {
        ret = fw_delete_rule(rule, index);
    }

    if(ret != 0) {
        SAH_TRACE_ERROR("Failed to handle (flag=%d) chain[%s] table[%s] index[%d]", flag, chain, table, index);
    } else {
        SAH_TRACE_INFO("Successfully handled (flag=%d) chain[%s] table[%s] index[%d]", flag, chain, table, index);
    }

    FW_PROFILING_OUT();
    return (ret == 0);
}

void common_init(common_t* common, amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    common->object = object;
    common->object_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    common->flags = FW_NEW;
    common->priv_data = NULL;

    folder_container_init(&common->folders, common);

    amxc_var_init(&common->protocols);
    amxc_var_set_type(&common->protocols, AMXC_VAR_ID_LIST);

    init_firewall_interface(&common->src_fwiface);
    init_firewall_interface(&common->src6_fwiface);
    init_firewall_interface(&common->dest_fwiface);
    init_firewall_interface(&common->dest6_fwiface);

    common->schedule_status = SCHEDULE_STATUS_EMPTY;

    FW_PROFILING_OUT();
}

void common_clean(common_t* common) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    if(common->object != NULL) {
        common->object->priv = NULL;
        common->object = NULL;
    }
    FREE(common->object_path);

    folder_container_clean(&common->folders);

    amxc_var_clean(&common->protocols);

    clean_firewall_interface(&common->src_fwiface);
    clean_firewall_interface(&common->src6_fwiface);
    clean_firewall_interface(&common->dest_fwiface);
    clean_firewall_interface(&common->dest6_fwiface);

    schedule_close(&(common->schedule));

    FW_PROFILING_OUT();
}

void common_set_status(common_t* common, status_t status) {
    common->status = status;
    set_status(common->object, status);
}

void folder_wrapper_disable_folders(folder_wrapper_t* folder_wrapper, ipversion_t ipversion) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null_trace(folder_wrapper, exit, ERROR, "Folder wrapper is NULL");

    if(ipversion != IPv6) {
        if(folder_wrapper->folder != NULL) {
            fw_folder_set_enabled(folder_wrapper->folder, false);
        }
    }
    if(ipversion != IPv4) {
        if(folder_wrapper->folder6 != NULL) {
            fw_folder_set_enabled(folder_wrapper->folder6, false);
        }
    }

exit:
    FW_PROFILING_OUT();
    return;
}

bool folder_wrapper_delete_rules(folder_wrapper_t* folder_wrapper, ipversion_t ipversion, bool commit) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    bool commit_changes = false;

    when_null_trace(folder_wrapper, exit, ERROR, "Folder wrapper is NULL");

    if(ipversion != IPv6) {
        if((folder_wrapper->folder != NULL) && fw_folder_get_enabled(folder_wrapper->folder)) {
            fw_folder_delete_rules(folder_wrapper->folder);
            commit_changes = commit;
        }
    }
    if(ipversion != IPv4) {
        if((folder_wrapper->folder6 != NULL) && fw_folder_get_enabled(folder_wrapper->folder6)) {
            fw_folder_delete_rules(folder_wrapper->folder6);
            commit_changes = commit;
        }
    }

    if(commit_changes) {
        if(fw_commit(fw_rule_callback) != 0) {
            SAH_TRACEZ_ERROR(ME, "fw_commit failed");
        }
        folder_wrapper_disable_folders(folder_wrapper, ipversion);
    }
exit:
    FW_PROFILING_OUT();
    return commit_changes;
}

void folder_wrapper_init(folder_wrapper_t* folder_wrapper) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null_trace(folder_wrapper, exit, ERROR, "Folder wrapper is NULL");

    fw_folder_new(&folder_wrapper->folder);
    fw_folder_new(&folder_wrapper->folder6);

exit:
    FW_PROFILING_OUT();
    return;
}

void folder_wrapper_clean(folder_wrapper_t* folder_wrapper) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    fw_folder_delete(&folder_wrapper->folder);
    fw_folder_delete(&folder_wrapper->folder6);
    fw_commit(fw_rule_callback);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_clean(&folder_wrapper->log);
#endif //FIREWALL_LOGS
    FW_PROFILING_OUT();
}

void folder_container_disable_folders(folder_container_t* folders, ipversion_t ipversion) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(folders, exit);

    if(ipversion != IPv6) {
        if(folders->log_folders.folder != NULL) {
            fw_folder_set_enabled(folders->log_folders.folder, false);
        }
        if(folders->rule_folders.folder != NULL) {
            fw_folder_set_enabled(folders->rule_folders.folder, false);
        }
    }
    if(ipversion != IPv4) {
        if(folders->log_folders.folder6 != NULL) {
            fw_folder_set_enabled(folders->log_folders.folder6, false);
        }
        if(folders->rule_folders.folder6 != NULL) {
            fw_folder_set_enabled(folders->rule_folders.folder6, false);
        }
    }

exit:
    FW_PROFILING_OUT();
    return;
}

bool folder_container_delete_rules(folder_container_t* folders, ipversion_t ipversion, bool commit) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    bool commit_changes = false;

    when_null(folders, exit);

    if(ipversion != IPv6) {
        if((folders->log_folders.folder != NULL) && fw_folder_get_enabled(folders->log_folders.folder)) {
            fw_folder_delete_rules(folders->log_folders.folder);
            commit_changes = commit;
        }
        if((folders->rule_folders.folder != NULL) && fw_folder_get_enabled(folders->rule_folders.folder)) {
            fw_folder_delete_rules(folders->rule_folders.folder);
            commit_changes = commit;
        }
    }
    if(ipversion != IPv4) {
        if((folders->log_folders.folder6 != NULL) && fw_folder_get_enabled(folders->log_folders.folder6)) {
            fw_folder_delete_rules(folders->log_folders.folder6);
            commit_changes = commit;
        }
        if((folders->rule_folders.folder6 != NULL) && fw_folder_get_enabled(folders->rule_folders.folder6)) {
            fw_folder_delete_rules(folders->rule_folders.folder6);
            commit_changes = commit;
        }
    }

    if(commit_changes) {
        if(fw_commit(fw_rule_callback) != 0) {
            SAH_TRACEZ_ERROR(ME, "fw_commit failed");
        }
        folder_container_disable_folders(folders, ipversion);
    }
exit:
    FW_PROFILING_OUT();
    return commit_changes;
}

bool is_folders_initialized(folder_container_t* folders) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool initialized = false;

    when_null(folders, exit);
    when_null(folders->log_folders.folder, exit);
    when_null(folders->rule_folders.folder, exit);
    when_null(folders->log_folders.folder6, exit);
    when_null(folders->rule_folders.folder6, exit);

    initialized = true;

exit:
    FW_PROFILING_OUT();
    return initialized;
}

void folder_container_init(folder_container_t* folders, common_t* common) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(folders, exit);

    folders->priv_data = common;

    fw_folder_new(&folders->log_folders.folder);
    fw_folder_new(&folders->rule_folders.folder);
    fw_folder_new(&folders->log_folders.folder6);
    fw_folder_new(&folders->rule_folders.folder6);

exit:
    FW_PROFILING_OUT();
    return;
}

void folder_container_clean(folder_container_t* folders) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    fw_folder_delete(&folders->log_folders.folder);
    fw_folder_delete(&folders->log_folders.folder6);
    fw_folder_delete(&folders->rule_folders.folder);
    fw_folder_delete(&folders->rule_folders.folder6);
    fw_commit(fw_rule_callback);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_clean(&folders->log);
#endif //FIREWALL_LOGS
    FW_PROFILING_OUT();
}

static bool is_folder_ordered(folder_container_t* folders) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool ordered = false;
    uint64_t order = 0;

    when_null(folders, exit);

    order = fw_folder_get_order(folders->log_folders.folder);
    when_true(order == 0, exit);
    when_false(fw_folder_get_order(folders->rule_folders.folder) == order++, exit);
    when_false(fw_folder_get_order(folders->log_folders.folder6) == order++, exit);
    when_false(fw_folder_get_order(folders->rule_folders.folder6) == order++, exit);

    ordered = true;
exit:
    FW_PROFILING_OUT();
    return ordered;
}

bool is_folders_array_ordered(folder_container_t** folder_container_array, int array_size) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool folders_ordered = true;
    uint32_t order = 0;
    uint32_t order_prev = 0;

    when_null(folder_container_array, exit);

    for(int i = 0; i < array_size; i++) {
        folders_ordered &= is_folder_ordered(folder_container_array[i]);
        when_false(folders_ordered, exit);
        order = fw_folder_get_order(folder_container_array[i]->log_folders.folder);
        if(i != 0) {
            order_prev = fw_folder_get_order(folder_container_array[i - 1]->rule_folders.folder6);
            folders_ordered &= (order == order_prev + 1);
        }
        when_false(folders_ordered, exit);
    }

exit:
    FW_PROFILING_OUT();
    return folders_ordered;
}

static bool set_folder_before_folder(fw_folder_t* folder, fw_folder_t* folder_next) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool updated = false;

    when_null(folder, exit);
    when_null(folder_next, exit);

    if(fw_folder_get_order(folder) < fw_folder_get_order(folder_next) - 1) {
        fw_folder_set_order(folder, fw_folder_get_order(folder_next) - 1);
        updated = true;
    } else if(fw_folder_get_order(folder) > fw_folder_get_order(folder_next) - 1) {
        fw_folder_set_order(folder, fw_folder_get_order(folder_next));
        updated = true;
    }
exit:
    FW_PROFILING_OUT();
    return updated;
}

/**
   @ingroup folder_container
   @brief
   Reorder an array of folder_container_t.

   Assuming folder_container_array = {first, second}

   After reordering the following order should be seen in the folder list on lib_fwrules:
   first->log_folder
   first->folder
   first->log_folder6
   first->folder6
   second->log_folder
   second->folder
   second->log_folder6
   second->folder6

   @details
   @param[in] folder_container_array Pointer to a an array of folder_container_t.
   @param[in] array_size Size of the array

   @returns @c @c true if the array was reordered, @c @c false if the array was not reordered
 */
bool reorder_array_folder_containers(folder_container_t** folder_container_array, int array_size) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool folders_reordered = false;
    bool commons_ordered = false;

    when_null(folder_container_array, exit);

    commons_ordered = is_folders_array_ordered(folder_container_array, array_size);

    when_true(commons_ordered, exit);

    for(int i = array_size - 1; i >= 0; i--) {
        folders_reordered |= folder_container_reorder(folder_container_array[i]);
        //Set the last folder(folder6) of the previous common to be before the first folder (log_folder) of the current common
        if(i > 0) { //Don't do it for the fisrt common as there is no "common" before it to be ordered.
            if((folder_container_array[i - 1] != NULL) && (folder_container_array[i] != NULL)) {
                folders_reordered |= set_folder_before_folder(folder_container_array[i - 1]->rule_folders.folder6, folder_container_array[i]->log_folders.folder);
            }
        }
    }

exit:
    FW_PROFILING_OUT();
    return folders_reordered;
}

/**
   @ingroup folder_container
   @brief
   Reorder folders inside a folder_container_t

   After reordering, the following order should be seen in the folder list on lib_fwrules:
   common->log_folder
   common->folder
   common->log_folder6
   common->folder6

   @details
   @param[in] common Pointer to the folder_container_t.

   @returns @c @c true if the array was reordered, @c @c false if the array was not reordered
 */
bool folder_container_reorder(folder_container_t* folders) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool updated = false;

    when_null(folders, exit);

    if(!is_folder_ordered(folders)) {
        updated |= set_folder_before_folder(folders->log_folders.folder6, folders->rule_folders.folder6);
        updated |= set_folder_before_folder(folders->rule_folders.folder, folders->log_folders.folder6);
        updated |= set_folder_before_folder(folders->log_folders.folder, folders->rule_folders.folder);
    }

exit:
    FW_PROFILING_OUT();
    return updated;
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
bool set_log_rule_specific_settings(fw_rule_t* r, const log_t* const log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_set_conntrack_state(r, "NEW");
    fw_rule_set_target_log_options(r, log_get_level(log), log_get_flags(log), log_get_prefix(log));

    FW_PROFILING_OUT();
    return true;
}
#endif //FIREWALL_LOGS

bool folder_feature_set_target_policy(fw_folder_t* folder, fw_rule_policy_t policy) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_rule_t* s = NULL;

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    when_null_trace(s, error, ERROR, FETCH_FAILED);

    fw_rule_set_target_policy(s, policy);
    fw_folder_push_rule(folder, s);
    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_protocol(fw_folder_t* folder, amxc_var_t* protocols) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;

    if(amxc_var_get_first(protocols) == NULL) {
        fw_folder_unset_feature(folder, FW_FEATURE_PROTOCOL); // could be set the previous time the folder was activated and might not be needed anymore
        goto exit;
    }

    fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);

    amxc_var_for_each(var, protocols) {
        fw_rule_t* s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL);
        int protocol = GET_INT32(var, NULL);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_protocol(s, protocol);
        fw_folder_push_rule(folder, s);
    }

exit:
    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool enable_rule(fw_folder_t* log_folder, fw_folder_t* folder, status_t* status, bool commit) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool log_enable = (log_folder != NULL);
    bool ret = false;

    if(log_enable) {
        fw_folder_set_enabled(log_folder, true);
    }
    fw_folder_set_enabled(folder, true);

    if(commit) {
        if(fw_commit(fw_rule_callback) == 0) {
            *status = FIREWALL_ENABLED;
        } else {
            SAH_TRACEZ_ERROR(ME, "Enable rule: Error");
            *status = FIREWALL_ERROR;
            if(log_enable) {
                fw_folder_delete_rules(log_folder);
            }
            fw_folder_delete_rules(folder);
            fw_commit(fw_rule_callback);
            if(log_enable) {
                fw_folder_set_enabled(log_folder, false);
            }
            fw_folder_set_enabled(folder, false);
            goto out;
        }
    }
    ret = true;
out:
    FW_PROFILING_OUT();
    return ret;
}

bool folder_feature_set_source_port(fw_folder_t* folder, amxc_llist_t* ports, bool exclude) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_rule_t* s = NULL;

    if(amxc_llist_is_empty(ports)) {
        fw_folder_unset_feature(folder, FW_FEATURE_SOURCE_PORT); // could be set the previous time the folder was activated and might not be needed anymore
        goto exit;
    }

    fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);

    amxc_llist_for_each(it, ports) {
        port_range_t* port_range = amxc_container_of(it, port_range_t, it);

        if(PORT_USED(port_range->start)) {
            s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
            when_null_trace(s, error, ERROR, FETCH_FAILED);

            fw_rule_set_source_port(s, port_range->start);
            fw_rule_set_source_port_excluded(s, exclude);
            if(port_range->end > port_range->start) {
                fw_rule_set_source_port_range_max(s, port_range->end);
            }
            fw_folder_push_rule(folder, s);
        }
    }

exit:
    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_destination_port(fw_folder_t* folder, amxc_llist_t* ports, bool exclude) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;

    if(amxc_llist_is_empty(ports)) {
        fw_folder_unset_feature(folder, FW_FEATURE_DESTINATION_PORT); // could be set the previous time the folder was activated and might not be needed anymore
        goto exit;
    }

    fw_folder_set_feature(folder, FW_FEATURE_DESTINATION_PORT);

    amxc_llist_for_each(it, ports) {
        port_range_t* port_range = amxc_container_of(it, port_range_t, it);
        fw_rule_t* s = NULL;

        if(port_range->start == 0) {
            continue;
        }

        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DESTINATION_PORT);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_destination_port(s, port_range->start);
        fw_rule_set_destination_port_excluded(s, exclude);
        if(port_range->end > port_range->start) {
            fw_rule_set_destination_port_range_max(s, port_range->end);
        }
        fw_folder_push_rule(folder, s);
    }
exit:
    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_source_mac(fw_folder_t* folder, const char* mac, bool excluded) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_rule_t* s = NULL;

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_MAC);
    when_null_trace(s, error, ERROR, FETCH_FAILED);

    fw_rule_set_source_mac_address(s, mac);
    fw_rule_set_source_mac_excluded(s, excluded);
    fw_folder_push_rule(folder, s);

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

// don't use this directly, imo it should not determine when to set a default rule and when a feature rule
// instead use folder_feature_set_source or folder_default_set_source
static bool set_source(fw_folder_t* folder, fw_rule_t* r, const char* ip, const char* netmask, bool excluded) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_rule_t* s = NULL;

    when_true_trace(((folder == NULL) && (r == NULL)), error, ERROR, "Missing folder and default rule");

    if(r == NULL) {
        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE);
        when_null_trace(s, error, ERROR, FETCH_FAILED);
    } else {
        s = r;
    }

    fw_rule_set_source(s, ip);
    if(netmask != NULL) {
        fw_rule_set_source_mask(s, netmask);
    }
    fw_rule_set_source_excluded(s, excluded);
    if(r == NULL) {
        fw_folder_push_rule(folder, s);
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_default_set_source(fw_rule_t* default_rule, const char* ip, const char* netmask, bool excluded) {
    bool rv = false;
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null_trace(default_rule, exit, ERROR, MISSING_DEFAULT_RULE);
    rv = set_source(NULL, default_rule, ip, netmask, excluded);
exit:
    FW_PROFILING_OUT();
    return rv;
}

// don't use this directly, imo it should not determine when to set a default rule and when a feature rule
// instead use folder_feature_set_destination or folder_default_set_destination
static bool set_destination(fw_folder_t* folder, fw_rule_t* r, const char* ip, const char* netmask, bool excluded) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_rule_t* s = NULL;

    when_true_trace(((folder == NULL) && (r == NULL)), error, ERROR, "Missing folder and default rule");

    if(r == NULL) {
        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DESTINATION);
        when_null_trace(s, error, ERROR, FETCH_FAILED);
    } else {
        s = r;
    }

    fw_rule_set_destination(s, ip);
    if(netmask != NULL) {
        fw_rule_set_destination_mask(s, netmask);
    }
    fw_rule_set_destination_excluded(s, excluded);
    if(r == NULL) {
        fw_folder_push_rule(folder, s);
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_default_set_destination(fw_rule_t* default_rule, const char* ip, const char* netmask, bool excluded) {
    bool rv = false;
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null_trace(default_rule, exit, ERROR, MISSING_DEFAULT_RULE);
    rv = set_destination(NULL, default_rule, ip, netmask, excluded);
exit:
    FW_PROFILING_OUT();
    return rv;
}

static bool require_ip_address(address_t* address, fw_feature_t feature, fw_folder_t* folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool res = false;

    when_null(folder, out);

    if(address == NULL) {
        fw_folder_unset_feature(folder, feature);
        res = true;
        goto out;
    }

    if(address_is_required(address)) {
        fw_folder_set_feature(folder, feature);
    } else {
        fw_folder_unset_feature(folder, feature);
    }

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

// if FW_FEATURE_SOURCE is not set, this function won't fetch a feature rule and return true
bool folder_require_source_feature(fw_folder_t* folder, address_t* address) {
    return require_ip_address(address, FW_FEATURE_SOURCE, folder);
}

// if FW_FEATURE_DESTINATION is not set, this function won't fetch a feature rule and return true
bool folder_require_dest_feature(fw_folder_t* folder, address_t* address) {
    return require_ip_address(address, FW_FEATURE_DESTINATION, folder);
}

// don't use this directly, imo it should not determine when to set a default rule and when a feature rule
// instead use folder_feature_set_address or folder_default_set_address
static bool enable_ip_address(address_t* address, bool ipv4, bool source, fw_rule_t* r /* can be NULL */, fw_folder_t* folder /* can be NULL */, bool exclude, const char* (*get_address)(amxc_var_t*)) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    size_t num_of_addresses = 0;
    bool rv = false;

    when_null_status(address, exit, rv = true);

    when_false_status(address_is_required(address), exit, rv = true);

    num_of_addresses = address_count(address, ipv4 ? IPv4 : IPv6);
    when_true_trace(num_of_addresses == 0, exit, ERROR, "No address, wait...");
    when_true_trace(((num_of_addresses > 1) && (r != NULL)), exit, ERROR, "Too many addresses for default rule");

    amxc_var_for_each(prefix, address->list[ipv4 ? ADDRESS_IPv4 : ADDRESS_IPv6]) {
        if(source) {
            when_false(set_source(folder, r, get_address(prefix), NULL, exclude), exit);
        } else {
            when_false(set_destination(folder, r, get_address(prefix), NULL, exclude), exit);
        }
    }

#ifdef WITH_GMAP
    amxc_var_for_each(devices, address->list[ipv4 ? ADDRESS_DEVICE_IPv4 : ADDRESS_DEVICE_IPv6]) {
        amxc_var_for_each(prefix, devices) {
            if(source) {
                when_false(set_source(folder, r, get_address(prefix), NULL, exclude), exit);
            } else {
                when_false(set_destination(folder, r, get_address(prefix), NULL, exclude), exit);
            }
        }
    }
#endif

    rv = true;
exit:
    FW_PROFILING_OUT();
    return rv;
}

bool folder_default_set_address(fw_rule_t* default_rule, address_t* address, bool ipv4, bool source, bool exclude) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;
    when_null_trace(default_rule, exit, ERROR, "No default rule");
    rv = enable_ip_address(address, ipv4, source, default_rule, NULL, exclude, network_address);
    FW_PROFILING_OUT();
exit:
    return rv;
}

bool folder_default_set_host_address(fw_rule_t* default_rule, address_t* address, bool ipv4, bool source, bool exclude) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;
    when_null_trace(default_rule, exit, ERROR, "No default rule");
    rv = enable_ip_address(address, ipv4, source, default_rule, NULL, exclude, host_address);
    FW_PROFILING_OUT();
exit:
    return rv;
}

bool folder_feature_set_address(fw_folder_t* folder, address_t* address, bool ipv4, bool source, bool exclude) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;
    rv = enable_ip_address(address, ipv4, source, NULL, folder, exclude, network_address);
    FW_PROFILING_OUT();
    return rv;
}

bool folder_default_set_in_and_out_interfaces(fw_rule_t* default_rule, const char* destination, bool dest_excl, const char* source, bool src_excl, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;

    when_null_trace(default_rule, exit, ERROR, MISSING_DEFAULT_RULE);

    if(!reverse) {
        if(source != NULL) {
            fw_rule_set_in_interface(default_rule, source);
            fw_rule_set_in_interface_excluded(default_rule, src_excl);
        }
        if(destination != NULL) {
            fw_rule_set_out_interface(default_rule, destination);
            fw_rule_set_out_interface_excluded(default_rule, dest_excl);
        }
    } else {
        if(source != NULL) {
            fw_rule_set_out_interface(default_rule, source);
            fw_rule_set_out_interface_excluded(default_rule, src_excl);
        }
        if(destination != NULL) {
            fw_rule_set_in_interface(default_rule, destination);
            fw_rule_set_in_interface_excluded(default_rule, dest_excl);
        }
    }

    rv = true;

exit:
    FW_PROFILING_OUT();
    return rv;
}

bool folder_feature_set_target(fw_folder_t* folder, target_t target, const char* chain_name, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* s = NULL;
    bool retval = false;
    amxc_string_t buffer;

    amxc_string_init(&buffer, 0);

    switch(target) {
    case TARGET_DROP:
        retval = folder_feature_set_target_policy(folder, FW_RULE_POLICY_DROP);
        break;
    case TARGET_ACCEPT:
        retval = folder_feature_set_target_policy(folder, FW_RULE_POLICY_ACCEPT);
        break;
    case TARGET_REJECT:
        retval = folder_feature_set_target_policy(folder, FW_RULE_POLICY_REJECT);
        break;
    case TARGET_CHAIN:
        if(!STRING_EMPTY(chain_name)) {
            bool is_input = false;
            const char* chain = NULL;

            s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
            when_null_trace(s, error, ERROR, FETCH_FAILED);

            chain = fw_rule_get_chain(s);
            is_input = (chain == NULL) || (strncmp(chain, "INPUT", 5) == 0);

            amxc_string_setf(&buffer, "%s%s_%s", (is_input) ? "INPUT" : "FORWARD", ipv4 ? "" : "6", chain_name);

            fw_rule_set_target_chain(s, amxc_string_get(&buffer, 0));
            fw_folder_push_rule(folder, s);
            retval = true;
        } else {
            SAH_TRACEZ_ERROR(ME, "Chain not found!");
        }
        break;
    case TARGET_RETURN:
        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_target_return(s);
        fw_folder_push_rule(folder, s);
        retval = true;
        break;
    case TARGET_NONE:
    default:
        SAH_TRACEZ_ERROR(ME, "Target needs to be defined");
        break;
    }

error:
    amxc_string_clean(&buffer);
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_source_and_destination_port(fw_folder_t* folder, amxc_llist_t* src_ports, bool sport_exclude, amxc_llist_t* dst_ports, bool dport_exclude, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = true;

    if(reverse == false) {
        retval &= folder_feature_set_source_port(folder, src_ports, sport_exclude);
        retval &= folder_feature_set_destination_port(folder, dst_ports, dport_exclude);
    } else {     //reverse, change source and destination Port
        retval &= folder_feature_set_destination_port(folder, src_ports, sport_exclude);
        retval &= folder_feature_set_source_port(folder, dst_ports, dport_exclude);
    }

    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_dscp(fw_folder_t* folder, int32_t dscp, bool exclude) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* s = NULL;
    bool retval = false;

    if(dscp > 0) {
        fw_folder_set_feature(folder, FW_FEATURE_DSCP);

        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DSCP);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_dscp(s, dscp);
        fw_rule_set_dscp_excluded(s, exclude);
        fw_folder_push_rule(folder, s);
    }
    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_source_ipset(fw_folder_t* folder, const char* source_set_path, const char* source_set_exclude_path) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const char* path = NULL;
    const char* source_set_name = NULL;
    amxd_object_t* source_set = NULL;
    fw_rule_t* s = NULL;
    bool exclude = false;
    bool retval = false;

    if(!STRING_EMPTY(source_set_path)) {
        path = source_set_path;
        exclude = false;
    } else if(!STRING_EMPTY(source_set_exclude_path)) {
        path = source_set_exclude_path;
        exclude = true;
    }

    if(!STRING_EMPTY(path)) {
        source_set = amxd_object_findf(amxd_dm_get_root(fw_get_dm()), "%s", path);
        when_null_trace(source_set, error, ERROR, "SET instance refcount is null");
        source_set_name = amxd_object_get_name(source_set, AMXD_OBJECT_NAMED);
        SAH_TRACEZ_INFO(ME, "Set %sSource SET '%s' for rule", exclude ? "excluded " : "", source_set_name);

        fw_folder_set_feature(folder, FW_FEATURE_SOURCE_IPSET);

        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_IPSET);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_source_ipset(s, source_set_name);
        fw_rule_set_source_ipset_excluded(s, exclude);
        fw_folder_push_rule(folder, s);
    } else {
        fw_folder_unset_feature(folder, FW_FEATURE_SOURCE_IPSET);
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_destination_ipset(fw_folder_t* folder, const char* dest_set_path, const char* dest_set_exclude_path) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const char* path = NULL;
    const char* dest_set_name = NULL;
    amxd_object_t* dest_set = NULL;
    fw_rule_t* s = NULL;
    bool exclude = false;
    bool retval = false;

    if(!STRING_EMPTY(dest_set_path)) {
        path = dest_set_path;
        exclude = false;
    } else if(!STRING_EMPTY(dest_set_exclude_path)) {
        path = dest_set_exclude_path;
        exclude = true;
    }

    if(!STRING_EMPTY(path)) {
        dest_set = amxd_object_findf(amxd_dm_get_root(fw_get_dm()), "%s", path);
        when_null_trace(dest_set, error, ERROR, "SET instance refcount is null");
        dest_set_name = amxd_object_get_name(dest_set, AMXD_OBJECT_NAMED);
        SAH_TRACEZ_INFO(ME, "Set %sDestination SET '%s' for rule", exclude ? "excluded " : "", dest_set_name);

        fw_folder_set_feature(folder, FW_FEATURE_DESTINATION_IPSET);

        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DESTINATION_IPSET);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_destination_ipset(s, dest_set_name);
        fw_rule_set_destination_ipset_excluded(s, exclude);
        fw_folder_push_rule(folder, s);
    } else {
        fw_folder_unset_feature(folder, FW_FEATURE_DESTINATION_IPSET);
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}


bool folder_feature_set_target_dnat(fw_folder_t* folder, const char* destination, int int_port, int int_port_max, int ext_port, int ext_port_max) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    fw_rule_t* s = NULL;

    when_str_empty(destination, error);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DNAT);
    when_null_trace(s, error, ERROR, FETCH_FAILED);

    fw_rule_set_target_dnat(s, destination, int_port, int_port_max);

    if(ext_port > 0) {
        fw_rule_set_destination_port(s, ext_port);
        if(ext_port_max > ext_port) {
            fw_rule_set_destination_port_range_max(s, ext_port_max);
        }
    }

    fw_folder_push_rule(folder, s);
    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

static bool folder_feature_set_target_dnat_with_ports(fw_folder_t* folder, const char* destination, amxc_llist_t* src_ports, amxc_llist_t* dst_ports) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;

    if(!amxc_llist_is_empty(src_ports) && !amxc_llist_is_empty(dst_ports)) {
        amxc_llist_iterate(it, src_ports) {
            port_range_t* extp = amxc_container_of(it, port_range_t, it);

            amxc_llist_iterate(it2, dst_ports) {
                port_range_t* intp = amxc_container_of(it2, port_range_t, it);

                when_false(folder_feature_set_target_dnat(folder, destination, intp->start, intp->end, extp->start, extp->end), error);
            }
        }
    } else if(!amxc_llist_is_empty(src_ports)) {
        amxc_llist_iterate(it, src_ports) {
            port_range_t* extp = amxc_container_of(it, port_range_t, it);

            when_false(folder_feature_set_target_dnat(folder, destination, 0, 0, extp->start, extp->end), error);
        }
    } else if(!amxc_llist_is_empty(dst_ports)) {
        amxc_llist_iterate(it, dst_ports) {
            port_range_t* intp = amxc_container_of(it, port_range_t, it);

            when_false(folder_feature_set_target_dnat(folder, destination, intp->start, intp->end, 0, 0), error);
        }
    } else {
        when_false(folder_feature_set_target_dnat(folder, destination, 0, 0, 0, 0), error);
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool folder_feature_set_target_snat(fw_folder_t* folder, address_t* address, int minport, int maxport) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;

    amxc_var_for_each(prefix, address->list[ADDRESS_IPv4]) {
        fw_rule_t* s = NULL;

        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SNAT);
        when_null_trace(s, error, ERROR, FETCH_FAILED);

        fw_rule_set_target_snat(s, host_address(prefix), minport, maxport);

        fw_folder_push_rule(folder, s);
    }

    retval = true;
error:
    FW_PROFILING_OUT();
    return retval;
}

bool fill_folder_dnat_rule(fw_folder_t* folder, dnat_args_t* args) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    bool res = false;
    const bool ipv4 = true; // always IPv4 for port forwarding
    const char* nat_address = NULL;

    // Only one destination IP address is supported for NAT
    nat_address = address_first_ipv4_char(args->target.address);
    when_str_empty_trace(nat_address, out, ERROR, "No nat dest address");

    fw_folder_set_feature(folder, FW_FEATURE_DNAT);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "Failed to fetch default rule");

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, args->chain);
    fw_rule_set_ipv4(r, ipv4);

    if(args->match.src_address != NULL) {
        when_false(folder_default_set_address(r, args->match.src_address, ipv4, true, false), out);
    }

    if(args->match.dest_address != NULL) {
        when_false(folder_default_set_host_address(r, args->match.dest_address, ipv4, false, false), out);
    }

    fw_folder_push_rule(folder, r);

    when_false(folder_feature_set_protocol(folder, args->match.protocols), out);
    when_false(folder_feature_set_destination_port(folder, args->match.dest_ports, false), out);
    when_false(folder_feature_set_target_dnat_with_ports(folder, nat_address, args->target.src_ports, args->target.dest_ports), out);

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

bool fill_folder_container_filter_rule(folder_container_t* folder_container, filter_args_t* args) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    bool res = true;
    bool ipv4 = (args->ipversion == IPv4);
    fw_folder_t* folder = ipv4 ? folder_container->rule_folders.folder : folder_container->rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? folder_container->log_folders.folder : folder_container->log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&folder_container->log) ? 0 : 1;
#else
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    amxc_llist_t* src_ports = args->reverse ? args->match.dest_ports : args->match.src_ports;
    amxc_llist_t* dest_ports = args->reverse ? args->match.src_ports : args->match.dest_ports;

    if(args->match.src_address != NULL) {
        bool waiting = false;

        waiting = address_is_required(args->match.src_address) &&
            !address_has_ipversion(args->match.src_address, args->ipversion);
        if(waiting) {
            SAH_TRACEZ_INFO(ME, "Rule is waiting for IPv%d source addresses", args->ipversion);
            res = true;
            goto out;
        }
    }

    if(args->match.dest_address != NULL) {
        bool waiting = false;

        waiting = address_is_required(args->match.dest_address) &&
            !address_has_ipversion(args->match.dest_address, args->ipversion);
        if(waiting) {
            SAH_TRACEZ_INFO(ME, "Rule is waiting for IPv%d dest addresses", args->ipversion);
            res = true;
            goto out;
        }
    }

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        if((folders[i] == folder) && (args->target.verdict == TARGET_LOG)) {
            continue;
        }

        if(folders[i] != log_folder) {
            fw_folder_set_feature(folders[i], FW_FEATURE_TARGET);
        }

        if(amxc_var_get_first(args->match.protocols) != NULL) {
            fw_folder_set_feature(folders[i], FW_FEATURE_PROTOCOL);
        }

        if(!args->reverse) {
            bool feature_res = false;
            feature_res = folder_require_source_feature(folders[i], args->match.src_address);
            when_false_trace(feature_res, out, INFO, "Source IPv%d addresses not yet configured", args->ipversion);
            feature_res = folder_require_dest_feature(folders[i], args->match.dest_address);
            when_false_trace(feature_res, out, INFO, "Dest IPv%d addresses not yet configured", args->ipversion);
        } else {
            bool feature_res = false;
            feature_res = folder_require_source_feature(folders[i], args->match.dest_address);
            when_false_trace(feature_res, out, INFO, "Dest IPv%d addresses not yet configured", args->ipversion);
            feature_res = folder_require_dest_feature(folders[i], args->match.src_address);
            when_false_trace(feature_res, out, INFO, "Source IPv%d addresses not yet configured", args->ipversion);
        }

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "Failed to fetch default %s rule", folders[i] == log_folder ? "log" : "");

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, args->chain);

        if(!args->reverse) {
            fw_rule_set_in_interface(r, args->match.iface);
        } else {
            fw_rule_set_out_interface(r, args->match.iface);
        }

        fw_rule_set_ipv4(r, ipv4);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &folder_container->log);
        }
#endif //FIREWALL_LOGS
        fw_folder_push_rule(folders[i], r);

        when_false(folder_feature_set_address(folders[i], args->match.dest_address, ipv4, args->reverse, false), out);
        when_false(folder_feature_set_address(folders[i], args->match.src_address, ipv4, !args->reverse, false), out);
        when_false(folder_feature_set_protocol(folders[i], args->match.protocols), out);

        if(folders[i] != log_folder) {
            when_false(folder_feature_set_target_policy(folders[i], FW_RULE_POLICY_ACCEPT), out);
        }

        when_false(folder_feature_set_source_port(folders[i], src_ports, false), out);
        when_false(folder_feature_set_destination_port(folders[i], dest_ports, false), out);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            log_set_enable(&folder_container->log, true);
        }
#endif //FIREWALL_LOGS
    }
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}