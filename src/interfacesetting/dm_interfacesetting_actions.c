/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/interfacesetting.h"
#include "interfacesetting/dm_spoofing_protection.h"
#include "interfacesetting/dm_icmpv6_passthrough.h"
#include "interfacesetting/dm_icmp_echo_request.h"
#include "interfacesetting/dm_stealth_mode.h"
#include "interfacesetting/dm_accept_udp_traceroute.h"
#include "interfacesetting/dm_log_in_drop.h"
#include "interfacesetting/dm_log_out_drop.h"
#include "interfacesetting/dm_log_out_accept.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define ME "firewall"

/**
   @brief
   Set the flags to "delete" and update the InterfaceSetting.

   @param[in] instance Object with the InterfaceSetting's instance to be cleaned-up.

   @return
   0 if success, 1 if failed.
 */
static int ifsetting_cleanup(amxd_object_t* instance) {
    ifsetting_t* ifsetting = NULL;
    bool res = false;
    int ret_value = 1;

    when_null_status(instance, exit, ret_value = 0);

    ifsetting = (ifsetting_t*) instance->priv;
    when_null_status(ifsetting, exit, ret_value = 0);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_out_accept_cleanup(ifsetting);
    log_out_drop_cleanup(ifsetting);
    log_in_drop_cleanup(ifsetting);
#endif //FIREWALL_LOGS
    fw_accept_udp_traceroute_cleanup(ifsetting);
    fw_stealth_mode_cleanup(ifsetting);
    fw_icmp_echo_request_cleanup(ifsetting);
    fw_icmpv6_passthrough_cleanup(ifsetting);
    fw_spoofing_cleanup(ifsetting);
    isolate_iface_cleanup(ifsetting);

    res = delete_fw_ifsetting(&ifsetting);
    when_false(res, exit);

    ret_value = 0;
exit:
    return ret_value;
}

amxd_status_t _ifsetting_instance_cleanup(amxd_object_t* object,
                                          UNUSED amxd_param_t* param,
                                          amxd_action_t reason,
                                          UNUSED const amxc_var_t* const args,
                                          UNUSED amxc_var_t* const retval,
                                          UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    int ret_value = 0;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    SAH_TRACEZ_NOTICE(ME, "Interface Setting[%s] object about to be destroyed", OBJECT_NAMED);

    ret_value = ifsetting_cleanup(object);
    when_failed_trace(ret_value, exit, ERROR, "Failed to cleanup the object.");

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}