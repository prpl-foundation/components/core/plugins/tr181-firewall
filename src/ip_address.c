/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ip_address.h"
#include "firewall_utilities.h" // for STRING_EMPTY
#include "profiling/profiling.h"
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <ipat/ipat.h>
#include <string.h>

#define ME "firewall"
#define GIVE_ME_CIDR true

const char* network_address(amxc_var_t* prefix) {
    return GETI_CHAR(prefix, 0) == NULL ? "" : GETI_CHAR(prefix, 0);
}

const char* host_address(amxc_var_t* prefix) {
    return GETI_CHAR(prefix, 1) == NULL ? "" : GETI_CHAR(prefix, 1);
}

ipversion_t int_to_ipversion(int value) {
    ipversion_t version = IPvUnknown;

    if(value == 4) {
        version = IPv4;
    } else if(value == 6) {
        version = IPv6;
    } else if((value == 0) || (value == -1)) {
        version = IPvAll;
    }

    return version;
}

static void address_clean_list(amxc_var_t* ip_list, size_t* count) {
    size_t num_of_addresses = 0;

    num_of_addresses = amxc_llist_size(amxc_var_constcast(amxc_llist_t, ip_list));
    if(num_of_addresses > (size_t) *count) {
        SAH_TRACEZ_ERROR(ME, "Something went wrong with address count: %zu", *count);
        *count = 0;
    } else {
        *count -= num_of_addresses;
    }

    amxc_var_set_type(ip_list, amxc_var_type_of(ip_list)); // clean variant but keep variant type
}

static bool prefix_create(amxc_var_t* address_var, address_t* address, amxc_var_t* list4, amxc_var_t* list6) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool ipv4 = true;
    bool rv = false;
    amxc_var_t* list = NULL;
    amxc_var_t* prefix;
    const char* new_address = GET_CHAR(address_var, NULL);
    ipat_t ip = IPAT_INITIALIZE;
    // DMZ and portmap require the host address so provide the option to select the prefixlen:
    // e.g. address_from_string("192.168.1.1/24", ...)
    // network_addr == '192.168.1.1/24'
    // host_addr == '192.168.1.1/32'
    char network_addr[IPAT_STRLEN_SAFE + 1] = {0};
    char host_addr[IPAT_STRLEN_SAFE + 1] = {0};
    int prefixlen = 0;

    when_str_empty_status(new_address, out, rv = true);

    // to check if valid
    when_false_trace(ipat_pton(&ip, AF_INET_ANY, new_address), out, ERROR, "Failed to convert address: %s", new_address);

    // e.g. 10.10.10.10 -> 10.10.10.10/32 or 10.10.10.10/24 -> 10.10.10.10/32
    prefixlen = ipat_prefix_length(&ip);
    if(ipv4) {
        ipat_set_prefix_length(&ip, 32);
    } else {
        ipat_set_prefix_length(&ip, 128);
    }
    when_null_trace(ipat_ntop(&ip, host_addr, sizeof(host_addr) - 1, GIVE_ME_CIDR), out, ERROR, "Address[%s] failed to form CIDR", new_address);
    ipat_set_prefix_length(&ip, prefixlen);

    // e.g. 10.10.10.10 -> 10.10.10.10/32 or 10.10.10.10/24 -> 10.10.10.10/24
    when_null_trace(ipat_ntop(&ip, network_addr, sizeof(network_addr) - 1, GIVE_ME_CIDR), out, ERROR, "Address[%s] failed to form CIDR", new_address);

    ipv4 = (ipat_family(&ip) == AF_INET);

    if(ipv4) {
        list = list4;
    } else {
        list = list6;
    }
    when_null(list, out);

    amxc_var_for_each(var, list) {
        if((strcmp(network_address(var), network_addr) == 0) &&
           (strcmp(host_address(var), host_addr) == 0)) {
            rv = true;
            goto out;
        }
    }

    prefix = amxc_var_add(amxc_llist_t, list, NULL);
    when_null(prefix, out);
    amxc_var_add(cstring_t, prefix, network_addr); // network_address() assumes index is 0
    amxc_var_add(cstring_t, prefix, host_addr);    // host_address() assumes index is 1

    if(ipv4) {
        address->ipv4_size++;
    } else {
        address->ipv6_size++;
    }

    rv = true;
out:
    FW_PROFILING_OUT();
    return rv;
}

static bool prefix_from_string(const char* addresses, address_t* address) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool retval = false;
    amxc_var_t address_csv;

    amxc_var_init(&address_csv);
    amxc_var_set(csv_string_t, &address_csv, addresses);
    amxc_var_cast(&address_csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(var, &address_csv) {
        when_false_trace(prefix_create(var, address, address->list[ADDRESS_IPv4], address->list[ADDRESS_IPv6]), out, ERROR, "Failed to convert %s", addresses);
    }

    retval = true;
out:
    amxc_var_clean(&address_csv);
    FW_PROFILING_OUT();
    return retval;
}

void address_init(address_t* address) {
    for(int i = 0; i < ADDRESS_NUM_OF_LISTS; i++) {
        if(address->list[i] == NULL) {
            amxc_var_new(&address->list[i]);
        }
        amxc_var_set_type(address->list[i], (i <= ADDRESS_IPv6) ? AMXC_VAR_ID_LIST : AMXC_VAR_ID_HTABLE);
    }

    address->ipv4_size = 0;
    address->ipv6_size = 0;
    address->required = 0;
}

void address_clean(address_t* address) {
    for(int i = 0; i < ADDRESS_NUM_OF_LISTS; i++) {
        amxc_var_delete(&address->list[i]);
    }
    address->ipv4_size = 0;
    address->ipv6_size = 0;
    address->required = 0;
}

static const char* get_first_ipv4(address_t* address, const char* (*get_address)(amxc_var_t*)) {
    amxc_var_t* prefix = NULL;
    const char* addr = NULL;

    when_null(address, exit);

    prefix = GETI_ARG(address->list[ADDRESS_IPv4], 0);

#ifdef WITH_GMAP
    if(prefix == NULL) {
        prefix = GETI_ARG(address->list[ADDRESS_DEVICE_IPv4], 0);
    }
#endif

    when_null(prefix, exit);

    addr = get_address(prefix);

exit:
    return addr;
}

const char* address_first_ipv4_char(address_t* address) {
    const char* first_address = NULL;
    when_null(address, exit);
    first_address = get_first_ipv4(address, network_address);
exit:
    return first_address;
}

const char* address_first_ipv4_host_address(address_t* address) {
    return get_first_ipv4(address, host_address);
}

bool address_from_string(const char* addresses, address_t* address) {
    bool retval = false;
    when_null_trace(address, exit, ERROR, "Missing address_t");

    address_clean_list(address->list[ADDRESS_IPv4], &address->ipv4_size);
    address_clean_list(address->list[ADDRESS_IPv6], &address->ipv6_size);

    if(STRING_EMPTY(addresses)) {
        require_address(false, address, REASON_ADDRESS_STRING);
        retval = true;
    } else {
        require_address(true, address, REASON_ADDRESS_STRING);
        retval = prefix_from_string(addresses, address);
    }

exit:
    return retval;
}

bool address_has_ipversion(address_t* address, ipversion_t ipversion) {
    bool rv = false;

    when_true_trace(ipversion == IPvUnknown, out, ERROR, "Unknown ip version");

    if(ipversion != IPv6) {
        when_true_status(address_count(address, IPv4) > 0, out, rv = true);
    }

    if(ipversion != IPv4) {
        when_true_status(address_count(address, IPv6) > 0, out, rv = true);
    }

out:
    return rv;
}

size_t address_count(address_t* address, ipversion_t ipversion) {
    size_t count = 0;

    when_true_trace(ipversion == IPvUnknown, out, ERROR, "Unknown ip version");

    if(ipversion != IPv6) {
        count += address->ipv4_size;
    }

    if(ipversion != IPv4) {
        count += address->ipv6_size;
    }

out:
    return count;
}

bool address_is_required(address_t* address) {
    return address->required > 0;
}

bool require_address(bool require, address_t* address, required_reason_t reason) {
    if(require) {
        address->required |= reason;
    } else {
        address->required &= ~reason;
    }

    return true;
}

#ifdef WITH_GMAP
bool address_delete_device(address_t* address, const char* alias) {
    amxc_var_t* list4 = address->list[ADDRESS_DEVICE_IPv4];
    amxc_var_t* list6 = address->list[ADDRESS_DEVICE_IPv6];

    if(list4 != NULL) {
        amxc_var_t* ip_list = NULL;

        ip_list = GET_ARG(list4, alias);

        address_clean_list(ip_list, &address->ipv4_size);
        amxc_var_delete(&ip_list);
    }

    if(list6 != NULL) {
        amxc_var_t* ip_list = NULL;

        ip_list = GET_ARG(list6, alias);

        address_clean_list(ip_list, &address->ipv6_size);
        amxc_var_delete(&ip_list);
    }

    return true;
}

static void address_clean_device_list(amxc_var_t* devices, size_t* count) {
    amxc_var_for_each(device, devices) {
        address_clean_list(device, count); // for count (not to clean the list because the list is removed anyway at next line)
    }

    amxc_var_set_type(devices, amxc_var_type_of(devices)); // clean variant but keep variant type
}

void address_clean_devices(address_t* address) {
    require_address(false, address, REASON_ADDRESS_DEVICE);
    address_clean_device_list(address->list[ADDRESS_DEVICE_IPv4], &address->ipv4_size);
    address_clean_device_list(address->list[ADDRESS_DEVICE_IPv6], &address->ipv6_size);
}

bool address_add_device(address_t* address, const char* alias, amxc_var_t* device_params) {
    amxc_var_t* ip_addrs = NULL;
    bool rv = false;

    ip_addrs = GET_ARG(device_params, "IPv4Address");
    if(ip_addrs != NULL) {
        if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, ip_addrs))) {
            amxc_var_t* ip_list = NULL;
            ip_list = amxc_var_add_key(amxc_llist_t, address->list[ADDRESS_DEVICE_IPv4], alias, NULL);
            amxc_var_for_each(ip_addr, ip_addrs) {
                when_false_trace(prefix_create(GET_ARG(ip_addr, "Address"), address, ip_list, NULL), out, ERROR, "Failed to convert %s", GET_CHAR(ip_addr, "Address"));
            }
        }
        amxc_var_delete(&ip_addrs);
    }

    ip_addrs = GET_ARG(device_params, "IPv6Address");
    if(ip_addrs != NULL) {
        if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, ip_addrs))) {
            amxc_var_t* ip_list = NULL;
            ip_list = amxc_var_add_key(amxc_llist_t, address->list[ADDRESS_DEVICE_IPv6], alias, NULL);
            amxc_var_for_each(ip_addr, ip_addrs) {
                when_false_trace(prefix_create(GET_ARG(ip_addr, "Address"), address, NULL, ip_list), out, ERROR, "Failed to convert %s", GET_CHAR(ip_addr, "Address"));
            }
        }
        amxc_var_delete(&ip_addrs);
    }

    rv = true;
out:
    return rv;
}
#endif