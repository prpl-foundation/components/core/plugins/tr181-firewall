/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall/dm_firewall.h"
#include "firewall/level.h"
#include "rules.h"
#include "policy/policy.h" // for extendedpolicy_t
#include "firewall.h"      // for fw_get_dm
#include "init_cleanup.h"
#include "firewall/enable.h"
#include "firewall/icmp_logs.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>

#define ME "firewall"

static config_t string_to_config(const char* str) {
    config_t config = HIGH;

    when_str_empty_trace(str, exit, ERROR, "Missing config, using default %d", config);

    if(strcmp(str, "Policy") == 0) {
        config = POLICY;
    } else if(strcmp(str, "Advanced") == 0) {
        config = ADVANCED;
    } else if(strcmp(str, "Low") == 0) {
        config = LOW;
    } else if(strcmp(str, "High") != 0) {
        SAH_TRACEZ_ERROR(ME, "Unknown config[%s], using default %d", str, config);
    }

exit:
    return config;
}

static const char* save_and_get_level(amxd_object_t* firewall) {
    level_t* level = (level_t*) firewall->priv;
    const char* path = NULL;

    level->config = string_to_config(object_da_string(firewall, "Config"));
    if(level->config == POLICY) {
        path = object_da_string(firewall, "PolicyLevel");
    } else if(level->config == ADVANCED) {
        path = object_da_string(firewall, "AdvancedLevel");
    } else if(level->config == LOW) {
        path = "Firewall.Level.Low.";
    } else {
        path = "Firewall.Level.High.";
    }
    snprintf(level->level_path, MAX_PATH_SIZE, "%s", path);
    return path;
}

static void activate_level(config_t config, amxd_object_t* object, amxd_dm_t* dm) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    if(object->priv == NULL) {
        extendedpolicy_t* data = NULL;
        data = (extendedpolicy_t*) calloc(1, sizeof(extendedpolicy_t));
        data->policy = NULL;
        object->priv = data;
    }
    if(config == POLICY) {
        SAH_TRACEZ_INFO(ME, "Activate level[%s] mode POLICY", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
        level_activate_policy(object);
    } else {
        SAH_TRACEZ_ERROR(ME, "Activate level[%s] mode NOT POLICY", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
        // jump to chain with the help of a "policy" without interfaces
        amxd_object_t* chain_object = NULL;
        chain_t* chain = NULL;
        uint32_t res = 0;
        const char* path = NULL;
        extendedpolicy_t* priv_data = NULL;
        const amxc_var_t* var = amxd_object_get_param_value(object, "IPVersion");
        if(var != NULL) {
            res = amxc_var_constcast(uint32_t, var);
        }
        priv_data = (extendedpolicy_t*) object->priv;
        if(priv_data->policy == NULL) {
            priv_data->policy = create_firewall_policy(NULL);
            when_null_trace(priv_data->policy, exit, ERROR, "Failed to create firewall policy");
        }
        path = object_da_string(object, "Chain");
        when_str_empty_trace(path, exit, ERROR, "Level[%s] without Chain", OBJECT_NAMED);
        chain_object = amxd_dm_findf(dm, "%s", path);
        when_null_trace(chain_object, exit, ERROR, "Level[%s] could not find Chain[%s]", OBJECT_NAMED, path);
        when_null_trace(chain_object->priv, exit, ERROR, "Chain[%s] without private data", amxd_object_get_name(chain_object, AMXD_OBJECT_NAMED));
        chain = (chain_t*) chain_object->priv;
        priv_data->policy->common.enable = true;
        priv_data->policy->common.ipversion = res;
        priv_data->policy->target = TARGET_CHAIN;
        priv_data->policy->chain = chain;
        level_activate_not_policy(object);
    }

exit:
    FW_PROFILING_OUT();
    return;
}

void _firewall_enable_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    bool status = false;
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxd_trans_t trans;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null_trace(params, exit, ERROR, "The parameters are NULL for the event generated by Firewall.Enable");
    var = GET_ARG(params, "Enable");
    when_null_trace(var, exit, ERROR, "Firewall.Enable parameter is NULL");

    enable_firewall();
    if(!GET_BOOL(var, "to")) {
        when_false_trace(disable_firewall(), exit, ERROR, "Failed to deactivate the firewall, reverting back to Enable");
    }
    status = true;

exit:
    when_null(object, out);
    if(!status) { //Reverts the Enable back to true if something fails.
        enable_firewall();
        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(bool, &trans, "Enable", true);
        if(amxd_trans_apply(&trans, fw_get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to apply transaction");
        }
        amxd_trans_clean(&trans);
    }
out:
    FW_PROFILING_OUT();
    return;
}

void _firewall_changed(UNUSED const char* const sig_name,
                       const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    level_t* level = NULL;
    bool update = false;
    amxc_string_t buf;

    when_null(object, exit);
    when_null(object->priv, exit);
    level = (level_t*) object->priv;

    var = GET_ARG(params, "Config");
    if(var != NULL) {
        update = true;
    } else {
        var = GET_ARG(params, "PolicyLevel");
        if(var != NULL) {
            update |= (level->config == POLICY);
        }

        var = GET_ARG(params, "AdvancedLevel");
        if(var != NULL) {
            update |= (level->config == ADVANCED);
        }
    }

    if(update) {
        const char* path = NULL;
        amxd_trans_t trans;
        amxd_trans_init(&trans);
        amxd_trans_select_pathf(&trans, "%s", level->level_path);
        amxd_trans_set_value(bool, &trans, "Enable", false);
        if(amxd_trans_apply(&trans, dm) != amxd_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Failed to apply transaction");
        }
        amxd_trans_clean(&trans);

        path = save_and_get_level(object);
        amxd_trans_init(&trans);
        amxd_trans_select_pathf(&trans, "%s", path);
        amxd_trans_set_value(bool, &trans, "Enable", true);
        if(amxd_trans_apply(&trans, dm) != amxd_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Failed to apply transaction");
        }
        amxd_trans_clean(&trans);
    }

    amxc_string_init(&buf, 0);
    amxc_string_setf(&buf, "%sMaxPinholeNumberOfEntries", fw_get_vendor_prefix());
    var = GET_ARG(params, amxc_string_get(&buf, 0));
    if(var != NULL) {
        uint32_t max = GET_UINT32(var, "to");
        amxd_object_t* Pinhole = amxd_object_get_child(object, "Pinhole");
        SAH_TRACEZ_NOTICE(ME, "Set new max instance count to [%d] for Pinhole object", max);
        amxd_object_set_max_instances(Pinhole, max);
    }
    amxc_string_clean(&buf);


exit:
    FW_PROFILING_OUT();
    return;
}

void _level_changed(UNUSED const char* const sig_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    amxc_var_t* var3 = NULL;
    level_t* level = NULL;
    bool level_is_active = false;
    const amxc_var_t* enable_var = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* firewall = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(dm, event_data);

    when_null(object, exit);
    firewall = amxd_dm_findf(dm, "Firewall.");
    when_null(firewall, exit);
    when_null(firewall->priv, exit);
    level = (level_t*) firewall->priv;
    enable_var = amxd_object_get_param_value(object, "Enable");
    if(enable_var != NULL) {
        level_is_active = amxc_var_constcast(bool, enable_var);
    }

    var2 = GET_ARG(params, "Policies");
    var = GET_ARG(params, "DefaultPolicy");
    var3 = GET_ARG(params, "Enable");

    if(var != NULL) {
        if(level->config == POLICY) {
            amxc_var_t policies;
            target_t target = string_to_target(GET_CHAR(var, "to"));
            char* char_policies = amxd_object_get_value(cstring_t, object, "Policies", NULL);
            amxc_var_init(&policies);
            amxc_var_set(csv_string_t, &policies, char_policies);
            amxc_var_cast(&policies, AMXC_VAR_ID_LIST);
            free(char_policies);
            amxc_var_for_each(policy_var, &policies) {
                const char* path = GET_CHAR(policy_var, NULL);
                amxd_object_t* policy_object = amxd_dm_findf(dm, "%s", path);
                policy_t* policy = NULL;

                if((policy_object == NULL) ||
                   (policy_object->priv == NULL)) {
                    continue;
                }

                policy = (policy_t*) policy_object->priv;
                if(policy->chain == NULL) {
                    continue;
                }

                SAH_TRACEZ_INFO(ME, "Policy[%s] has new default policy %s", amxd_object_get_name(policy->common.object, AMXD_OBJECT_NAMED), GET_CHAR(policy_var, "to"));
                policy->chain->target = target;
                if(level_is_active && (var2 == NULL)) {
                    policy->common.flags = FW_MODIFIED;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
                    update_policy_logs(policy, NULL, NULL, NULL);
#endif //FIREWALL_LOGS
                    policy_activate(policy);
                }
            }
            amxc_var_clean(&policies);
        }
    }
    if(var3 != NULL) {
        config_t config = string_to_config(object_da_string(firewall, "Config"));
        bool state = GET_BOOL(var3, "to");
        if(state == false) {
            extendedpolicy_t* priv_data = NULL;
            priv_data = object->priv;
            when_null(priv_data, exit);
            if(priv_data->policy == NULL) {
                SAH_TRACEZ_INFO(ME, "Desactivate level[%s] mode POLICY", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
                level_desactivate_policy(object);
            } else {
                SAH_TRACEZ_INFO(ME, "Desactivate level[%s] mode NOT POLICY", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
                level_desactivate_not_policy(object);
                policy_free(priv_data->policy);
                priv_data->policy = NULL;
            }
        } else {
            activate_level(config, object, dm);
        }
    }

    when_false(level_is_active, exit);

    if(var2 != NULL) {
        if(level->config == POLICY) {
            const char* path = NULL;
            amxd_trans_t trans;
            amxd_trans_init(&trans);
            amxd_trans_select_pathf(&trans, "%s", level->level_path);
            amxd_trans_set_value(bool, &trans, "Enable", false);
            amxd_trans_apply(&trans, dm);
            amxd_trans_clean(&trans);

            path = save_and_get_level(firewall);
            amxd_trans_init(&trans);
            amxd_trans_select_pathf(&trans, "%s", path);
            amxd_trans_set_value(bool, &trans, "Enable", true);
            amxd_trans_apply(&trans, dm);
            amxd_trans_clean(&trans);
        }
    }

exit:
    FW_PROFILING_OUT();
    return;
}

void _update_last_changed(UNUSED const char* const sig_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* object = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* status = NULL;
    amxc_var_t* last_change = NULL;
    amxc_htable_t* hparams = (amxc_htable_t*) amxc_var_constcast(amxc_htable_t, params);

    status = GET_ARG(params, "Status");
    amxc_var_take_it(status);
    last_change = GET_ARG(params, "LastChange");
    amxc_var_take_it(last_change);
    when_true(amxc_htable_is_empty(hparams), exit); // no need to update LastChange

    if(last_change == NULL) {
        amxc_ts_t now;
        amxd_trans_t trans;
        amxc_ts_now(&now);
        now.nsec = 0;

        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(amxc_ts_t, &trans, "LastChange", &now);
        amxd_trans_apply(&trans, dm);
        amxd_trans_clean(&trans);
    }

exit:
    amxc_var_set_key(params, "Status", status, AMXC_VAR_FLAG_DEFAULT);          // restore Status
    amxc_var_set_key(params, "LastChange", last_change, AMXC_VAR_FLAG_DEFAULT); // restore LastChange
    FW_PROFILING_OUT();
    return;
}

static int dm_rule_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    rule_init(object);
    return amxd_status_ok;
}

static int dm_chain_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    chain_init(object);
    amxd_object_for_all(object, ".Rule.*.", dm_rule_init, NULL);
    return amxd_status_ok;
}

void dm_firewall_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");
    amxd_object_for_all(firewall, ".Chain.*.", dm_chain_init, NULL);
    FW_PROFILING_OUT();
}

void dm_firewall_start(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");
    level_t* level = NULL;
    bool res = false;
    const amxc_var_t* var = NULL;
    amxd_dm_t* dm = NULL;
    const char* current_level = NULL;
    amxd_object_t* my_level = NULL;
    when_false(firewall->priv == NULL, exit);

    level = (level_t*) calloc(1, sizeof(level_t));
    when_null(level, exit);
    firewall->priv = level;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    init_icmp_logs(firewall);
#endif //FIREWALL_LOGS

    current_level = save_and_get_level(firewall);
    when_null_trace(current_level, exit, ERROR, "Can't get Level from firewall object");
    dm = fw_get_dm();
    SAH_TRACEZ_INFO(ME, "Start firewall with level %s", current_level);
    my_level = amxd_dm_findf(dm, "%s", current_level);
    var = amxd_object_get_param_value(my_level, "Enable");
    if(var != NULL) {
        res = amxc_var_constcast(bool, var);
        if(res) {
            // at init and if level is already enabled activate it
            config_t config = string_to_config(object_da_string(firewall, "Config"));
            activate_level(config, my_level, dm);
        } else {
            // send transition to Enable the level
            amxd_trans_t trans;
            amxd_trans_init(&trans);
            amxd_trans_select_pathf(&trans, "%s", current_level);
            amxd_trans_set_value(bool, &trans, "Enable", true);
            amxd_trans_apply(&trans, dm);
            amxd_trans_clean(&trans);
        }
    }
exit:
    FW_PROFILING_OUT();
    return;
}
