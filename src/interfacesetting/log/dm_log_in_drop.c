/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/dm_log_in_drop.h"
#include "interfacesetting/log_in_drop.h"
#include "logs/logs.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define ME "firewall"

/**
   @brief
   Translate the Netmodel interface and add to the common structure.

   @param[in] log_in_drop LOGDroppedIncomingConnection internal structure.
   @param[in] interface NetModel interface.
 */
static void set_interface(log_in_drop_t* log_in_drop, const char* interface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    firewall_interface_t* fwiface = &log_in_drop->common.dest_fwiface;
    if(!STRING_EMPTY(interface)) {
        to_linux_interface(interface, 0, fwiface);
    } else {
        init_firewall_interface(fwiface);
    }
    FW_PROFILING_OUT();
}

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    common_t* common = (common_t*) fwiface->query_priv;
    when_null_trace(common, exit, ERROR, "Common object is NULL.");
    log_in_drop_t* log_in_drop = (log_in_drop_t*) common->priv_data;
    when_null_trace(log_in_drop, exit, ERROR, "LOGDroppedIncomingConnection object is NULL.");

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

    log_update(&common->folders.log,
               log_get_dm_enable_param(common->object, NULL, "LOGDroppedIncomingConnection"),
               log_get_dm_controller_param(common->object, NULL, NULL),
               !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
               NULL);
    log_in_drop_implement_changes(log_in_drop);
exit:
    FW_PROFILING_OUT();
}

/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    ifsetting_t* ifsetting = NULL;
    log_in_drop_t* log_in_drop = NULL;
    bool update = false;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    ifsetting = (ifsetting_t*) object->priv;
    when_null_trace(ifsetting, exit, ERROR, "IterfaceSetting[%s] is NULL", OBJECT_NAMED);

    log_in_drop = ifsetting->log_in_drop;
    when_null(log_in_drop, exit);

    update = log_update(log,
                        log_get_dm_enable_param(object, NULL, "LOGDroppedIncomingConnection"),
                        log_get_dm_controller_param(object, NULL, NULL),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        data);
    if(update) {
        log_in_drop->common.flags = FW_MODIFIED;
        log_in_drop_implement_changes(log_in_drop);
    }

exit:
    FW_PROFILING_OUT();
    return;
}

/**
   @brief
   Initialise the LOGDroppedIncomingConnection internal structure.

   @param[in] instance Object with the InterfaceSetting's instance.

   @return
   0 if success, 1 if failed.
 */
int log_in_drop_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;
    log_in_drop_t* log_in_drop = NULL;
    bool enable = false;
    int ret_value = 1;

    when_null(object, exit);
    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);

    log_in_drop = ifsetting->log_in_drop;
    if(log_in_drop == NULL) {
        log_in_drop = create_log_in_drop(object);
        when_null(log_in_drop, exit);
        ifsetting->log_in_drop = log_in_drop;
    }

    log_in_drop->common.dest_fwiface.query_priv = &log_in_drop->common;
    log_in_drop->common.dest_fwiface.query = NULL;
    log_in_drop->common.dest_fwiface.query_result_changed = query_result_changed;
    log_in_drop->common.priv_data = log_in_drop;

    enable = log_get_dm_enable_param(object, NULL, "LOGDroppedIncomingConnection");

    if(enable) {
        set_interface(log_in_drop, object_da_string(object, "Interface"));
    }

    log_init(object, &log_in_drop->common.folders.log, &log_in_drop->common.dest_fwiface, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             enable, log_get_dm_controller_param(object, NULL, NULL), TARGET_DROP, update_rule_log_info_cb);

    log_in_drop->common.flags = FW_NEW;

    ret_value = 0;
exit:
    FW_PROFILING_OUT();
    return ret_value;
}

/**
   @brief
   Set the flags to "delete" and update the LOGDroppedIncomingConnection.

   @param[in] ifsetting InterfaceSetting's struct

   @return
   0 if success, 1 if failed.
 */
int log_in_drop_cleanup(ifsetting_t* ifsetting) {
    log_in_drop_t* log_in_drop = NULL;
    bool res;
    int ret_value = 1;

    when_null(ifsetting, exit);

    log_in_drop = ifsetting->log_in_drop;
    if(log_in_drop != NULL) {
        log_in_drop->common.flags = FW_DELETED;
        res = log_in_drop_implement_changes(log_in_drop);
        when_false(res, exit);
    }

    ret_value = 0;
exit:
    return ret_value;
}

void _log_in_drop_changed(const char* const sig_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    ifsetting_t* ifsetting = NULL;
    log_in_drop_t* log_in_drop = NULL;
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* obj_setting = amxd_dm_signal_get_object(dm, event_data);
    bool update = false;
    bool enable = false;
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null(dm, exit);
    when_null(obj_setting, exit);
    ifsetting = (ifsetting_t*) obj_setting->priv;
    when_null(ifsetting, exit);
    log_in_drop = ifsetting->log_in_drop;
    when_null(log_in_drop, exit);

    enable = log_get_dm_enable_param(obj_setting, event_data, "LOGDroppedIncomingConnection");

    if(enable && (log_in_drop->common.dest_fwiface.query == NULL)) {
        set_interface(log_in_drop, object_da_string(log_in_drop->common.object, "Interface"));
    }

    update = log_update(&log_in_drop->common.folders.log,
                        enable,
                        log_get_dm_controller_param(obj_setting, event_data, NULL),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        event_data);

    if(update) {
        log_in_drop->common.flags = FW_MODIFIED;
        log_in_drop_implement_changes(log_in_drop);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
