/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "connectiontracking/dm_connectiontracking.h"
#include "connectiontracking/connectiontracking.h"
#include "init_cleanup.h"
#include "firewall.h"  // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_object.h>
#include <string.h>

#define ME "connectiontracking"
#define TYPE_SIP "SIP"

static bool connectiontracking_init(amxd_object_t* object) {
    bool res = false;
    connectiontracking_t* c = NULL;
    const char* type = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL"); // in case c is handled after object is removed?
    when_not_null_trace(object->priv, exit, ERROR, "Private data should not exist");

    c = create_firewall_connectiontracking(object);
    when_null_trace(c, exit, ERROR, "Could not create connectiontracking object");

    type = OBJECT_NAMED;
    if((type != NULL) && (strcmp(type, TYPE_SIP) == 0)) {
        c->type = FW_CONNECTIONTRACKING_TYPE_SIP;
    } else {
        SAH_TRACEZ_ERROR(ME, "ConnectionTracking %s not supported yet.", type);
        goto exit;
    }

    res = true;

exit:
    return res;
}

void dm_connectiontracking_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = amxd_dm_findf(fw_get_dm(), "Firewall.ConnectionTracking.%s.", TYPE_SIP);

    connectiontracking_init(object);
    connectiontracking_implement_changes();
    FW_PROFILING_OUT();
}

void _connectiontracking_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const event_data,
                                 UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null_trace(object->priv, exit, ERROR, "Object private data is NULL");

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        bool enable = GET_BOOL(var, "to");
        SAH_TRACEZ_INFO(ME, "ConnectionTracking[%s] configure enable[%s]", OBJECT_NAMED, enable ? "true" : "false");
    }

    connectiontracking_implement_changes();

exit:
    FW_PROFILING_OUT();
    return;
}